#!/usr/bin/python3

# Takes the profileing data produced by the profile build of Grid Locked and makes it
# into a human readable format.

import subprocess
import os

with open("profile.txt", "w") as profile, subprocess.Popen(["gprof", "bin/Profile_Linux/GridLocked", "enviorment/gmon.out"], stdout=profile) as proc:
        proc.wait()

os.remove("enviorment/gmon.out")

