#!/usr/bin/python3

# Collects files and packages them in arcives for the purpose of releasing to the public.

import shutil
import subprocess
import shlex
import os
import zipfile
import tarfile
import importlib

manual_id = "1H8d382or8l7Ye7X28BBgekBXkyl8DgUn3vl5YTqevpY"

drive_loader = importlib.find_loader("pydrive")
drive_library_found = drive_loader is not None

examples_folder = "./Release/Examples"
windows_release = os.path.join(examples_folder, "Windows-Release")

linux_release = os.path.join(examples_folder, "Linux-Release")

windows_release_package = os.path.join(examples_folder, "Windows-Release.zip")
linux_release_package = os.path.join(examples_folder, "Linux-Release.tar.gz")

bin_folder = "./bin"
environment_folder = "./environment"

distrobution_folder = "./Release/Distrobution"
distro_bin = os.path.join(distrobution_folder, "bin")

windows_release_bin = os.path.join(bin_folder, "Release_Windows")
linux_release_bin = os.path.join(bin_folder, "Release_Linux")

def execute_native_command(command, cwd="."):
    print("Running Command: {}".format(command))

    args = shlex.split(command)

    with subprocess.Popen(args, cwd=cwd) as proc:
        proc.wait()

def compress_folder_as_zip(source, target):
    with zipfile.ZipFile(target, "w", zipfile.ZIP_DEFLATED) as archive:
        for root, dirs, files, in os.walk(source):
            for file in files:
                path = os.path.join(root, file)
                archive.write(path, os.path.relpath(path, source))


def compress_folder_as_tar(source, target):
    with tarfile.TarFile(target, "w") as archive:
        for root, dirs, files, in os.walk(source):
            for file in files:
                path = os.path.join(root, file)
                archive.add(path, os.path.relpath(path, source))


def copy_environment(target, root=environment_folder):
    os.makedirs(target)

    if not target.endswith("screenshots"):

        for file in os.listdir(root):
            source = os.path.join(root, file)
            dest = os.path.join(target, file)

            if os.path.isfile(source):
                shutil.copy(source, dest)
            else:
                copy_environment(dest, source)


def merge_folder(root, target):
    if not os.path.exists(target):
        os.makedirs(target)

    for file in os.listdir(root):
        source = os.path.join(root, file)
        dest = os.path.join(target, file)

        if os.path.isfile(source):
            if not os.path.exists(dest):
                shutil.copy(source, dest)
            else:
                print("Warning: Destination files already exists: {}".format(path))
        else:
            merge_folder(dest, source)


def download_manual():
    if drive_library_found: # We won't try to download the manual if we don't have the googledocs library avalible.
        print("Downloading manual from Google Drive...")

        from pydrive.auth import GoogleAuth
        from pydrive.drive import GoogleDrive

        gauth = GoogleAuth()
        gauth.LocalWebserverAuth()
        drive = GoogleDrive(gauth)

        manual = drive.CreateFile({"id": manual_id})
        manual.GetContentFile(os.path.join(environment_folder, "Manual.pdf"), mimetype='application/pdf')

        print("Done.")
    else:
        print("PyDrive is not installed. Manual can not be downloaded.")


def packageExamples():
    if os.path.exists(examples_folder):
        shutil.rmtree(examples_folder)

    os.makedirs(examples_folder)

    print("Examples Folder: {}".format(examples_folder))
    print("Linux Release Folder:   {}".format(linux_release))
    print("Windows Release Folder: {}".format(windows_release))

    # download_manual()

    copy_environment(linux_release)
    merge_folder(linux_release_bin, os.path.join(linux_release, "bin"))

    copy_environment(windows_release)
    merge_folder(windows_release_bin, os.path.join(windows_release, "bin"))

    compress_folder_as_tar(linux_release, linux_release_package)
    compress_folder_as_zip(windows_release, windows_release_package)


def pushToRepository():
    # In the occasional case where the script may have failed last time, clean out any leftover files.
    if os.path.exists(distrobution_folder):
        shutil.rmtree(distrobution_folder)

    # First we need to set up the distrobution folder
    os.makedirs(distrobution_folder)
    username = input("Enter your Bit Bucket Username:")
    execute_native_command("git clone https://{}@bitbucket.org/draconicentertainment/release.git .".format(username), cwd=distrobution_folder)

    # Include the lisenses in there.
    if os.path.exists(os.path.join(distrobution_folder, "licenses.txt")):
        os.remove(os.path.join(distrobution_folder, "licenses.txt"))
    shutil.copy(os.path.join(environment_folder, "licenses.txt"), os.path.join(distrobution_folder, "licenses.txt"))

    if os.path.exists(distro_bin):
        shutil.rmtree(distro_bin)

    # Linux Release
    merge_folder(linux_release_bin, distro_bin)

    # Windows Release
    merge_folder(windows_release_bin, distro_bin)

    execute_native_command("git add *", cwd=distrobution_folder)
    execute_native_command("git commit -m \"Automated update push.\"", cwd=distrobution_folder)

    execute_native_command("git push origin master", cwd=distro_bin)

    shutil.rmtree(distrobution_folder)

def main():
    packageExamples()
    #pushToRepository()

if __name__ == "__main__":
    main()
