/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "Shader.hpp"
#include "ShaderManager.hpp"

#include <iostream>

namespace GL {
    ShaderManager::ShaderManager() {

    }

    ShaderManager::~ShaderManager() {
        if (!shaders.empty()) {
            std::cout << "Warning: Not all shaders were disposed before shutdown. This is an engine level bug. Please find a way to reproduce it and report it." << std::endl;
        }
    }

    RM::ref<Shader> ShaderManager::getShader(RM::Path path) {
        std::string key = path.getAbsolute().string();

        RM::ref<Shader> shader;

        std::map<std::string, Shader*>::const_iterator result = shaders.find(key);
        if (result == shaders.end()) {
            shader = new Shader(path);
        } else {
            shader = result->second;
        }

        return shader;
    }

    RM::ref<Shader> ShaderManager::getInternalShader(const char * shaderString)
    {
        RM::ref<Shader> shader;

        std::unordered_map<const char *, Shader*>::const_iterator result = internalShaders.find(shaderString);
        if (result == internalShaders.end()) {
            shader = new Shader(shaderString);
        } else {
            shader = result->second;
        }

        return shader;
    }

    void ShaderManager::addShader(std::string path, Shader * shader) {
        shaders[path] = shader;
    }

    void ShaderManager::removeShader(std::string path) {
        shaders.erase(path);
    }

    void ShaderManager::addInternalShader(const char * shaderString, Shader * shader) {
        internalShaders[shaderString] = shader;
    }

    void ShaderManager::removeInternalShader(const char * shaderString) {
        internalShaders.erase(shaderString);
    }
}
