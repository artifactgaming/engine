/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "main.hpp"
#include "globals.hpp"

#include <iostream>
#include <glad.h>

#include <math.h>

#include <boost/thread/thread.hpp>
#include <boost/chrono.hpp>
#include <boost/filesystem.hpp>

#include <glm/gtc/matrix_transform.hpp>

#include <boost/iostreams/tee.hpp>
#include <boost/iostreams/stream.hpp>

#include <nanogui/messagedialog.h>

#include "LuaSupport/luaSupport.hpp"
#include "utils/glTests.hpp"
#include "utils/SystemOpen.hpp"

#include "BuiltinFilePaths.hpp"
#include "SystemStats.hpp"

#include "ShaderManager.hpp"
#include "ShaderProgram.hpp"

#include "World.hpp"

#include <iomanip>

namespace HardwareAccelleration {
    bool computeShaders = false;

    bool supportComputeShaders() {
        return computeShaders;
    }
}

namespace bio = boost::iostreams;

typedef bio::tee_device<std::ostream, std::ofstream> TeeDevice;
typedef bio::stream<TeeDevice> TeeStream;
std::stringstream debugStream;

const char developerModeMessage[] = "You have launched Grid Locked in developer mode.\n\n"\
                                    "Most multi-player servers will not permit you to join while in developer mode.\n\n"\
                                    "You can not unlock achievements while in developer mode.";

GridLocked::GridLocked(bool developerMode) {
    terminal = nullptr;
    runGame = true;
    this->developerMode = developerMode;
    debugGlEnabled = false;

    resourceManager = new RM::ResourceManager();
    shaderManager = new GL::ShaderManager();
    controllerManager = new World::ControllerManager();
}

GridLocked::~GridLocked() {
    LuaSupport::shutdownGame();

    if (terminal) {
        delete terminal;
    }

    screen->setVisible(false);
    controlManager->dispose();
    resourceManager->shutdown();
    controlManager->decRef();

    delete controllerManager;
    delete shaderManager;
    delete resourceManager;

    nanogui::shutdown();
    glfwTerminate();
}

void GridLocked::shutdown() {
    runGame = false;
}

void GridLocked::start() {
    resourceManager->start();
    nanogui::init();

    controlManager = new RM::ControlManager();
    controlManager->incRef();

    screen = controlManager;
    minFrameTime = 1.0f/controlManager->getFramerate();

    if (!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress)) {
        throw std::runtime_error("GLAD Failed to load additional OpenGL functions.");
    }

    glTest();

	std::cout << "Bonus OpenGL extensions:" << std::endl;
    GLint numExtensions;
    glGetIntegerv(GL_NUM_EXTENSIONS, &numExtensions);
    for (GLint i = 0; i < numExtensions; i++) {
        std::string extension = reinterpret_cast<const char *>(glGetStringi(GL_EXTENSIONS, i));

        if (extension == "GL_ARB_compute_shader") {
            HardwareAccelleration::computeShaders = true;
            std::cout << "Compute Shaders." << std::endl;
            continue;
        }

        if (extension == "GL_KHR_debug") {
            std::cout << "OpenGL debug tools." << std::endl;
            debugGlEnabled = true;

            // Debug tools are available, so let's set them up.
            glEnable(GL_DEBUG_OUTPUT);
            glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);

            glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DEBUG_SEVERITY_NOTIFICATION, 0, nullptr, false);
            glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DEBUG_SEVERITY_LOW, 0, nullptr, false);

            glDebugMessageCallback(+[](
                GLenum source,
                GLenum type,
                GLuint id,
                GLenum severity,
                GLsizei length,
                const GLchar* message,
                const void* userParam)
            {
                std::cout << "GL MESSAGE: " << message << std::endl;
            },
            nullptr);

            continue;
        }

    }

	//Init rendering mode
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	// glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); // FIXME: DEPRICATED

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    //Create the screenshot directory.
    boost::system::error_code returnedError;
    boost::filesystem::create_directories(CONFIG_DIR, returnedError);

    if (returnedError) {
        std::string message = "Failed to create config directory directory\nReason returned by OS:\n";
        message += returnedError.message();
        throw std::runtime_error(message);
    }

    try {
        LuaSupport::startGame();
    } catch (std::exception & e) {
        auto t = std::time(nullptr);
        auto tm = *std::localtime(&t);
        std::cerr << "Time of Error: " << std::put_time(&tm, "%Y-%m-%d_%H-%M-%S") << std::endl;
        std::cerr << e.what() << std::endl;

        std::string message = "Error while loading game init script:\n\n";
        message += e.what();

        nanogui::MessageDialog * dialog = screen->add<nanogui::MessageDialog>(nanogui::MessageDialog::Type::Warning, "BAD INIT", message, "Ok");

        dialog->setCallback(
        [this](int selection) {
            this->shutdown();
        });

        return;
    }

    // Warn that they are in developer mode, if they are.
    if (developerMode) {
        terminal = new DevTools::DeveloperTerminal(screen);

        nanogui::MessageDialog * dialog = screen->add<nanogui::MessageDialog>(nanogui::MessageDialog::Type::Warning, "Developer Mode", developerModeMessage, "Ok", "More Info", true);

        dialog->setCallback(
        [dialog](int selection) {
            if (selection == 1) {
                SystemOpen::open("https://bitbucket.org/artifactgaming/engine/wiki/developermode");
            }
        });
    }

    controlManager->load();
}

bool GridLocked::isDeveloperMode() {
    return developerMode;
}

void GridLocked::update(float elapsed) {
    updateWorlds(elapsed);
}

void GridLocked::render() {

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Render our GUIs.
    screen->drawContents();
    screen->drawWidgets();

    //screen->drawAll();

    glfwSwapBuffers(screen->glfwWindow());

}

void GridLocked::mainLoop() {
    sol::state_view lua = resourceManager->getStateOnly();

    screen->setVisible(true);
    screen->performLayout();
    controlManager->center();

    glfwSetTime(0);

    double time = 0;
    unsigned frames = 0;
    double fpsWait = 0;

    while (!glfwWindowShouldClose(screen->glfwWindow()) && runGame)
    {
        LuaSupport::resetScriptTimeout(lua);
        glfwSetTime(0);
        fpsWait += time;

        resourceManager->syncronusUpdate();

        glfwPollEvents();
        update(time);

        disposeWidgets();

        if (terminal) {
            terminal->print(debugStream);
            debugStream.str("");
            debugStream.clear();
        }

        render();

        time = glfwGetTime();
        frames++;

        if (fpsWait > 1) {

            int fps = static_cast<int>(frames / fpsWait);

            if (terminal && terminal->isVisible()) {

                float CPUusage;
                long memoryUsage;
                SystemStats::getSystemUsageStats(CPUusage, memoryUsage);

                terminal->setFPS(fps);
                terminal->setLuaMemoryUsage(resourceManager->getLuaMemoryUsage());
                terminal->setSystemMemoryUsage(memoryUsage);
                terminal->setSystemCPUUsage(CPUusage);
                terminal->setNumberResources(this->resourceManager->getNumResources());
                terminal->setNumberGroups(this->resourceManager->getNumGroups());
            }

            fpsWait = 0;
            frames = 0;

            // If we're keeping a good frame rate, we'll aggressively collect garbage.
            #ifndef DEBUG
            if (fps > 50)
            {
                lua.collect_garbage();
            }
            #else
            // Debug builds always aggressively collect, because valgrind will always keep the framerate low.
            lua.collect_garbage();
            #endif
        }

        if (time < minFrameTime) {
            double sleepDuration = minFrameTime - time;
            boost::chrono::milliseconds duration(static_cast<int>(sleepDuration * 1000));
            boost::this_thread::sleep_for(duration);
            time = glfwGetTime();
        }
    }
}

GridLocked * game = nullptr;
RM::ResourceManager * resourceManager = nullptr;
RM::WidgetManager * widgetManager = nullptr;
GL::ShaderManager * shaderManager = nullptr;

GridLocked * getGameInstance()
{
    return game;
}

RM::ResourceManager * getResourceManager()
{
    return resourceManager;
}

RM::WidgetManager * getWidgetManager()
{
    return widgetManager;
}

GL::ShaderManager * getShaderManager()
{
    return shaderManager;
}

RM::ControlManager * getControlManager()
{
    return game->getControlManager();
}

World::ControllerManager * getControllerManager()
{
    return game->getControllerManager();
}

World::WorldManager * getWorldManager()
{
    return static_cast<World::WorldManager*>(game);
}

bool isPhysicsDebugEnabled()
{
    return game->isPhysicsDebugEnabled();
}

bool isDeveloperMode()
{
    return game->isDeveloperMode();
}

void shutdown()
{
    game->shutdown();
}

#include "NBOF.hpp"

/**********************************************************************************
* This is the main function of the game. It will set up, run, and clean up all
* components of the game.
* Parameters:
*  argc - the number of parameters passed from the command terminal.
*  argv - strings containing those parameters passed from the command terminal.
* Returns: 0 if all is well. Other values indicate something went wrong.
**********************************************************************************/
int main(int argc, char ** argv) {
    boost::filesystem::current_path("../");

    bool developerMode = false;

    for (int arg = 0; arg < argc; arg++) {
        std::string argument(argv[arg]);

        if (argument == "--developer") {
            developerMode = true;
        }
    }

    // Start by making logs.
    std::ofstream file;
    file.open("last_run.log");

    std::ostream logFile(file.rdbuf());

    std::ostream coutcopy(std::cout.rdbuf());
    bio::tee_device<std::ostream, std::ostream> outputDevice(coutcopy, logFile);
    bio::stream<bio::tee_device<std::ostream, std::ostream>> logger(outputDevice);

    std::ostream cerrcopy(std::cerr.rdbuf());
    bio::tee_device<std::ostream, std::ostream> outputErrDevice(cerrcopy, logFile);
    bio::stream<bio::tee_device<std::ostream, std::ostream>> errlogger(outputErrDevice);

    std::cout.rdbuf(logger.rdbuf());
    std::cerr.rdbuf(errlogger.rdbuf());

    // If developer mode, we'll make a string stream that gets a copy of everything printed.
    std::ostream logcopy(logFile.rdbuf());
    bio::tee_device<std::ostream, std::ostream> devTermDevice(logcopy, debugStream);
    bio::stream<bio::tee_device<std::ostream, std::ostream>> devTermStream(devTermDevice);

    if (developerMode) {
        logFile.rdbuf(devTermStream.rdbuf());
    }

    {
        auto t = std::time(nullptr);
        auto tm = *std::localtime(&t);
        std::cout << "Log start: " << std::put_time(&tm, "%Y-%m-%d_%H-%M-%S") << std::endl;
    }

    try {//This scope insures clean memory by the point of shutdown.
        std::cout << "Operating directory: " << boost::filesystem::current_path().string() << std::endl;

        if (developerMode) {
            std::cout << "Engine has been launched in developer mode.\n";
        }

        game = new GridLocked(developerMode);

        resourceManager = game->getResourceManager();
        widgetManager = game;
        shaderManager = game->getShaderManager();

        game->start();
        game->mainLoop();

        delete game;

    } catch (std::exception& e) {
        auto t = std::time(nullptr);
        auto tm = *std::localtime(&t);
        std::cerr << "Time of Error: " << std::put_time(&tm, "%Y-%m-%d_%H-%M-%S") << std::endl;
        std::cerr << e.what() << std::endl;
    }

    std::cout << "END OF LOG." << std::endl;

    logFile.rdbuf(logcopy.rdbuf());

    // Clean up the logs and return the cout and cerr streams to normal.
    std::cout.rdbuf(coutcopy.rdbuf());
    std::cerr.rdbuf(cerrcopy.rdbuf());

    logger.close();

    return 0;
}
