/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "DeveloperTerminal.hpp"
#include "globals.hpp"

#include "SystemStats.hpp"
#include "EngineeringFormatter.hpp"
#include "main.hpp"

#include <iomanip>
#include <sstream>

using namespace nanogui;

#define TERM_LINE_LIMIT 2000
#define TERM_WIDTH  500
#define TERM_HEIGHT 400

namespace DevTools {
    DeveloperTerminal::DeveloperTerminal(nanogui::Screen * screen)
    {
        window = screen->add<Window>("Developer Terminal");
        window->setFixedSize(glm::ivec2(TERM_WIDTH, TERM_HEIGHT));

        window->setLayout(new GridLayout(Orientation::Horizontal, 1, Alignment::Fill, 2, 0));
        window->setVisible(false);

        Widget * buttonPanel = window->buttonPanel();
        Button * buttonF5 = buttonPanel->add<Button>("F5");

        tabs = window->add<TabWidget>();

        // The Lua terminal.
        Widget * luaTab = tabs->createTab("Lua Terminal");
        luaTab->setLayout(new GroupLayout(0, 6, 14, 20));

        Widget * terminalPanel = luaTab->add<Widget>();
        terminalPanel->setLayout(new GroupLayout(0, 6, 14, 20));

        //terminal
        terminal = terminalPanel->add<VScrollPanel>();
        terminal->setFixedHeight(300);
        terminal->setLayout(new GroupLayout(0, 6, 14, 20));

        terminalLabel = terminal->add<Label>("");
        terminalLabel->setFixedWidth(400);

        textBox = terminalPanel->add<TextBox>("");
        textBox->setFixedHeight(30);
        textBox->setAlignment(TextBox::Alignment::Left);
        textBox->setEditable(true);

        getResourceManager()->registerEnvironment(
            "debug",
            [](sol::state_view lua)
            {
                return sol::environment(lua, sol::create, lua.globals());
            }
        );

        sol::environment env;
        sol::state_view lua = getResourceManager()->getEnvironment("debug", env);

        textBox->addChangeCallback([this](std::string text)
        {
            try {
                sol::environment env;
                sol::state_view lua = getResourceManager()->getEnvironment("debug", env);

                lua.script(text, env);

                if (text != "again()") {
                    lastCommand = text;
                }

                if (text != "last()") {
                    this->textBox->setValue("");
                }

            } catch (std::exception & e) {
                std::cerr << "Error while processing Lua string from Developer Terminal.\n";
                std::cerr << e.what() << std::endl;
            }

            this->textBox->setFocused(true);

            return true;
        });

        env["cls"] = [this]() {
            this->clear();
        };

        env["again"] = [this]() {
            sol::environment env;
            sol::state_view lua = getResourceManager()->getEnvironment("debug", env);

            lua.script(this->lastCommand, env);
        };

        env["last"] = [this]() {
            this->textBox->setValue(this->lastCommand);
        };

        // The Stats Tab
        Widget * statTab = tabs->createTab("Statistics");
        statTab->setLayout(new GroupLayout(0, 0, 0, 0));
        statTab = statTab->add<Widget>();
        statTab->setLayout(new GridLayout(Orientation::Horizontal, 1, Alignment::Maximum, 2, 2));

        std::stringstream coreScale;
        coreScale << "[One Core = " << (SystemStats::getCoreCompensation() * 100)<< "%]";

        fps = createStatWidget(statTab, "Frame Rate:", "FPS");
        systemCPUUsage = createStatWidget(statTab, "System CPU: " + coreScale.str(), "%");

        systemMemoryUsage = createStatWidget(statTab, "System Memory:", "Bytes");
        luaMemoryUsage = createStatWidget(statTab, "Lua Memory:", "Bytes");
        numResources = createStatWidget(statTab, "Number of Resources:", "");
        numGroups = createStatWidget(statTab, "Number of Resource Groups:", "");

        createStatWidget(statTab, "Loaded Textures:", "");

        // OpenGL debug tools.
        Widget * oglDebugTab = tabs->createTab("OpenGL Debug");
        oglDebugTab->setLayout(new GroupLayout(0, 0, 0, 0));
        oglDebugTab = oglDebugTab->add<Widget>();
        oglDebugTab->setLayout(new GroupLayout());

        std::string oglMessage;

        if (getGameInstance()->openGLDebugEnabled()) {
            oglMessage = "Your graphics driver reports that advanced OpenGL debug tools are available.\nOnly OpenGL debug messages that contain the checked items in this list will be logged.";
        } else {
            oglMessage = "Your graphics driver reports that advanced OpenGL debug tools are not available.\nPlease make certain that your graphics driver is up to date.";
        }

        Label * oglDebugLabel = oglDebugTab->add<Label>(oglMessage);
        oglDebugLabel->setFixedWidth(460);

        if (getGameInstance()->openGLDebugEnabled()) {
            Widget * severityPanel = oglDebugTab->add<Widget>();
            severityPanel->setLayout(new GridLayout(Orientation::Horizontal, 2, Alignment::Minimum, 15, 5));

            createDebugSettingWidget(severityPanel, "Severity: Notification", [](bool enable)
            {
                glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DEBUG_SEVERITY_NOTIFICATION, 0, nullptr, enable);
            }, false);

            createDebugSettingWidget(severityPanel, "Severity: Low", [](bool enable)
            {
                glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DEBUG_SEVERITY_LOW, 0, nullptr, enable);
            }, false);

            createDebugSettingWidget(severityPanel, "Severity: Medium", [](bool enable)
            {
                glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DEBUG_SEVERITY_MEDIUM, 0, nullptr, enable);
            }, true);

            createDebugSettingWidget(severityPanel, "Severity: High", [](bool enable)
            {
                glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DEBUG_SEVERITY_HIGH, 0, nullptr, enable);
            }, true);
        }

        // OpenGL debug tools.
        Widget * debugSettingsTab = tabs->createTab("Debug Settings");
        debugSettingsTab->setLayout(new GroupLayout(0, 0, 0, 0));
        debugSettingsTab = debugSettingsTab->add<Widget>();
        debugSettingsTab->setLayout(new GroupLayout());

        createDebugSettingWidget(debugSettingsTab, "Enable Physics Debug Renderer", [](bool enable)
        {
            getGameInstance()->debugRendererEnabled = enable;
        }, true);

        // The Resource Browser.
        Widget * resourceTab = tabs->createTab("Resource Browser");
        resourceTab->setLayout(new GroupLayout(0, 6, 14, 20));

        Window * windowp = this->window;
        TextBox * textboxp = this->textBox;
        std::function<void(bool)> F5Callback = [windowp, textboxp](bool pressed) {
            if (pressed) {
                windowp->center();
                windowp->setVisible(!windowp->visible());
                textboxp->requestFocus();

            }
        };

        buttonF5->addPushCallback([F5Callback]()
        {
            F5Callback(true);
        });

        getGameInstance()->getControlManager()->newBooleanControl("System", "Toggle Developer Terminal", true, RM::KeyboardButtonID::Button_F5);
        getGameInstance()->getControlManager()->addBooleanCallback("System", "Toggle Developer Terminal", F5Callback);
        tabs->setActiveTab(0);
    }

    DeveloperTerminal::~DeveloperTerminal() {

    }

    void DeveloperTerminal::print(std::stringstream & stream) {

        size_t numLines = lines.size();

        for (std::string line; std::getline(stream, line);) {
            lines.push_back(line);
        }

        if (numLines < lines.size()) {
            terminal->setScroll(1.0f);
        }

        updateTerminal();
    }

    void DeveloperTerminal::clear() {
        lines.clear();
        updateTerminal();
    }

    void DeveloperTerminal::updateTerminal() {
        while (lines.size() > TERM_LINE_LIMIT) {
            lines.pop_front();
        }

        std::string term = "";

        for(auto it=lines.begin(); it != lines.end(); ++it) {
            term += *it + '\n';
        }

        terminalLabel->setCaption(term);
    }

    nanogui::TextBox * DeveloperTerminal::createStatWidget(Widget * tab, std::string name, std::string defaultUnit) {
        Widget * panel = tab->add<Widget>();
        panel->setLayout(new BoxLayout(Orientation::Horizontal, Alignment::Maximum, 0, 6));

        Label * label = panel->add<Label>(name, "sans", 20);
        label->setFixedWidth(355);

        TextBox * text = panel->add<TextBox>("--");
        text->setFixedWidth(120);
        text->setUnits(defaultUnit);
        text->setAlignment(TextBox::Alignment::Left);

        return text;
    }

    void DeveloperTerminal::setFPS(int fps) {
        this->fps->setValue(std::to_string(fps));
    }

    void DeveloperTerminal::setLuaMemoryUsage(int kbytes) {
        char * unit;
        std::string value = ENGFormatting::format(kbytes * 1000, &unit);

        this->luaMemoryUsage->setValue(value);
        this->luaMemoryUsage->setUnits(std::string(unit) + "Bytes");
    }

    void DeveloperTerminal::setSystemMemoryUsage(long bytes) {
        char * unit;
        std::string value = ENGFormatting::format(bytes, &unit);

        this->systemMemoryUsage->setValue(value);
        this->systemMemoryUsage->setUnits(std::string(unit) + "Bytes");
    }

    void DeveloperTerminal::setSystemCPUUsage(float cpuUsage) {
        std::stringstream stream;
        stream << std::fixed << std::setprecision(1) << (cpuUsage * 100);

        this->systemCPUUsage->setValue(stream.str());
    }

    void DeveloperTerminal::setNumberResources(long resources) {
        this->numResources->setValue(std::to_string(resources));
    }

    void DeveloperTerminal::setNumberGroups(long groups) {
        this->numGroups->setValue(std::to_string(groups));
    }

    bool DeveloperTerminal::isVisible() {
        return window->visible();
    }

    nanogui::CheckBox * DeveloperTerminal::createDebugSettingWidget(Widget * tab, std::string name, const std::function< void(bool)> & callback, bool enabled) {
        CheckBox * box = tab->add<CheckBox>(name);
        box->addToggleCallback(callback);
        box->setChecked(enabled);

        callback(enabled);

        return box;
    }
}
