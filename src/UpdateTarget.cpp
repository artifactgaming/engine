/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "UpdateTarget.hpp"

#include <stddef.h>

UpdateTarget::UpdateTarget() {
    container = NULL;
}

UpdateTarget::~UpdateTarget() {
    if (container) {
        for (unsigned int i = 0; i < container->size(); i++) {
            if ((*container)[i] == this) {
                container->erase(container->begin() + i);
            }
        }
    }
}
