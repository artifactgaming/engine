/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "ImageSaver.hpp"
#include "ControlManager.hpp"

using namespace nanogui;

#include <iostream>
#include <map>

#include <sstream>
#include <iomanip>
#include <json/json.h>

#include "BuiltinFilePaths.hpp"
#include "Path.hpp"

namespace RM {
    std::string getButtonName(KeyboardButtonID button) {
        switch (button) {
            case KeyboardButtonID::Space:           return "Space";
            case KeyboardButtonID::Apostrophe:      return "Apostrophe";
            case KeyboardButtonID::Comma:           return "Comma";
            case KeyboardButtonID::Minus:           return "Minus";
            case KeyboardButtonID::Period:          return "Period";
            case KeyboardButtonID::Slash:           return "Slash";
            case KeyboardButtonID::Button_0:        return "0)";
            case KeyboardButtonID::Button_1:        return "1!";
            case KeyboardButtonID::Button_2:        return "2@";
            case KeyboardButtonID::Button_3:        return "3#";
            case KeyboardButtonID::Button_4:        return "4$";
            case KeyboardButtonID::Button_5:        return "5%";
            case KeyboardButtonID::Button_6:        return "6^";
            case KeyboardButtonID::Button_7:        return "7&";
            case KeyboardButtonID::Button_8:        return "8*";
            case KeyboardButtonID::Button_9:        return "9(";
            case KeyboardButtonID::Semicolon:       return "Semicolon";
            case KeyboardButtonID::Equal:           return "Equal";
            case KeyboardButtonID::Button_A:        return "A";
            case KeyboardButtonID::Button_B:        return "B";
            case KeyboardButtonID::Button_C:        return "C";
            case KeyboardButtonID::Button_D:        return "D";
            case KeyboardButtonID::Button_E:        return "E";
            case KeyboardButtonID::Button_F:        return "F";
            case KeyboardButtonID::Button_G:        return "G";
            case KeyboardButtonID::Button_H:        return "H";
            case KeyboardButtonID::Button_I:        return "I";
            case KeyboardButtonID::Button_J:        return "J";
            case KeyboardButtonID::Button_K:        return "K";
            case KeyboardButtonID::Button_L:        return "L";
            case KeyboardButtonID::Button_M:        return "M";
            case KeyboardButtonID::Button_N:        return "N";
            case KeyboardButtonID::Button_O:        return "O";
            case KeyboardButtonID::Button_P:        return "P";
            case KeyboardButtonID::Button_Q:        return "Q";
            case KeyboardButtonID::Button_R:        return "R";
            case KeyboardButtonID::Button_S:        return "S";
            case KeyboardButtonID::Button_T:        return "T";
            case KeyboardButtonID::Button_U:        return "U";
            case KeyboardButtonID::Button_V:        return "V";
            case KeyboardButtonID::Button_W:        return "W";
            case KeyboardButtonID::Button_X:        return "X";
            case KeyboardButtonID::Button_Y:        return "Y";
            case KeyboardButtonID::Button_Z:        return "Z";
            case KeyboardButtonID::LeftBracket:     return "Left Bracket";
            case KeyboardButtonID::Backslash:       return "Backslash";
            case KeyboardButtonID::RightBracket:    return "Right Bracket";
            case KeyboardButtonID::GraveAccent:     return "Grave Accent";
            case KeyboardButtonID::World_1:         return "World 1";
            case KeyboardButtonID::World_2:         return "World 2";
            case KeyboardButtonID::Escape:          return "Escape";
            case KeyboardButtonID::Enter:           return "Enter";
            case KeyboardButtonID::Tab:             return "Tab";
            case KeyboardButtonID::Backspace:       return "Backspace";
            case KeyboardButtonID::Insert:          return "Insert";
            case KeyboardButtonID::Delete:          return "Delete";
            case KeyboardButtonID::Right:           return "Right";
            case KeyboardButtonID::Left:            return "Left";
            case KeyboardButtonID::Down:            return "Down";
            case KeyboardButtonID::Up:              return "Up";
            case KeyboardButtonID::PageUp:          return "Page Up";
            case KeyboardButtonID::PageDown:        return "Page Down";
            case KeyboardButtonID::Home:            return "Home";
            case KeyboardButtonID::End:             return "End";
            case KeyboardButtonID::CapsLock:        return "Caps Lock";
            case KeyboardButtonID::ScrollLock:      return "Scroll Lock";
            case KeyboardButtonID::NumLock:         return "Num Lock";
            case KeyboardButtonID::PrintScreen:     return "Print Screen";
            case KeyboardButtonID::KeyPause:        return "Key Pause";
            case KeyboardButtonID::Button_F1:       return "F1";
            case KeyboardButtonID::Button_F2:       return "F2";
            case KeyboardButtonID::Button_F3:       return "F3";
            case KeyboardButtonID::Button_F4:       return "F4";
            case KeyboardButtonID::Button_F5:       return "F5";
            case KeyboardButtonID::Button_F6:       return "F6";
            case KeyboardButtonID::Button_F7:       return "F7";
            case KeyboardButtonID::Button_F8:       return "F8";
            case KeyboardButtonID::Button_F9:       return "F9";
            case KeyboardButtonID::Button_F10:      return "F10";
            case KeyboardButtonID::Button_F11:      return "F11";
            case KeyboardButtonID::Button_F12:      return "F12";
            case KeyboardButtonID::Button_F13:      return "F13";
            case KeyboardButtonID::Button_F14:      return "F14";
            case KeyboardButtonID::Button_F15:      return "F15";
            case KeyboardButtonID::Button_F16:      return "F16";
            case KeyboardButtonID::Button_F17:      return "F17";
            case KeyboardButtonID::Button_F18:      return "F18";
            case KeyboardButtonID::Button_F19:      return "F19";
            case KeyboardButtonID::Button_F20:      return "F20";
            case KeyboardButtonID::Button_F21:      return "F21";
            case KeyboardButtonID::Button_F22:      return "F22";
            case KeyboardButtonID::Button_F23:      return "F23";
            case KeyboardButtonID::Button_F24:      return "F24";
            case KeyboardButtonID::Button_F25:      return "F25";
            case KeyboardButtonID::KeyPad_0:        return "KeyPad 0";
            case KeyboardButtonID::KeyPad_1:        return "KeyPad 1";
            case KeyboardButtonID::KeyPad_2:        return "KeyPad 2";
            case KeyboardButtonID::KeyPad_3:        return "KeyPad 3";
            case KeyboardButtonID::KeyPad_4:        return "KeyPad 4";
            case KeyboardButtonID::KeyPad_5:        return "KeyPad 5";
            case KeyboardButtonID::KeyPad_6:        return "KeyPad 6";
            case KeyboardButtonID::KeyPad_7:        return "KeyPad 7";
            case KeyboardButtonID::KeyPad_8:        return "KeyPad 8";
            case KeyboardButtonID::KeyPad_9:        return "KeyPa 9";
            case KeyboardButtonID::KeyPad_Decimal:  return "KeyPad Decimal";
            case KeyboardButtonID::KeyPad_Divide:   return "KeyPad Divide";
            case KeyboardButtonID::KeyPad_Multiply: return "KeyPad Multiply";
            case KeyboardButtonID::KeyPad_Subtract: return "KeyPad Subtract";
            case KeyboardButtonID::KeyPad_Add:      return "KeyPad Add";
            case KeyboardButtonID::KeyPad_Enter:    return "KeyPad Enter";
            case KeyboardButtonID::KeyPad_Equal:    return "KeyPad Equal";
            case KeyboardButtonID::LeftShift:       return "Left Shift";
            case KeyboardButtonID::LeftControl:     return "Left Control";
            case KeyboardButtonID::LeftAlt:         return "Left Alt";
            case KeyboardButtonID::LeftSuper:       return "Left Super";
            case KeyboardButtonID::RightShift:      return "Right Shift";
            case KeyboardButtonID::RightControl:    return "Right Control";
            case KeyboardButtonID::RightAlt:        return "Right Alt";
            case KeyboardButtonID::RightSuper:      return "Right Super";
            case KeyboardButtonID::Menu:            return "Menu";

            case KeyboardButtonID::Mouse4:          return "Mouse 4";
            case KeyboardButtonID::Mouse5:          return "Mouse 5";
            case KeyboardButtonID::Mouse6:          return "Mouse 6";
            case KeyboardButtonID::Mouse7:          return "Mouse 7";
            case KeyboardButtonID::MouseLast:       return "Mouse Last";
            case KeyboardButtonID::MouseLeft:       return "Mouse Left";
            case KeyboardButtonID::MouseRight:      return "Mouse Right";
            case KeyboardButtonID::MouseMiddle:     return "Mouse Middle";

            default:              return "Unknown";
        }
    }

    std::string getAnalogName(MouseAnalogID analog) {
        switch (analog) {
            case MouseAnalogID::axisX:   return "Mouse X";
            case MouseAnalogID::axisY:   return "Mouse Y";
            case MouseAnalogID::scrollH: return "Horizontal Scroll";
            case MouseAnalogID::scrollV: return "Vertical Scroll";
            default: return "Unknown";
        }
    }

    std::map<std::string, KeyboardButtonID> buttonMap = {
        {"Space",               KeyboardButtonID::Space},
        {"Apostrophe",          KeyboardButtonID::Apostrophe},
        {"Comma",               KeyboardButtonID::Comma},
        {"Minus",               KeyboardButtonID::Minus},
        {"Period",              KeyboardButtonID::Period},
        {"Slash",               KeyboardButtonID::Slash},
        {"0)",                  KeyboardButtonID::Button_0},
        {"1!",                  KeyboardButtonID::Button_1},
        {"2@",                  KeyboardButtonID::Button_2},
        {"3#",                  KeyboardButtonID::Button_3},
        {"4$",                  KeyboardButtonID::Button_4},
        {"5%",                  KeyboardButtonID::Button_5},
        {"6^",                  KeyboardButtonID::Button_6},
        {"7&",                  KeyboardButtonID::Button_7},
        {"8*",                  KeyboardButtonID::Button_8},
        {"9(",                  KeyboardButtonID::Button_9},
        {"Semicolon",           KeyboardButtonID::Semicolon},
        {"Equal",               KeyboardButtonID::Equal},
        {"A",                   KeyboardButtonID::Button_A},
        {"B",                   KeyboardButtonID::Button_B},
        {"C",                   KeyboardButtonID::Button_C},
        {"D",                   KeyboardButtonID::Button_D},
        {"E",                   KeyboardButtonID::Button_E},
        {"F",                   KeyboardButtonID::Button_F},
        {"G",                   KeyboardButtonID::Button_G},
        {"H",                   KeyboardButtonID::Button_H},
        {"I",                   KeyboardButtonID::Button_I},
        {"J",                   KeyboardButtonID::Button_J},
        {"K",                   KeyboardButtonID::Button_K},
        {"L",                   KeyboardButtonID::Button_L},
        {"M",                   KeyboardButtonID::Button_M},
        {"N",                   KeyboardButtonID::Button_N},
        {"O",                   KeyboardButtonID::Button_O},
        {"P",                   KeyboardButtonID::Button_P},
        {"Q",                   KeyboardButtonID::Button_Q},
        {"R",                   KeyboardButtonID::Button_R},
        {"S",                   KeyboardButtonID::Button_S},
        {"T",                   KeyboardButtonID::Button_T},
        {"U",                   KeyboardButtonID::Button_U},
        {"V",                   KeyboardButtonID::Button_V},
        {"W",                   KeyboardButtonID::Button_W},
        {"X",                   KeyboardButtonID::Button_X},
        {"Y",                   KeyboardButtonID::Button_Y},
        {"Z",                   KeyboardButtonID::Button_Z},
        {"Left Bracket",        KeyboardButtonID::LeftBracket},
        {"Backslash",           KeyboardButtonID::Backslash},
        {"Right Bracket",       KeyboardButtonID::RightBracket},
        {"Grave Accent",        KeyboardButtonID::GraveAccent},
        {"World 1",             KeyboardButtonID::World_1},
        {"World 2",             KeyboardButtonID::World_2},
        {"Escape",              KeyboardButtonID::Escape},
        {"Enter",               KeyboardButtonID::Enter},
        {"Tab",                 KeyboardButtonID::Tab},
        {"Backspace",           KeyboardButtonID::Backspace},
        {"Insert",              KeyboardButtonID::Insert},
        {"Delete",              KeyboardButtonID::Delete},
        {"Right",               KeyboardButtonID::Right},
        {"Left",                KeyboardButtonID::Left},
        {"Down",                KeyboardButtonID::Down},
        {"Up",                  KeyboardButtonID::Up},
        {"Page Up",             KeyboardButtonID::PageUp},
        {"Page Down",           KeyboardButtonID::PageDown},
        {"Home",                KeyboardButtonID::Home},
        {"End",                 KeyboardButtonID::End},
        {"Caps Lock",           KeyboardButtonID::CapsLock},
        {"Scroll Lock",         KeyboardButtonID::ScrollLock},
        {"Num Lock",            KeyboardButtonID::NumLock},
        {"Print Screen",        KeyboardButtonID::PrintScreen},
        {"Key Pause",           KeyboardButtonID::KeyPause},
        {"F1",                  KeyboardButtonID::Button_F1},
        {"F2",                  KeyboardButtonID::Button_F2},
        {"F3",                  KeyboardButtonID::Button_F3},
        {"F4",                  KeyboardButtonID::Button_F4},
        {"F5",                  KeyboardButtonID::Button_F5},
        {"F6",                  KeyboardButtonID::Button_F6},
        {"F7",                  KeyboardButtonID::Button_F7},
        {"F8",                  KeyboardButtonID::Button_F8},
        {"F9",                  KeyboardButtonID::Button_F9},
        {"F10",                 KeyboardButtonID::Button_F10},
        {"F11",                 KeyboardButtonID::Button_F11},
        {"F12",                 KeyboardButtonID::Button_F12},
        {"F13",                 KeyboardButtonID::Button_F13},
        {"F14",                 KeyboardButtonID::Button_F14},
        {"F15",                 KeyboardButtonID::Button_F15},
        {"F16",                 KeyboardButtonID::Button_F16},
        {"F17",                 KeyboardButtonID::Button_F17},
        {"F18",                 KeyboardButtonID::Button_F18},
        {"F19",                 KeyboardButtonID::Button_F19},
        {"F20",                 KeyboardButtonID::Button_F20},
        {"F21",                 KeyboardButtonID::Button_F21},
        {"F22",                 KeyboardButtonID::Button_F22},
        {"F23",                 KeyboardButtonID::Button_F23},
        {"F24",                 KeyboardButtonID::Button_F24},
        {"F25",                 KeyboardButtonID::Button_F25},
        {"KeyPad 0",            KeyboardButtonID::KeyPad_0},
        {"KeyPad 1",            KeyboardButtonID::KeyPad_1},
        {"KeyPad 2",            KeyboardButtonID::KeyPad_2},
        {"KeyPad 3",            KeyboardButtonID::KeyPad_3},
        {"KeyPad 4",            KeyboardButtonID::KeyPad_4},
        {"KeyPad 5",            KeyboardButtonID::KeyPad_5},
        {"KeyPad 6",            KeyboardButtonID::KeyPad_6},
        {"KeyPad 7",            KeyboardButtonID::KeyPad_7},
        {"KeyPad 8",            KeyboardButtonID::KeyPad_8},
        {"KeyPad 9",            KeyboardButtonID::KeyPad_9},
        {"KeyPad Decimal",      KeyboardButtonID::KeyPad_Decimal},
        {"KeyPad Divide",       KeyboardButtonID::KeyPad_Divide},
        {"KeyPad Multiply",     KeyboardButtonID::KeyPad_Multiply},
        {"KeyPad Subtract",     KeyboardButtonID::KeyPad_Subtract},
        {"KeyPad Add",          KeyboardButtonID::KeyPad_Add},
        {"KeyPad Enter",        KeyboardButtonID::KeyPad_Enter},
        {"KeyPad Equal",        KeyboardButtonID::KeyPad_Equal},
        {"Left Shift",          KeyboardButtonID::LeftShift},
        {"Left Control",        KeyboardButtonID::LeftControl},
        {"Left Alt",            KeyboardButtonID::LeftAlt},
        {"Left Super",          KeyboardButtonID::LeftSuper},
        {"Right Shift",         KeyboardButtonID::RightShift},
        {"Right Control",       KeyboardButtonID::RightControl},
        {"Right Alt",           KeyboardButtonID::RightAlt},
        {"Right Super",         KeyboardButtonID::RightSuper},
        {"Menu",                KeyboardButtonID::Menu},

        {"Mouse 1",             KeyboardButtonID::Mouse1},
        {"Mouse 2",             KeyboardButtonID::Mouse2},
        {"Mouse 3",             KeyboardButtonID::Mouse3},
        {"Mouse 4",             KeyboardButtonID::Mouse4},
        {"Mouse 5",             KeyboardButtonID::Mouse5},
        {"Mouse 6",             KeyboardButtonID::Mouse6},
        {"Mouse 7",             KeyboardButtonID::Mouse7},
        {"Mouse 8",             KeyboardButtonID::Mouse8},
        {"Mouse Last",          KeyboardButtonID::MouseLast},
        {"Mouse Left",          KeyboardButtonID::MouseLeft},
        {"Mouse Right",         KeyboardButtonID::MouseRight},
        {"Mouse Middle",        KeyboardButtonID::MouseMiddle}
    };

    KeyboardButtonID getButtonID(std::string name) {
        std::map<std::string, KeyboardButtonID>::const_iterator result = buttonMap.find(name);
        if (result == buttonMap.end()) {
            std::cout << "WARNING: unknown button requested \"" << name << "\". Replacing it with button \"Unknown\".";
            return KeyboardButtonID::Unknown;
        } else {
            return result->second;
        }
    }

    std::map<std::string, MouseAnalogID> analogMap = {
        {
            {"Mouse X"          , MouseAnalogID::axisX},
            {"Mouse Y"          , MouseAnalogID::axisY},
            {"Horizontal Scroll", MouseAnalogID::scrollH},
            {"Vertical Scroll"  , MouseAnalogID::scrollV}
        }
    };

    MouseAnalogID getAnalogID(std::string name) {
        std::map<std::string, MouseAnalogID>::const_iterator result = analogMap.find(name);
        if (result == analogMap.end()) {
            std::cout << "WARNING: unknown button requested \"" << name << "\". Replacing it with button \"Unknown\".";
            return MouseAnalogID::Unknown;
        } else {
            return result->second;
        }
    }

    ControlManager::ControlManager() : Screen(glm::ivec2(defaultWidth, defaultHeight),
                                              defaultWindowTitle,
                                              defaultResizable,
                                              defaultFullscreen,
                                              defaultColorBits,
                                              defaultAlphaBits,
                                              defaultDepthBits,
                                              defaultStencilBits,
                                              default_nSamples,
                                              defaultGlMajor,
                                              defaultGlMinor)
    {
        monitor = glfwGetPrimaryMonitor();
        mode = glfwGetVideoMode(monitor);

        newBooleanControl("System", "Toggle Fullscreen", true, KeyboardButtonID::Button_F11);
        addBooleanCallback("System", "Toggle Fullscreen", [this](bool pushed) {
            if (pushed) {
                if (this->mFullscreen) {
                    glfwSetWindowMonitor(this->mGLFWWindow, NULL, (this->mode->width / 2) - (defaultWidth / 2), (mode->height / 2) - (defaultHeight / 2), defaultWidth, defaultHeight, 60);
                    this->mFullscreen = false;
                } else {
                    glfwSetWindowMonitor(this->mGLFWWindow, this->monitor, 0, 0, mode->width, mode->height, mode->refreshRate);
                    this->mFullscreen = true;
                }
            }
        });

        newBooleanControl("System", "Take Screenshot", true, KeyboardButtonID::Button_F1);
        addBooleanCallback("System", "Take Screenshot", [this](bool pushed) {
            if (pushed) {
                this->takeScreenshot();
            }
        });

        //Create the screenshot directory.
        boost::system::error_code returnedError;
        boost::filesystem::create_directories(SCREENSHOT_DIR, returnedError);

        if (returnedError) {
            std::string message = "Failed to create screenshot directory\nReason returned by OS:\n";
            message += returnedError.message();
            throw std::runtime_error(message);
        }

        controlAssigner = nullptr;
    }

    ControlManager::~ControlManager() {

    }

    void ControlManager::updateEditors() {
        for (auto v : editors) {
            v->update();
        }
    }

    void ControlManager::newBooleanControl(std::string group, std::string name, bool ignore_focus, KeyboardButtonID defaultButton) {
        if (group.find(':') != std::string::npos) {
            throw std::runtime_error("Control group name must not contain \":\".");
        }

        if (name.find(':') != std::string::npos) {
            throw std::runtime_error("Control name must not contain \":\".");
        }

        boost::recursive_mutex::scoped_lock lock(mutex);

        std::string key = group + ":" + name;
        std::map<std::string, std::shared_ptr<Control>>::const_iterator result = namedControls.find(key);
        if (result != namedControls.end()) {
            throw std::runtime_error("Control already exists: " + key);
        }

        std::shared_ptr<Control> control = std::make_shared<Control>(group, name, ignore_focus, defaultButton);
        controls.push_back(control);
        namedControls.insert(std::pair<std::string, std::shared_ptr<Control>>(key, control));
        idedBoolControls.insert(std::pair<KeyboardButtonID, std::shared_ptr<Control>>(control->boolean_id, control));

        updateEditors();
    }

    RM::ref<nanogui::ListenerReference<void(bool pushed)>> ControlManager::addBooleanCallback(std::string group, std::string name, std::function <void(bool pushed)> func) {

        boost::recursive_mutex::scoped_lock lock(mutex);

        std::string key = group + ":" + name;
        std::map<std::string, std::shared_ptr<Control>>::const_iterator result = namedControls.find(key);
        if (result != namedControls.end()) {
            std::shared_ptr<Control> control = result->second;
            if (control->type == ControlType::boolean) {
                return control->booleanFunc.addListener(func);
            } else {
                throw std::runtime_error("Attempted to change the boolean callback of an analog control.");
            }
        } else {
            std::string message = "Failed find boolean control \"" + key + "\".";
            throw std::runtime_error(message);
        }
    }

    void ControlManager::removeBooleanCallback(std::string group, std::string name, RM::ref<nanogui::ListenerReference<void(bool pushed)>> reference) {

        boost::recursive_mutex::scoped_lock lock(mutex);

        std::string key = group + ":" + name;
        std::map<std::string, std::shared_ptr<Control>>::const_iterator result = namedControls.find(key);
        if (result != namedControls.end()) {
            std::shared_ptr<Control> control = result->second;
            if (control->type == ControlType::boolean) {
                control->booleanFunc.removeListener(reference);
            } else {
                throw std::runtime_error("Attempted to remove the boolean callback of an analog control.");
            }
        } else {
            std::string message = "Failed find boolean control \"" + key + "\".";
            throw std::runtime_error(message);
        }
    }

    void ControlManager::changeBooleanBinding(std::string group, std::string name, KeyboardButtonID newButton) {

        boost::recursive_mutex::scoped_lock lock(mutex);

        std::string key = group + ":" + name;
        std::map<std::string, std::shared_ptr<Control>>::const_iterator result = namedControls.find(key);
        if (result != namedControls.end()) {
            const std::shared_ptr<Control> & control = result->second;

            if (control->type == ControlType::boolean) {
                idedBoolControls.erase(control->boolean_id);

                control->boolean_id = newButton;
                idedBoolControls[newButton] = control;
            } else {
                throw std::runtime_error("Attempted to change the boolean binding of an analog control.");
            }
        } else {
            std::string message = "Failed find boolean control \"" + key + "\".";
            throw std::runtime_error(message);
        }

        updateEditors();
    }

    void ControlManager::changeBooleanBinding(std::string group, std::string name) {

        boost::recursive_mutex::scoped_lock lock(mutex);

        std::string key = group + ":" + name;
        std::map<std::string, std::shared_ptr<Control>>::const_iterator result = namedControls.find(key);
        if (result != namedControls.end()) {
            const std::shared_ptr<Control> & control = result->second;

            if (control->type == ControlType::boolean) {
                idedBoolControls.erase(control->boolean_id);

                control->boolean_id = KeyboardButtonID::Unknown;
                toAssign = control;

                controlAssigner = this->add<Window>("Change Button Binding");
                controlAssigner->setLayout(new GroupLayout());
                controlAssigner->add<Label>("Press the button you would like to assign to the control: " + name);

                controlAssigner->center();
                controlAssigner->requestFocus();

                this->screen()->performLayout();
            } else {
                throw std::runtime_error("Attempted to change the boolean binding of an analog control.");
            }
        } else {
            std::string message = "Failed find boolean control \"" + key + "\".";
            throw std::runtime_error(message);
        }

        updateEditors();
    }

    void ControlManager::newAnalogControl(std::string group, std::string name, bool ignore_focus, MouseAnalogID defaultAnalog) {
        if (group.find(':') != std::string::npos) {
            throw std::runtime_error("Control group name must not contain \":\".");
        }

        if (name.find(':') != std::string::npos) {
            throw std::runtime_error("Control name must not contain \":\".");
        }

        boost::recursive_mutex::scoped_lock lock(mutex);

        std::string key = group + ":" + name;
        std::map<std::string, std::shared_ptr<Control>>::const_iterator result = namedControls.find(key);
        if (result != namedControls.end()) {
            throw std::runtime_error("Control already exists: " + key);
        }

        std::shared_ptr<Control> control = std::make_shared<Control>(group, name, ignore_focus, defaultAnalog);
        controls.push_back(control);
        namedControls.insert(std::pair<std::string, std::shared_ptr<Control>>(key, control));
        idedAnalogControls.insert(std::pair<MouseAnalogID, std::shared_ptr<Control>>(control->analog_id, control));

        updateEditors();
    }

    RM::ref<nanogui::ListenerReference<void(float delta)>> ControlManager::addAnalogCallback(std::string group, std::string name, std::function <void(float delta)> func) {

        boost::recursive_mutex::scoped_lock lock(mutex);

        std::string key = group + ":" + name;
        std::map<std::string, std::shared_ptr<Control>>::const_iterator result = namedControls.find(key);
        if (result != namedControls.end()) {
            std::shared_ptr<Control> control = result->second;
            if (control->type == ControlType::analog) {
                return control->analogFunc.addListener(func);
            } else {
                throw std::runtime_error("Attempted to change the analog callback of a boolean control.");
            }
        } else {
            std::string message = "Failed find analog control \"" + key + "\".";
            throw std::runtime_error(message);
        }
    }

    void ControlManager::removeAnalogCallback(std::string group, std::string name, RM::ref<nanogui::ListenerReference<void(float delta)>> reference) {

        boost::recursive_mutex::scoped_lock lock(mutex);

        std::string key = group + ":" + name;
        std::map<std::string, std::shared_ptr<Control>>::const_iterator result = namedControls.find(key);
        if (result != namedControls.end()) {
            std::shared_ptr<Control> control = result->second;
            if (control->type == ControlType::analog) {
                control->analogFunc.removeListener(reference);
            } else {
                throw std::runtime_error("Attempted to change the analog callback of a boolean control.");
            }
        } else {
            std::string message = "Failed find analog control \"" + key + "\".";
            throw std::runtime_error(message);
        }
    }

    void ControlManager::changeAnalogBinding(std::string group, std::string name, MouseAnalogID newAnalog) {

        boost::recursive_mutex::scoped_lock lock(mutex);

        std::string key = group + ":" + name;
        std::map<std::string, std::shared_ptr<Control>>::const_iterator result = namedControls.find(key);
        if (result != namedControls.end()) {
            const std::shared_ptr<Control> & control = result->second;

            if (control->type == ControlType::analog) {
                idedAnalogControls.erase(control->analog_id);

                control->analog_id = newAnalog;
                idedAnalogControls[newAnalog] = control;
            } else {
                throw std::runtime_error("Attempted to change the analog binding of a boolean control.");
            }
        } else {
            std::string message = "Failed find analog control \"" + key + "\".";
            throw std::runtime_error(message);
        }

        updateEditors();
    }

    void ControlManager::changeAnalogBinding(std::string group, std::string name) {

        boost::recursive_mutex::scoped_lock lock(mutex);

        std::string key = group + ":" + name;
        std::map<std::string, std::shared_ptr<Control>>::const_iterator result = namedControls.find(key);
        if (result != namedControls.end()) {
            const std::shared_ptr<Control> & control = result->second;

            if (control->type == ControlType::analog) {
                idedAnalogControls.erase(control->analog_id);

                control->analog_id = MouseAnalogID::Unknown;
                toAssign = control;

                controlAssigner = this->add<Window>("Change Analog Binding");
                controlAssigner->setLayout(new GroupLayout());
                controlAssigner->add<Label>("Move the analog control you would like to assign to the control: " + name);

                controlAssigner->center();
                controlAssigner->requestFocus();

                this->screen()->performLayout();
            } else {
                throw std::runtime_error("Attempted to change the analog binding of a boolean control.");
            }

        } else {
            std::string message = "Failed find analog control \"" + key + "\".";
            throw std::runtime_error(message);
        }

        updateEditors();
    }

    void ControlManager::load() {

        boost::recursive_mutex::scoped_lock lock(mutex);

        Path configPath(std::string(CONTROL_CONF));

        if (configPath.exists()) {
            Json::Value config;
            std::string input;

            boost::filesystem::ifstream file(CONTROL_CONF, std::ios::in | std::ios::binary);
            if (!file) {
                throw std::runtime_error("Failed to open file.");
            }

            file.seekg(0, std::ios::end);
            input.resize(file.tellg());
            file.seekg(0, std::ios::beg);
            file.read(&input[0], input.size());
            file.close();

            Json::Reader reader;
            if (reader.parse(input, config)) {
                Json::Value::Members members = config.getMemberNames();

                idedBoolControls.clear();
                idedAnalogControls.clear();

                for (auto name : members) {
                    Json::Value jsonControl = config[name];

                    std::map<std::string, std::shared_ptr<Control>>::const_iterator result = namedControls.find(name);
                    if (result != namedControls.end()) {
                        const std::shared_ptr<Control> & control = result->second;

                        if (control->type == ControlType::boolean) {
                            if (jsonControl["type"] != "boolean") {
                                std::cout << "WARNING: Control \"" << name << "\" was found in config file \"" << CONTROL_CONF << "\" as a \"" << jsonControl["type"] << "\" when it was expected to be a boolean control." << std::endl;
                                std::cout << "This control will be deleted from the config file the next time controls are modified and need to be saved." << std::endl;
                            }

                            KeyboardButtonID button = static_cast<KeyboardButtonID>(jsonControl["scancode"].asInt());
                            if (getButtonName(button) == "Unknown") {
                                std::cout << "WARNING: Control \"" << name << "\" was found in config file \"" << CONTROL_CONF << "\" assigned to an invalid button value." << std::endl;
                                std::cout << "This control will be deleted from the config file the next time controls are modified and need to be saved." << std::endl;
                            }

                            control->boolean_id = button;
                            idedBoolControls[button] = control;

                        } else if (control->type == ControlType::analog) {
                            if (jsonControl["type"] != "analog") {
                                std::cout << "WARNING: Control \"" << name << "\" was found in config file \"" << CONTROL_CONF << "\" as a \"" << jsonControl["type"] << "\" when it was expected to be an analog control." << std::endl;
                                std::cout << "This control will be deleted from the config file the next time controls are modified and need to be saved." << std::endl;
                            }

                            MouseAnalogID analog = static_cast<MouseAnalogID>(jsonControl["scancode"].asInt());
                            if (getAnalogName(analog) == "Unknown") {
                                std::cout << "WARNING: Control \"" << name << "\" was found in config file \"" << CONTROL_CONF << "\" assigned to an invalid analog value." << std::endl;
                                std::cout << "This control will be deleted from the config file the next time controls are modified and need to be saved." << std::endl;
                            }

                            control->analog_id = analog;
                            control->sensitivity = jsonControl["sensitivity"].asFloat();
                            idedAnalogControls[analog] = control;
                        }

                    } else {
                        std::cout << "WARNING: Control \"" << name << "\" was found in config file \"" << CONTROL_CONF << "\" but has not been registered as a control by Grid Locked or any loaded mods." << std::endl;
                        std::cout << "This control will be deleted from the config file the next time controls are modified and need to be saved." << std::endl;
                    }
                }

                updateEditors();

                std::cout << "Loaded control config from file: " << CONTROL_CONF << std::endl;

            } else {
                std::string error = "Error while reading controls config file \"";
                error += CONTROL_CONF;
                error += "\":\n";
                error += reader.getFormattedErrorMessages();

                throw std::runtime_error(error);
            }
        } else {
            std::cout << "Control config file \"" << CONTROL_CONF << "\" does not exist." << std::endl;
            std::cout << "Generating config file..." << std::endl;
            save();
        }
    }

    void ControlManager::save() {
        Json::Value config;

        for (std::shared_ptr<Control> cont : this->controls) {
            std::string & group = cont->group;
            std::string & name = cont->name;

            std::string key = group + ":" + name;

            Json::Value jsonCont;

            if (cont->type == ControlType::boolean) {
                jsonCont["type"] = "boolean";
                jsonCont["scancode"] = static_cast<unsigned int>(cont->boolean_id);
            } else {
                jsonCont["type"] = "analog";
                jsonCont["scancode"] = static_cast<unsigned int>(cont->analog_id);
                jsonCont["sensitivity"] = cont->sensitivity;
            }

            config[key] = jsonCont;
        }

        Json::StreamWriterBuilder wbuilder;
        wbuilder["indentation"] = "    ";

        std::string filecontent = Json::writeString(wbuilder, config);

        boost::filesystem::ofstream file(CONTROL_CONF);
        if (!file) {
            throw std::runtime_error("Failed to open file.");
        }

        file << filecontent;
        file.close();

        std::cout << "Saved control config to file:" << CONTROL_CONF << std::endl;
    }

    void ControlManager::center() {
        // Just centers the window.
        if (!this->mFullscreen) {
            glfwSetWindowMonitor(this->mGLFWWindow, NULL, (this->mode->width / 2) - (defaultWidth / 2), (mode->height / 2) - (defaultHeight / 2), defaultWidth, defaultHeight, 60);
        }
    }

    bool ControlManager::mouseMotionEvent(const glm::ivec2& p, const glm::ivec2& rel, int button, int modifiers) {
        if (controlAssigner == nullptr) {
            if (rel.x != 0) {
                std::unordered_map<MouseAnalogID, std::shared_ptr<Control>, EnumHash>::const_iterator result = idedAnalogControls.find(MouseAnalogID::axisX);
                if (result != idedAnalogControls.end()) {
                    std::shared_ptr<Control> cont = result->second;
                    if (cont->ignore_focus || (!mFocusPath.empty() && mFocusPath.front()->permitsControl())) {
                        cont->analogFunc(static_cast<float>(rel.x) * cont->sensitivity);
                    }
                }
            }

            if (rel.y != 0) {
                std::unordered_map<MouseAnalogID, std::shared_ptr<Control>, EnumHash>::const_iterator result = idedAnalogControls.find(MouseAnalogID::axisY);
                if (result != idedAnalogControls.end()) {
                    std::shared_ptr<Control> cont = result->second;
                    if (cont->ignore_focus || (!mFocusPath.empty() && mFocusPath.front()->permitsControl())) {
                        cont->analogFunc(static_cast<float>(rel.y) * cont->sensitivity);
                    }
                }
            }

            if (!mFocusPath.empty() && mFocusPath.front()->grabsMouse())
            {
                return false;
            }
            else
            {
                return this->Screen::mouseMotionEvent(p, rel, button, modifiers);
            }
        } else {
            if (toAssign->type == ControlType::analog && controlAssigner->contains(p)) {
                if (abs(rel.x) > abs(rel.y)) {
                    // Set to X.
                    toAssign->analog_id = MouseAnalogID::axisX;
                    idedAnalogControls[MouseAnalogID::axisX] = toAssign;

                } else {
                    // Set to Y.
                    toAssign->analog_id = MouseAnalogID::axisY;
                    idedAnalogControls[MouseAnalogID::axisY] = toAssign;

                }

                controlAssigner->dispose();
                controlAssigner = nullptr;

                updateEditors();
            }

            return false;
        }
    }

    bool ControlManager::mouseButtonEvent(const glm::ivec2& p, int button, bool down, int modifiers) {

        if (controlAssigner == nullptr) {
            std::unordered_map<KeyboardButtonID, std::shared_ptr<Control>, EnumHash>::const_iterator result = idedBoolControls.find(static_cast<KeyboardButtonID>(button));
            if (result != idedBoolControls.end()) {
                std::shared_ptr<Control> cont = result->second;
                if (cont->ignore_focus || (!mFocusPath.empty() && mFocusPath.front()->permitsControl())) {
                    cont->booleanFunc(down);
                }
            }

            if (!mFocusPath.empty() && mFocusPath.front()->grabsMouse())
            {
                return false;
            }
            else
            {
                return this->Screen::mouseButtonEvent(p, button, down, modifiers);
            }
        } else {
            if (toAssign->type == ControlType::boolean) {
                toAssign->boolean_id = static_cast<KeyboardButtonID>(button);
                idedBoolControls[static_cast<KeyboardButtonID>(button)] = toAssign;

                controlAssigner->dispose();
                controlAssigner = nullptr;

                updateEditors();
            }

            mDragActive = false;
            mDragWidget = nullptr;
            return false;
        }
    }

    bool ControlManager::keyboardEvent(int key, int scancode, int action, int modifiers) {
        if (controlAssigner == nullptr) {
            if (action != GLFW_REPEAT && action != GLFW_KEY_UNKNOWN) {

                std::unordered_map<KeyboardButtonID, std::shared_ptr<Control>, EnumHash>::const_iterator result = idedBoolControls.find(static_cast<KeyboardButtonID>(key));
                if (result != idedBoolControls.end()) {
                    std::shared_ptr<Control> cont = result->second;

                    if (cont->ignore_focus || (!mFocusPath.empty() && mFocusPath.front()->permitsControl())) {
                        cont->booleanFunc(action == GLFW_PRESS);
                    }
                }
            }

            return this->Screen::keyboardEvent(key, scancode, action, modifiers);
        } else {
            if (toAssign->type == ControlType::boolean) {
                toAssign->boolean_id = static_cast<KeyboardButtonID>(key);
                idedBoolControls[static_cast<KeyboardButtonID>(key)] = toAssign;

                controlAssigner->dispose();
                controlAssigner = nullptr;

                updateEditors();
            }

            return false;
        }
    }

    bool ControlManager::scrollEvent(const glm::ivec2 &p, const glm::vec2 &rel) {
        if (controlAssigner == nullptr) {
            if (rel.x != 0) {
                std::unordered_map<MouseAnalogID, std::shared_ptr<Control>, EnumHash>::const_iterator result = idedAnalogControls.find(MouseAnalogID::scrollH);
                if (result != idedAnalogControls.end()) {
                    std::shared_ptr<Control> cont = result->second;
                    if (cont->ignore_focus || (!mFocusPath.empty() && mFocusPath.front()->permitsControl())) {
                        cont->analogFunc(static_cast<float>(rel.x) * cont->sensitivity);
                    }
                }
            }

            if (rel.y != 0) {
                std::unordered_map<MouseAnalogID, std::shared_ptr<Control>, EnumHash>::const_iterator result = idedAnalogControls.find(MouseAnalogID::scrollV);
                if (result != idedAnalogControls.end()) {
                    std::shared_ptr<Control> cont = result->second;
                    if (cont->ignore_focus || (!mFocusPath.empty() && mFocusPath.front()->permitsControl())) {
                        cont->analogFunc(static_cast<float>(rel.y) * cont->sensitivity);
                    }
                }
            }

            return this->Screen::scrollEvent(p, rel);
        } else {
            if (toAssign->type == ControlType::analog) {
                if (abs(rel.x) > abs(rel.y)) {
                    // Set to X.
                    toAssign->analog_id = MouseAnalogID::scrollH;
                    idedAnalogControls[MouseAnalogID::scrollH] = toAssign;

                } else {
                    // Set to Y.
                    toAssign->analog_id = MouseAnalogID::scrollV;
                    idedAnalogControls[MouseAnalogID::scrollV] = toAssign;

                }

                controlAssigner->dispose();
                controlAssigner = nullptr;

                updateEditors();
            }

            return false;
        }
    }

    void ControlManager::takeScreenshot() {
        std::ostringstream fileNameBuilder;

        Path file = SCREENSHOT_DIR;

        auto t = std::time(nullptr);
        auto tm = *std::localtime(&t);
        fileNameBuilder << "screenshot-" << std::put_time(&tm, "%d-%m-%Y_%H-%M-%S") << ".png";
        file = file / fileNameBuilder.str();

        std::cout << file.string() << "\"." << std::endl;
        std::cout.flush();

        int width;
        int height;

        glfwGetFramebufferSize(glfwWindow(), &width, &height);

        //3 Bytes [Red, Green, Blue] for each pixel.
        //Do not allocate these on the stack! There's a good chance you'll overflow it if you do.
        GLubyte * pixels = new GLubyte[width * height * 4];

        glReadPixels(0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, pixels);

        // This function deletes pixels for us.
        RM::saveImage(file, pixels, width, height);
    }

    Window * ControlManager::openControlEditor(std::function<void(void)> onShutdown) {
        // Deletes self at proper time.
        return new ControlManager::ControlEditor(static_cast<Widget*>(this), this, onShutdown);
    }

    ControlManager::ControlEditor::ControlEditor(nanogui::Widget * parent, ControlManager * manager, std::function<void(void)> onShutdown) :
        Window(parent, "Controls"),
        onShutdown(onShutdown),
        manager(manager)
    {
        manager->editors.push_back(this);

        setLayout(new GroupLayout());

        tabHeader = this->add<TabWidget>();
        tabHeader->setFixedSize(glm::ivec2(450, 500));
        tabHeader->addSwitchCallback([this](int selection)
        {
            for (auto button : this->editButtons) {
                button->setPushed(false);
                button->popup()->setVisible(false);
            }
        });

        Button * backButton = add<Button>("Back", ENTYPO_ICON_ARROW_LEFT);
        backButton->addPushCallback([this]()
        {
            this->manager->save();
            this->dispose();
        });

        update();
        tabHeader->setActiveTab(0);
    }

    ControlManager::ControlEditor::~ControlEditor() {

    }

    void ControlManager::ControlEditor::dispose() {
        onShutdown();

        std::deque<ControlEditor*> & editors = manager->editors;

        auto index = std::find(editors.begin(), editors.end(), this);

        if (index != editors.end()) {
            editors.erase(index);
        } else {
            std::cerr << "WARNING: Failed to remove control editor from manager list." << std::endl;
            std::cerr << "If you are seeing this message, this is a major engine level bug. Please find a way to reproduce it and report." << std::endl;
        }

        this->Window::dispose();
    }

    void ControlManager::ControlEditor::update() {
        for (std::shared_ptr<ControlManager::Control> cont : manager->controls) {
            std::string & group = cont->group;
            std::string & name = cont->name;

            std::string key = group + ":" + name;

            std::map<std::string, ControlWidget*>::const_iterator result = controls.find(key);
            ControlWidget * control;

            if (result != controls.end()) {
                control = result->second;
            } else {
                Widget * tab = nullptr;

                std::map<std::string, Widget*>::const_iterator tab_result = tabs.find(group);
                if (tab_result != tabs.end()) {
                    tab = tab_result->second;
                } else {
                    Widget * panel = tabHeader->createTab(group);
                    VScrollPanel * scrollPanel = panel->add<VScrollPanel>();
                    scrollPanel->setFixedHeight(470);
                    tab = scrollPanel->add<Widget>();
                    tab->setLayout(new GroupLayout());
                    tabs[group] = tab;
                }

                control = tab->add<ControlWidget>(this->manager, cont, group, name);
                controls[key] = control;

                PopupButton * button = control->edit;
                editButtons.push_back(button);
                button->addPushCallback([this, button]()
                {
                    for (auto but : this->editButtons) {
                        if (but != button) {
                            but->setPushed(false);
                        }
                    }
                });
            }

            if (control->control->type == ControlType::boolean) {
                control->textbox->setValue(getButtonName(cont->boolean_id));
            } else {
                control->textbox->setValue(getAnalogName(cont->analog_id));
            }

        }

        center();
    }

    ControlManager::ControlEditor::ControlWidget::ControlWidget(Widget * parent, ControlManager * manager, std::shared_ptr<ControlManager::Control> control, std::string group, std::string name):
    Widget(parent),
    group(group),
    name(name),
    control(control),
    manager(manager)
    {
        setLayout(new BoxLayout(Orientation::Horizontal, Alignment::Maximum, 0, 6));
        label = this->add<Label>(name);
        label->setFixedWidth(150);

        textbox = this->add<TextBox>();
        textbox->setEditable(false);
        textbox->setFixedWidth(150);

        struct ControlPopupButton: PopupButton {
            ControlPopupButton(Widget * parent, std::string text) : PopupButton(parent, text) {

            }

            virtual void performLayout(NVGcontext *ctx) {
                Widget::performLayout(ctx);

                const Window *parentWindow = window();

                int posY = absolutePosition().y - parentWindow->position().y + mSize.y /2;
                mPopup->setAnchorPos(glm::ivec2(this->position().x + 15, posY));
            }
        };

        edit = this->add<ControlPopupButton>("Edit");
        edit->setSide(Popup::Side::Left);

        Popup * popup = edit->popup();
        popup->setLayout(new GroupLayout());
        Label * label = popup->add<Label>("Editing Control: " + control->name);
        label->setFixedWidth(200);

        if (control->type == ControlType::analog) {
            Button * removeButton = popup->add<Button>("Remove Binding");
            removeButton->addPushCallback([this]()
            {
                this->manager->idedAnalogControls.erase(this->control->analog_id);
                this->control->analog_id = MouseAnalogID::Unknown;
                this->manager->updateEditors();
            });

            Button * change = popup->add<Button>("Change Binding");
            change->addPushCallback([this]()
            {
                this->edit->setPushed(false);
                this->manager->changeAnalogBinding(this->control->group, this->control->name);
            });

            Label * sensitivityLabel = popup->add<Label>("Sensitivity: " + std::to_string(static_cast<int>(control->sensitivity * 100)) + "%");
            Slider * slider = popup->add<Slider>();
            slider->setValue(control->sensitivity * 0.5f);

            slider->addChangeCallback([this, sensitivityLabel](float value) {
                this->control->sensitivity = value * 2;
                sensitivityLabel->setCaption("Sensitivity: " + std::to_string(static_cast<int>(this->control->sensitivity * 100)) + "%");
            });

        } else if (control->type == ControlType::boolean) {
            Button * removeButton = popup->add<Button>("Remove Binding");
            removeButton->addPushCallback([this]()
            {
                this->manager->idedBoolControls.erase(this->control->boolean_id);
                this->control->boolean_id = KeyboardButtonID::Unknown;
                this->manager->updateEditors();
            });

            Button * change = popup->add<Button>("Change Binding");
            change->addPushCallback([this]()
            {
                this->edit->setPushed(false);
                this->manager->changeBooleanBinding(this->control->group, this->control->name);
            });
        }

        parent->screen()->performLayout();
    }

    ControlManager::ControlEditor::ControlWidget::~ControlWidget() {

    }

    const unsigned int ControlManager::defaultWidth = 800;
    const unsigned int ControlManager::defaultHeight = 600;
    const char ControlManager::defaultWindowTitle[] = "Grid Locked";
    const bool ControlManager::defaultResizable = true;
    const bool ControlManager::defaultFullscreen = false;
    const int ControlManager::defaultColorBits = 8;
    const int ControlManager::defaultAlphaBits = 8;
    const int ControlManager::defaultDepthBits = 24;
    const int ControlManager::defaultStencilBits = 8;
    const int ControlManager::default_nSamples = 0;
    const unsigned int ControlManager::defaultGlMajor = 3;
    const unsigned int ControlManager::defaultGlMinor = 3;
}
