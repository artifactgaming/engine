/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "BasicResource.hpp"

#include "ResourceManager.hpp"
#include "globals.hpp"

namespace RM {
    BasicResource::BasicResource(std::string name, std::string type, ResourceGroup * group) : group(group), name(name), type(type) {
        onProgressSet = false;
        onCompletionSet = false;

        if (name.empty())
        {
            throw std::runtime_error("All resources must be given a name.");
        }

        // If the name starts with a ~, don't make announcements about it in the logs.
        announce = (name[0] != '~');

        getResourceManager()->resources.push_back(this);

        stepsToLoad = 0;
        stepsCompleted = 0;

        if (group) {
            group->addMember(this);
        }
    }

    BasicResource::~BasicResource() {
        if (group) {
            group->removeMember(this);
        }

        ResourceManager * manager = getResourceManager();
        std::deque<BasicResource*> & resources = manager->resources;

        auto index = std::find(resources.begin(), resources.end(), this);
        if (index != resources.end()) {
            resources.erase(index);
            if (announce)
            {
                std::cout << "Disposed resource " << type << ":" << name << std::endl;
            }
        } else {
            std::cout << "Warning: Engine level bug. Resource \"" << name << "\" of type \"" << type << "\" could not be found in the resource manager. Please find a way to reproduce this warning and report it." << std::endl;
            std::cout << "THIS: " << this << std::endl;

            for (BasicResource * resource : resources) {
                std::cout << resource << std::endl;
            }
        }
    }

    unsigned int BasicResource::getResourceStepsToLoad() {
        return stepsToLoad;
    }

    unsigned int BasicResource::getResourceStepsCompleted() {
        return stepsCompleted;
    }

    void BasicResource::setResourceStepsToLoad(unsigned int number) {

        if (group) {
            group->stepsToLoad -= stepsToLoad;
            group->stepsToLoad += number;
        }

        stepsToLoad = number;
    }

    void BasicResource::incrementResourceProgress(int number) {
        stepsCompleted += number;

        if (onProgressSet) {
            onProgress(stepsCompleted);
        }

        if (onCompletionSet && stepsCompleted >= stepsToLoad) {
            onCompletion();
        }

        if (group) {
            group->progress(number);
        }
    }

    std::string BasicResource::resourceName() {
        return name;
    }

    std::string BasicResource::resourceType() {
        return type;
    }

    void BasicResource::setProgressCallback(std::function<void(unsigned int)> onProgress) {
        this->onProgress = onProgress;
        onProgressSet = true;

        onProgress(stepsCompleted);
    }

    void BasicResource::setCompletionCallback(std::function<void(void)> onCompletion) {
        this->onCompletion = onCompletion;
        onCompletionSet = true;

        if (stepsCompleted >= stepsToLoad) {
            onCompletion();
        }
    }

    ResourceGroup * BasicResource::getGroup() {
        return group;
    }
}
