/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "ResourceGroup.hpp"

#include <iostream>

#include "globals.hpp"
#include "ResourceManager.hpp"

namespace RM {
    ResourceGroup::ResourceGroup(std::string name) : name(name) {
        stepsCompleted = 0;
        stepsToLoad = 0;

        onProgressSet = false;
        onCompletionSet = false;

        getResourceManager()->groups.push_back(this);
    }

    ResourceGroup::~ResourceGroup() {
        std::cout << "Group abandoned: " << name << std::endl;

        ResourceManager * manager = getResourceManager();
        std::deque<ResourceGroup*> & groups = manager->groups;

        auto index = std::find(groups.begin(), groups.end(), this);
        if (index != groups.end()) {
            groups.erase(index);
            std::cout << "Disposed group " << name << std::endl;
        } else {
            std::cout << "Warning: Engine level bug. Group \"" << name << "\" could not be found in the resource manager. Please find a way to reproduce this warning and report it." << std::endl;
            std::cout << "THIS: " << this << std::endl;

            for (ResourceGroup * group : groups) {
                std::cout << group << std::endl;
            }
        }
    }

    void ResourceGroup::setProgressCallback(std::function<void(unsigned int)> onProgress) {
        this->onProgress = onProgress;
        onProgressSet = true;

        onProgress(stepsCompleted);
    }

    void ResourceGroup::setCompletionCallback(std::function<void(void)> onCompletion) {
        this->onCompletion = onCompletion;
        onCompletionSet = true;

        if (stepsCompleted >= stepsToLoad) {
            onCompletion();
        }
    }

    unsigned int ResourceGroup::getStepsToLoad() {
        return stepsToLoad;
    }

    unsigned int ResourceGroup::getStepsCompleted() {
        return stepsCompleted;
    }

    std::string ResourceGroup::getName() {
        return name;
    }

    size_t ResourceGroup::getNumMembers() {
        return members.size();
    }

    std::vector<BasicResource*> & ResourceGroup::getMembers() {
        return members;
    }

    void ResourceGroup::addMember(BasicResource * member) {
        this->incRef();
        members.push_back(member);
    }

    void ResourceGroup::removeMember(BasicResource * member) {
        auto index = std::find(members.begin(), members.end(), member);
        if (index != members.end()) {
            members.erase(index);
            this->decRef();
        } else {
            std::cout << "Warning: Engine level bug. Resource \"" << member->resourceName() << "\" of type \"" << member->resourceType() << "\" could not be found in the resource group \"" << name << "\". Please find a way to reproduce this warning and report it." << std::endl;
        }
    }

    void ResourceGroup::progress(unsigned int progress) {
        stepsCompleted += progress;

        if (onProgressSet) {
            onProgress(stepsCompleted);
        }

        if (onCompletionSet && stepsCompleted >= stepsToLoad) {
            onCompletion();
        }
    }
}
