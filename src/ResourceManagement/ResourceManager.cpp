/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "ResourceManager.hpp"
#include "EngineeringFormatter.hpp"

#include <iostream>
#include <boost/thread/recursive_mutex.hpp>

#include "luaSupport.hpp"
#include "luaWorld.hpp"

#define THREAD_COUNT boost::thread::hardware_concurrency()

namespace RM {
    ResourceManager::ResourceManager():
    initBarrier(THREAD_COUNT+1)
    {
        alive = true;
    }

    ResourceManager::~ResourceManager() {
        if (resources.size() > 0) {
            std::cout << "Warning: Not all resources were disposed before shutdown." << std::endl;
            printResourceStats();
        }
    }

    void ResourceManager::start()
    {
        {
            boost::mutex::scoped_lock lock(mutex);
            luaStates.emplace(std::piecewise_construct, std::make_tuple(boost::this_thread::get_id()), std::make_tuple());
        }

        LuaSupport::loadCoreAPI();
        LuaSupport::loadClientAPI();
        registerWorldEnvs();

        int numThreads = THREAD_COUNT; // One thread per concurrency.

        for (int t = 0; t < numThreads; t++) {
            threads.emplace_back(&ResourceManager::run, this);
        }

        initBarrier.wait();
    }

    ResourceManager::ThreadLuaState & ResourceManager::internalGetState(const boost::thread::id & id)
    {
        std::map<boost::thread::id, ThreadLuaState>::iterator result = luaStates.find(id);
        if (result != luaStates.end()) {
            return result->second;
        } else {
            throw std::runtime_error("An unregistered thread attempted to grab a Lua state. This is an engine level bug. Please report it.");
        }
    }

    std::function<sol::environment(sol::state_view)> ResourceManager::getEnvLoader(std::string name)
    {
        std::map<std::string, std::function<sol::environment(sol::state_view)>>::iterator result = envLoaders.find(name);
        if (result != envLoaders.end()) {
            return result->second;
        } else {
            throw std::runtime_error("Attempted to load a Lua environment that has not been registered. This is an engine level bug. Please report it.");
        }
    }

    void ResourceManager::registerEnvironment(std::string name, std::function<sol::environment(sol::state_view)> loader)
    {
        // Only one write at a time.
        boost::unique_lock<boost::shared_mutex> lock(stateMutex);
        std::map<std::string, std::function<sol::environment(sol::state_view)>>::iterator result = envLoaders.find(name);
        if (result == envLoaders.end()) {
            envLoaders[name] = loader;
        } else {
            throw std::runtime_error("Attempted to register a Lua environment that has already been registered. This is an engine level bug. Please report it.");
        }
    }

    void ResourceManager::removeEnvironment(const std::string name)
    {
        // Only one write at a time.
        boost::unique_lock<boost::shared_mutex> lock(stateMutex);

        // First remove the loader.
        envLoaders.erase(envLoaders.find(name));

        // Remove all instances that may have already been created.
        std::map<boost::thread::id, ThreadLuaState>::iterator it = luaStates.begin();

        while (it != luaStates.end())
        {
            boost::mutex::scoped_lock(it->second.mutex);

            std::map<std::string, sol::environment> & envs = it->second.envs;
            std::map<std::string, sol::environment>::iterator toRemove = envs.find(name);

            if (toRemove != envs.end())
            {
                envs.erase(toRemove);
            }

            it++;
        }
    }

    sol::state_view ResourceManager::getEnvironment(std::string envName, sol::environment & env)
    {
        boost::shared_lock<boost::shared_mutex> lock(stateMutex);

        ThreadLuaState & state = internalGetState(boost::this_thread::get_id());

        std::map<std::string, sol::environment> & envs = state.envs;
        sol::state_view lua = state.lua;

        std::map<std::string, sol::environment>::iterator result = envs.find(envName);
        if (result == envs.end()) {
            env = getEnvLoader(envName)(lua);
            envs[envName] = env;
        } else {
            env = result->second;
        }

        return lua;
    }

    sol::state_view ResourceManager::getStateOnly()
    {
        ThreadLuaState & state = internalGetState(boost::this_thread::get_id());
        return state.lua;
    }

    void ResourceManager::run() {

        boost::thread::id threadID = boost::this_thread::get_id();

        {
            boost::mutex::scoped_lock lock(mutex);
            luaStates.emplace(std::piecewise_construct, std::make_tuple(boost::this_thread::get_id()), std::make_tuple());
        }

        initBarrier.wait();

        LuaSupport::loadCoreAPI();

        ThreadLuaState & state = internalGetState(threadID);
        unsigned int numThreads = THREAD_COUNT;

        while (true) {
            AsyncResource * resource = popAsyncResource();

            if (resource) {
                boost::mutex::scoped_lock lock(state.mutex);
                boost::recursive_mutex::scoped_lock lock2(resource->mutex);

                LuaSupport::resetScriptTimeout(state.lua);
                resource->async_load();
                resource->cond.notify_all();
                resource->added = false;

                // If we're keeping up well with the load, aggressively collect garbage.
                if (queue.size() < numThreads)
                {
                    state.lua.collect_garbage();
                }
            }
            else
            {
                break;
            }
        }
    }

    size_t ResourceManager::getNumResources() {
        return this->resources.size();
    }

    void ResourceManager::printResourceStats() {
        std::cout << "Resource Statistics:\n[Type][Group][Name][Reference Count]\n";
        for (BasicResource * resource : resources) {
            std::string groupName = "None";
            ResourceGroup * group = resource->getGroup();
            if (group ) {
                groupName = group->getName();
            }

            std::cout << '[' << resource->resourceType() << "][" << groupName << "][" << resource->resourceName() << "][" << resource->getRefCount() << "]\n";
        }

        std::cout.flush();
    }

    size_t ResourceManager::getNumGroups() {
        return groups.size();
    }

    void ResourceManager::printGroupStats() {
        std::cout << "Group Statistics:\n[Name][Num Members][Reference Count]\n";
        for (ResourceGroup * group : groups) {
            std::cout << '[' << group->getName() << "][" << group->getNumMembers() << "][" << group->getRefCount() << "]\n";
        }

        std::cout.flush();
    }

    void ResourceManager::printLuaEnvironments()
    {
        std::map<std::string, std::function<sol::environment(sol::state_view)>>::iterator it = envLoaders.begin();

        while (it != envLoaders.end())
        {
            std::cout << it->first << std::endl;
            it++;
        }
    }

    int ResourceManager::getLuaMemoryUsage()
    {
        int memoryUsage = 0;

        std::map<boost::thread::id, ThreadLuaState>::iterator it = luaStates.begin();

        while (it != luaStates.end())
        {
            boost::mutex::scoped_lock(it->second.mutex);
            memoryUsage += lua_gc(it->second.lua, LUA_GCCOUNT, 0);
            it++;
        }

        return memoryUsage;
    }

    void ResourceManager::syncronusUpdate() {
        boost::mutex::scoped_lock lock(syncUpdateMutex);
        while (!syncUpdateQueue.empty()) {
            RM::ref<AsyncResource> resource = syncUpdateQueue.front();
            syncUpdateQueue.pop();

            boost::recursive_mutex::scoped_lock lock2(resource->mutex);
            resource->syncUpdate();
        }
    }

    void ResourceManager::shutdown() {
        alive = false;
        cond.notify_all();

        int numThreads = threads.size();
        for (int t = 0; t < numThreads; t++) {
            threads[t].join();
        }

        // Finish off all of the remaining resources.
        syncronusUpdate();

        std::map<boost::thread::id, ThreadLuaState>::iterator it = luaStates.begin();

        while (it != luaStates.end())
        {
            sol::state_view lua = it->second.lua;
            lua.collect_garbage();
            it++;
        }

        luaStates.clear();

        std::cout << "Resource Manager has shut down." << std::endl;
    }

    void ResourceManager::pushAsyncResource(AsyncResource * resource) {
        boost::mutex::scoped_lock lock(mutex);
        queue.push(resource);
        cond.notify_one();
    }

    RM::ref<AsyncResource> ResourceManager::popAsyncResource() {
        boost::mutex::scoped_lock lock(mutex);

        while(queue.empty() && alive)
        {
            cond.wait(lock);
        }

        if (!queue.empty())
        {
            AsyncResource * resource = queue.front();
            queue.pop();

            return resource;
        }
        else
        {
            return nullptr;
        }

    }

    void ResourceManager::readyForSyncUpdate(AsyncResource * resource)
    {
        boost::mutex::scoped_lock lock(syncUpdateMutex);
        boost::recursive_mutex::scoped_lock lock2(resource->mutex);
        syncUpdateQueue.push(resource);
    }
}
