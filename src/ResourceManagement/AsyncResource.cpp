/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "AsyncResource.hpp"

#include "globals.hpp"

namespace RM {
    AsyncResource::AsyncResource(std::string name, std::string type, ResourceGroup * group) : RM::BasicResource(name, type, group)
    {
        readyForUpdate = false;
        added = false;
        progress = 0;
        manager = getResourceManager();
        incRef();
    }

    AsyncResource::~AsyncResource()
    {

    }

    void AsyncResource::startLoad() {
        if (!added)
        {
            manager->pushAsyncResource(this);
            added = true;
        }
    }

    void AsyncResource::syncUpdate() {
        {
            boost::recursive_mutex::scoped_lock lock(mutex);

            if (onProgressSet) {
                onProgress(stepsCompleted);
            }

            if (stepsCompleted >= stepsToLoad) {
                sync_load();

                if (onCompletionSet) {
                    onCompletion();
                }
            }

            if (group) {
                group->progress(progress);
                progress = 0;
            }

            readyForUpdate = false;
        }

        if (stepsCompleted >= stepsToLoad) {
            decRef();
        }
    }

    void AsyncResource::setResourceStepsToLoad(unsigned int number) {
        boost::recursive_mutex::scoped_lock lock(mutex);

        if (group) {
            group->stepsToLoad -= stepsToLoad;
            group->stepsToLoad += number;
        }

        stepsToLoad = number;
    }

    void AsyncResource::incrementResourceProgress(int number) {
        boost::recursive_mutex::scoped_lock lock(mutex);
        progress += number;
        stepsCompleted += number;
        if (!readyForUpdate) {
            readyForUpdate = true;
            manager->readyForSyncUpdate(this);
        }
    }
}
