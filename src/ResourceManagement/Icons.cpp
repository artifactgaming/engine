/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "Icons.hpp"

#include <nanogui/nanogui.h>
using namespace nanogui;

// 500px ENTYPO_ICON_500PX
// 500px_with_circle ENTYPO_ICON_500PX_WITH_CIRCLE
// add_to_list ENTYPO_ICON_ADD_TO_LIST
// add_user ENTYPO_ICON_ADD_USER
// address ENTYPO_ICON_ADDRESS
// adjust ENTYPO_ICON_ADJUST
// air ENTYPO_ICON_AIR
// aircraft ENTYPO_ICON_AIRCRAFT
// aircraft_landing ENTYPO_ICON_AIRCRAFT_LANDING
// aircraft_take_off ENTYPO_ICON_AIRCRAFT_TAKE_OFF
// align_bottom ENTYPO_ICON_ALIGN_BOTTOM
// align_horizontal_middle ENTYPO_ICON_ALIGN_HORIZONTAL_MIDDLE
// align_left ENTYPO_ICON_ALIGN_LEFT
// align_right ENTYPO_ICON_ALIGN_RIGHT
// align_top ENTYPO_ICON_ALIGN_TOP
// align_vertical_middle ENTYPO_ICON_ALIGN_VERTICAL_MIDDLE
// app_store ENTYPO_ICON_APP_STORE
// archive ENTYPO_ICON_ARCHIVE
// arrow_bold_down ENTYPO_ICON_ARROW_BOLD_DOWN
// arrow_bold_left ENTYPO_ICON_ARROW_BOLD_LEFT
// arrow_bold_right ENTYPO_ICON_ARROW_BOLD_RIGHT
// arrow_bold_up ENTYPO_ICON_ARROW_BOLD_UP
// arrow_down ENTYPO_ICON_ARROW_DOWN
// arrow_left ENTYPO_ICON_ARROW_LEFT
// arrow_long_down ENTYPO_ICON_ARROW_LONG_DOWN
// arrow_long_left ENTYPO_ICON_ARROW_LONG_LEFT
// arrow_long_right ENTYPO_ICON_ARROW_LONG_RIGHT
// arrow_long_up ENTYPO_ICON_ARROW_LONG_UP
// arrow_right ENTYPO_ICON_ARROW_RIGHT
// arrow_up ENTYPO_ICON_ARROW_UP
// arrow_with_circle_down ENTYPO_ICON_ARROW_WITH_CIRCLE_DOWN
// arrow_with_circle_left ENTYPO_ICON_ARROW_WITH_CIRCLE_LEFT
// arrow_with_circle_right ENTYPO_ICON_ARROW_WITH_CIRCLE_RIGHT
// arrow_with_circle_up ENTYPO_ICON_ARROW_WITH_CIRCLE_UP
// attachment ENTYPO_ICON_ATTACHMENT
// awareness_ribbon ENTYPO_ICON_AWARENESS_RIBBON
// back ENTYPO_ICON_BACK
// back_in_time ENTYPO_ICON_BACK_IN_TIME
// baidu ENTYPO_ICON_BAIDU
// bar_graph ENTYPO_ICON_BAR_GRAPH
// basecamp ENTYPO_ICON_BASECAMP
// battery ENTYPO_ICON_BATTERY
// behance ENTYPO_ICON_BEHANCE
// bell ENTYPO_ICON_BELL
// blackboard ENTYPO_ICON_BLACKBOARD
// block ENTYPO_ICON_BLOCK
// book ENTYPO_ICON_BOOK
// bookmark ENTYPO_ICON_BOOKMARK
// bookmarks ENTYPO_ICON_BOOKMARKS
// bowl ENTYPO_ICON_BOWL
// box ENTYPO_ICON_BOX
// briefcase ENTYPO_ICON_BRIEFCASE
// browser ENTYPO_ICON_BROWSER
// brush ENTYPO_ICON_BRUSH
// bucket ENTYPO_ICON_BUCKET
// bug ENTYPO_ICON_BUG
// cake ENTYPO_ICON_CAKE
// calculator ENTYPO_ICON_CALCULATOR
// calendar ENTYPO_ICON_CALENDAR
// camera ENTYPO_ICON_CAMERA
// ccw ENTYPO_ICON_CCW
// chat ENTYPO_ICON_CHAT
// check ENTYPO_ICON_CHECK
// chevron_down ENTYPO_ICON_CHEVRON_DOWN
// chevron_left ENTYPO_ICON_CHEVRON_LEFT
// chevron_right ENTYPO_ICON_CHEVRON_RIGHT
// chevron_small_down ENTYPO_ICON_CHEVRON_SMALL_DOWN
// chevron_small_left ENTYPO_ICON_CHEVRON_SMALL_LEFT
// chevron_small_right ENTYPO_ICON_CHEVRON_SMALL_RIGHT
// chevron_small_up ENTYPO_ICON_CHEVRON_SMALL_UP
// chevron_thin_down ENTYPO_ICON_CHEVRON_THIN_DOWN
// chevron_thin_left ENTYPO_ICON_CHEVRON_THIN_LEFT
// chevron_thin_right ENTYPO_ICON_CHEVRON_THIN_RIGHT
// chevron_thin_up ENTYPO_ICON_CHEVRON_THIN_UP
// chevron_up ENTYPO_ICON_CHEVRON_UP
// chevron_with_circle_down ENTYPO_ICON_CHEVRON_WITH_CIRCLE_DOWN
// chevron_with_circle_left ENTYPO_ICON_CHEVRON_WITH_CIRCLE_LEFT
// chevron_with_circle_right ENTYPO_ICON_CHEVRON_WITH_CIRCLE_RIGHT
// chevron_with_circle_up ENTYPO_ICON_CHEVRON_WITH_CIRCLE_UP
// circle ENTYPO_ICON_CIRCLE
// circle_with_cross ENTYPO_ICON_CIRCLE_WITH_CROSS
// circle_with_minus ENTYPO_ICON_CIRCLE_WITH_MINUS
// circle_with_plus ENTYPO_ICON_CIRCLE_WITH_PLUS
// circular_graph ENTYPO_ICON_CIRCULAR_GRAPH
// clapperboard ENTYPO_ICON_CLAPPERBOARD
// classic_computer ENTYPO_ICON_CLASSIC_COMPUTER
// clipboard ENTYPO_ICON_CLIPBOARD
// clock ENTYPO_ICON_CLOCK
// cloud ENTYPO_ICON_CLOUD
// code ENTYPO_ICON_CODE
// cog ENTYPO_ICON_COG
// colours ENTYPO_ICON_COLOURS
// compass ENTYPO_ICON_COMPASS
// controller_fast_backward ENTYPO_ICON_CONTROLLER_FAST_BACKWARD
// controller_fast_forward ENTYPO_ICON_CONTROLLER_FAST_FORWARD
// controller_jump_to_start ENTYPO_ICON_CONTROLLER_JUMP_TO_START
// controller_next ENTYPO_ICON_CONTROLLER_NEXT
// controller_paus ENTYPO_ICON_CONTROLLER_PAUS
// controller_play ENTYPO_ICON_CONTROLLER_PLAY
// controller_record ENTYPO_ICON_CONTROLLER_RECORD
// controller_stop ENTYPO_ICON_CONTROLLER_STOP
// controller_volume ENTYPO_ICON_CONTROLLER_VOLUME
// copy ENTYPO_ICON_COPY
// creative_cloud ENTYPO_ICON_CREATIVE_CLOUD
// creative_commons ENTYPO_ICON_CREATIVE_COMMONS
// creative_commons_attribution ENTYPO_ICON_CREATIVE_COMMONS_ATTRIBUTION
// creative_commons_noderivs ENTYPO_ICON_CREATIVE_COMMONS_NODERIVS
// creative_commons_noncommercial_eu ENTYPO_ICON_CREATIVE_COMMONS_NONCOMMERCIAL_EU
// creative_commons_noncommercial_us ENTYPO_ICON_CREATIVE_COMMONS_NONCOMMERCIAL_US
// creative_commons_public_domain ENTYPO_ICON_CREATIVE_COMMONS_PUBLIC_DOMAIN
// creative_commons_remix ENTYPO_ICON_CREATIVE_COMMONS_REMIX
// creative_commons_share ENTYPO_ICON_CREATIVE_COMMONS_SHARE
// creative_commons_sharealike ENTYPO_ICON_CREATIVE_COMMONS_SHAREALIKE
// credit ENTYPO_ICON_CREDIT
// credit_card ENTYPO_ICON_CREDIT_CARD
// crop ENTYPO_ICON_CROP
// cross ENTYPO_ICON_CROSS
// cup ENTYPO_ICON_CUP
// cw ENTYPO_ICON_CW
// cycle ENTYPO_ICON_CYCLE
// database ENTYPO_ICON_DATABASE
// dial_pad ENTYPO_ICON_DIAL_PAD
// direction ENTYPO_ICON_DIRECTION
// document ENTYPO_ICON_DOCUMENT
// document_landscape ENTYPO_ICON_DOCUMENT_LANDSCAPE
// documents ENTYPO_ICON_DOCUMENTS
// dot_single ENTYPO_ICON_DOT_SINGLE
// dots_three_horizontal ENTYPO_ICON_DOTS_THREE_HORIZONTAL
// dots_three_vertical ENTYPO_ICON_DOTS_THREE_VERTICAL
// dots_two_horizontal ENTYPO_ICON_DOTS_TWO_HORIZONTAL
// dots_two_vertical ENTYPO_ICON_DOTS_TWO_VERTICAL
// download ENTYPO_ICON_DOWNLOAD
// dribbble ENTYPO_ICON_DRIBBBLE
// dribbble_with_circle ENTYPO_ICON_DRIBBBLE_WITH_CIRCLE
// drink ENTYPO_ICON_DRINK
// drive ENTYPO_ICON_DRIVE
// drop ENTYPO_ICON_DROP
// dropbox ENTYPO_ICON_DROPBOX
// edit ENTYPO_ICON_EDIT
// email ENTYPO_ICON_EMAIL
// emoji_flirt ENTYPO_ICON_EMOJI_FLIRT
// emoji_happy ENTYPO_ICON_EMOJI_HAPPY
// emoji_neutral ENTYPO_ICON_EMOJI_NEUTRAL
// emoji_sad ENTYPO_ICON_EMOJI_SAD
// erase ENTYPO_ICON_ERASE
// eraser ENTYPO_ICON_ERASER
// evernote ENTYPO_ICON_EVERNOTE
// export ENTYPO_ICON_EXPORT
// eye ENTYPO_ICON_EYE
// eye_with_line ENTYPO_ICON_EYE_WITH_LINE
// facebook ENTYPO_ICON_FACEBOOK
// facebook_with_circle ENTYPO_ICON_FACEBOOK_WITH_CIRCLE
// feather ENTYPO_ICON_FEATHER
// fingerprint ENTYPO_ICON_FINGERPRINT
// flag ENTYPO_ICON_FLAG
// flash ENTYPO_ICON_FLASH
// flashlight ENTYPO_ICON_FLASHLIGHT
// flat_brush ENTYPO_ICON_FLAT_BRUSH
// flattr ENTYPO_ICON_FLATTR
// flickr ENTYPO_ICON_FLICKR
// flickr_with_circle ENTYPO_ICON_FLICKR_WITH_CIRCLE
// flow_branch ENTYPO_ICON_FLOW_BRANCH
// flow_cascade ENTYPO_ICON_FLOW_CASCADE
// flow_line ENTYPO_ICON_FLOW_LINE
// flow_parallel ENTYPO_ICON_FLOW_PARALLEL
// flow_tree ENTYPO_ICON_FLOW_TREE
// flower ENTYPO_ICON_FLOWER
// folder ENTYPO_ICON_FOLDER
// folder_images ENTYPO_ICON_FOLDER_IMAGES
// folder_music ENTYPO_ICON_FOLDER_MUSIC
// folder_video ENTYPO_ICON_FOLDER_VIDEO
// forward ENTYPO_ICON_FORWARD
// foursquare ENTYPO_ICON_FOURSQUARE
// funnel ENTYPO_ICON_FUNNEL
// game_controller ENTYPO_ICON_GAME_CONTROLLER
// gauge ENTYPO_ICON_GAUGE
// github ENTYPO_ICON_GITHUB
// github_with_circle ENTYPO_ICON_GITHUB_WITH_CIRCLE
// globe ENTYPO_ICON_GLOBE
// google_drive ENTYPO_ICON_GOOGLE_DRIVE
// google_hangouts ENTYPO_ICON_GOOGLE_HANGOUTS
// google_play ENTYPO_ICON_GOOGLE_PLAY
// google_plus ENTYPO_ICON_GOOGLE_PLUS
// google_plus_with_circle ENTYPO_ICON_GOOGLE_PLUS_WITH_CIRCLE
// graduation_cap ENTYPO_ICON_GRADUATION_CAP
// grid ENTYPO_ICON_GRID
// grooveshark ENTYPO_ICON_GROOVESHARK
// hair_cross ENTYPO_ICON_HAIR_CROSS
// hand ENTYPO_ICON_HAND
// heart ENTYPO_ICON_HEART
// heart_outlined ENTYPO_ICON_HEART_OUTLINED
// help ENTYPO_ICON_HELP
// help_with_circle ENTYPO_ICON_HELP_WITH_CIRCLE
// home ENTYPO_ICON_HOME
// hour_glass ENTYPO_ICON_HOUR_GLASS
// houzz ENTYPO_ICON_HOUZZ
// icloud ENTYPO_ICON_ICLOUD
// image ENTYPO_ICON_IMAGE
// image_inverted ENTYPO_ICON_IMAGE_INVERTED
// images ENTYPO_ICON_IMAGES
// inbox ENTYPO_ICON_INBOX
// infinity ENTYPO_ICON_INFINITY
// info ENTYPO_ICON_INFO
// info_with_circle ENTYPO_ICON_INFO_WITH_CIRCLE
// instagram ENTYPO_ICON_INSTAGRAM
// instagram_with_circle ENTYPO_ICON_INSTAGRAM_WITH_CIRCLE
// install ENTYPO_ICON_INSTALL
// key ENTYPO_ICON_KEY
// keyboard ENTYPO_ICON_KEYBOARD
// lab_flask ENTYPO_ICON_LAB_FLASK
// landline ENTYPO_ICON_LANDLINE
// language ENTYPO_ICON_LANGUAGE
// laptop ENTYPO_ICON_LAPTOP
// lastfm ENTYPO_ICON_LASTFM
// lastfm_with_circle ENTYPO_ICON_LASTFM_WITH_CIRCLE
// layers ENTYPO_ICON_LAYERS
// leaf ENTYPO_ICON_LEAF
// level_down ENTYPO_ICON_LEVEL_DOWN
// level_up ENTYPO_ICON_LEVEL_UP
// lifebuoy ENTYPO_ICON_LIFEBUOY
// light_bulb ENTYPO_ICON_LIGHT_BULB
// light_down ENTYPO_ICON_LIGHT_DOWN
// light_up ENTYPO_ICON_LIGHT_UP
// line_graph ENTYPO_ICON_LINE_GRAPH
// link ENTYPO_ICON_LINK
// linkedin ENTYPO_ICON_LINKEDIN
// linkedin_with_circle ENTYPO_ICON_LINKEDIN_WITH_CIRCLE
// list ENTYPO_ICON_LIST
// location ENTYPO_ICON_LOCATION
// location_pin ENTYPO_ICON_LOCATION_PIN
// lock ENTYPO_ICON_LOCK
// lock_open ENTYPO_ICON_LOCK_OPEN
// log_out ENTYPO_ICON_LOG_OUT
// login ENTYPO_ICON_LOGIN
// loop ENTYPO_ICON_LOOP
// magnet ENTYPO_ICON_MAGNET
// magnifying_glass ENTYPO_ICON_MAGNIFYING_GLASS
// mail ENTYPO_ICON_MAIL
// mail_with_circle ENTYPO_ICON_MAIL_WITH_CIRCLE
// man ENTYPO_ICON_MAN
// map ENTYPO_ICON_MAP
// mask ENTYPO_ICON_MASK
// medal ENTYPO_ICON_MEDAL
// medium ENTYPO_ICON_MEDIUM
// medium_with_circle ENTYPO_ICON_MEDIUM_WITH_CIRCLE
// megaphone ENTYPO_ICON_MEGAPHONE
// menu ENTYPO_ICON_MENU
// merge ENTYPO_ICON_MERGE
// message ENTYPO_ICON_MESSAGE
// mic ENTYPO_ICON_MIC
// minus ENTYPO_ICON_MINUS
// mixi ENTYPO_ICON_MIXI
// mobile ENTYPO_ICON_MOBILE
// modern_mic ENTYPO_ICON_MODERN_MIC
// moon ENTYPO_ICON_MOON
// mouse ENTYPO_ICON_MOUSE
// mouse_pointer ENTYPO_ICON_MOUSE_POINTER
// music ENTYPO_ICON_MUSIC
// network ENTYPO_ICON_NETWORK
// new ENTYPO_ICON_NEW
// new_message ENTYPO_ICON_NEW_MESSAGE
// news ENTYPO_ICON_NEWS
// newsletter ENTYPO_ICON_NEWSLETTER
// note ENTYPO_ICON_NOTE
// notification ENTYPO_ICON_NOTIFICATION
// notifications_off ENTYPO_ICON_NOTIFICATIONS_OFF
// old_mobile ENTYPO_ICON_OLD_MOBILE
// old_phone ENTYPO_ICON_OLD_PHONE
// onedrive ENTYPO_ICON_ONEDRIVE
// open_book ENTYPO_ICON_OPEN_BOOK
// palette ENTYPO_ICON_PALETTE
// paper_plane ENTYPO_ICON_PAPER_PLANE
// paypal ENTYPO_ICON_PAYPAL
// pencil ENTYPO_ICON_PENCIL
// phone ENTYPO_ICON_PHONE
// picasa ENTYPO_ICON_PICASA
// pie_chart ENTYPO_ICON_PIE_CHART
// pin ENTYPO_ICON_PIN
// pinterest ENTYPO_ICON_PINTEREST
// pinterest_with_circle ENTYPO_ICON_PINTEREST_WITH_CIRCLE
// plus ENTYPO_ICON_PLUS
// popup ENTYPO_ICON_POPUP
// power_plug ENTYPO_ICON_POWER_PLUG
// price_ribbon ENTYPO_ICON_PRICE_RIBBON
// price_tag ENTYPO_ICON_PRICE_TAG
// print ENTYPO_ICON_PRINT
// progress_empty ENTYPO_ICON_PROGRESS_EMPTY
// progress_full ENTYPO_ICON_PROGRESS_FULL
// progress_one ENTYPO_ICON_PROGRESS_ONE
// progress_two ENTYPO_ICON_PROGRESS_TWO
// publish ENTYPO_ICON_PUBLISH
// qq ENTYPO_ICON_QQ
// qq_with_circle ENTYPO_ICON_QQ_WITH_CIRCLE
// quote ENTYPO_ICON_QUOTE
// radio ENTYPO_ICON_RADIO
// raft ENTYPO_ICON_RAFT
// raft_with_circle ENTYPO_ICON_RAFT_WITH_CIRCLE
// rainbow ENTYPO_ICON_RAINBOW
// rdio ENTYPO_ICON_RDIO
// rdio_with_circle ENTYPO_ICON_RDIO_WITH_CIRCLE
// remove_user ENTYPO_ICON_REMOVE_USER
// renren ENTYPO_ICON_RENREN
// reply ENTYPO_ICON_REPLY
// reply_all ENTYPO_ICON_REPLY_ALL
// resize_100_percent ENTYPO_ICON_RESIZE_100_PERCENT
// resize_full_screen ENTYPO_ICON_RESIZE_FULL_SCREEN
// retweet ENTYPO_ICON_RETWEET
// rocket ENTYPO_ICON_ROCKET
// round_brush ENTYPO_ICON_ROUND_BRUSH
// rss ENTYPO_ICON_RSS
// ruler ENTYPO_ICON_RULER
// save ENTYPO_ICON_SAVE
// scissors ENTYPO_ICON_SCISSORS
// scribd ENTYPO_ICON_SCRIBD
// select_arrows ENTYPO_ICON_SELECT_ARROWS
// share ENTYPO_ICON_SHARE
// share_alternative ENTYPO_ICON_SHARE_ALTERNATIVE
// shareable ENTYPO_ICON_SHAREABLE
// shield ENTYPO_ICON_SHIELD
// shop ENTYPO_ICON_SHOP
// shopping_bag ENTYPO_ICON_SHOPPING_BAG
// shopping_basket ENTYPO_ICON_SHOPPING_BASKET
// shopping_cart ENTYPO_ICON_SHOPPING_CART
// shuffle ENTYPO_ICON_SHUFFLE
// signal ENTYPO_ICON_SIGNAL
// sina_weibo ENTYPO_ICON_SINA_WEIBO
// skype ENTYPO_ICON_SKYPE
// skype_with_circle ENTYPO_ICON_SKYPE_WITH_CIRCLE
// slideshare ENTYPO_ICON_SLIDESHARE
// smashing ENTYPO_ICON_SMASHING
// sound ENTYPO_ICON_SOUND
// sound_mix ENTYPO_ICON_SOUND_MIX
// sound_mute ENTYPO_ICON_SOUND_MUTE
// soundcloud ENTYPO_ICON_SOUNDCLOUD
// sports_club ENTYPO_ICON_SPORTS_CLUB
// spotify ENTYPO_ICON_SPOTIFY
// spotify_with_circle ENTYPO_ICON_SPOTIFY_WITH_CIRCLE
// spreadsheet ENTYPO_ICON_SPREADSHEET
// squared_cross ENTYPO_ICON_SQUARED_CROSS
// squared_minus ENTYPO_ICON_SQUARED_MINUS
// squared_plus ENTYPO_ICON_SQUARED_PLUS
// star ENTYPO_ICON_STAR
// star_outlined ENTYPO_ICON_STAR_OUTLINED
// stopwatch ENTYPO_ICON_STOPWATCH
// stumbleupon ENTYPO_ICON_STUMBLEUPON
// stumbleupon_with_circle ENTYPO_ICON_STUMBLEUPON_WITH_CIRCLE
// suitcase ENTYPO_ICON_SUITCASE
// swap ENTYPO_ICON_SWAP
// swarm ENTYPO_ICON_SWARM
// sweden ENTYPO_ICON_SWEDEN
// switch ENTYPO_ICON_SWITCH
// ablet NTYPO_ICON_TABLET
// tablet_mobile_combo ENTYPO_ICON_TABLET_MOBILE_COMBO
// tag ENTYPO_ICON_TAG
// text ENTYPO_ICON_TEXT
// text_document ENTYPO_ICON_TEXT_DOCUMENT
// text_document_inverted ENTYPO_ICON_TEXT_DOCUMENT_INVERTED
// thermometer ENTYPO_ICON_THERMOMETER
// thumbs_down ENTYPO_ICON_THUMBS_DOWN
// thumbs_up ENTYPO_ICON_THUMBS_UP
// thunder_cloud ENTYPO_ICON_THUNDER_CLOUD
// ticket ENTYPO_ICON_TICKET
// time_slot ENTYPO_ICON_TIME_SLOT
// tools ENTYPO_ICON_TOOLS
// traffic_cone ENTYPO_ICON_TRAFFIC_CONE
// trash ENTYPO_ICON_TRASH
// tree ENTYPO_ICON_TREE
// triangle_down ENTYPO_ICON_TRIANGLE_DOWN
// triangle_left ENTYPO_ICON_TRIANGLE_LEFT
// triangle_right ENTYPO_ICON_TRIANGLE_RIGHT
// triangle_up ENTYPO_ICON_TRIANGLE_UP
// tripadvisor ENTYPO_ICON_TRIPADVISOR
// trophy ENTYPO_ICON_TROPHY
// tumblr ENTYPO_ICON_TUMBLR
// tumblr_with_circle ENTYPO_ICON_TUMBLR_WITH_CIRCLE
// tv ENTYPO_ICON_TV
// twitter ENTYPO_ICON_TWITTER
// twitter_with_circle ENTYPO_ICON_TWITTER_WITH_CIRCLE
// typing ENTYPO_ICON_TYPING
// uninstall ENTYPO_ICON_UNINSTALL
// unread ENTYPO_ICON_UNREAD
// untag ENTYPO_ICON_UNTAG
// upload ENTYPO_ICON_UPLOAD
// upload_to_cloud ENTYPO_ICON_UPLOAD_TO_CLOUD
// user ENTYPO_ICON_USER
// users ENTYPO_ICON_USERS
// v_card ENTYPO_ICON_V_CARD
// video ENTYPO_ICON_VIDEO
// video_camera ENTYPO_ICON_VIDEO_CAMERA
// vimeo ENTYPO_ICON_VIMEO
// vimeo_with_circle ENTYPO_ICON_VIMEO_WITH_CIRCLE
// vine ENTYPO_ICON_VINE
// vine_with_circle ENTYPO_ICON_VINE_WITH_CIRCLE
// vinyl ENTYPO_ICON_VINYL
// vk ENTYPO_ICON_VK
// vk_alternitive ENTYPO_ICON_VK_ALTERNITIVE
// vk_with_circle ENTYPO_ICON_VK_WITH_CIRCLE
// voicemail ENTYPO_ICON_VOICEMAIL
// wallet ENTYPO_ICON_WALLET
// warning ENTYPO_ICON_WARNING
// water ENTYPO_ICON_WATER
// windows_store ENTYPO_ICON_WINDOWS_STORE
// xing ENTYPO_ICON_XING
// xing_with_circle ENTYPO_ICON_XING_WITH_CIRCLE
// yelp ENTYPO_ICON_YELP
// youko ENTYPO_ICON_YOUKO
// youko_with_circle ENTYPO_ICON_YOUKO_WITH_CIRCLE
// youtube ENTYPO_ICON_YOUTUBE
// youtube_with_circle ENTYPO_ICON_YOUTUBE_WITH_CIRCLE


#include <map>

std::map<std::string, int> iconMap
(
    {
        {"500px", ENTYPO_ICON_500PX},
        {"500px_with_circle", ENTYPO_ICON_500PX_WITH_CIRCLE},
        {"add_to_list", ENTYPO_ICON_ADD_TO_LIST},
        {"add_user", ENTYPO_ICON_ADD_USER},
        {"address", ENTYPO_ICON_ADDRESS},
        {"adjust", ENTYPO_ICON_ADJUST},
        {"air", ENTYPO_ICON_AIR},
        {"aircraft", ENTYPO_ICON_AIRCRAFT},
        {"aircraft_landing", ENTYPO_ICON_AIRCRAFT_LANDING},
        {"aircraft_take_off", ENTYPO_ICON_AIRCRAFT_TAKE_OFF},
        {"align_bottom", ENTYPO_ICON_ALIGN_BOTTOM},
        {"align_horizontal_middle", ENTYPO_ICON_ALIGN_HORIZONTAL_MIDDLE},
        {"align_left", ENTYPO_ICON_ALIGN_LEFT},
        {"align_right", ENTYPO_ICON_ALIGN_RIGHT},
        {"align_top", ENTYPO_ICON_ALIGN_TOP},
        {"align_vertical_middle", ENTYPO_ICON_ALIGN_VERTICAL_MIDDLE},
        {"app_store", ENTYPO_ICON_APP_STORE},
        {"archive", ENTYPO_ICON_ARCHIVE},
        {"arrow_bold_down", ENTYPO_ICON_ARROW_BOLD_DOWN},
        {"arrow_bold_left", ENTYPO_ICON_ARROW_BOLD_LEFT},
        {"arrow_bold_right", ENTYPO_ICON_ARROW_BOLD_RIGHT},
        {"arrow_bold_up", ENTYPO_ICON_ARROW_BOLD_UP},
        {"arrow_down", ENTYPO_ICON_ARROW_DOWN},
        {"arrow_left", ENTYPO_ICON_ARROW_LEFT},
        {"arrow_long_down", ENTYPO_ICON_ARROW_LONG_DOWN},
        {"arrow_long_left", ENTYPO_ICON_ARROW_LONG_LEFT},
        {"arrow_long_right", ENTYPO_ICON_ARROW_LONG_RIGHT},
        {"arrow_long_up", ENTYPO_ICON_ARROW_LONG_UP},
        {"arrow_right", ENTYPO_ICON_ARROW_RIGHT},
        {"arrow_up", ENTYPO_ICON_ARROW_UP},
        {"arrow_with_circle_down", ENTYPO_ICON_ARROW_WITH_CIRCLE_DOWN},
        {"arrow_with_circle_left", ENTYPO_ICON_ARROW_WITH_CIRCLE_LEFT},
        {"arrow_with_circle_right", ENTYPO_ICON_ARROW_WITH_CIRCLE_RIGHT},
        {"arrow_with_circle_up", ENTYPO_ICON_ARROW_WITH_CIRCLE_UP},
        {"attachment", ENTYPO_ICON_ATTACHMENT},
        {"awareness_ribbon", ENTYPO_ICON_AWARENESS_RIBBON},
        {"back", ENTYPO_ICON_BACK},
        {"back_in_time", ENTYPO_ICON_BACK_IN_TIME},
        {"baidu", ENTYPO_ICON_BAIDU},
        {"bar_graph", ENTYPO_ICON_BAR_GRAPH},
        {"basecamp", ENTYPO_ICON_BASECAMP},
        {"battery", ENTYPO_ICON_BATTERY},
        {"behance", ENTYPO_ICON_BEHANCE},
        {"bell", ENTYPO_ICON_BELL},
        {"blackboard", ENTYPO_ICON_BLACKBOARD},
        {"block", ENTYPO_ICON_BLOCK},
        {"book", ENTYPO_ICON_BOOK},
        {"bookmark", ENTYPO_ICON_BOOKMARK},
        {"bookmarks", ENTYPO_ICON_BOOKMARKS},
        {"bowl", ENTYPO_ICON_BOWL},
        {"box", ENTYPO_ICON_BOX},
        {"briefcase", ENTYPO_ICON_BRIEFCASE},
        {"browser", ENTYPO_ICON_BROWSER},
        {"brush", ENTYPO_ICON_BRUSH},
        {"bucket", ENTYPO_ICON_BUCKET},
        {"bug", ENTYPO_ICON_BUG},
        {"cake", ENTYPO_ICON_CAKE},
        {"calculator", ENTYPO_ICON_CALCULATOR},
        {"calendar", ENTYPO_ICON_CALENDAR},
        {"camera", ENTYPO_ICON_CAMERA},
        {"ccw", ENTYPO_ICON_CCW},
        {"chat", ENTYPO_ICON_CHAT},
        {"check", ENTYPO_ICON_CHECK},
        {"chevron_down", ENTYPO_ICON_CHEVRON_DOWN},
        {"chevron_left", ENTYPO_ICON_CHEVRON_LEFT},
        {"chevron_right", ENTYPO_ICON_CHEVRON_RIGHT},
        {"chevron_small_down", ENTYPO_ICON_CHEVRON_SMALL_DOWN},
        {"chevron_small_left", ENTYPO_ICON_CHEVRON_SMALL_LEFT},
        {"chevron_small_right", ENTYPO_ICON_CHEVRON_SMALL_RIGHT},
        {"chevron_small_up", ENTYPO_ICON_CHEVRON_SMALL_UP},
        {"chevron_thin_down", ENTYPO_ICON_CHEVRON_THIN_DOWN},
        {"chevron_thin_left", ENTYPO_ICON_CHEVRON_THIN_LEFT},
        {"chevron_thin_right", ENTYPO_ICON_CHEVRON_THIN_RIGHT},
        {"chevron_thin_up", ENTYPO_ICON_CHEVRON_THIN_UP},
        {"chevron_up", ENTYPO_ICON_CHEVRON_UP},
        {"chevron_with_circle_down", ENTYPO_ICON_CHEVRON_WITH_CIRCLE_DOWN},
        {"chevron_with_circle_left", ENTYPO_ICON_CHEVRON_WITH_CIRCLE_LEFT},
        {"chevron_with_circle_right", ENTYPO_ICON_CHEVRON_WITH_CIRCLE_RIGHT},
        {"chevron_with_circle_up", ENTYPO_ICON_CHEVRON_WITH_CIRCLE_UP},
        {"circle", ENTYPO_ICON_CIRCLE},
        {"circle_with_cross", ENTYPO_ICON_CIRCLE_WITH_CROSS},
        {"circle_with_minus", ENTYPO_ICON_CIRCLE_WITH_MINUS},
        {"circle_with_plus", ENTYPO_ICON_CIRCLE_WITH_PLUS},
        {"circular_graph", ENTYPO_ICON_CIRCULAR_GRAPH},
        {"clapperboard", ENTYPO_ICON_CLAPPERBOARD},
        {"classic_computer", ENTYPO_ICON_CLASSIC_COMPUTER},
        {"clipboard", ENTYPO_ICON_CLIPBOARD},
        {"clock", ENTYPO_ICON_CLOCK},
        {"cloud", ENTYPO_ICON_CLOUD},
        {"code", ENTYPO_ICON_CODE},
        {"cog", ENTYPO_ICON_COG},
        {"colours", ENTYPO_ICON_COLOURS},
        {"compass", ENTYPO_ICON_COMPASS},
        {"controller_fast_backward", ENTYPO_ICON_CONTROLLER_FAST_BACKWARD},
        {"controller_fast_forward", ENTYPO_ICON_CONTROLLER_FAST_FORWARD},
        {"controller_jump_to_start", ENTYPO_ICON_CONTROLLER_JUMP_TO_START},
        {"controller_next", ENTYPO_ICON_CONTROLLER_NEXT},
        {"controller_paus", ENTYPO_ICON_CONTROLLER_PAUS},
        {"controller_play", ENTYPO_ICON_CONTROLLER_PLAY},
        {"controller_record", ENTYPO_ICON_CONTROLLER_RECORD},
        {"controller_stop", ENTYPO_ICON_CONTROLLER_STOP},
        {"controller_volume", ENTYPO_ICON_CONTROLLER_VOLUME},
        {"copy", ENTYPO_ICON_COPY},
        {"creative_cloud", ENTYPO_ICON_CREATIVE_CLOUD},
        {"creative_commons", ENTYPO_ICON_CREATIVE_COMMONS},
        {"creative_commons_attribution", ENTYPO_ICON_CREATIVE_COMMONS_ATTRIBUTION},
        {"creative_commons_noderivs", ENTYPO_ICON_CREATIVE_COMMONS_NODERIVS},
        {"creative_commons_noncommercial_eu", ENTYPO_ICON_CREATIVE_COMMONS_NONCOMMERCIAL_EU},
        {"creative_commons_noncommercial_us", ENTYPO_ICON_CREATIVE_COMMONS_NONCOMMERCIAL_US},
        {"creative_commons_public_domain", ENTYPO_ICON_CREATIVE_COMMONS_PUBLIC_DOMAIN},
        {"creative_commons_remix", ENTYPO_ICON_CREATIVE_COMMONS_REMIX},
        {"creative_commons_share", ENTYPO_ICON_CREATIVE_COMMONS_SHARE},
        {"creative_commons_sharealike", ENTYPO_ICON_CREATIVE_COMMONS_SHAREALIKE},
        {"credit", ENTYPO_ICON_CREDIT},
        {"credit_card", ENTYPO_ICON_CREDIT_CARD},
        {"crop", ENTYPO_ICON_CROP},
        {"cross", ENTYPO_ICON_CROSS},
        {"cup", ENTYPO_ICON_CUP},
        {"cw", ENTYPO_ICON_CW},
        {"cycle", ENTYPO_ICON_CYCLE},
        {"database", ENTYPO_ICON_DATABASE},
        {"dial_pad", ENTYPO_ICON_DIAL_PAD},
        {"direction", ENTYPO_ICON_DIRECTION},
        {"document", ENTYPO_ICON_DOCUMENT},
        {"document_landscape", ENTYPO_ICON_DOCUMENT_LANDSCAPE},
        {"documents", ENTYPO_ICON_DOCUMENTS},
        {"dot_single", ENTYPO_ICON_DOT_SINGLE},
        {"dots_three_horizontal", ENTYPO_ICON_DOTS_THREE_HORIZONTAL},
        {"dots_three_vertical", ENTYPO_ICON_DOTS_THREE_VERTICAL},
        {"dots_two_horizontal", ENTYPO_ICON_DOTS_TWO_HORIZONTAL},
        {"dots_two_vertical", ENTYPO_ICON_DOTS_TWO_VERTICAL},
        {"download", ENTYPO_ICON_DOWNLOAD},
        {"dribbble", ENTYPO_ICON_DRIBBBLE},
        {"dribbble_with_circle", ENTYPO_ICON_DRIBBBLE_WITH_CIRCLE},
        {"drink", ENTYPO_ICON_DRINK},
        {"drive", ENTYPO_ICON_DRIVE},
        {"drop", ENTYPO_ICON_DROP},
        {"dropbox", ENTYPO_ICON_DROPBOX},
        {"edit", ENTYPO_ICON_EDIT},
        {"email", ENTYPO_ICON_EMAIL},
        {"emoji_flirt", ENTYPO_ICON_EMOJI_FLIRT},
        {"emoji_happy", ENTYPO_ICON_EMOJI_HAPPY},
        {"emoji_neutral", ENTYPO_ICON_EMOJI_NEUTRAL},
        {"emoji_sad", ENTYPO_ICON_EMOJI_SAD},
        {"erase", ENTYPO_ICON_ERASE},
        {"eraser", ENTYPO_ICON_ERASER},
        {"evernote", ENTYPO_ICON_EVERNOTE},
        {"export", ENTYPO_ICON_EXPORT},
        {"eye", ENTYPO_ICON_EYE},
        {"eye_with_line", ENTYPO_ICON_EYE_WITH_LINE},
        {"facebook", ENTYPO_ICON_FACEBOOK},
        {"facebook_with_circle", ENTYPO_ICON_FACEBOOK_WITH_CIRCLE},
        {"feather", ENTYPO_ICON_FEATHER},
        {"fingerprint", ENTYPO_ICON_FINGERPRINT},
        {"flag", ENTYPO_ICON_FLAG},
        {"flash", ENTYPO_ICON_FLASH},
        {"flashlight", ENTYPO_ICON_FLASHLIGHT},
        {"flat_brush", ENTYPO_ICON_FLAT_BRUSH},
        {"flattr", ENTYPO_ICON_FLATTR},
        {"flickr", ENTYPO_ICON_FLICKR},
        {"flickr_with_circle", ENTYPO_ICON_FLICKR_WITH_CIRCLE},
        {"flow_branch", ENTYPO_ICON_FLOW_BRANCH},
        {"flow_cascade", ENTYPO_ICON_FLOW_CASCADE},
        {"flow_line", ENTYPO_ICON_FLOW_LINE},
        {"flow_parallel", ENTYPO_ICON_FLOW_PARALLEL},
        {"flow_tree", ENTYPO_ICON_FLOW_TREE},
        {"flower", ENTYPO_ICON_FLOWER},
        {"folder", ENTYPO_ICON_FOLDER},
        {"folder_images", ENTYPO_ICON_FOLDER_IMAGES},
        {"folder_music", ENTYPO_ICON_FOLDER_MUSIC},
        {"folder_video", ENTYPO_ICON_FOLDER_VIDEO},
        {"forward", ENTYPO_ICON_FORWARD},
        {"foursquare", ENTYPO_ICON_FOURSQUARE},
        {"funnel", ENTYPO_ICON_FUNNEL},
        {"game_controller", ENTYPO_ICON_GAME_CONTROLLER},
        {"gauge", ENTYPO_ICON_GAUGE},
        {"github", ENTYPO_ICON_GITHUB},
        {"github_with_circle", ENTYPO_ICON_GITHUB_WITH_CIRCLE},
        {"globe", ENTYPO_ICON_GLOBE},
        {"google_drive", ENTYPO_ICON_GOOGLE_DRIVE},
        {"google_hangouts", ENTYPO_ICON_GOOGLE_HANGOUTS},
        {"google_play", ENTYPO_ICON_GOOGLE_PLAY},
        {"google_plus", ENTYPO_ICON_GOOGLE_PLUS},
        {"google_plus_with_circle", ENTYPO_ICON_GOOGLE_PLUS_WITH_CIRCLE},
        {"graduation_cap", ENTYPO_ICON_GRADUATION_CAP},
        {"grid", ENTYPO_ICON_GRID},
        {"grooveshark", ENTYPO_ICON_GROOVESHARK},
        {"hair_cross", ENTYPO_ICON_HAIR_CROSS},
        {"hand", ENTYPO_ICON_HAND},
        {"heart", ENTYPO_ICON_HEART},
        {"heart_outlined", ENTYPO_ICON_HEART_OUTLINED},
        {"help", ENTYPO_ICON_HELP},
        {"help_with_circle", ENTYPO_ICON_HELP_WITH_CIRCLE},
        {"home", ENTYPO_ICON_HOME},
        {"hour_glass", ENTYPO_ICON_HOUR_GLASS},
        {"houzz", ENTYPO_ICON_HOUZZ},
        {"icloud", ENTYPO_ICON_ICLOUD},
        {"image", ENTYPO_ICON_IMAGE},
        {"image_inverted", ENTYPO_ICON_IMAGE_INVERTED},
        {"images", ENTYPO_ICON_IMAGES},
        {"inbox", ENTYPO_ICON_INBOX},
        {"infinity", ENTYPO_ICON_INFINITY},
        {"info", ENTYPO_ICON_INFO},
        {"info_with_circle", ENTYPO_ICON_INFO_WITH_CIRCLE},
        {"instagram", ENTYPO_ICON_INSTAGRAM},
        {"instagram_with_circle", ENTYPO_ICON_INSTAGRAM_WITH_CIRCLE},
        {"install", ENTYPO_ICON_INSTALL},
        {"key", ENTYPO_ICON_KEY},
        {"keyboard", ENTYPO_ICON_KEYBOARD},
        {"lab_flask", ENTYPO_ICON_LAB_FLASK},
        {"landline", ENTYPO_ICON_LANDLINE},
        {"language", ENTYPO_ICON_LANGUAGE},
        {"laptop", ENTYPO_ICON_LAPTOP},
        {"lastfm", ENTYPO_ICON_LASTFM},
        {"lastfm_with_circle", ENTYPO_ICON_LASTFM_WITH_CIRCLE},
        {"layers", ENTYPO_ICON_LAYERS},
        {"leaf", ENTYPO_ICON_LEAF},
        {"level_down", ENTYPO_ICON_LEVEL_DOWN},
        {"level_up", ENTYPO_ICON_LEVEL_UP},
        {"lifebuoy", ENTYPO_ICON_LIFEBUOY},
        {"light_bulb", ENTYPO_ICON_LIGHT_BULB},
        {"light_down", ENTYPO_ICON_LIGHT_DOWN},
        {"light_up", ENTYPO_ICON_LIGHT_UP},
        {"line_graph", ENTYPO_ICON_LINE_GRAPH},
        {"link", ENTYPO_ICON_LINK},
        {"linkedin", ENTYPO_ICON_LINKEDIN},
        {"linkedin_with_circle", ENTYPO_ICON_LINKEDIN_WITH_CIRCLE},
        {"list", ENTYPO_ICON_LIST},
        {"location", ENTYPO_ICON_LOCATION},
        {"location_pin", ENTYPO_ICON_LOCATION_PIN},
        {"lock", ENTYPO_ICON_LOCK},
        {"lock_open", ENTYPO_ICON_LOCK_OPEN},
        {"log_out", ENTYPO_ICON_LOG_OUT},
        {"login", ENTYPO_ICON_LOGIN},
        {"loop", ENTYPO_ICON_LOOP},
        {"magnet", ENTYPO_ICON_MAGNET},
        {"magnifying_glass", ENTYPO_ICON_MAGNIFYING_GLASS},
        {"mail", ENTYPO_ICON_MAIL},
        {"mail_with_circle", ENTYPO_ICON_MAIL_WITH_CIRCLE},
        {"man", ENTYPO_ICON_MAN},
        {"map", ENTYPO_ICON_MAP},
        {"mask", ENTYPO_ICON_MASK},
        {"medal", ENTYPO_ICON_MEDAL},
        {"medium", ENTYPO_ICON_MEDIUM},
        {"medium_with_circle", ENTYPO_ICON_MEDIUM_WITH_CIRCLE},
        {"megaphone", ENTYPO_ICON_MEGAPHONE},
        {"menu", ENTYPO_ICON_MENU},
        {"merge", ENTYPO_ICON_MERGE},
        {"message", ENTYPO_ICON_MESSAGE},
        {"mic", ENTYPO_ICON_MIC},
        {"minus", ENTYPO_ICON_MINUS},
        {"mixi", ENTYPO_ICON_MIXI},
        {"mobile", ENTYPO_ICON_MOBILE},
        {"modern_mic", ENTYPO_ICON_MODERN_MIC},
        {"moon", ENTYPO_ICON_MOON},
        {"mouse", ENTYPO_ICON_MOUSE},
        {"mouse_pointer", ENTYPO_ICON_MOUSE_POINTER},
        {"music", ENTYPO_ICON_MUSIC},
        {"network", ENTYPO_ICON_NETWORK},
        {"new", ENTYPO_ICON_NEW},
        {"new_message", ENTYPO_ICON_NEW_MESSAGE},
        {"news", ENTYPO_ICON_NEWS},
        {"newsletter", ENTYPO_ICON_NEWSLETTER},
        {"note", ENTYPO_ICON_NOTE},
        {"notification", ENTYPO_ICON_NOTIFICATION},
        {"notifications_off", ENTYPO_ICON_NOTIFICATIONS_OFF},
        {"old_mobile", ENTYPO_ICON_OLD_MOBILE},
        {"old_phone", ENTYPO_ICON_OLD_PHONE},
        {"onedrive", ENTYPO_ICON_ONEDRIVE},
        {"open_book", ENTYPO_ICON_OPEN_BOOK},
        {"palette", ENTYPO_ICON_PALETTE},
        {"paper_plane", ENTYPO_ICON_PAPER_PLANE},
        {"paypal", ENTYPO_ICON_PAYPAL},
        {"pencil", ENTYPO_ICON_PENCIL},
        {"phone", ENTYPO_ICON_PHONE},
        {"picasa", ENTYPO_ICON_PICASA},
        {"pie_chart", ENTYPO_ICON_PIE_CHART},
        {"pin", ENTYPO_ICON_PIN},
        {"pinterest", ENTYPO_ICON_PINTEREST},
        {"pinterest_with_circle", ENTYPO_ICON_PINTEREST_WITH_CIRCLE},
        {"plus", ENTYPO_ICON_PLUS},
        {"popup", ENTYPO_ICON_POPUP},
        {"power_plug", ENTYPO_ICON_POWER_PLUG},
        {"price_ribbon", ENTYPO_ICON_PRICE_RIBBON},
        {"price_tag", ENTYPO_ICON_PRICE_TAG},
        {"print", ENTYPO_ICON_PRINT},
        {"progress_empty", ENTYPO_ICON_PROGRESS_EMPTY},
        {"progress_full", ENTYPO_ICON_PROGRESS_FULL},
        {"progress_one", ENTYPO_ICON_PROGRESS_ONE},
        {"progress_two", ENTYPO_ICON_PROGRESS_TWO},
        {"publish", ENTYPO_ICON_PUBLISH},
        {"qq", ENTYPO_ICON_QQ},
        {"qq_with_circle", ENTYPO_ICON_QQ_WITH_CIRCLE},
        {"quote", ENTYPO_ICON_QUOTE},
        {"radio", ENTYPO_ICON_RADIO},
        {"raft", ENTYPO_ICON_RAFT},
        {"raft_with_circle", ENTYPO_ICON_RAFT_WITH_CIRCLE},
        {"rainbow", ENTYPO_ICON_RAINBOW},
        {"rdio", ENTYPO_ICON_RDIO},
        {"rdio_with_circle", ENTYPO_ICON_RDIO_WITH_CIRCLE},
        {"remove_user", ENTYPO_ICON_REMOVE_USER},
        {"renren", ENTYPO_ICON_RENREN},
        {"reply", ENTYPO_ICON_REPLY},
        {"reply_all", ENTYPO_ICON_REPLY_ALL},
        {"resize_100_percent", ENTYPO_ICON_RESIZE_100_PERCENT},
        {"resize_full_screen", ENTYPO_ICON_RESIZE_FULL_SCREEN},
        {"retweet", ENTYPO_ICON_RETWEET},
        {"rocket", ENTYPO_ICON_ROCKET},
        {"round_brush", ENTYPO_ICON_ROUND_BRUSH},
        {"rss", ENTYPO_ICON_RSS},
        {"ruler", ENTYPO_ICON_RULER},
        {"save", ENTYPO_ICON_SAVE},
        {"scissors", ENTYPO_ICON_SCISSORS},
        {"scribd", ENTYPO_ICON_SCRIBD},
        {"select_arrows", ENTYPO_ICON_SELECT_ARROWS},
        {"share", ENTYPO_ICON_SHARE},
        {"share_alternative", ENTYPO_ICON_SHARE_ALTERNATIVE},
        {"shareable", ENTYPO_ICON_SHAREABLE},
        {"shield", ENTYPO_ICON_SHIELD},
        {"shop", ENTYPO_ICON_SHOP},
        {"shopping_bag", ENTYPO_ICON_SHOPPING_BAG},
        {"shopping_basket", ENTYPO_ICON_SHOPPING_BASKET},
        {"shopping_cart", ENTYPO_ICON_SHOPPING_CART},
        {"shuffle", ENTYPO_ICON_SHUFFLE},
        {"signal", ENTYPO_ICON_SIGNAL},
        {"sina_weibo", ENTYPO_ICON_SINA_WEIBO},
        {"skype", ENTYPO_ICON_SKYPE},
        {"skype_with_circle", ENTYPO_ICON_SKYPE_WITH_CIRCLE},
        {"slideshare", ENTYPO_ICON_SLIDESHARE},
        {"smashing", ENTYPO_ICON_SMASHING},
        {"sound", ENTYPO_ICON_SOUND},
        {"sound_mix", ENTYPO_ICON_SOUND_MIX},
        {"sound_mute", ENTYPO_ICON_SOUND_MUTE},
        {"soundcloud", ENTYPO_ICON_SOUNDCLOUD},
        {"sports_club", ENTYPO_ICON_SPORTS_CLUB},
        {"spotify", ENTYPO_ICON_SPOTIFY},
        {"spotify_with_circle", ENTYPO_ICON_SPOTIFY_WITH_CIRCLE},
        {"spreadsheet", ENTYPO_ICON_SPREADSHEET},
        {"squared_cross", ENTYPO_ICON_SQUARED_CROSS},
        {"squared_minus", ENTYPO_ICON_SQUARED_MINUS},
        {"squared_plus", ENTYPO_ICON_SQUARED_PLUS},
        {"star", ENTYPO_ICON_STAR},
        {"star_outlined", ENTYPO_ICON_STAR_OUTLINED},
        {"stopwatch", ENTYPO_ICON_STOPWATCH},
        {"stumbleupon", ENTYPO_ICON_STUMBLEUPON},
        {"stumbleupon_with_circle", ENTYPO_ICON_STUMBLEUPON_WITH_CIRCLE},
        {"suitcase", ENTYPO_ICON_SUITCASE},
        {"swap", ENTYPO_ICON_SWAP},
        {"swarm", ENTYPO_ICON_SWARM},
        {"sweden", ENTYPO_ICON_SWEDEN},
        {"switch", ENTYPO_ICON_SWITCH},
        {"ablet", ENTYPO_ICON_TABLET},
        {"tablet_mobile_combo", ENTYPO_ICON_TABLET_MOBILE_COMBO},
        {"tag", ENTYPO_ICON_TAG},
        {"text", ENTYPO_ICON_TEXT},
        {"text_document", ENTYPO_ICON_TEXT_DOCUMENT},
        {"text_document_inverted", ENTYPO_ICON_TEXT_DOCUMENT_INVERTED},
        {"thermometer", ENTYPO_ICON_THERMOMETER},
        {"thumbs_down", ENTYPO_ICON_THUMBS_DOWN},
        {"thumbs_up", ENTYPO_ICON_THUMBS_UP},
        {"thunder_cloud", ENTYPO_ICON_THUNDER_CLOUD},
        {"ticket", ENTYPO_ICON_TICKET},
        {"time_slot", ENTYPO_ICON_TIME_SLOT},
        {"tools", ENTYPO_ICON_TOOLS},
        {"traffic_cone", ENTYPO_ICON_TRAFFIC_CONE},
        {"trash", ENTYPO_ICON_TRASH},
        {"tree", ENTYPO_ICON_TREE},
        {"triangle_down", ENTYPO_ICON_TRIANGLE_DOWN},
        {"triangle_left", ENTYPO_ICON_TRIANGLE_LEFT},
        {"triangle_right", ENTYPO_ICON_TRIANGLE_RIGHT},
        {"triangle_up", ENTYPO_ICON_TRIANGLE_UP},
        {"tripadvisor", ENTYPO_ICON_TRIPADVISOR},
        {"trophy", ENTYPO_ICON_TROPHY},
        {"tumblr", ENTYPO_ICON_TUMBLR},
        {"tumblr_with_circle", ENTYPO_ICON_TUMBLR_WITH_CIRCLE},
        {"tv", ENTYPO_ICON_TV},
        {"twitter", ENTYPO_ICON_TWITTER},
        {"twitter_with_circle", ENTYPO_ICON_TWITTER_WITH_CIRCLE},
        {"typing", ENTYPO_ICON_TYPING},
        {"uninstall", ENTYPO_ICON_UNINSTALL},
        {"unread", ENTYPO_ICON_UNREAD},
        {"untag", ENTYPO_ICON_UNTAG},
        {"upload", ENTYPO_ICON_UPLOAD},
        {"upload_to_cloud", ENTYPO_ICON_UPLOAD_TO_CLOUD},
        {"user", ENTYPO_ICON_USER},
        {"users", ENTYPO_ICON_USERS},
        {"v_card", ENTYPO_ICON_V_CARD},
        {"video", ENTYPO_ICON_VIDEO},
        {"video_camera", ENTYPO_ICON_VIDEO_CAMERA},
        {"vimeo", ENTYPO_ICON_VIMEO},
        {"vimeo_with_circle", ENTYPO_ICON_VIMEO_WITH_CIRCLE},
        {"vine", ENTYPO_ICON_VINE},
        {"vine_with_circle", ENTYPO_ICON_VINE_WITH_CIRCLE},
        {"vinyl", ENTYPO_ICON_VINYL},
        {"vk", ENTYPO_ICON_VK},
        {"vk_alternitive", ENTYPO_ICON_VK_ALTERNITIVE},
        {"vk_with_circle", ENTYPO_ICON_VK_WITH_CIRCLE},
        {"voicemail", ENTYPO_ICON_VOICEMAIL},
        {"wallet", ENTYPO_ICON_WALLET},
        {"warning", ENTYPO_ICON_WARNING},
        {"water", ENTYPO_ICON_WATER},
        {"windows_store", ENTYPO_ICON_WINDOWS_STORE},
        {"xing", ENTYPO_ICON_XING},
        {"xing_with_circle", ENTYPO_ICON_XING_WITH_CIRCLE},
        {"yelp", ENTYPO_ICON_YELP},
        {"youko", ENTYPO_ICON_YOUKO},
        {"youko_with_circle", ENTYPO_ICON_YOUKO_WITH_CIRCLE},
        {"youtube", ENTYPO_ICON_YOUTUBE},
        {"youtube_with_circle", ENTYPO_ICON_YOUTUBE_WITH_CIRCLE}
    }
);

int getIcon(std::string name) {
    std::map<std::string, int>::const_iterator result = iconMap.find(name);
    if (result == iconMap.end()) {
        std::cout << "WARNING: unknown icon requested \"" << name << "\". Replacing it with no icon.";
        return 0;
    } else {
        return result->second;
    }
}

std::string getIconName(int icon) {
    switch (icon) {
        case ENTYPO_ICON_500PX: return "500px";
        case ENTYPO_ICON_500PX_WITH_CIRCLE: return "500px_with_circle";
        case ENTYPO_ICON_ADD_TO_LIST: return "add_to_list";
        case ENTYPO_ICON_ADD_USER: return "add_user";
        case ENTYPO_ICON_ADDRESS: return "address";
        case ENTYPO_ICON_ADJUST: return "adjust";
        case ENTYPO_ICON_AIR: return "air";
        case ENTYPO_ICON_AIRCRAFT: return "aircraft";
        case ENTYPO_ICON_AIRCRAFT_LANDING: return "aircraft_landing";
        case ENTYPO_ICON_AIRCRAFT_TAKE_OFF: return "aircraft_take_off";
        case ENTYPO_ICON_ALIGN_BOTTOM: return "align_bottom";
        case ENTYPO_ICON_ALIGN_HORIZONTAL_MIDDLE: return "align_horizontal_middle";
        case ENTYPO_ICON_ALIGN_LEFT: return "align_left";
        case ENTYPO_ICON_ALIGN_RIGHT: return "align_right";
        case ENTYPO_ICON_ALIGN_TOP: return "align_top";
        case ENTYPO_ICON_ALIGN_VERTICAL_MIDDLE: return "align_vertical_middle";
        case ENTYPO_ICON_APP_STORE: return "app_store";
        case ENTYPO_ICON_ARCHIVE: return "archive";
        case ENTYPO_ICON_ARROW_BOLD_DOWN: return "arrow_bold_down";
        case ENTYPO_ICON_ARROW_BOLD_LEFT: return "arrow_bold_left";
        case ENTYPO_ICON_ARROW_BOLD_RIGHT: return "arrow_bold_right";
        case ENTYPO_ICON_ARROW_BOLD_UP: return "arrow_bold_up";
        case ENTYPO_ICON_ARROW_DOWN: return "arrow_down";
        case ENTYPO_ICON_ARROW_LEFT: return "arrow_left";
        case ENTYPO_ICON_ARROW_LONG_DOWN: return "arrow_long_down";
        case ENTYPO_ICON_ARROW_LONG_LEFT: return "arrow_long_left";
        case ENTYPO_ICON_ARROW_LONG_RIGHT: return "arrow_long_right";
        case ENTYPO_ICON_ARROW_LONG_UP: return "arrow_long_up";
        case ENTYPO_ICON_ARROW_RIGHT: return "arrow_right";
        case ENTYPO_ICON_ARROW_UP: return "arrow_up";
        case ENTYPO_ICON_ARROW_WITH_CIRCLE_DOWN: return "arrow_with_circle_down";
        case ENTYPO_ICON_ARROW_WITH_CIRCLE_LEFT: return "arrow_with_circle_left";
        case ENTYPO_ICON_ARROW_WITH_CIRCLE_RIGHT: return "arrow_with_circle_right";
        case ENTYPO_ICON_ARROW_WITH_CIRCLE_UP: return "arrow_with_circle_up";
        case ENTYPO_ICON_ATTACHMENT: return "attachment";
        case ENTYPO_ICON_AWARENESS_RIBBON: return "awareness_ribbon";
        case ENTYPO_ICON_BACK: return "back";
        case ENTYPO_ICON_BACK_IN_TIME: return "back_in_time";
        case ENTYPO_ICON_BAIDU: return "baidu";
        case ENTYPO_ICON_BAR_GRAPH: return "bar_graph";
        case ENTYPO_ICON_BASECAMP: return "basecamp";
        case ENTYPO_ICON_BATTERY: return "battery";
        case ENTYPO_ICON_BEHANCE: return "behance";
        case ENTYPO_ICON_BELL: return "bell";
        case ENTYPO_ICON_BLACKBOARD: return "blackboard";
        case ENTYPO_ICON_BLOCK: return "block";
        case ENTYPO_ICON_BOOK: return "book";
        case ENTYPO_ICON_BOOKMARK: return "bookmark";
        case ENTYPO_ICON_BOOKMARKS: return "bookmarks";
        case ENTYPO_ICON_BOWL: return "bowl";
        case ENTYPO_ICON_BOX: return "box";
        case ENTYPO_ICON_BRIEFCASE: return "briefcase";
        case ENTYPO_ICON_BROWSER: return "browser";
        case ENTYPO_ICON_BRUSH: return "brush";
        case ENTYPO_ICON_BUCKET: return "bucket";
        case ENTYPO_ICON_BUG: return "bug";
        case ENTYPO_ICON_CAKE: return "cake";
        case ENTYPO_ICON_CALCULATOR: return "calculator";
        case ENTYPO_ICON_CALENDAR: return "calendar";
        case ENTYPO_ICON_CAMERA: return "camera";
        case ENTYPO_ICON_CCW: return "ccw";
        case ENTYPO_ICON_CHAT: return "chat";
        case ENTYPO_ICON_CHECK: return "check";
        case ENTYPO_ICON_CHEVRON_DOWN: return "chevron_down";
        case ENTYPO_ICON_CHEVRON_LEFT: return "chevron_left";
        case ENTYPO_ICON_CHEVRON_RIGHT: return "chevron_right";
        case ENTYPO_ICON_CHEVRON_SMALL_DOWN: return "chevron_small_down";
        case ENTYPO_ICON_CHEVRON_SMALL_LEFT: return "chevron_small_left";
        case ENTYPO_ICON_CHEVRON_SMALL_RIGHT: return "chevron_small_right";
        case ENTYPO_ICON_CHEVRON_SMALL_UP: return "chevron_small_up";
        case ENTYPO_ICON_CHEVRON_THIN_DOWN: return "chevron_thin_down";
        case ENTYPO_ICON_CHEVRON_THIN_LEFT: return "chevron_thin_left";
        case ENTYPO_ICON_CHEVRON_THIN_RIGHT: return "chevron_thin_right";
        case ENTYPO_ICON_CHEVRON_THIN_UP: return "chevron_thin_up";
        case ENTYPO_ICON_CHEVRON_UP: return "chevron_up";
        case ENTYPO_ICON_CHEVRON_WITH_CIRCLE_DOWN: return "chevron_with_circle_down";
        case ENTYPO_ICON_CHEVRON_WITH_CIRCLE_LEFT: return "chevron_with_circle_left";
        case ENTYPO_ICON_CHEVRON_WITH_CIRCLE_RIGHT: return "chevron_with_circle_right";
        case ENTYPO_ICON_CHEVRON_WITH_CIRCLE_UP: return "chevron_with_circle_up";
        case ENTYPO_ICON_CIRCLE: return "circle";
        case ENTYPO_ICON_CIRCLE_WITH_CROSS: return "circle_with_cross";
        case ENTYPO_ICON_CIRCLE_WITH_MINUS: return "circle_with_minus";
        case ENTYPO_ICON_CIRCLE_WITH_PLUS: return "circle_with_plus";
        case ENTYPO_ICON_CIRCULAR_GRAPH: return "circular_graph";
        case ENTYPO_ICON_CLAPPERBOARD: return "clapperboard";
        case ENTYPO_ICON_CLASSIC_COMPUTER: return "classic_computer";
        case ENTYPO_ICON_CLIPBOARD: return "clipboard";
        case ENTYPO_ICON_CLOCK: return "clock";
        case ENTYPO_ICON_CLOUD: return "cloud";
        case ENTYPO_ICON_CODE: return "code";
        case ENTYPO_ICON_COG: return "cog";
        case ENTYPO_ICON_COLOURS: return "colours";
        case ENTYPO_ICON_COMPASS: return "compass";
        case ENTYPO_ICON_CONTROLLER_FAST_BACKWARD: return "controller_fast_backward";
        case ENTYPO_ICON_CONTROLLER_FAST_FORWARD: return "controller_fast_forward";
        case ENTYPO_ICON_CONTROLLER_JUMP_TO_START: return "controller_jump_to_start";
        case ENTYPO_ICON_CONTROLLER_NEXT: return "controller_next";
        case ENTYPO_ICON_CONTROLLER_PAUS: return "controller_paus";
        case ENTYPO_ICON_CONTROLLER_PLAY: return "controller_play";
        case ENTYPO_ICON_CONTROLLER_RECORD: return "controller_record";
        case ENTYPO_ICON_CONTROLLER_STOP: return "controller_stop";
        case ENTYPO_ICON_CONTROLLER_VOLUME: return "controller_volume";
        case ENTYPO_ICON_COPY: return "copy";
        case ENTYPO_ICON_CREATIVE_CLOUD: return "creative_cloud";
        case ENTYPO_ICON_CREATIVE_COMMONS: return "creative_commons";
        case ENTYPO_ICON_CREATIVE_COMMONS_ATTRIBUTION: return "creative_commons_attribution";
        case ENTYPO_ICON_CREATIVE_COMMONS_NODERIVS: return "creative_commons_noderivs";
        case ENTYPO_ICON_CREATIVE_COMMONS_NONCOMMERCIAL_EU: return "creative_commons_noncommercial_eu";
        case ENTYPO_ICON_CREATIVE_COMMONS_NONCOMMERCIAL_US: return "creative_commons_noncommercial_us";
        case ENTYPO_ICON_CREATIVE_COMMONS_PUBLIC_DOMAIN: return "creative_commons_public_domain";
        case ENTYPO_ICON_CREATIVE_COMMONS_REMIX: return "creative_commons_remix";
        case ENTYPO_ICON_CREATIVE_COMMONS_SHARE: return "creative_commons_share";
        case ENTYPO_ICON_CREATIVE_COMMONS_SHAREALIKE: return "creative_commons_sharealike";
        case ENTYPO_ICON_CREDIT: return "credit";
        case ENTYPO_ICON_CREDIT_CARD: return "credit_card";
        case ENTYPO_ICON_CROP: return "crop";
        case ENTYPO_ICON_CROSS: return "cross";
        case ENTYPO_ICON_CUP: return "cup";
        case ENTYPO_ICON_CW: return "cw";
        case ENTYPO_ICON_CYCLE: return "cycle";
        case ENTYPO_ICON_DATABASE: return "database";
        case ENTYPO_ICON_DIAL_PAD: return "dial_pad";
        case ENTYPO_ICON_DIRECTION: return "direction";
        case ENTYPO_ICON_DOCUMENT: return "document";
        case ENTYPO_ICON_DOCUMENT_LANDSCAPE: return "document_landscape";
        case ENTYPO_ICON_DOCUMENTS: return "documents";
        case ENTYPO_ICON_DOT_SINGLE: return "dot_single";
        case ENTYPO_ICON_DOTS_THREE_HORIZONTAL: return "dots_three_horizontal";
        case ENTYPO_ICON_DOTS_THREE_VERTICAL: return "dots_three_vertical";
        case ENTYPO_ICON_DOTS_TWO_HORIZONTAL: return "dots_two_horizontal";
        case ENTYPO_ICON_DOTS_TWO_VERTICAL: return "dots_two_vertical";
        case ENTYPO_ICON_DOWNLOAD: return "download";
        case ENTYPO_ICON_DRIBBBLE: return "dribbble";
        case ENTYPO_ICON_DRIBBBLE_WITH_CIRCLE: return "dribbble_with_circle";
        case ENTYPO_ICON_DRINK: return "drink";
        case ENTYPO_ICON_DRIVE: return "drive";
        case ENTYPO_ICON_DROP: return "drop";
        case ENTYPO_ICON_DROPBOX: return "dropbox";
        case ENTYPO_ICON_EDIT: return "edit";
        case ENTYPO_ICON_EMAIL: return "email";
        case ENTYPO_ICON_EMOJI_FLIRT: return "emoji_flirt";
        case ENTYPO_ICON_EMOJI_HAPPY: return "emoji_happy";
        case ENTYPO_ICON_EMOJI_NEUTRAL: return "emoji_neutral";
        case ENTYPO_ICON_EMOJI_SAD: return "emoji_sad";
        case ENTYPO_ICON_ERASE: return "erase";
        case ENTYPO_ICON_ERASER: return "eraser";
        case ENTYPO_ICON_EVERNOTE: return "evernote";
        case ENTYPO_ICON_EXPORT: return "export";
        case ENTYPO_ICON_EYE: return "eye";
        case ENTYPO_ICON_EYE_WITH_LINE: return "eye_with_line";
        case ENTYPO_ICON_FACEBOOK: return "facebook";
        case ENTYPO_ICON_FACEBOOK_WITH_CIRCLE: return "facebook_with_circle";
        case ENTYPO_ICON_FEATHER: return "feather";
        case ENTYPO_ICON_FINGERPRINT: return "fingerprint";
        case ENTYPO_ICON_FLAG: return "flag";
        case ENTYPO_ICON_FLASH: return "flash";
        case ENTYPO_ICON_FLASHLIGHT: return "flashlight";
        case ENTYPO_ICON_FLAT_BRUSH: return "flat_brush";
        case ENTYPO_ICON_FLATTR: return "flattr";
        case ENTYPO_ICON_FLICKR: return "flickr";
        case ENTYPO_ICON_FLICKR_WITH_CIRCLE: return "flickr_with_circle";
        case ENTYPO_ICON_FLOW_BRANCH: return "flow_branch";
        case ENTYPO_ICON_FLOW_CASCADE: return "flow_cascade";
        case ENTYPO_ICON_FLOW_LINE: return "flow_line";
        case ENTYPO_ICON_FLOW_PARALLEL: return "flow_parallel";
        case ENTYPO_ICON_FLOW_TREE: return "flow_tree";
        case ENTYPO_ICON_FLOWER: return "flower";
        case ENTYPO_ICON_FOLDER: return "folder";
        case ENTYPO_ICON_FOLDER_IMAGES: return "folder_images";
        case ENTYPO_ICON_FOLDER_MUSIC: return "folder_music";
        case ENTYPO_ICON_FOLDER_VIDEO: return "folder_video";
        case ENTYPO_ICON_FORWARD: return "forward";
        case ENTYPO_ICON_FOURSQUARE: return "foursquare";
        case ENTYPO_ICON_FUNNEL: return "funnel";
        case ENTYPO_ICON_GAME_CONTROLLER: return "game_controller";
        case ENTYPO_ICON_GAUGE: return "gauge";
        case ENTYPO_ICON_GITHUB: return "github";
        case ENTYPO_ICON_GITHUB_WITH_CIRCLE: return "github_with_circle";
        case ENTYPO_ICON_GLOBE: return "globe";
        case ENTYPO_ICON_GOOGLE_DRIVE: return "google_drive";
        case ENTYPO_ICON_GOOGLE_HANGOUTS: return "google_hangouts";
        case ENTYPO_ICON_GOOGLE_PLAY: return "google_play";
        case ENTYPO_ICON_GOOGLE_PLUS: return "google_plus";
        case ENTYPO_ICON_GOOGLE_PLUS_WITH_CIRCLE: return "google_plus_with_circle";
        case ENTYPO_ICON_GRADUATION_CAP: return "graduation_cap";
        case ENTYPO_ICON_GRID: return "grid";
        case ENTYPO_ICON_GROOVESHARK: return "grooveshark";
        case ENTYPO_ICON_HAIR_CROSS: return "hair_cross";
        case ENTYPO_ICON_HAND: return "hand";
        case ENTYPO_ICON_HEART: return "heart";
        case ENTYPO_ICON_HEART_OUTLINED: return "heart_outlined";
        case ENTYPO_ICON_HELP: return "help";
        case ENTYPO_ICON_HELP_WITH_CIRCLE: return "help_with_circle";
        case ENTYPO_ICON_HOME: return "home";
        case ENTYPO_ICON_HOUR_GLASS: return "hour_glass";
        case ENTYPO_ICON_HOUZZ: return "houzz";
        case ENTYPO_ICON_ICLOUD: return "icloud";
        case ENTYPO_ICON_IMAGE: return "image";
        case ENTYPO_ICON_IMAGE_INVERTED: return "image_inverted";
        case ENTYPO_ICON_IMAGES: return "images";
        case ENTYPO_ICON_INBOX: return "inbox";
        case ENTYPO_ICON_INFINITY: return "infinity";
        case ENTYPO_ICON_INFO: return "info";
        case ENTYPO_ICON_INFO_WITH_CIRCLE: return "info_with_circle";
        case ENTYPO_ICON_INSTAGRAM: return "instagram";
        case ENTYPO_ICON_INSTAGRAM_WITH_CIRCLE: return "instagram_with_circle";
        case ENTYPO_ICON_INSTALL: return "install";
        case ENTYPO_ICON_KEY: return "key";
        case ENTYPO_ICON_KEYBOARD: return "keyboard";
        case ENTYPO_ICON_LAB_FLASK: return "lab_flask";
        case ENTYPO_ICON_LANDLINE: return "landline";
        case ENTYPO_ICON_LANGUAGE: return "language";
        case ENTYPO_ICON_LAPTOP: return "laptop";
        case ENTYPO_ICON_LASTFM: return "lastfm";
        case ENTYPO_ICON_LASTFM_WITH_CIRCLE: return "lastfm_with_circle";
        case ENTYPO_ICON_LAYERS: return "layers";
        case ENTYPO_ICON_LEAF: return "leaf";
        case ENTYPO_ICON_LEVEL_DOWN: return "level_down";
        case ENTYPO_ICON_LEVEL_UP: return "level_up";
        case ENTYPO_ICON_LIFEBUOY: return "lifebuoy";
        case ENTYPO_ICON_LIGHT_BULB: return "light_bulb";
        case ENTYPO_ICON_LIGHT_DOWN: return "light_down";
        case ENTYPO_ICON_LIGHT_UP: return "light_up";
        case ENTYPO_ICON_LINE_GRAPH: return "line_graph";
        case ENTYPO_ICON_LINK: return "link";
        case ENTYPO_ICON_LINKEDIN: return "linkedin";
        case ENTYPO_ICON_LINKEDIN_WITH_CIRCLE: return "linkedin_with_circle";
        case ENTYPO_ICON_LIST: return "list";
        case ENTYPO_ICON_LOCATION: return "location";
        case ENTYPO_ICON_LOCATION_PIN: return "location_pin";
        case ENTYPO_ICON_LOCK: return "lock";
        case ENTYPO_ICON_LOCK_OPEN: return "lock_open";
        case ENTYPO_ICON_LOG_OUT: return "log_out";
        case ENTYPO_ICON_LOGIN: return "login";
        case ENTYPO_ICON_LOOP: return "loop";
        case ENTYPO_ICON_MAGNET: return "magnet";
        case ENTYPO_ICON_MAGNIFYING_GLASS: return "magnifying_glass";
        case ENTYPO_ICON_MAIL: return "mail";
        case ENTYPO_ICON_MAIL_WITH_CIRCLE: return "mail_with_circle";
        case ENTYPO_ICON_MAN: return "man";
        case ENTYPO_ICON_MAP: return "map";
        case ENTYPO_ICON_MASK: return "mask";
        case ENTYPO_ICON_MEDAL: return "medal";
        case ENTYPO_ICON_MEDIUM: return "medium";
        case ENTYPO_ICON_MEDIUM_WITH_CIRCLE: return "medium_with_circle";
        case ENTYPO_ICON_MEGAPHONE: return "megaphone";
        case ENTYPO_ICON_MENU: return "menu";
        case ENTYPO_ICON_MERGE: return "merge";
        case ENTYPO_ICON_MESSAGE: return "message";
        case ENTYPO_ICON_MIC: return "mic";
        case ENTYPO_ICON_MINUS: return "minus";
        case ENTYPO_ICON_MIXI: return "mixi";
        case ENTYPO_ICON_MOBILE: return "mobile";
        case ENTYPO_ICON_MODERN_MIC: return "modern_mic";
        case ENTYPO_ICON_MOON: return "moon";
        case ENTYPO_ICON_MOUSE: return "mouse";
        case ENTYPO_ICON_MOUSE_POINTER: return "mouse_pointer";
        case ENTYPO_ICON_MUSIC: return "music";
        case ENTYPO_ICON_NETWORK: return "network";
        case ENTYPO_ICON_NEW: return "new";
        case ENTYPO_ICON_NEW_MESSAGE: return "new_message";
        case ENTYPO_ICON_NEWS: return "news";
        case ENTYPO_ICON_NEWSLETTER: return "newsletter";
        case ENTYPO_ICON_NOTE: return "note";
        case ENTYPO_ICON_NOTIFICATION: return "notification";
        case ENTYPO_ICON_NOTIFICATIONS_OFF: return "notifications_off";
        case ENTYPO_ICON_OLD_MOBILE: return "old_mobile";
        case ENTYPO_ICON_OLD_PHONE: return "old_phone";
        case ENTYPO_ICON_ONEDRIVE: return "onedrive";
        case ENTYPO_ICON_OPEN_BOOK: return "open_book";
        case ENTYPO_ICON_PALETTE: return "palette";
        case ENTYPO_ICON_PAPER_PLANE: return "paper_plane";
        case ENTYPO_ICON_PAYPAL: return "paypal";
        case ENTYPO_ICON_PENCIL: return "pencil";
        case ENTYPO_ICON_PHONE: return "phone";
        case ENTYPO_ICON_PICASA: return "picasa";
        case ENTYPO_ICON_PIE_CHART: return "pie_chart";
        case ENTYPO_ICON_PIN: return "pin";
        case ENTYPO_ICON_PINTEREST: return "pinterest";
        case ENTYPO_ICON_PINTEREST_WITH_CIRCLE: return "pinterest_with_circle";
        case ENTYPO_ICON_PLUS: return "plus";
        case ENTYPO_ICON_POPUP: return "popup";
        case ENTYPO_ICON_POWER_PLUG: return "power_plug";
        case ENTYPO_ICON_PRICE_RIBBON: return "price_ribbon";
        case ENTYPO_ICON_PRICE_TAG: return "price_tag";
        case ENTYPO_ICON_PRINT: return "print";
        case ENTYPO_ICON_PROGRESS_EMPTY: return "progress_empty";
        case ENTYPO_ICON_PROGRESS_FULL: return "progress_full";
        case ENTYPO_ICON_PROGRESS_ONE: return "progress_one";
        case ENTYPO_ICON_PROGRESS_TWO: return "progress_two";
        case ENTYPO_ICON_PUBLISH: return "publish";
        case ENTYPO_ICON_QQ: return "qq";
        case ENTYPO_ICON_QQ_WITH_CIRCLE: return "qq_with_circle";
        case ENTYPO_ICON_QUOTE: return "quote";
        case ENTYPO_ICON_RADIO: return "radio";
        case ENTYPO_ICON_RAFT: return "raft";
        case ENTYPO_ICON_RAFT_WITH_CIRCLE: return "raft_with_circle";
        case ENTYPO_ICON_RAINBOW: return "rainbow";
        case ENTYPO_ICON_RDIO: return "rdio";
        case ENTYPO_ICON_RDIO_WITH_CIRCLE: return "rdio_with_circle";
        case ENTYPO_ICON_REMOVE_USER: return "remove_user";
        case ENTYPO_ICON_RENREN: return "renren";
        case ENTYPO_ICON_REPLY: return "reply";
        case ENTYPO_ICON_REPLY_ALL: return "reply_all";
        case ENTYPO_ICON_RESIZE_100_PERCENT: return "resize_100_percent";
        case ENTYPO_ICON_RESIZE_FULL_SCREEN: return "resize_full_screen";
        case ENTYPO_ICON_RETWEET: return "retweet";
        case ENTYPO_ICON_ROCKET: return "rocket";
        case ENTYPO_ICON_ROUND_BRUSH: return "round_brush";
        case ENTYPO_ICON_RSS: return "rss";
        case ENTYPO_ICON_RULER: return "ruler";
        case ENTYPO_ICON_SAVE: return "save";
        case ENTYPO_ICON_SCISSORS: return "scissors";
        case ENTYPO_ICON_SCRIBD: return "scribd";
        case ENTYPO_ICON_SELECT_ARROWS: return "select_arrows";
        case ENTYPO_ICON_SHARE: return "share";
        case ENTYPO_ICON_SHARE_ALTERNATIVE: return "share_alternative";
        case ENTYPO_ICON_SHAREABLE: return "shareable";
        case ENTYPO_ICON_SHIELD: return "shield";
        case ENTYPO_ICON_SHOP: return "shop";
        case ENTYPO_ICON_SHOPPING_BAG: return "shopping_bag";
        case ENTYPO_ICON_SHOPPING_BASKET: return "shopping_basket";
        case ENTYPO_ICON_SHOPPING_CART: return "shopping_cart";
        case ENTYPO_ICON_SHUFFLE: return "shuffle";
        case ENTYPO_ICON_SIGNAL: return "signal";
        case ENTYPO_ICON_SINA_WEIBO: return "sina_weibo";
        case ENTYPO_ICON_SKYPE: return "skype";
        case ENTYPO_ICON_SKYPE_WITH_CIRCLE: return "skype_with_circle";
        case ENTYPO_ICON_SLIDESHARE: return "slideshare";
        case ENTYPO_ICON_SMASHING: return "smashing";
        case ENTYPO_ICON_SOUND: return "sound";
        case ENTYPO_ICON_SOUND_MIX: return "sound_mix";
        case ENTYPO_ICON_SOUND_MUTE: return "sound_mute";
        case ENTYPO_ICON_SOUNDCLOUD: return "soundcloud";
        case ENTYPO_ICON_SPORTS_CLUB: return "sports_club";
        case ENTYPO_ICON_SPOTIFY: return "spotify";
        case ENTYPO_ICON_SPOTIFY_WITH_CIRCLE: return "spotify_with_circle";
        case ENTYPO_ICON_SPREADSHEET: return "spreadsheet";
        case ENTYPO_ICON_SQUARED_CROSS: return "squared_cross";
        case ENTYPO_ICON_SQUARED_MINUS: return "squared_minus";
        case ENTYPO_ICON_SQUARED_PLUS: return "squared_plus";
        case ENTYPO_ICON_STAR: return "star";
        case ENTYPO_ICON_STAR_OUTLINED: return "star_outlined";
        case ENTYPO_ICON_STOPWATCH: return "stopwatch";
        case ENTYPO_ICON_STUMBLEUPON: return "stumbleupon";
        case ENTYPO_ICON_STUMBLEUPON_WITH_CIRCLE: return "stumbleupon_with_circle";
        case ENTYPO_ICON_SUITCASE: return "suitcase";
        case ENTYPO_ICON_SWAP: return "swap";
        case ENTYPO_ICON_SWARM: return "swarm";
        case ENTYPO_ICON_SWEDEN: return "sweden";
        case ENTYPO_ICON_SWITCH: return "switch";
        case ENTYPO_ICON_TABLET: return "ablet";
        case ENTYPO_ICON_TABLET_MOBILE_COMBO: return "tablet_mobile_combo";
        case ENTYPO_ICON_TAG: return "tag";
        case ENTYPO_ICON_TEXT: return "text";
        case ENTYPO_ICON_TEXT_DOCUMENT: return "text_document";
        case ENTYPO_ICON_TEXT_DOCUMENT_INVERTED: return "text_document_inverted";
        case ENTYPO_ICON_THERMOMETER: return "thermometer";
        case ENTYPO_ICON_THUMBS_DOWN: return "thumbs_down";
        case ENTYPO_ICON_THUMBS_UP: return "thumbs_up";
        case ENTYPO_ICON_THUNDER_CLOUD: return "thunder_cloud";
        case ENTYPO_ICON_TICKET: return "ticket";
        case ENTYPO_ICON_TIME_SLOT: return "time_slot";
        case ENTYPO_ICON_TOOLS: return "tools";
        case ENTYPO_ICON_TRAFFIC_CONE: return "traffic_cone";
        case ENTYPO_ICON_TRASH: return "trash";
        case ENTYPO_ICON_TREE: return "tree";
        case ENTYPO_ICON_TRIANGLE_DOWN: return "triangle_down";
        case ENTYPO_ICON_TRIANGLE_LEFT: return "triangle_left";
        case ENTYPO_ICON_TRIANGLE_RIGHT: return "triangle_right";
        case ENTYPO_ICON_TRIANGLE_UP: return "triangle_up";
        case ENTYPO_ICON_TRIPADVISOR: return "tripadvisor";
        case ENTYPO_ICON_TROPHY: return "trophy";
        case ENTYPO_ICON_TUMBLR: return "tumblr";
        case ENTYPO_ICON_TUMBLR_WITH_CIRCLE: return "tumblr_with_circle";
        case ENTYPO_ICON_TV: return "tv";
        case ENTYPO_ICON_TWITTER: return "twitter";
        case ENTYPO_ICON_TWITTER_WITH_CIRCLE: return "twitter_with_circle";
        case ENTYPO_ICON_TYPING: return "typing";
        case ENTYPO_ICON_UNINSTALL: return "uninstall";
        case ENTYPO_ICON_UNREAD: return "unread";
        case ENTYPO_ICON_UNTAG: return "untag";
        case ENTYPO_ICON_UPLOAD: return "upload";
        case ENTYPO_ICON_UPLOAD_TO_CLOUD: return "upload_to_cloud";
        case ENTYPO_ICON_USER: return "user";
        case ENTYPO_ICON_USERS: return "users";
        case ENTYPO_ICON_V_CARD: return "v_card";
        case ENTYPO_ICON_VIDEO: return "video";
        case ENTYPO_ICON_VIDEO_CAMERA: return "video_camera";
        case ENTYPO_ICON_VIMEO: return "vimeo";
        case ENTYPO_ICON_VIMEO_WITH_CIRCLE: return "vimeo_with_circle";
        case ENTYPO_ICON_VINE: return "vine";
        case ENTYPO_ICON_VINE_WITH_CIRCLE: return "vine_with_circle";
        case ENTYPO_ICON_VINYL: return "vinyl";
        case ENTYPO_ICON_VK: return "vk";
        case ENTYPO_ICON_VK_ALTERNITIVE: return "vk_alternitive";
        case ENTYPO_ICON_VK_WITH_CIRCLE: return "vk_with_circle";
        case ENTYPO_ICON_VOICEMAIL: return "voicemail";
        case ENTYPO_ICON_WALLET: return "wallet";
        case ENTYPO_ICON_WARNING: return "warning";
        case ENTYPO_ICON_WATER: return "water";
        case ENTYPO_ICON_WINDOWS_STORE: return "windows_store";
        case ENTYPO_ICON_XING: return "xing";
        case ENTYPO_ICON_XING_WITH_CIRCLE: return "xing_with_circle";
        case ENTYPO_ICON_YELP: return "yelp";
        case ENTYPO_ICON_YOUKO: return "youko";
        case ENTYPO_ICON_YOUKO_WITH_CIRCLE: return "youko_with_circle";
        case ENTYPO_ICON_YOUTUBE: return "youtube";
        case ENTYPO_ICON_YOUTUBE_WITH_CIRCLE: return "youtube_with_circle";
        default: return "unknown";
    }
}
