/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "Path.hpp"

#include <iostream>

namespace RM {
    Path::Path() {

    }

    Path::Path(const char * path) : bpath(path)
    {

    }

    Path::Path(std::string path) : bpath(path)
    {

    }

    Path::Path(const Path & path) : bpath(path.bpath)
    {

    }

    Path::Path(boost::filesystem::path path) : bpath(path)
    {

    }

    Path::~Path()
    {

    }

    Path Path::operator/(const Path & that) const
    {
        Path path(*this);
        path.bpath /= that.bpath;

        return path;
    }

    Path Path::extendString(const std::string & that) const
    {
        Path path(*this);
        path.bpath /= that;

        return path;
    }

    Path Path::operator+(const Path & that) const
    {
        Path path(*this);
        path.bpath += that.bpath;

        return path;
    }

    Path Path::appendString(const std::string & that) const
    {
        Path path(*this);
        path.bpath += that;

        return path;
    }

    std::string Path::string() const
    {
        std::string path = bpath.string();

        #ifdef __WIN32__
        std::replace(path.begin(), path.end(), '\\', '/');
        #endif // WIN32

        return path;
    }

    bool Path::operator==(const Path & that) const
    {
        return boost::filesystem::equivalent(this->bpath, that.bpath);
    }

    bool Path::operator==(const std::string that) const
    {
        return this->bpath == boost::filesystem::path(that);
    }

    Path& Path::operator=(const Path & path) {
        this->bpath = path.bpath;

        return *this;
    }

    std::string Path::getExtension() const
    {
        return bpath.extension().string();
    }

    void Path::setExtension(const std::string ext)
    {
        bpath.replace_extension(ext);
    }

    std::string Path::getName() const
    {
        return bpath.filename().string();
    }

    Path Path::getParent() const
    {
        Path path(bpath.parent_path());

        return path;
    }

    Path Path::getAbsolute() const
    {
        boost::filesystem::path bpath = boost::filesystem::absolute(this->bpath);
        Path rpath = boost::filesystem::relative(bpath, sroot);
        return rpath;
    }

    Path Path::getRelative(const Path & path) const
    {
        Path rpath(boost::filesystem::relative(path.bpath, this->bpath));
        return rpath;
    }

    bool Path::copyTo(Path & target) const
    {
        // TODO make this support directories.

        if (target.isLegal())
        {
            try {
                boost::filesystem::copy(bpath, target.bpath);
                return true;
            } catch (boost::filesystem::filesystem_error& e) {

                std::cout << "Warning: Failed to copy \"" << this->string() << "\" to \"" << target.string() << "\".\n";
                std::cout << "The filesystem returned the following error:\n" << e.what() << std::endl;

                return false;
            }
        } else {
            return false;
        }
    }

    bool Path::mkdirs()
    {
        if (this->isLegal()) {
            return boost::filesystem::create_directories(bpath);
        } else {
            return false;
        }
    }

    bool Path::exists() const
    {
        return this->isLegal() && boost::filesystem::exists(bpath);
    }

    size_t Path::size() const
    {
        if (this->exists()) {
            return boost::filesystem::file_size(bpath);
        } else {
            return 0;
        }
    }

    bool Path::isDirectory() const
    {
        return this->exists() && boost::filesystem::is_directory(bpath);
    }

    bool Path::isEmpty() const
    {
        return this->exists() && boost::filesystem::is_empty(bpath);
    }

    bool Path::isRegularFile() const
    {
        return this->exists() && boost::filesystem::is_regular_file(bpath);
    }

    size_t Path::remove()
    {
        if (this->isLegal())
        {
            try {
                return boost::filesystem::remove_all(bpath);
            } catch (boost::filesystem::filesystem_error& e) {

                std::cout << "Warning: Failed to delete \"" << bpath.string() << "\".\n";
                std::cout << "The filesystem returned the following error:\n" << e.what() << std::endl;

                return static_cast<long long unsigned int>(0);
            }
        } else {
            return false;
        }
    }

    bool Path::contains(const Path & path) const
    {
        boost::filesystem::path bpath = boost::filesystem::absolute(this->bpath);
        boost::filesystem::path cpath = boost::filesystem::absolute(path.bpath);

        // If it's not a folder, then it's impossible to do this.
        if (boost::filesystem::is_directory(bpath))
        {
            // If dir ends with "/" and isn't the root directory, then the final
            // component returned by iterators will include "." and will interfere
            // with the std::equal check below, so we strip it before proceeding.
            if (bpath.filename() == ".")
            bpath.remove_filename();

            // We're also not interested in the file's name.
            cpath.remove_filename();

            // If dir has more components than file, then file can't possibly
            // reside in dir.
            auto dir_len = std::distance(bpath.begin(), bpath.end());
            auto file_len = std::distance(cpath.begin(), cpath.end());
            if (dir_len > file_len)
            return false;

            // This stops checking when it reaches dir.end(), so it's OK if file
            // has more directory components afterward. They won't be checked.
            return std::equal(bpath.begin(), bpath.end(), cpath.begin());
        } else {
            // It's impossible for something that is not a directory to contain stuff.
            return false;
        }
    }

    bool Path::move(const Path newPath) {
        // This is a legal path.
        if (newPath.isLegal()) {
            try {
                boost::filesystem::rename(this->bpath, newPath.bpath);
                return true;
            } catch (boost::filesystem::filesystem_error& e) {

                std::cout << "Warning: Failed to rename \"" << this->string() << "\" to \"" << newPath.string() << "\".\n";
                std::cout << "The filesystem returned the following error:\n" << e.what() << std::endl;

                return false;
            }
        } else {
            return false;
        }
    }

    bool Path::move(const std::string newPath) {
        Path test(newPath);

        // This is a legal path.
        if (test.isLegal()) {
            try {
                boost::filesystem::rename(this->bpath, test.bpath);
                return true;
            } catch (boost::filesystem::filesystem_error& e) {

                std::cout << "Warning: Failed to rename \"" << this->string() << "\" to \"" << newPath << "\".\n";
                std::cout << "The filesystem returned the following error:\n" << e.what() << std::endl;

                return false;
            }
        } else {
            return false;
        }
    }

    sol::table Path::listContents(sol::this_state lua) const
    {
        sol::table contents(lua, sol::new_table{});

        // Start at 1 because Lua is weird.
        size_t index = 1;
        for (boost::filesystem::directory_entry& e : boost::filesystem::directory_iterator(bpath)) {
            Path path(e.path());
            contents[index++] = path;
        }

        return contents;
    }

    void Path::listContents(std::vector<Path> * contents) const
    {
        for (boost::filesystem::directory_entry& e : boost::filesystem::directory_iterator(bpath)) {
            Path path(e.path());
            contents->push_back(path);
        }
    }

    boost::filesystem::path Path::sroot = boost::filesystem::current_path().parent_path();

    bool Path::isLegal() const
    {
        const Path path(sroot);
        return path.contains(*this);
    }

    bool Path::isLegal(const boost::filesystem::path path) const
    {
        Path cpath(path);
        return Path(sroot).contains(cpath);
    }

    Path Path::root()
    {
        // Make a copy so they can't modify it.
        return Path(sroot);
    }

    Path Path::current(sol::this_state lua)
    {
        int level = 0;
        lua_Debug ar;

        while (lua_getstack(lua, level++, &ar)) {
            lua_getinfo(lua, "Sn", &ar);

            if (strcmp(ar.what, "Lua") == 0 || strcmp(ar.what, "main") == 0) { // Is there a name?
                Path path(std::string(ar.source+1));

                if (path.exists()) {
                    return path;
                }
            }
        }

        // If you can't figure out the path, assume the root directory.
        return sroot;
    }

    boost::filesystem::path Path::getBoostPath() const {
        return this->bpath;
    }

    std::shared_ptr<boost::filesystem::fstream> Path::open(const std::string & mode) {
        bool isDir = this->isDirectory();

        if (this->exists() && !isDir) {
            std::ios_base::openmode openmode;

            if (mode == "r") {
                openmode = std::ios::in;
            } else if (mode == "w") {
                openmode = std::ios::out;
            } else if (mode == "a") {
                openmode = std::ios::app;
            } else if (mode == "r+") {
                openmode = std::ios::app;
            } else if (mode == "w+") {
                openmode = std::ios::trunc;
            } else if (mode == "a+") {
                openmode = std::ios::app;
            } else {
                throw std::runtime_error("Unknown file mode: " + mode);
            }

            return std::make_shared<boost::filesystem::fstream>(bpath, openmode);
        } else {
            if (!isDir) {
                throw std::runtime_error("File does not exist.");
            } else {
                throw std::runtime_error("Path is a folder.");
            }
        }
    }

    void Path::createFile()
    {
        if (isLegal())
        {
            boost::filesystem::ofstream stream(bpath);
        }
        else
        {
            throw std::runtime_error("Can not create file.");
        }
    }
}
