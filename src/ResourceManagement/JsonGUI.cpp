/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "RenderTarget.hpp"
#include "JsonGUI.hpp"
using namespace nanogui;

#include "GUI/luaGUI.hpp"

#include "Icons.hpp"
#include <iostream>
#include <unordered_map>
#include <boost/algorithm/string/predicate.hpp>

#include "luaRM.hpp"
#include "globals.hpp"
#include "ResourceManager.hpp"

namespace RM {

    std::unordered_map<std::string, Button::IconPosition> buttonIconPositionMap(
    {
        {"left", Button::IconPosition::Left},
        {"left centered", Button::IconPosition::LeftCentered},
        {"right centered", Button::IconPosition::RightCentered},
        {"right", Button::IconPosition::Right}
    });

    struct JsonGUIError {
        JsonGUIError(const Json::Value & value, const std::string & message):
            value(value),
            message(message)
        {

        }

        Json::Value value;
        std::string message;
    };

    JsonGUI::JsonGUI(ResourceGroup * group, nanogui::Widget * parent, sol::state_view & lua, const sol::table & args, const Path & path):
        RM::BasicResource(path.getName(), "JsonGUI", group),
        path(path),
        lua(lua.lua_state()),
        parent(parent)
    {

        sol::environment clientAPI;
        getResourceManager()->getEnvironment("ClientAPI", clientAPI);

        env = sol::environment(lua, sol::create, clientAPI);
        env["args"] = args;

        if (!path.exists()) {
            std::string message = "File does not exist: ";
            message += path.string();
            throw std::runtime_error(message);
        }

        std::cout << "Building JSON GUI from " << path.string() << std::endl;
        if (!boost::algorithm::ends_with(path.string(), "-gui.json")) {
            std::cout << "Warning: Json files containing GUI should have names ending with \"-gui.json\"." << std::endl;
        }

        std::string input;

        boost::filesystem::ifstream file(path.getBoostPath(), std::ios::in | std::ios::binary);
        if (!file) {
            throw std::runtime_error("Failed to open file.");
        }

        file.seekg(0, std::ios::end);
        input.resize(file.tellg());
        file.seekg(0, std::ios::beg);
        file.read(&input[0], input.size());
        file.close();

        Json::Value root;
        Json::Reader reader;
        if (!reader.parse(input, root)) {
            std::string error = "Error while reading JSON string:\n";
            error += reader.getFormattedErrorMessages();

            throw std::runtime_error(error);
        }

        setResourceStepsToLoad(1);
        std::queue<sol::protected_function> setupScripts;

        try {
            addWidgetsToWidget(setupScripts, parent, root, reader);
        } catch (JsonGUIError & error) {
            reader.pushError(error.value, error.message);
            throw std::runtime_error("Bad Value in JsonGUI: " + reader.getFormatedErrorMessages());
        }

        parent->screen()->performLayout();

        while (!setupScripts.empty()) {
            sol::protected_function func = setupScripts.front();
            setupScripts.pop();

            sol::protected_function_result result = func();
            if (!result.valid()) {
                sol::error err = result;
                std::string what = err.what();
                std::cerr << "Error in widget setup:\n" << what << std::endl;
            }
        }

        parent->screen()->performLayout();
        incrementResourceProgress(1);
    }

    JsonGUI::~JsonGUI() {

    }

    sol::protected_function JsonGUI::loadScript(Json::Value & scriptVal, Json::Reader & reader) {
        sol::protected_function func;

        if (scriptVal.isString()) {
            std::string script = scriptVal.asString();

            if (script.empty() || script[0] != '#') {
                func = lua.load(script);
            } else {
                Path path(script.substr(1));
                if (path.exists()) {
                    func = lua.load_file(path.string());
                } else {
                    std::string message = "Could not open script file: ";
                    message += script;
                    throw JsonGUIError(scriptVal, message);
                }
            }
        } else if (scriptVal.isArray()) {
            std::string script;
            unsigned int numLines = scriptVal.size();
            for (unsigned int i = 0; i < numLines; i++) {
                script+= scriptVal[i].asString();
                script+= '\n';
            }

            int line, column;
            reader.getValueLineAndColumn(scriptVal, line, column);

            func = lua.load(script, resourceName() + " line: " + std::to_string(line) + " column: " + std::to_string(column));
        } else {
            throw JsonGUIError(scriptVal, "Expected a string or an array of strings for embedded Lua script.");
        }

        return func;
    }

    void JsonGUI::readBasicWidgetProperties(std::queue<sol::protected_function> & setupScripts, nanogui::Widget * widget, Json::Value & root, Json::Reader & reader) {

        Json::Value setup = root["setup"];

        if (!setup.isNull()) {
            sol::protected_function func = loadScript(setup, reader);
            sol::set_environment(env, func);

            setupScripts.push(func);
        }

        Json::Value layout = root["layout"];
        if (!layout.isNull()) {
            setLayout(widget, layout);
        }

        Json::Value fixedWidth = root["fixed width"];
        if (fixedWidth.isInt()) {
            widget->setFixedWidth(fixedWidth.asInt());
        }

        Json::Value fixedHeight = root["fixed height"];
        if (fixedHeight.isInt()) {
            widget->setFixedHeight(fixedHeight.asInt());
        }

        Json::Value enabled = root["enabled"];
        if (enabled.isBool()) {
            widget->setEnabled(enabled.asBool());
        }

        Json::Value tooltip = root["tooltip"];
        if (tooltip.isString()) {
            widget->setTooltip(tooltip.asString());
        }

        Json::Value fontSize = root["font size"];
        if (fontSize.isInt()) {
            widget->setFontSize(fontSize.asInt());
        }

        Json::Value permitsControls = root["permits controls"];
        if (permitsControls.isBool()) {
            widget->setPermitsControls(permitsControls.asBool());
        }

        Json::Value grabsMouse = root["grabs mouse"];
        if (grabsMouse.isBool()) {
            widget->setGrabsMouse(grabsMouse.asBool());
        }

        Json::Value children = root["children"];

        addWidgetsToWidget(setupScripts, widget, children, reader);
    }

    void JsonGUI::addWidget(std::queue<sol::protected_function> & setupScripts, Widget * parent, Json::Value & root, Json::Reader & reader) {
        Json::Value typeNameValue = root["type"];

        if (typeNameValue.isString()) {
            std::string typeName = typeNameValue.asString();

            if (typeName == "Window") {
                addWindow(setupScripts, parent, root, reader);
            } else if (typeName == "Widget") {
                Widget * widget = parent->add<Widget>();

                bindToLua<Widget>(widget, root);
                readBasicWidgetProperties(setupScripts, widget, root, reader);
            } else if (typeName == "Button") {
                addButton(setupScripts, parent, root, reader);
            } else if (typeName == "PopupButton") {
                addPopupButton(setupScripts, parent, root, reader);
            } else if (typeName == "CheckBox") {
                addCheckBox(setupScripts, parent, root, reader);
            } else if (typeName == "ColorWheel") {

            } else if (typeName == "RenderTarget") {
                addRenderTarget(setupScripts, parent, root, reader);
            } else if (typeName == "Graph") {

            } else if (typeName == "ImagePanel") {

            } else if (typeName == "ImageView") {

            } else if (typeName == "Label") {
                addLabel(setupScripts, parent, root, reader);
            } else if (typeName == "ProgressBar") {

            } else if (typeName == "Slider") {
                addSlider(setupScripts, parent, root, reader);
            } else if (typeName == "StackedWidget") {

            } else if (typeName == "TabHeader") {

            } else if (typeName == "TabWidget") {

            } else if (typeName == "TextBox") {
                addTextBox(setupScripts, parent, root, reader);
            } else if (typeName == "FloatBox") {
                addFloatBox(setupScripts, parent, root, reader);
            } else if (typeName == "IntBox") {
                addIntBox(setupScripts, parent, root, reader);
            } else if (typeName == "VScrollPanel") {
                addVScrollPanel(setupScripts, parent, root, reader);
            } else {
                std::string message = "Unknown widget type: ";
                message += typeName;

                throw JsonGUIError(typeNameValue, message);
            }


        } else {
            throw JsonGUIError(root, "No widget type was provided.");
        }
    }

    template<typename W>
    inline void JsonGUI::bindToLua(W * widget, Json::Value & root) {
        Json::Value nameValue = root["name"];
        if (nameValue.isString()) {
            std::string name = nameValue.asString();
            if (this->env[name] != sol::lua_nil) {
                std::cout << "Warning: Overwriting name \"" << name << "\" with a widget.\n";
            }

            this->env[name] = RM::ref<W>(widget);
        }
    }

    nanogui::Alignment JsonGUI::getAlignment(Json::Value value) {
        if (value.isString()) {
            std::string type = value.asString();

            if (type == "minimum") {
                return Alignment::Minimum;
            }

            if (type == "middle") {
                return Alignment::Middle;
            }

            if (type == "maximum") {
                return Alignment::Maximum;
            }

            if (type == "fill") {
                return Alignment::Fill;
            }

            std::string message = "Unknown Alignment type: ";
            message += type;
            throw JsonGUIError(value, message);
        } else {
            throw JsonGUIError(value, "Expected a string for Alignment type.");
        }
    }

    nanogui::Orientation JsonGUI::getOrientation(Json::Value value) {
        if (value.isString()) {
            std::string type = value.asString();

            if (type == "horizontal") {
                return Orientation::Horizontal;
            }

            if (type == "vertical") {
                return Orientation::Vertical;
            }

            std::string message = "Unknown Orientation type: ";
            message += type;
            throw JsonGUIError(value, message);
        } else {
            throw JsonGUIError(value, "Expected a string for Orientation type.");
        }
    }

    glm::vec4 JsonGUI::getColor(Json::Value value) {
        if (value.size() == 4) {
            return glm::vec4(value[0].asFloat(), value[1].asFloat(), value[2].asFloat(), value[3].asFloat());
        } else {
            throw JsonGUIError(value, "Wrong number of integers for a color.");
        }
    }

    void JsonGUI::setLayout(Widget * widget, Json::Value layout) {
        if (layout.isObject()) {
            Json::Value typeVal = layout["type"];
            std::string type = typeVal.asString();

            if (type == "Advanced Grid") {
                throw JsonGUIError(typeVal, "Advanced Grid layout is not yet supported.");
            } else if (type == "Box") {
                Orientation orientation = getOrientation(layout["orientation"]);
                Alignment alignment = getAlignment(layout.get("alignment", "middle"));
                int margin = layout.get("margin", 0).asInt();
                int spacing = layout.get("spacing", 0).asInt();

                widget->setLayout(new BoxLayout(orientation, alignment, margin, spacing));
            } else if (type == "Grid") {
                Orientation orientation = getOrientation(layout.get("orientation", "horizontal"));
                int resolution = layout.get("resolution", 2).asInt();
                Alignment alignment = getAlignment(layout.get("alignment", "middle"));
                int margin = layout.get("margin", 0).asInt();
                int spacing = layout.get("spacing", 0).asInt();

                widget->setLayout(new GridLayout(orientation, resolution, alignment, margin, spacing));
            } else if (type == "Group") {
                int margin = layout.get("margin", 15).asInt();
                int spacing = layout.get("spacing", 6).asInt();
                int groupSpacing = layout.get("group spacing", 14).asInt();
                int groupIndent = layout.get("group indent", 20).asInt();

                widget->setLayout(new GroupLayout(margin, spacing, groupSpacing, groupIndent));
            } else {
                std::string message = "Unknown layout type: ";
                message += type;
                throw JsonGUIError(typeVal, message);
            }
        } else {
            throw JsonGUIError(layout, "Layout and its parameters must be stored in an object.");
        }
    }

    void JsonGUI::addWidgetsToWidget(std::queue<sol::protected_function> & setupScripts, Widget * widget, Json::Value & children, Json::Reader & reader) {
        if (children.isArray()) {
            for (unsigned int i = 0; i < children.size(); i++) {
                addWidget(setupScripts, widget, children[i], reader);
            }
        } else if (!children.isNull()) {
            throw JsonGUIError(children, "Expected array for child list.");
        }
    }

    void JsonGUI::addWindow(std::queue<sol::protected_function> & setupScripts, nanogui::Widget * parent, Json::Value & root, Json::Reader & reader) {
        std::string title = root.get("title", "UNAMED" ).asString();
        Window * window = parent->add<Window>(title);

        bindToLua<Window>(window, root);
        readBasicWidgetProperties(setupScripts, window, root, reader);

        Json::Value modal = root["modal"];
        if (modal.isBool()) {
            window->setModal(modal.asBool());
        }

        Json::Value callback = root["dispose callback"];
        if (!callback.isNull()) {
            sol::protected_function func = loadScript(callback, reader);
            ref<JsonGUI> self = this;

            window->addDisposeCallback([self, func]()
            {
                sol::set_environment(self->env, func);

                sol::protected_function_result result = func();
                if (!result.valid()) {
                    sol::error err = result;
                    std::string what = err.what();
                    std::cerr << "Error in widget dispose callback:\n" << what << std::endl;
                }
            });
        }

    }

    void JsonGUI::addButton(std::queue<sol::protected_function> & setupScripts, Widget * parent, Json::Value & root, Json::Reader & reader) {

        std::string caption = root.get("caption", "X").asString();

        int icon = 0;
        Json::Value iconValue = root["icon"];
        if (iconValue.isString()) {
            icon = getIcon(iconValue.asString());
        }

        Button * button = parent->add<Button>(caption, icon);

        bindToLua<Button>(button, root);
        readBasicWidgetProperties(setupScripts, button, root, reader);

        Json::Value bgColor = root["background color"];
        if (bgColor.isArray()) {
            button->setBackgroundColor(getColor(bgColor));
        }

        Json::Value textColor = root["text color"];
        if (textColor.isArray()) {
            button->setTextColor(getColor(textColor));
        }

        Json::Value iconPosition = root["icon position"];
        if (iconPosition.isString()) {
            std::unordered_map<std::string, Button::IconPosition>::const_iterator result = buttonIconPositionMap.find(iconPosition.asString());
            if (result != buttonIconPositionMap.end()) {
                button->setIconPosition(result->second);
            } else {
                std::cout << "WARNING: unknown icon position requested \"" << iconPosition.asString() << "\". Choosing to ignore.";
            }
        }

        Json::Value pushed = root["pushed"];
        if (pushed.isBool()) {
            button->setPushed(pushed.asBool());
        }

        Json::Value callback = root["push callback"];
        if (!callback.isNull()) {
            sol::protected_function func = loadScript(callback, reader);
            ref<JsonGUI> self = this;

            button->addPushCallback([self, func]()
            {
                sol::set_environment(self->env, func);

                sol::protected_function_result result = func();
                if (!result.valid()) {
                    sol::error err = result;
                    std::string what = err.what();
                    std::cerr << "Error in push callback:\n" << what << std::endl;
                }
            });
        }

        Json::Value isRadio = root["radio"];
        if (isRadio.isBool()) {
            if (isRadio.asBool()) {
                button->setFlags(Button::Flags::RadioButton);
            }
        }
    }

    void JsonGUI::addPopupButton(std::queue<sol::protected_function> & setupScripts, Widget * parent, Json::Value & root, Json::Reader & reader) {

        std::string caption = root.get("caption", "X").asString();

        int icon = 0;
        Json::Value iconValue = root["icon"];
        if (iconValue.isString()) {
            icon = getIcon(iconValue.asString());
        }

        PopupButton * button = parent->add<PopupButton>(caption, icon);
        bindToLua<PopupButton>(button, root);

        readBasicWidgetProperties(setupScripts, button, root, reader);

        Json::Value bgColor = root["background color"];
        if (bgColor.isArray()) {
            button->setBackgroundColor(getColor(bgColor));
        }

        Json::Value textColor = root["text color"];
        if (textColor.isArray()) {
            button->setTextColor(getColor(textColor));
        }

        Json::Value iconPosition = root["icon position"];
        if (iconPosition.isString()) {
            std::unordered_map<std::string, Button::IconPosition>::const_iterator result = buttonIconPositionMap.find(iconPosition.asString());
            if (result != buttonIconPositionMap.end()) {
                button->setIconPosition(result->second);
            } else {
                std::cout << "WARNING: unknown icon position requested \"" << iconPosition.asString() << "\". Choosing to ignore.";
            }
        }

        Json::Value pushed = root["pushed"];
        if (pushed.isBool()) {
            button->setPushed(pushed.asBool());
        }

        Json::Value callback = root["push callback"];
        if (!callback.isNull()) {
            sol::protected_function func = loadScript(callback, reader);
            ref<JsonGUI> self = this;

            button->addPushCallback([self, func]()
            {
                sol::set_environment(self->env, func);

                sol::protected_function_result result = func();
                if (!result.valid()) {
                    sol::error err = result;
                    std::string what = err.what();
                    std::cerr << "Error in push callback:\n" << what << std::endl;
                }
            });
        }

        Json::Value popup = root["popup"];

        if (popup.isObject()) {
            readBasicWidgetProperties(setupScripts, button->popup(), popup, reader);
        }
    }

    void JsonGUI::addCheckBox(std::queue<sol::protected_function> & setupScripts, Widget * parent, Json::Value & root, Json::Reader & reader) {

        std::string caption = root.get("caption", "X").asString();

        CheckBox * box = parent->add<CheckBox>(caption);

        bindToLua<CheckBox>(box, root);
        readBasicWidgetProperties(setupScripts, box, root, reader);

        Json::Value pushed = root["pushed"];
        if (pushed.isBool()) {
            box->setPushed(pushed.asBool());
        }

        Json::Value checked = root["checked"];
        if (pushed.isBool()) {
            box->setChecked(checked.asBool());
        }

        Json::Value callback = root["toggle callback"];
        if (!callback.isNull()) {
            sol::protected_function func = loadScript(callback, reader);
            ref<JsonGUI> self = this;

            box->addToggleCallback([self, this, func](bool checked)
            {
                sol::object temp = self->env["checked"];
                env["checked"] = checked;
                sol::set_environment(self->env, func);

                sol::protected_function_result result = func();
                env["checked"] = temp;

                if (!result.valid()) {
                    sol::error err = result;
                    std::string what = err.what();
                    std::cerr << "Error in toggle callback:\n" << what << std::endl;
                }
            });
        }
    }

    /*void JsonGUI::addColorWheel(std::queue<sol::protected_function> & setupScripts, Widget * parent, Json::Value & root, Json::Reader & reader) {

        ColorWheel * wheel;
        bindToLua<ColorWheel>(wheel, root);
        readBasicWidgetProperties(wheel, root);
    }*/

    void JsonGUI::addRenderTarget(std::queue<sol::protected_function> & setupScripts, Widget * parent, Json::Value & root, Json::Reader & reader) {
        Json::Value callback = root["render callback"];


        if (!callback.isNull()) {
            sol::protected_function func = loadScript(callback, reader);
            ref<JsonGUI> self = this;

            GL::RenderTarget * target = parent->add<GL::RenderTarget>([self, this, func](GL::RenderTarget * target)
            {
                sol::object temp = self->env["target"];
                env["target"] = target;
                sol::set_environment(self->env, func);

                sol::protected_function_result result = func();
                env["target"] = temp;

                if (!result.valid()) {
                    sol::error err = result;
                    std::string what = err.what();
                    std::cerr << "Error in render callback:\n" << what << std::endl;
                }
            });

            bindToLua<GL::RenderTarget>(target, root);
            readBasicWidgetProperties(setupScripts, target, root, reader);
        } else {
            throw JsonGUIError(root, "Render target needs a render callback.");
        }
    }

    /*void JsonGUI::addGraph(std::queue<sol::protected_function> & setupScripts, Widget * parent, Json::Value & root, Json::Reader & reader) {

        Graph * graph;
        bindToLua<Graph>(graph, root);
        readBasicWidgetProperties(graph, root);
    }

    void JsonGUI::addImagePanel(std::queue<sol::protected_function> & setupScripts, Widget * parent, Json::Value & root, Json::Reader & reader) {

        ImagePanel * panel;
        bindToLua<ImagePanel>(panel, root);
        readBasicWidgetProperties(panel, root);
    }

    void JsonGUI::addImageView(std::queue<sol::protected_function> & setupScripts, Widget * parent, Json::Value & root, Json::Reader & reader) {

        ImageView * view;
        bindToLua<ImageView>(view, root);
        readBasicWidgetProperties(view, root);
    }*/

    void JsonGUI::addLabel(std::queue<sol::protected_function> & setupScripts, Widget * parent, Json::Value & root, Json::Reader & reader) {

        std::string caption = root.get("caption", "X").asString();
        std::string font = root.get("font", "sans").asString();
        Label * label = parent->add<Label>(caption, font);

        bindToLua<Label>(label, root);
        readBasicWidgetProperties(setupScripts, label, root, reader);

        Json::Value color = root["text color"];
        if (color.isArray()) {
            label->setColor(getColor(color));
        }
    }

    /*void JsonGUI::addProgressBar(std::queue<sol::protected_function> & setupScripts, Widget * parent, Json::Value & root, Json::Reader & reader) {

        ProgressBar * bar;
        bindToLua<ProgressBar>(bar, root);
        readBasicWidgetProperties(me, bar, root);
    }*/

    void JsonGUI::addSlider(std::queue<sol::protected_function> & setupScripts, Widget * parent, Json::Value & root, Json::Reader & reader) {

        Slider * slider = parent->add<Slider>();

        bindToLua<Slider>(slider, root);
        readBasicWidgetProperties(setupScripts, slider, root, reader);

        Json::Value value = root["value"];
        if (value.isNumeric()) {
            slider->setValue(value.asFloat());
        }

        Json::Value highlight_color = root["highlight color"];
        if (highlight_color.isArray()) {
            slider->setHighlightColor(getColor(highlight_color));
        }

        Json::Value range = root["range"];
        if (range.isArray()) {
            if (range.size() == 2) {
                slider->setRange(std::pair<float, float>(range[0].asFloat(), range[1].asFloat()));
            } else {
                throw JsonGUIError(range, "Wrong number of arguments for a range.");
            }
        }

        Json::Value highlight_range = root["highlight range"];
        if (highlight_range.isArray()) {
            if (highlight_range.size() == 2) {
                slider->setHighlightedRange(std::pair<float, float>(highlight_range[0].asFloat(), highlight_range[1].asFloat()));
            } else {
                throw JsonGUIError(highlight_range, "Wrong number of arguments for a highlight range.");
            }
        }

        Json::Value slide_callback = root["slide callback"];
        if (!slide_callback.isNull()) {

            sol::protected_function func = loadScript(slide_callback, reader);
            ref<JsonGUI> self = this;

            slider->addChangeCallback([self, this, func](float value)
            {
                sol::object temp = env["value"];
                env["value"] = value;
                sol::set_environment(env, func);

                sol::protected_function_result result = func();
                if (!result.valid()) {
                    sol::error err = result;
                    std::string what = err.what();
                    std::cerr << "Error in slide callback:\n" << what << std::endl;
                }

                env["value"] = temp;
            });
        }

        Json::Value set_callback = root["set callback"];
        if (!set_callback.isNull()) {

            sol::protected_function func = loadScript(set_callback, reader);
            ref<JsonGUI> self = this;

            slider->addSelectCallback([self, this, func](float value)
            {
                sol::object temp = env["value"];
                env["value"] = value;
                sol::set_environment(env, func);

                sol::protected_function_result result = func();
                if (!result.valid()) {
                    sol::error err = result;
                    std::string what = err.what();
                    std::cerr << "Error in set callback:\n" << what << std::endl;
                }

                env["value"] = temp;
            });
        }
    }

    /*void JsonGUI::addStackedWidget(std::queue<sol::protected_function> & setupScripts, Widget * parent, Json::Value & root, Json::Reader & reader) {

        StackedWidget * stacked;
        bindToLua<StackedWidget>(stacked, root);
        readBasicWidgetProperties(stacked, root);
    }

    void JsonGUI::addTabHeader(std::queue<sol::protected_function> & setupScripts, Widget * parent, Json::Value & root, Json::Reader & reader) {

        TabHeader * header;
        bindToLua<TabHeader>(header, root);
        readBasicWidgetProperties(header, root);
    }

    void JsonGUI::addTabWidget(std::queue<sol::protected_function> & setupScripts, Widget * parent, Json::Value & root, Json::Reader & reader) {

        TabWidget * tab;
        bindToLua<TabWidget>(tab, root);
        readBasicWidgetProperties(tab, root);
    }*/

    void JsonGUI::addTextBox(std::queue<sol::protected_function> & setupScripts, Widget * parent, Json::Value & root, Json::Reader & reader) {

        TextBox * box = parent->add<TextBox>();

        bindToLua<TextBox>(box, root);
        readBasicWidgetProperties(setupScripts, box, root, reader);

        Json::Value value = root["value"];
        if (value.isString()) {
            box->setValue(value.asString());
        }

        Json::Value units = root["units"];
        if (units.isString()) {
            box->setUnits(units.asString());
        }

        Json::Value placeholder = root["placeholder"];
        if (placeholder.isString()) {
            box->setPlaceholder(placeholder.asString());
        }

        Json::Value permit_edit = root["permit edit"];
        if (permit_edit.isBool()) {
            box->setEditable(permit_edit.asBool());
        }

        /*Json::Value iconValue = root["icon"];
        if (iconValue.isString()) {
            int icon = getIcon(iconValue.asString());
            box->setUnitsImage(icon);
        }*/

        Json::Value alignment = root["alignment"];
        if (alignment.isString()) {
            std::string type = alignment.asString();

            if (type == "left") {
                box->setAlignment(TextBox::Alignment::Left);
            } else if (type == "center") {
                box->setAlignment(TextBox::Alignment::Center);
            } else if (type == "right") {
                box->setAlignment(TextBox::Alignment::Right);
            } else {
                std::string message = "Unknown alignment type for TextBox: ";
                message += type;
                throw JsonGUIError(alignment, message);
            }
        }

        Json::Value callback = root["change callback"];
        if (!callback.isNull()) {

            sol::protected_function func = loadScript(callback, reader);
            ref<JsonGUI> self = this;

            box->addChangeCallback([self, this, func](std::string value)
            {
                sol::object temp = env["value"];
                env["value"] = value;
                sol::set_environment(env, func);

                sol::protected_function_result result = func();
                if (!result.valid()) {
                    sol::error err = result;
                    std::string what = err.what();
                    std::cerr << "Error in change callback:\n" << what << std::endl;
                }

                env["value"] = temp;

                bool ret = result;
                return ret;
            });
        }
    }

    void JsonGUI::addFloatBox(std::queue<sol::protected_function> & setupScripts, Widget * parent, Json::Value & root, Json::Reader & reader) {
        FloatBox<float> * box = parent->add<FloatBox<float>>();

        bindToLua<FloatBox<float>>(box, root);
        readBasicWidgetProperties(setupScripts, box, root, reader);

        Json::Value value = root["value"];
        if (value.isNumeric()) {
            box->setValue(value.asFloat());
        }

        Json::Value units = root["units"];
        if (units.isString()) {
            box->setUnits(units.asString());
        }

        Json::Value placeholder = root["placeholder"];
        if (placeholder.isString()) {
            box->setPlaceholder(placeholder.asString());
        }

        Json::Value permit_edit = root["permit edit"];
        if (permit_edit.isBool()) {
            box->setEditable(permit_edit.asBool());
        }

        Json::Value spins = root["spins"];
        if (spins.isBool()) {
            box->setSpinnable(spins.asBool());
        }

        /*Json::Value iconValue = root["icon"];
        if (iconValue.isString()) {
            int icon = getIcon(iconValue.asString());
            box->setUnitsImage(icon);
        }*/

        Json::Value alignment = root["alignment"];
        if (alignment.isString()) {
            std::string type = alignment.asString();

            if (type == "left") {
                box->setAlignment(TextBox::Alignment::Left);
            } else if (type == "center") {
                box->setAlignment(TextBox::Alignment::Center);
            } else if (type == "right") {
                box->setAlignment(TextBox::Alignment::Right);
            } else {
                std::string message = "Unknown alignment type for TextBox: ";
                message += type;
                throw JsonGUIError(alignment, message);
            }
        }

        Json::Value callback = root["change callback"];
        if (!callback.isNull()) {

            sol::protected_function func = loadScript(callback, reader);
            ref<JsonGUI> self = this;

            box->addChangeCallback([self, this, func](float value)
            {
                sol::object temp = env["value"];
                env["value"] = value;
                sol::set_environment(env, func);

                sol::protected_function_result result = func();
                if (!result.valid()) {
                    sol::error err = result;
                    std::string what = err.what();
                    std::cerr << "Error in change callback:\n" << what << std::endl;
                }

                env["value"] = temp;

                bool ret = result;
                return ret;
            });
        }
    }

    void JsonGUI::addIntBox(std::queue<sol::protected_function> & setupScripts, Widget * parent, Json::Value & root, Json::Reader & reader) {
        IntBox<long> * box = parent->add<IntBox<long>>();

        bindToLua<IntBox<long>>(box, root);
        readBasicWidgetProperties(setupScripts, box, root, reader);

        Json::Value value = root["value"];
        if (value.isInt64()) {
            box->setValue(value.asInt());
        }

        Json::Value units = root["units"];
        if (units.isString()) {
            box->setUnits(units.asString());
        }

        Json::Value placeholder = root["placeholder"];
        if (placeholder.isString()) {
            box->setPlaceholder(placeholder.asString());
        }

        Json::Value permit_edit = root["permit edit"];
        if (permit_edit.isBool()) {
            box->setEditable(permit_edit.asBool());
        }

        Json::Value spins = root["spins"];
        if (spins.isBool()) {
            box->setSpinnable(spins.asBool());
        }

        /*Json::Value iconValue = root["icon"];
        if (iconValue.isString()) {
            int icon = getIcon(iconValue.asString());
            box->setUnitsImage(icon);
        }*/

        Json::Value alignment = root["alignment"];
        if (alignment.isString()) {
            std::string type = alignment.asString();

            if (type == "left") {
                box->setAlignment(TextBox::Alignment::Left);
            } else if (type == "center") {
                box->setAlignment(TextBox::Alignment::Center);
            } else if (type == "right") {
                box->setAlignment(TextBox::Alignment::Right);
            } else {
                std::string message = "Unknown alignment type for TextBox: ";
                message += type;
                throw JsonGUIError(alignment, message);
            }
        }

        Json::Value callback = root["change callback"];
        if (!callback.isNull()) {

            sol::protected_function func = loadScript(callback, reader);
            ref<JsonGUI> self = this;

            box->addChangeCallback([self, this, func](long value)
            {
                sol::object temp = env["value"];
                env["value"] = value;
                sol::set_environment(env, func);

                sol::protected_function_result result = func();
                if (!result.valid()) {
                    sol::error err = result;
                    std::string what = err.what();
                    std::cerr << "Error in change callback:\n" << what << std::endl;
                }

                env["value"] = temp;

                bool ret = result;
                return ret;
            });
        }
    }

    void JsonGUI::addVScrollPanel(std::queue<sol::protected_function> & setupScripts, Widget * parent, Json::Value & root, Json::Reader & reader) {

        VScrollPanel * panel = parent->add<VScrollPanel>();

        bindToLua<VScrollPanel>(panel, root);
        readBasicWidgetProperties(setupScripts, panel, root, reader);

        Json::Value scroll = root["scroll"];
        if (scroll.isNumeric()) {
            panel->setScroll(scroll.asFloat());
        }
    }
}
