/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "GCObject.hpp"

#include <iostream>

namespace RM
{
    void GCObject::decRef(bool dealloc) const noexcept {
        --m_refCount;
        if (m_refCount == 0 && dealloc) {
            delete this;
        } else if (m_refCount < 0) {
            std::cout << "Internal error: Object reference count < 0!" << std::endl;
            abort();
        }
    }

    GCObject::~GCObject() { }
}
