/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "luaControls.hpp"

#include "globals.hpp"
#include "ResourceManager.hpp"
#include "ControlManager.hpp"

namespace luaControls {
    void loadControlEnum(sol::state_view lua, sol::table & cm) {

        {
            sol::table buttons(lua, sol::create);
            cm["buttons"] = buttons;

            sol::table computer(lua, sol::create);
            buttons["computer"] = computer;

            computer[" "]           = RM::KeyboardButtonID::Space;
            computer["'\""]      = RM::KeyboardButtonID::Apostrophe;
            computer[",<"]           = RM::KeyboardButtonID::Comma;
            computer["-_"]           = RM::KeyboardButtonID::Minus;
            computer[".>"]          = RM::KeyboardButtonID::Period;
            computer["/?"]           = RM::KeyboardButtonID::Slash;
            computer["0)"]              = RM::KeyboardButtonID::Button_0;
            computer["1!"]              = RM::KeyboardButtonID::Button_1;
            computer["2@"]              = RM::KeyboardButtonID::Button_2;
            computer["3#"]              = RM::KeyboardButtonID::Button_3;
            computer["4$"]              = RM::KeyboardButtonID::Button_4;
            computer["5%"]              = RM::KeyboardButtonID::Button_5;
            computer["6^"]              = RM::KeyboardButtonID::Button_6;
            computer["7&"]              = RM::KeyboardButtonID::Button_7;
            computer["8*"]              = RM::KeyboardButtonID::Button_8;
            computer["9("]              = RM::KeyboardButtonID::Button_9;
            computer[";:"]       = RM::KeyboardButtonID::Semicolon;
            computer["=+"]           = RM::KeyboardButtonID::Equal;
            computer["A"]               = RM::KeyboardButtonID::Button_A;
            computer["B"]               = RM::KeyboardButtonID::Button_B;
            computer["C"]               = RM::KeyboardButtonID::Button_C;
            computer["D"]               = RM::KeyboardButtonID::Button_D;
            computer["E"]               = RM::KeyboardButtonID::Button_E;
            computer["F"]               = RM::KeyboardButtonID::Button_F;
            computer["G"]               = RM::KeyboardButtonID::Button_G;
            computer["H"]               = RM::KeyboardButtonID::Button_H;
            computer["I"]               = RM::KeyboardButtonID::Button_I;
            computer["J"]               = RM::KeyboardButtonID::Button_J;
            computer["K"]               = RM::KeyboardButtonID::Button_K;
            computer["L"]               = RM::KeyboardButtonID::Button_L;
            computer["M"]               = RM::KeyboardButtonID::Button_M;
            computer["N"]               = RM::KeyboardButtonID::Button_N;
            computer["O"]               = RM::KeyboardButtonID::Button_O;
            computer["P"]               = RM::KeyboardButtonID::Button_P;
            computer["Q"]               = RM::KeyboardButtonID::Button_Q;
            computer["R"]               = RM::KeyboardButtonID::Button_R;
            computer["S"]               = RM::KeyboardButtonID::Button_S;
            computer["T"]               = RM::KeyboardButtonID::Button_T;
            computer["U"]               = RM::KeyboardButtonID::Button_U;
            computer["V"]               = RM::KeyboardButtonID::Button_V;
            computer["W"]               = RM::KeyboardButtonID::Button_W;
            computer["X"]               = RM::KeyboardButtonID::Button_X;
            computer["Y"]               = RM::KeyboardButtonID::Button_Y;
            computer["Z"]               = RM::KeyboardButtonID::Button_Z;
            computer["[{"]              = RM::KeyboardButtonID::LeftBracket;
            computer["\\|"]             = RM::KeyboardButtonID::Backslash;
            computer["]}"]   = RM::KeyboardButtonID::RightBracket;
            computer["`~"]              = RM::KeyboardButtonID::GraveAccent;
            computer["World 1"]         = RM::KeyboardButtonID::World_1;
            computer["World 2"]         = RM::KeyboardButtonID::World_2;
            computer["Escape"]          = RM::KeyboardButtonID::Escape;
            computer["Enter"]           = RM::KeyboardButtonID::Enter;
            computer["Tab"]             = RM::KeyboardButtonID::Tab;
            computer["Backspace"]       = RM::KeyboardButtonID::Backspace;
            computer["Insert"]          = RM::KeyboardButtonID::Insert;
            computer["Delete"]          = RM::KeyboardButtonID::Delete;
            computer["Right"]           = RM::KeyboardButtonID::Right;
            computer["Left"]            = RM::KeyboardButtonID::Left;
            computer["Down"]            = RM::KeyboardButtonID::Down;
            computer["Up"]              = RM::KeyboardButtonID::Up;
            computer["Page Up"]         = RM::KeyboardButtonID::PageUp;
            computer["Page Down"]       = RM::KeyboardButtonID::PageDown;
            computer["Home"]            = RM::KeyboardButtonID::Home;
            computer["End"]             = RM::KeyboardButtonID::End;
            computer["Caps Lock"]       = RM::KeyboardButtonID::CapsLock;
            computer["Scroll Lock"]     = RM::KeyboardButtonID::ScrollLock;
            computer["Num Lock"]        = RM::KeyboardButtonID::NumLock;
            computer["Print Screen"]    = RM::KeyboardButtonID::PrintScreen;
            computer["Key Pause"]       = RM::KeyboardButtonID::KeyPause;
            computer["F1"]              = RM::KeyboardButtonID::Button_F1;
            computer["F2"]              = RM::KeyboardButtonID::Button_F2;
            computer["F3"]              = RM::KeyboardButtonID::Button_F3;
            computer["F4"]              = RM::KeyboardButtonID::Button_F4;
            computer["F5"]              = RM::KeyboardButtonID::Button_F5;
            computer["F6"]              = RM::KeyboardButtonID::Button_F6;
            computer["F7"]              = RM::KeyboardButtonID::Button_F7;
            computer["F8"]              = RM::KeyboardButtonID::Button_F8;
            computer["F9"]              = RM::KeyboardButtonID::Button_F9;
            computer["F10"]             = RM::KeyboardButtonID::Button_F10;
            computer["F11"]             = RM::KeyboardButtonID::Button_F11;
            computer["F12"]             = RM::KeyboardButtonID::Button_F12;
            computer["F13"]             = RM::KeyboardButtonID::Button_F13;
            computer["F14"]             = RM::KeyboardButtonID::Button_F14;
            computer["F15"]             = RM::KeyboardButtonID::Button_F15;
            computer["F16"]             = RM::KeyboardButtonID::Button_F16;
            computer["F17"]             = RM::KeyboardButtonID::Button_F17;
            computer["F18"]             = RM::KeyboardButtonID::Button_F18;
            computer["F19"]             = RM::KeyboardButtonID::Button_F19;
            computer["F20"]             = RM::KeyboardButtonID::Button_F20;
            computer["F21"]             = RM::KeyboardButtonID::Button_F21;
            computer["F22"]             = RM::KeyboardButtonID::Button_F22;
            computer["F23"]             = RM::KeyboardButtonID::Button_F23;
            computer["F24"]             = RM::KeyboardButtonID::Button_F24;
            computer["F25"]             = RM::KeyboardButtonID::Button_F25;
            computer["KeyPad 0"]        = RM::KeyboardButtonID::KeyPad_0;
            computer["KeyPad 1"]        = RM::KeyboardButtonID::KeyPad_1;
            computer["KeyPad 2"]        = RM::KeyboardButtonID::KeyPad_2;
            computer["KeyPad 3"]        = RM::KeyboardButtonID::KeyPad_3;
            computer["KeyPad 4"]        = RM::KeyboardButtonID::KeyPad_4;
            computer["KeyPad 5"]        = RM::KeyboardButtonID::KeyPad_5;
            computer["KeyPad 6"]        = RM::KeyboardButtonID::KeyPad_6;
            computer["KeyPad 7"]        = RM::KeyboardButtonID::KeyPad_7;
            computer["KeyPad 8"]        = RM::KeyboardButtonID::KeyPad_8;
            computer["KeyPad 9"]        = RM::KeyboardButtonID::KeyPad_9;
            computer["KeyPad Decimal"]  = RM::KeyboardButtonID::KeyPad_Decimal;
            computer["KeyPad Divide"]   = RM::KeyboardButtonID::KeyPad_Divide;
            computer["KeyPad Multiply"] = RM::KeyboardButtonID::KeyPad_Multiply;
            computer["KeyPad Subtract"] = RM::KeyboardButtonID::KeyPad_Subtract;
            computer["KeyPad Add"]      = RM::KeyboardButtonID::KeyPad_Add;
            computer["KeyPad Enter"]    = RM::KeyboardButtonID::KeyPad_Enter;
            computer["KeyPad Equal"]    = RM::KeyboardButtonID::KeyPad_Equal;
            computer["Left Shift"]      = RM::KeyboardButtonID::LeftShift;
            computer["Left Control"]    = RM::KeyboardButtonID::LeftControl;
            computer["Left Alt"]        = RM::KeyboardButtonID::LeftAlt;
            computer["Left Super"]      = RM::KeyboardButtonID::LeftSuper;
            computer["Right Shift"]     = RM::KeyboardButtonID::RightShift;
            computer["Right Control"]   = RM::KeyboardButtonID::RightControl;
            computer["Right Alt"]       = RM::KeyboardButtonID::RightAlt;
            computer["Right Super"]     = RM::KeyboardButtonID::RightSuper;
            computer["Menu"]            = RM::KeyboardButtonID::Menu;

            computer["Mouse 1"]         = RM::KeyboardButtonID::Mouse1;
            computer["Mouse 2"]         = RM::KeyboardButtonID::Mouse2;
            computer["Mouse 3"]         = RM::KeyboardButtonID::Mouse3;
            computer["Mouse 4"]         = RM::KeyboardButtonID::Mouse4;
            computer["Mouse 5"]         = RM::KeyboardButtonID::Mouse5;
            computer["Mouse 6"]         = RM::KeyboardButtonID::Mouse6;
            computer["Mouse 7"]         = RM::KeyboardButtonID::Mouse7;
            computer["Mouse 8"]         = RM::KeyboardButtonID::Mouse8;
            computer["Mouse Last"]      = RM::KeyboardButtonID::MouseLast;
            computer["Mouse Left"]      = RM::KeyboardButtonID::MouseLeft;
            computer["Mouse Right"]     = RM::KeyboardButtonID::MouseRight;
            computer["Mouse Middle"]    = RM::KeyboardButtonID::MouseMiddle;
        }

        {
            sol::table analogs(lua, sol::create);
            cm["analogs"] = analogs;

            sol::table computer(lua, sol::create);
            analogs["computer"] = computer;

            computer["Mouse X"]           = RM::MouseAnalogID::axisX;
            computer["Mouse Y"]           = RM::MouseAnalogID::axisY;
            computer["Horizontal Scroll"] = RM::MouseAnalogID::scrollH;
            computer["Vertical Scroll"]   = RM::MouseAnalogID::scrollV;
        }
    }
}

void loadLuaControlsAPI(sol::table env) {

    sol::state_view lua = getResourceManager()->getStateOnly();
    sol::table cm = lua.create_table();
    env["cm"] = cm;

    cm.new_usertype<RM::ControlManager>("ControlManager",
                                        sol::base_classes, sol::bases<RM::GCObject>(),
                                         "new", sol::no_constructor);

    sol::table controlManager = cm["ControlManager"];
    controlManager["newBooleanControl"] = &RM::ControlManager::newBooleanControl;
    controlManager["addBooleanCallback"] = +[](RM::ControlManager & cm, std::string group, std::string name, sol::function func) {

        sol::bytecode bytecode = func.dump();

        RM::ref<nanogui::ListenerReference<void(bool pushed)>> reference = cm.addBooleanCallback(group, name, [bytecode](bool pressed)
        {
            sol::environment env;
            sol::state_view lua = getResourceManager()->getEnvironment("init", env);
            sol::function func = lua.load(bytecode.as_string_view());
            sol::set_environment(env, func);

            sol::protected_function_result result = func(pressed);
            if (!result.valid())
            {
                sol::error err = result;
                std::string what = "Error in control callback: ";
                what += err.what();
                throw std::runtime_error(what);
            }
        });

        return reference;
    };

    controlManager["newAnalogControl"] = &RM::ControlManager::newAnalogControl;
    controlManager["addAnalogCallback"] = +[](RM::ControlManager & cm, std::string group, std::string name, sol::function func) {

        sol::bytecode bytecode = func.dump();

        return cm.addAnalogCallback(group, name, [bytecode](float delta)
        {
            sol::environment env;
            sol::state_view lua = getResourceManager()->getEnvironment("init", env);
            sol::function func = lua.load(bytecode.as_string_view());
            sol::set_environment(env, func);

            sol::protected_function_result result = func(delta);
            if (!result.valid())
            {
                sol::error err = result;
                std::string what = "Error in control callback: ";
                what += err.what();
                throw std::runtime_error(what);
            }
        });
    };

    controlManager["removeBooleanCallback"] = +[](RM::ControlManager & cm, std::string group, std::string name, RM::ref<nanogui::ListenerReference<void(bool pushed)>> reference) {
        cm.removeBooleanCallback(group, name, reference);
    };

    controlManager["removeAnalogCallback"] = +[](RM::ControlManager & cm, std::string group, std::string name, RM::ref<nanogui::ListenerReference<void(float delta)>> reference) {
        cm.removeAnalogCallback(group, name, reference);
    };

    controlManager["openControlEditor"] = +[](RM::ControlManager & cm, sol::function callback) {
        cm.openControlEditor([callback]()
        {
            callback();
        });
    };

    luaControls::loadControlEnum(lua, cm);
    cm["manager"] = getControlManager();
}
