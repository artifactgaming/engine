/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "luaFormatting.hpp"
#include "EngineeringFormatter.hpp"

#include "luaSupport.hpp"

namespace LuaSupport {
    void loadLuaFormattingAPI(sol::state_view lua) {
        sol::table format = lua.create_table();
        lua["format"] = makeTableReadOnly(lua, format);

        format["engineering"] = +[](double value) {

            char * unitExt;
            std::string str = ENGFormatting::format(value, &unitExt);

            return std::tuple<std::string, std::string>(str, std::string(unitExt));
        };
    }
}
