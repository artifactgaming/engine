/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "luaWorld.hpp"
#include "luaGL.hpp"

#include "luaRM.hpp"

#include <unordered_map>

#include "World.hpp"

#include "BlockController.hpp"
#include "JSONBlockController.hpp"

#include "Block.hpp"
#include "BlockEntity.hpp"

#include "JSONDynamicEntityController.hpp"

#include "Path.hpp"

#include "globals.hpp"
#include "ControllerManager.hpp"

#include "TerrainProvider.hpp"
#include "TerrainChunk.hpp"

#include "TrueConcurrency.hpp"
#include "BinaryFileTools.hpp"

namespace luaWorld {

    class LuaTerrainProvider: public World::TerrainProvider
    {
        public:
        LuaTerrainProvider(std::string name,
                           std::vector<sol::function> & terrainProviders,
                           sol::function terrainSaver
        ):
            name('~' + name)
        {
            this->terrainProviders.reserve(terrainProviders.size());
            for (sol::function func : terrainProviders)
            {
                this->terrainProviders.push_back(LuaSupport::TrueConcurrency::dumpFunction(func));
            }

            this->terrainSaver = LuaSupport::TrueConcurrency::dumpFunction(terrainSaver);
        }

        ~LuaTerrainProvider()
        {
            std::cout << "Terrain Provider Disposed." << std::endl;
        }

        private:

        std::vector<std::string> terrainProviders;
        std::string terrainSaver;
        std::string name;

        struct TerrainGenerator: public LuaSupport::TrueConcurrency
        {
            World::World * world;
            World::TerrainRef chunk;
            std::vector<std::string> & funcDumps;

            TerrainGenerator(std::string name, World::TerrainRef chunk, std::vector<std::string> & funcDumps):
                LuaSupport::TrueConcurrency(name, "Lua Terrain Generator", nullptr),
                chunk(chunk),
                funcDumps(funcDumps)
            {
                world = chunk->getWorld();
                world->incRef();

                this->setResourceStepsToLoad(1);
                this->startLoad();
            }

            void async_load()
            {
                sol::environment env_core;
                sol::state_view lua = manager->getEnvironment("Terrain Management", env_core);
                sol::environment env(lua, sol::create, env_core);

                bool finishedGeneration = false;

                for (std::string funcDump : funcDumps)
                {
                    sol::function func = loadString(lua, funcDump);
                    sol::set_environment(env, func);

                    try
                    {
                        sol::protected_function_result result = func(chunk.get(), chunk->getWorld());

                        if (result.valid())
                        {
                            finishedGeneration = result;
                            if (finishedGeneration)
                            {
                                break;
                            }
                        }
                        else
                        {
                            sol::error err = result;
                            std::string what = err.what();

                            std::cerr << "Error generating chunk " << resourceName() << " at "
                                      << chunk->getNativeX() << ", "
                                      << chunk->getNativeY() << ", "
                                      << chunk->getNativeZ()
                                      << ":\n" << what << std::endl;
                        }
                    } catch (sol::error & e) {
                        std::cerr << e.what() << std::endl;
                    }
                }

                if (!finishedGeneration)
                {
                    std::cout << "Warning: Chunk " << std::hex << chunk->getHashCode() << std::dec << " was never marked for terrain generation having been finished."
                              << "This could be caused by all terrain generator functions failing or by the last terrain generator function forgetting to return a value."
                              << std::endl;
                }

                chunk->setTerrainGenerated();
                this->incrementResourceProgress(1);
            }

            void sync_load()
            {
                world->decRef();
            }
        };

        struct TerrainSaver: public LuaSupport::TrueConcurrency
        {
            World::World * world;
            World::TerrainRef chunk;
            std::string funcDump;

            TerrainSaver(std::string name, World::TerrainRef chunk, std::string funcDump):
                LuaSupport::TrueConcurrency(name, "Lua Terrain Saver", nullptr),
                chunk(chunk),
                funcDump(funcDump)
            {
                world = chunk->getWorld();
                world->incRef();

                this->setResourceStepsToLoad(1);
                this->startLoad();
            }

            void async_load()
            {
                sol::environment env_core;
                sol::state_view lua = manager->getEnvironment("Terrain Management", env_core);
                sol::environment env(lua, sol::create, env_core);

                sol::function func = loadString(lua, funcDump);
                sol::set_environment(env, func);

                try
                {
                    sol::protected_function_result result = func(chunk.get(), world);

                    if (!result.valid())
                    {
                        sol::error err = result;
                        std::string what = err.what();

                        std::cerr << "Error saving chunk " << resourceName() << " at "
                                  << chunk->getNativeX() << ", "
                                  << chunk->getNativeY() << ", "
                                  << chunk->getNativeZ()
                                  << ":\n" << what << std::endl;
                    }
                } catch (sol::error & e) {
                    std::cerr << e.what() << std::endl;
                }

                this->incrementResourceProgress(1);
            }

            void sync_load()
            {
                world->decRef();
            }
        };

        std::deque<World::TerrainRef> chunks;

        void generateBlocks(World::TerrainChunk * chunk)
        {
            new TerrainGenerator(name, chunk, terrainProviders);
        }

        void saveBlocks(World::TerrainChunk * chunk)
        {
            new TerrainSaver(name, chunk, terrainSaver);
        }
    };

    inline RM::ref<World::World> worldFactory1(RM::Path path, const std::string & name,
        sol::table block_controllers,
        sol::table terrain_providers,
        sol::function terrain_saver
        )
    {

        std::vector<RM::ref<World::BlockController>> block_controllers_raw;
        block_controllers_raw.reserve(block_controllers.size());

        for (auto controller : block_controllers)
        {
            block_controllers_raw.push_back(controller.second.as<World::BlockController*>());
        }

        std::vector<sol::function> terrainProviders;
        terrainProviders.reserve(terrain_providers.size());

        for (auto provider : terrain_providers)
        {
            terrainProviders.push_back(provider.second.as<sol::function>());
        }

        LuaTerrainProvider * terrainProvider = new LuaTerrainProvider(name, terrainProviders, terrain_saver);

        RM::ref<World::World> world = new World::World(path, name, block_controllers_raw, terrainProvider);
        return world;
    }

    RM::ref<World::World> worldFactory2(std::string path, const std::string & name,
        sol::table block_controllers,
        sol::table terrain_providers,
        sol::function terrain_saver
        )
    {
        return worldFactory1(path, name, block_controllers, terrain_providers, terrain_saver);
    }

    RM::ref<World::BlockController> getBlockController1(sol::this_state lua, std::string path)
    {
        RM::ref<World::BlockController> result = getControllerManager()->getBlockController(path);
        return result;
    }

    RM::ref<World::BlockController> getBlockController2(sol::this_state lua, RM::Path path)
    {
        RM::ref<World::BlockController> result = getControllerManager()->getBlockController(path);
        return result;
    }

    RM::ref<World::DynamicEntityController> getDynamicEntityController1(sol::this_state lua, std::string path)
    {
        RM::ref<World::DynamicEntityController> result = getControllerManager()->getDynamicEntityController(path);
        return result;
    }

    RM::ref<World::DynamicEntityController> getDynamicEntityController2(sol::this_state lua, RM::Path path)
    {
        RM::ref<World::DynamicEntityController> result = getControllerManager()->getDynamicEntityController(path);
        return result;
    }

    class LuaBlockInterface
    {
        public:
            LuaBlockInterface(World::Block block, World::BlockController * controller):
                block(block)
            {
                World::JSONBlockController * jsonController = dynamic_cast<World::JSONBlockController*>(controller);
                if (jsonController)
                {
                    this->controller = jsonController;
                }
                else
                {
                    throw std::runtime_error("Only JSON block controllers are supported right now.");
                }
            }

            sol::object getter(sol::stack_object key, sol::this_state L) {
                sol::table table = controller->getFunctions();
                return sol::object(L, sol::in_place, table[key]);
            }

            void setter(sol::stack_object key, sol::stack_object rawValue, sol::this_state) {
                throw std::runtime_error("You can not directly set values of a block interface. Its not supported yet.");
            }

            World::Block getBlock()
            {
                return block;
            }

            void setBitsSigned(std::string name, int value)
            {
                block = controller->setMetaValue(block, name, value);
            }

            void setBitsUnsigned(std::string name, unsigned int value)
            {
                block = controller->setMetaValue(block, name, value);
            }

            int getBitsSigned(std::string name)
            {
                return controller->getMetaValue(block, name);
            }

            unsigned int getBitsUnsigned(std::string name)
            {
                return controller->getMetaValue(block, name);
            }

        protected:
            RM::ref<World::JSONBlockController> controller;
            World::Block block;
        private:
    };

    namespace DynamicEntity
    {
        sol::object getter(World::DynamicEntity & entity, sol::stack_object key, sol::this_state lua) {
            RM::ref<World::JSONDynamicEntityController> controller = dynamic_cast<World::JSONDynamicEntityController*>(entity.getController());

            if (controller)
            {
                sol::object obj = entity.userdata->getValue(key, lua);
                if (obj.valid())
                {
                    return obj;
                }
                else
                {
                    sol::table table = controller->getFunctions();
                    return sol::object(lua, sol::in_place, table[key]);
                }
            }
            else
            {
                return sol::object(lua, sol::in_place, sol::lua_nil);
            }
        }

        void setter(World::DynamicEntity & entity, sol::stack_object key, sol::stack_object rawValue, sol::this_state lua) {
            entity.userdata->setValue(key, rawValue, lua);
        }
    }

    sol::table readWorldHeader(sol::state_view lua, RM::Path headerFile)
    {
        // If it exists, we keep a backup of the old header just in case things go wrong.
        if (!headerFile.exists())
        {
            // TODO Look for the header file backup.
            throw std::runtime_error("Could not find world's header file.");
        }

        std::string input;

        boost::filesystem::ifstream file(headerFile.getBoostPath(), std::ios::in | std::ios::binary);
        if (!file) {
            throw std::runtime_error("Failed to open world header file.");
        }

        uint16_t version;
        BinTools::readStruct(file, version);

        std::string name;
        BinTools::readStruct(file, name);

        double time;
        BinTools::readStruct(file, time);

        sol::table header = lua.create_table();
        header["version"] = version;
        header["name"] = name;
        header["time"] = time;

        return header;
    }

    void loadLuaWorldCoreAPI(sol::state_view lua, sol::table env)
    {
        env.new_usertype<World::Block>("Block");

        sol::table Hdirections = lua.create_table();
        Hdirections["north"] = 0;
        Hdirections["east"] = 1;
        Hdirections["south"] = 2;
        Hdirections["west"] = 3;
        env["Block"]["Hdirections"] = Hdirections;

        sol::table Vdirections = lua.create_table();
        Vdirections["0"] = 0;
        Vdirections["90"] = 1;
        Vdirections["180"] = 2;
        Vdirections["270"] = 3;
        env["Block"]["Vdirections"] = Vdirections;

        env.new_usertype<World::BlockController>(
            "BlockController",
            sol::base_classes, sol::bases<RM::BasicResource, RM::GCObject>(),
            "get", sol::overload(&luaWorld::getBlockController1, &luaWorld::getBlockController2)
        );

        env.new_usertype<World::DynamicEntityController>(
            "DynamicEntityController",
            sol::base_classes, sol::bases<RM::BasicResource, RM::GCObject>(),
            "get", sol::overload(&luaWorld::getDynamicEntityController1, &luaWorld::getDynamicEntityController2)
        );

        env.new_usertype<World::TerrainChunk>(
            "TerrainChunk",
            "saveToFile", &World::TerrainChunk::saveTerrainToFile,
            "loadFromFile", &World::TerrainChunk::loadTerrainFromFile,
            "getNativeX", &World::TerrainChunk::getNativeX,
            "getNativeY", &World::TerrainChunk::getNativeY,
            "getNativeZ", &World::TerrainChunk::getNativeZ,
            "getNativeX_unsigned", &World::TerrainChunk::getNativeX_unsigned,
            "getNativeY_unsigned", &World::TerrainChunk::getNativeY_unsigned,
            "getNativeZ_unsigned", &World::TerrainChunk::getNativeZ_unsigned,

            "getVisualRef", &World::TerrainChunk::getVisualRef,

            "getBlockLocal_safe", &World::TerrainChunk::getBlockLocal_safe,
            "setBlockLocal_unsafe", +[](World::TerrainChunk & self, uint8_t x, uint8_t y, uint8_t z, luaWorld::LuaBlockInterface & block)
            {
                self.setBlockLocal_unsafe(x, y, z, block.getBlock());
            },
            "setBlockGlobal_unsafe", +[](World::TerrainChunk & self, uint8_t x, uint8_t y, uint8_t z, luaWorld::LuaBlockInterface & block)
            {
                self.setBlockGlobal_unsafe(x, y, z, block.getBlock());
            },
            "setBlockLocal_safe", +[](World::TerrainChunk & self, int32_t x, int32_t y, int32_t z, luaWorld::LuaBlockInterface & block)
            {
                self.setBlockLocal_safe(x, y, z, block.getBlock());
            },
            "setBlockGlobal_safe", +[](World::TerrainChunk & self, int32_t x, int32_t y, int32_t z, luaWorld::LuaBlockInterface & block)
            {
                self.setBlockGlobal_safe(x, y, z, block.getBlock());
            }
        );

        env.new_usertype<luaWorld::LuaBlockInterface>(
            "BlockInterface",
            sol::meta_function::index, &luaWorld::LuaBlockInterface::getter,
            sol::meta_function::new_index, &luaWorld::LuaBlockInterface::setter,
            "setBitsUnsigned", &luaWorld::LuaBlockInterface::setBitsUnsigned,
            "getBitsUnsigned", &luaWorld::LuaBlockInterface::getBitsUnsigned,
            "setBitsSigned", &luaWorld::LuaBlockInterface::setBitsSigned,
            "getBitsSigned", &luaWorld::LuaBlockInterface::getBitsSigned
        );

        env.new_usertype<World::DynamicEntity>(
            "DynamicEntity",
            sol::meta_function::index, &luaWorld::DynamicEntity::getter,
            sol::meta_function::new_index, &luaWorld::DynamicEntity::setter,
            "getWorld", &World::DynamicEntity::getWorld
        );

        env.new_usertype<World::World>(
            "World",
            sol::base_classes, sol::bases<RM::BasicResource, RM::GCObject>(),
            "new", sol::factories(&luaWorld::worldFactory1, &luaWorld::worldFactory2),
            "renderTerrain", &World::World::renderTerrain,
            "save", &World::World::save,
            "loadEntities", &World::World::loadEntities,
            "spawnDynamicEntity", &World::World::spawnDynamicEntity,
            "createBlock", +[](World::World & world, std::string name)
            {
                // TODO make the world's create block function return the block controller too.
                World::Block block = world.createBlock(name);
                return luaWorld::LuaBlockInterface(block, getControllerManager()->getBlockController(name));
            },
            "getFolder", &World::World::getFolder,
            "getUserdata", &World::World::getUserdata,

            "getChunkNativeCoords_unsafe", &World::World::getChunkNativeCoords_unsafe,
            "getChunkBlockCoords_unsafe", &World::World::getChunkBlockCoords_unsafe,
            "getChunkNativeCoords_safe", &World::World::getChunkNativeCoords_safe,
            "getChunkBlockCoords_safe", &World::World::getChunkBlockCoords_safe
        );

        env["readWorldHeader"] = sol::overload(
            +[](sol::this_state lua, std::string path)
            {
               return luaWorld::readWorldHeader(lua, path);
            },
            +[](sol::this_state lua, RM::Path path)
            {
               return luaWorld::readWorldHeader(lua, path);
            }
        );
    }

    sol::environment terrainManagementLuaLoader(sol::state_view lua)
    {
        sol::environment env(lua, sol::create, lua.globals());
        sol::table world = lua.create_table();
        env["world"] = world;

        loadLuaWorldCoreAPI(lua, world);
        // FIXME not everything in the lua GL API should be accessable from this environment. We need to sandbox this to only a few basic types.
        loadLuaGLAPI(env);

        return env;
    };
}

void loadLuaWorldAPI(sol::table env) {
    sol::state_view lua = getResourceManager()->getStateOnly();
    sol::table world = lua.create_table();
    env["world"] = world;

    luaWorld::loadLuaWorldCoreAPI(lua, world);
}

void registerWorldEnvs()
{
    getResourceManager()->registerEnvironment("Terrain Management",
        luaWorld::terrainManagementLuaLoader
    );
}
