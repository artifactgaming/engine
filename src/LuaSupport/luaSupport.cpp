/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "LuaSupport/luaSupport.hpp"

#include "globals.hpp"
#include "ResourceManager.hpp"

#include <sol.hpp>
#include <boost/thread/mutex.hpp>
#include <string>

// Phase 1 APIs
#include "luaPlatform.hpp"
#include "StateFreeTable.hpp"
#include "GLM/luaGLM.hpp"
#include "luaFilesystem.hpp"
#include "luaSerial.hpp"
#include "luaNative.hpp"
#include "luaFormatting.hpp"
#include "luaWorld.hpp"

// Phase 2 APIs
#include "GUI/luaGUI.hpp"
#include "luaControls.hpp"
#include "RM/luaRM.hpp"
#include "luaGL.hpp"

#define MAIN_THREAD_INSTRUCTION_LIMIT 100000

// This code was shamelessly borrowed from the original Lua implementation for our
// dirty purposes. It was also reformatted to better fit in with the rest of this document.
namespace luaBorrowed {

    boost::mutex mutex;

    inline lua_State* getthread (lua_State * lua, int * arg) {
        if (lua_isthread(lua, 1)) {
            *arg = 1;
            return lua_tothread(lua, 1);
        } else {
            *arg = 0;
            return lua;
        }
    }

    #define LEVELS1	12	// Size of the first part of the stack.
    #define LEVELS2	10	// Size of the second part of the stack.

    inline int db_errorfb (lua_State * L) {
        int level;
        int firstpart = 1;  // Still before eventual `...'.
        int arg;
        lua_State *L1 = getthread(L, &arg);
        lua_Debug ar;

        if (lua_isnumber(L, arg+2)) {
            level = (int)lua_tointeger(L, arg+2);
            lua_pop(L, 1);
        } else {
            level = (L == L1) ? 1 : 0; // Level 0 may be this own function.

        }

        if (lua_gettop(L) == arg) {
            lua_pushliteral(L, "");
        } else if (!lua_isstring(L, arg+1)) { // Message is not a string.
            return 1;
        } else {
            lua_pushliteral(L, "\n");
        }

        while (lua_getstack(L1, level++, &ar)) {
            if (level > LEVELS1 && firstpart) {
                // No more than `LEVELS2' more levels?
                if (!lua_getstack(L1, level+LEVELS2, &ar)) {
                    level--; // Keep going
                } else {
                    lua_pushliteral(L, "\n\t...");  // Too many levels.

                    while (lua_getstack(L1, level+LEVELS2, &ar)) { // find last levels.
                        level++;
                    }
                }

                firstpart = 0;
                continue;
            }

            lua_pushliteral(L, "\n\t");
            lua_getinfo(L1, "Snl", &ar);
            lua_pushfstring(L, "%s:", ar.short_src);

            if (ar.currentline > 0) {
                lua_pushfstring(L, "%d:", ar.currentline);
            }

            if (*ar.namewhat != '\0') { // Is there a name?
                lua_pushfstring(L, " in function " LUA_QS, ar.name);
            } else {
                if (*ar.what == 'm') { // Main?
                    lua_pushfstring(L, " in main chunk");
                } else if (*ar.what == 'C' || *ar.what == 't') { // C function or tail call
                    lua_pushliteral(L, " ?");
                } else {
                    lua_pushfstring(L, " in function <%s:%d>", ar.short_src, ar.linedefined);
                }
            }

            lua_concat(L, lua_gettop(L) - arg);
        }

        lua_concat(L, lua_gettop(L) - arg);
        return 1;
    }

    static int luaB_print (lua_State *L) {
        int n = lua_gettop(L);  /* number of arguments */
        int i;
        lua_getglobal(L, "tostring");

        // I used Boost's tee_device implementation tends to be a little unstable with threads, so this is to help improve that.
        boost::mutex::scoped_lock lock(mutex);

        for (i=1; i<=n; i++) {
            const char *s;
            lua_pushvalue(L, -1);  /* function to be called */
            lua_pushvalue(L, i);   /* value to print */
            lua_call(L, 1, 1);
            s = lua_tostring(L, -1);  /* get result */

            if (s == NULL) {
                return luaL_error(L, LUA_QL("tostring") " must return a string to "
                LUA_QL("print"));
            }

            if (i>1) {
                std::cout << '\t';
            }

            std::cout << s;

            lua_pop(L, 1);  /* pop result */
        }

        std::cout << std::endl;
        return 0;
    }
}

namespace LuaSupport {

    sol::table makeTableReadOnly(sol::state_view lua, sol::table & obj_metatable) {
        obj_metatable[sol::meta_function::new_index] = +[](lua_State* L)
        {
            return luaL_error(L, "Attempt to write to read only table.");
        };
        obj_metatable[sol::meta_function::index] = obj_metatable;

        sol::table table = lua.create_table();
        table[sol::metatable_key] = obj_metatable;

        return table;
    }

    std::string getStacktrace(lua_State * lua) {

        luaBorrowed::db_errorfb(lua);
        std::string traceback = lua_tostring(lua, -1);
        return traceback;
    }

    int error_handler(lua_State * lua, sol::protected_function_result pfr) {
        std::string traceback = getStacktrace(lua);
        sol::error err = pfr;

        std::cout << "Lua error: " << err.what() << "\n" << traceback << std::endl;

        return 0;
    }

    int exception_handler(lua_State * lua, sol::optional<const std::exception&> maybe_exception, sol::string_view description) {
        std::cout << "An exception occurred in a within Lua." << std::endl;
        if (maybe_exception) {
            std::cout << "(straight from the exception): ";
            const std::exception& ex = *maybe_exception;
            std::cout << ex.what() << std::endl;
        }
        else {
            std::cout << "(from the description parameter): ";
            std::cout.write(description.data(), description.size());
            std::cout << std::endl;
        }

        std::string traceback = getStacktrace(lua);
        std::cout << "Stack Trace:" << traceback << std::endl;

        return sol::stack::push(lua, description);
    }

    void timeout_handler(lua_State * lua, lua_Debug * ar) {
        if(ar->event == LUA_HOOKCOUNT) {
            lua_pushstring(lua, "Lua script took too long to return.");
            lua_error(lua);
        }
    }

    void loadCoreAPI()
    {
        sol::state_view lua = getResourceManager()->getStateOnly();

        lua.open_libraries(sol::lib::base, sol::lib::string, sol::lib::table, sol::lib::math);

        if (isDeveloperMode()) {
            lua.open_libraries(sol::lib::debug);
        }

        //lua.set_panic(&error_handler);
        //lua.set_error(&error_handler);
        //lua.set_exception_handler(&exception_handler);

        // math.random() can create some serious problems for multi-player support, so we get rid of it.
        lua["math"]["random"] = sol::lua_nil;

        lua["dofile"] = sol::overload(
            +[](std::string file_path, sol::this_environment te)
            {
                sol::state_view lua = getResourceManager()->getStateOnly();

                RM::Path file(file_path);
                if (file.isLegal())
                {
                    if (te) {
                        sol::environment & env = te;
                        lua.safe_script_file(file.string(), env);
                    }
                }
                else
                {
                    throw std::runtime_error("Attempted to run script outside of engine's sandbox.");
                }
            },
            +[](RM::Path file, sol::this_environment te)
            {
                sol::state_view lua = getResourceManager()->getStateOnly();

                if (file.isLegal())
                {
                    if (te) {
                        sol::environment & env = te;
                        lua.safe_script_file(file.string(), env);
                    }
                }
                else
                {
                    throw std::runtime_error("Attempted to run script outside of engine's sandbox.");
                }
            }
        );

        lua["loadfile"] = sol::overload(
        +[](sol::this_state lua, std::string path) {
            if (RM::Path(path).exists()) {
                sol::state_view view(lua);

                sol::load_result script = view.load_file(path);
                return script.get<sol::object>();

            } else {
                throw std::runtime_error("Path does not exist.");
            }
        },
        +[](sol::this_state lua, RM::Path & path) {
            if (path.exists()) {
                sol::state_view view(lua);

                sol::load_result script = view.load_file(path.string());
                return script.get<sol::object>();

            } else {
                throw std::runtime_error("Path does not exist.");
            }
        });

        lua["print"] = &luaBorrowed::luaB_print;
        lua["shutdown"] = +[]() {
            shutdown();
        };

        // Load libraries that are universal to all Lua states.
        luaSupport::loadStatefreeTable(lua);
        loadLuaPlatformAPI(lua);
        loadLuaGLMAPI(lua);
        loadLuaFilesystemAPI(lua);
        loadLuaSerialAPI(lua);
        loadLuaNativeAPI(lua);
        loadLuaFormattingAPI(lua);
        loadLuaResourceManagement(lua);

        // A hung Lua script will crash because of this.
        resetScriptTimeout(lua);
    }

    void loadClientAPI() {

        getResourceManager()->registerEnvironment("ClientAPI",
            [](sol::state_view lua)
            {
                sol::environment env(lua, sol::create, lua.globals());

                loadLuaWorldAPI(env);
                loadLuaGUIAPI(env);
                loadLuaControlsAPI(env);
                loadLuaGLAPI(env);

                return env;
            }
        );
    }

    void startGame() {

        // Setup the Root Environment.
        getResourceManager()->registerEnvironment(
            "init",
            [](sol::state_view lua)
            {
                sol::environment clientAPI;
                getResourceManager()->getEnvironment("ClientAPI", clientAPI);

                return sol::environment(lua, sol::create, clientAPI);
            }
        );

        sol::environment initEnv;
        sol::state_view lua = getResourceManager()->getEnvironment("init", initEnv);

        lua.safe_script_file("main/init.lua", initEnv);

        if (initEnv["onStart"] == sol::lua_nil) {
            throw std::runtime_error("Function \"onStart()\" was not defined in \"main/init.lua\"");
        }

        if (initEnv["onShutdown"] == sol::lua_nil) {
            throw std::runtime_error("Function \"onShutdown()\" was not defined in \"main/init.lua\"");
        }

        sol::protected_function onStart = initEnv["onStart"];

        sol::protected_function_result result = onStart();
        if (!result.valid()) {
            sol::error err = result;
            std::string what = err.what();
            throw std::runtime_error(what);
        }
    }

    void shutdownGame() {
        sol::environment initEnv;
        sol::state_view lua = getResourceManager()->getEnvironment("init", initEnv);

        sol::protected_function onShutdown = initEnv["onShutdown"];

        sol::protected_function_result result = onShutdown();
        if (!result.valid()) {
            sol::error err = result;
            std::string what = err.what();
            throw std::runtime_error(what);
        }
    }

    void resetScriptTimeout(sol::state_view lua) {
        lua_sethook(lua, &timeout_handler, LUA_MASKCOUNT, MAIN_THREAD_INSTRUCTION_LIMIT);
    }
}
