/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "globals.hpp"

#include "luaGL.hpp"
#include "luaRM.hpp"
#include "luaSupport.hpp"

#include "View.hpp"
#include "ShaderProgram.hpp"
#include "RenderTarget.hpp"

#include "TextureSource.hpp"
#include "LocalFile.hpp"

#include "Sampler2D.hpp"

#include <nanogui/widget.h>

namespace luaGL {

    static RM::ref<GL::RenderTarget> renderTargetFactory(nanogui::Widget * parent, sol::function func, sol::this_state lua) {

        std::function<void(GL::RenderTarget * target)> render = [func](GL::RenderTarget * target)
        {
            func(target);
        };

        RM::ref<GL::RenderTarget> result = new GL::RenderTarget(parent, render);
        return result;
    }

    void loadRenderTarget(sol::table & lua) {

        lua.new_usertype<GL::RenderTarget>(
            "RenderTarget",
            sol::base_classes, sol::bases<nanogui::Widget, RM::GCObject>(),
            "new", sol::factories(&renderTargetFactory)
        );

        // TODO add way to change background color.
    }

    inline RM::ref<GL::ShaderProgram> shaderProgramFactory1(RM::ResourceGroup * group, sol::table shaders, sol::this_state lua) {
        GL::ShaderFileList files;

        for (auto f : shaders) {
            auto str_ref = f.second.as<sol::optional<std::string>>();

            if (str_ref) {
                const std::string& str = *str_ref;
                files.push_back(RM::Path(str));
                continue;
            }

            auto path_ref = f.second.as<sol::optional<RM::Path>>();

            if (path_ref) {
                const RM::Path& path = *path_ref;
                files.push_back(RM::Path(path));
                continue;
            }

            throw std::runtime_error("Shader file paths must be strings or path objects.");
        }

        RM::ref<GL::ShaderProgram> result = new GL::ShaderProgram(group, luaRM::assumeNameForResource(lua), files);
        return result;
    }

    RM::ref<GL::ShaderProgram> shaderProgramFactory2(sol::table shaders, sol::this_state lua) {
        return shaderProgramFactory1(nullptr, shaders, lua);
    }

    void loadShaderProgram(sol::table & lua) {

        lua.new_usertype<GL::ShaderProgram>(
            "ShaderProgram",
            sol::base_classes, sol::bases<RM::BasicResource, RM::GCObject>(),
            "new", sol::factories(
                &shaderProgramFactory1,
                &shaderProgramFactory2
            )
        );

        lua.new_usertype<GL::ShaderProgramScope>(
            "ShaderProgramScope",
            "new", sol::no_constructor,
            sol::base_classes, sol::bases<RM::GCObject>(),
            "drawVertexBuffers", &GL::ShaderProgramScope::drawVertexBuffers,
            "setFloat", &GL::ShaderProgramScope::setFloat,
            "setVec2", &GL::ShaderProgramScope::setVec2,
            "setVec3", &GL::ShaderProgramScope::setVec3,
            "setVec4", &GL::ShaderProgramScope::setVec4,

            "setMatrix2x2", &GL::ShaderProgramScope::setMatrix2x2,
            "setMatrix3x3", &GL::ShaderProgramScope::setMatrix3x3,
            "setMatrix4x4", &GL::ShaderProgramScope::setMatrix4x4,

            "setSampler", &GL::ShaderProgramScope::setSampler,

            "printDebug", &GL::ShaderProgramScope::printDebug
        );

        lua["ScopedCall"] = +[](GL::RenderTarget * target, GL::ShaderProgram & program, sol::protected_function func, sol::this_state lua) {
            GL::ShaderProgramScope scope(target, &program);

            sol::protected_function_result result = func(target, scope);
            if (!result.valid()) {
                sol::error err = result;
                std::string what = err.what();
                std::cerr << "Error in Scoped Call \"" << luaRM::assumeNameForResource(lua) << "\":\n" << what << std::endl;
            }
        };
    }

    RM::ref<GL::TextureSource::LocalFile> LocalFileTextureSourceFactory1(RM::Path path, sol::this_state lua) {
        return RM::ref<GL::TextureSource::LocalFile>(new GL::TextureSource::LocalFile(nullptr, path, luaRM::assumeNameForResource(lua)));
    }

    RM::ref<GL::TextureSource::LocalFile> LocalFileTextureSourceFactory2(RM::ResourceGroup * group, RM::Path path, sol::this_state lua) {
        return RM::ref<GL::TextureSource::LocalFile>(new GL::TextureSource::LocalFile(group, path, luaRM::assumeNameForResource(lua)));
    }

    RM::ref<GL::TextureSource::LocalFile> LocalFileTextureSourceFactory3(std::string path, sol::this_state lua) {
        return RM::ref<GL::TextureSource::LocalFile>(new GL::TextureSource::LocalFile(nullptr, path, luaRM::assumeNameForResource(lua)));
    }

    RM::ref<GL::TextureSource::LocalFile> LocalFileTextureSourceFactory4(RM::ResourceGroup * group, std::string path, sol::this_state lua) {
        return RM::ref<GL::TextureSource::LocalFile>(new GL::TextureSource::LocalFile(group, path, luaRM::assumeNameForResource(lua)));
    }

    RM::ref<GL::Sampler2D> Sampler2DFactory1(RM::ResourceGroup * group, GL::TextureSource::TextureSource * source, sol::this_state lua) {
        return RM::ref<GL::Sampler2D>(new GL::Sampler2D(group, source, luaRM::assumeNameForResource(lua)));
    }

    RM::ref<GL::Sampler2D> Sampler2DFactory2(GL::TextureSource::TextureSource * source, sol::this_state lua) {
        return RM::ref<GL::Sampler2D>(new GL::Sampler2D(nullptr, source, luaRM::assumeNameForResource(lua)));
    }

    void loadTextures(sol::table & lua) {
        lua.new_usertype<GL::TextureSource::TextureSource>(
            "TextureSource",
            sol::base_classes, sol::bases<RM::GCObject>(),
            "new", sol::no_constructor,
            "getWidth", &GL::TextureSource::TextureSource::getWidth,
            "getHeight", &GL::TextureSource::TextureSource::getHeight,

            "reload", &GL::TextureSource::TextureSource::reload,
            "clear", &GL::TextureSource::TextureSource::clear,
            "isLoaded", &GL::TextureSource::TextureSource::isLoaded
        );

        lua.new_usertype<GL::TextureSource::LocalFile>(
            "LocalFile",
            sol::base_classes, sol::bases<GL::TextureSource::TextureSource, RM::GCObject>(),
            "new", sol::factories(
                &LocalFileTextureSourceFactory1,
                &LocalFileTextureSourceFactory2,
                &LocalFileTextureSourceFactory3,
                &LocalFileTextureSourceFactory4
            )
        );
    }

    void loadSamplers(sol::table & lua) {
        lua.new_usertype<GL::Sampler>(
            "Sampler",
            sol::base_classes, sol::bases<RM::GCObject>(),
            "new", sol::no_constructor
        );

        lua.new_usertype<GL::Sampler2D>(
            "Sampler2D",
            sol::base_classes, sol::bases<GL::Sampler, RM::GCObject>(),
            "new", sol::factories(
                &Sampler2DFactory1,
                &Sampler2DFactory2
            )
        );
    }

    void loadView(sol::table & lua) {
        lua.new_usertype<GL::View>(
            "View",
            "new", LuaSupport::WrappedConstructorList<GL::View(GL::ShaderProgram&)>(),
            sol::base_classes, sol::bases<RM::GCObject>(),
            "setProjectionMatrix", &GL::View::setProjectionMatrix,
            "setViewMatrix",       &GL::View::setViewMatrix,
            "getProjectionMatrix", &GL::View::getProjectionMatrix,
            "getViewMatrix",       &GL::View::getViewMatrix,
            "getCombinedMatrix",   &GL::View::getCombinedMatrix,
            "getScale",            &GL::View::getScale,
            "getRotation",         &GL::View::getRotation,
            "getTranslation",      &GL::View::getTranslation,
            "getSkew",             &GL::View::getSkew,
            "getPerspective",      &GL::View::getPerspective,
            "inViewport",          &GL::View::inViewport
        );
    }
}

SOL_BASE_CLASSES(GL::ShaderProgram, RM::GCObject);
SOL_DERIVED_CLASSES(RM::GCObject, GL::ShaderProgram);

void loadLuaGLAPI(sol::table env) {
    sol::state_view lua = getResourceManager()->getStateOnly();
    sol::table GL = lua.create_table();
    env["GL"] = GL;

    luaGL::loadVBOs(GL, lua);
    luaGL::loadShaderProgram(GL);
    luaGL::loadRenderTarget(GL);
    luaGL::loadSamplers(GL);
    luaGL::loadView(GL);

    sol::table TextureSources = lua.create_table();
    GL["TextureSources"] = TextureSources;
    luaGL::loadTextures(TextureSources);
}
