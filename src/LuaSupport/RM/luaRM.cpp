/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "luaRM.hpp"

#include "globals.hpp"
#include "BasicResource.hpp"
#include "AsyncResource.hpp"
#include "ResourceGroup.hpp"
#include "AsyncTester.hpp"
#include "AsyncLuaTester.hpp"

namespace luaRM {
    void loadBasicResource(sol::table & rm) {

        rm.new_usertype<RM::GCObject>(
            "GCObject"
        );

        rm.new_usertype<RM::BasicResource>(
            "BasicResource",
            sol::base_classes, sol::bases<RM::GCObject>(),
            "resourceName",              &RM::BasicResource::resourceName,
            "resourceType",              &RM::BasicResource::resourceType,
            "getGroup",                  &RM::BasicResource::getGroup,
            "getResourceStepsToLoad",    &RM::BasicResource::getResourceStepsToLoad,
            "getResourceStepsCompleted", &RM::BasicResource::getResourceStepsCompleted,

            "setProgressCallback", +[](RM::BasicResource & resource, sol::function callback)
            {
                resource.setProgressCallback(
                    [callback](unsigned int progress)
                    {
                        callback(progress);
                    }
                );
            },
            "setCompletionCallback", +[](RM::BasicResource & resource, sol::function callback)
            {
                resource.setCompletionCallback(
                    [callback]()
                    {
                        callback();
                    }
                );
            }
        );

        rm.new_usertype<RM::AsyncResource>(
            "AsyncResource",
            sol::base_classes, sol::bases<RM::BasicResource, RM::GCObject>()
        );
    }

    static RM::ref<RM::ResourceGroup> groupFactory(sol::this_state lua, std::string name) {
        sol::state_view view(lua);
        RM::ref<RM::ResourceGroup> result = new RM::ResourceGroup(name);
        return result;
    }

    void loadResourceGroup(sol::table & rm) {
        rm.new_usertype<RM::ResourceGroup>("ResourceGroup",
                                           "new",               sol::factories(&groupFactory),
                                           sol::base_classes, sol::bases<RM::GCObject>(),
                                           "getStepsToLoad",    &RM::ResourceGroup::getStepsToLoad,
                                           "getStepsCompleted", &RM::ResourceGroup::getStepsCompleted,
                                           "getName",           &RM::ResourceGroup::getName,
                                           "getNumMembers",     &RM::ResourceGroup::getNumMembers,

                                           "setProgressCallback", +[](RM::ResourceGroup & group, sol::function callback)
                                           {
                                               group.setProgressCallback([callback](unsigned int progress)
                                                                        {
                                                                            callback(progress);
                                                                        });
                                           },
                                           "setCompletionCallback", +[](RM::ResourceGroup & group, sol::function callback)
                                           {
                                               group.setCompletionCallback([callback]()
                                                                          {
                                                                              callback();
                                                                          });
                                           },

                                           "add", +[](RM::ResourceGroup & group, sol::function constructor, sol::variadic_args args)
                                           {
                                                return constructor(&group, args);
                                           });
    }

    RM::ref<AsyncTester> AsyncTesterFactoryGrouped(RM::ResourceGroup * group, unsigned int wait) {
        RM::ref<AsyncTester> resource = new AsyncTester(group, wait);
        return resource;
    }

    RM::ref<AsyncTester> AsyncTesterFactory(unsigned int wait) {
        RM::ref<AsyncTester> resource = new AsyncTester(wait);
        return resource;
    }

    void loadAsyncResourceTester(sol::state_view lua) {
        if (isDeveloperMode()) {
            lua.new_usertype<AsyncTester>(
                "AsyncTester",
                "new", sol::factories(&AsyncTesterFactory, &AsyncTesterFactoryGrouped),
                sol::base_classes, sol::bases<RM::AsyncResource, RM::BasicResource, RM::GCObject>()
            );
        }
    }

    RM::ref<AsyncLuaTester> AsyncLuaTesterFactoryGrouped(RM::ResourceGroup * group, sol::function func, sol::function syncfunc, sol::variadic_args args, sol::this_state lua) {
        RM::ref<AsyncLuaTester> resource = new AsyncLuaTester(group, func, syncfunc, args, luaRM::assumeNameForResource(lua));
        return resource;
    }

    RM::ref<AsyncLuaTester> AsyncLuaTesterFactory(sol::function func, sol::function syncfunc, sol::variadic_args args, sol::this_state lua) {
        RM::ref<AsyncLuaTester> resource = new AsyncLuaTester(nullptr, func, syncfunc, args, luaRM::assumeNameForResource(lua));
        return resource;
    }

    void loadAsyncLuaTester(sol::state_view lua)
    {
        if (isDeveloperMode())
        {
            lua.new_usertype<AsyncLuaTester>(
                "AsyncLuaTester",
                "new", sol::factories(&AsyncLuaTesterFactory, &AsyncLuaTesterFactoryGrouped),
                sol::base_classes, sol::bases<RM::AsyncResource, RM::BasicResource, RM::GCObject>()
            );
        }
    }

    std::string assumeNameForResource(sol::state_view lua) {

        std::string name;

        int level = 0;
        lua_Debug ar;

        while (lua_getstack(lua, level++, &ar)) {
            lua_getinfo(lua, "Snl", &ar);

            if (ar.currentline != -1) { // Is it a valid line?
                name += std::string(ar.source) + ":";
                name += std::to_string(ar.currentline);
                break;
            }
        }

        return name;
    }
}

void loadLuaResourceManagement(sol::state_view lua) {
    sol::table rm = lua.create_table();
    lua["rm"] = rm;
    rm["printResourceStats"] = +[]()
    {
        getResourceManager()->printResourceStats();
    };
    rm["printGroupStats"] = +[]()
    {
        getResourceManager()->printGroupStats();
    };

    rm["printLuaEnvironments"] = +[]()
    {
        getResourceManager()->printLuaEnvironments();
    };

    luaRM::loadBasicResource(rm);
    luaRM::loadResourceGroup(rm);
    luaRM::loadAsyncResourceTester(lua);
    luaRM::loadAsyncLuaTester(lua);
}
