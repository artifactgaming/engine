/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "luaGL.hpp"

#include "VertexBuffer.hpp"
#include "VertexBufferArray.hpp"

#include "luaSupport.hpp"

// These VBOs are so fat that we had to put them in a different file.
namespace luaGL
{
    template<class T>
    RM::ref<T> VertexFactory1(RM::ResourceGroup * group, unsigned int drawType, sol::this_state lua) {
        return RM::ref<T>(new T(group, luaRM::assumeNameForResource(lua), drawType));
    }

    template<class T>
    RM::ref<T> VertexFactory2(unsigned int drawType, sol::this_state lua) {
        return RM::ref<T>(new T(luaRM::assumeNameForResource(lua), drawType));
    }

    RM::ref<GL::VertexBufferArray> VertexArrayFactory1(RM::ResourceGroup * group, GL::ShaderProgram * program, sol::this_state lua) {
        return RM::ref<GL::VertexBufferArray>(new GL::VertexBufferArray(group, luaRM::assumeNameForResource(lua), program));
    }

    RM::ref<GL::VertexBufferArray> VertexArrayFactory2(GL::ShaderProgram * program, sol::this_state lua) {
        return RM::ref<GL::VertexBufferArray>(new GL::VertexBufferArray(luaRM::assumeNameForResource(lua), program));
    }

    void loadVBOs(sol::table & GL, sol::state_view lua) {
        sol::table bufferTypes = lua.create_table();
        bufferTypes["static"] = GL_STATIC_DRAW;
        bufferTypes["dynamic"] = GL_DYNAMIC_DRAW;
        bufferTypes["stream"] = GL_STREAM_DRAW;

        bufferTypes = LuaSupport::makeTableReadOnly(lua, bufferTypes);

        GL.new_usertype<GL::VertexBuffer>(
            "VertexBuffer",
            sol::base_classes, sol::bases<RM::GCObject>(),
            "new", sol::no_constructor
        );

        GL["VertexBuffer"]["types"] = bufferTypes;

        GL.new_usertype<GL::VertexBuffer1D>(
            "VertexBuffer1D",
            sol::base_classes, sol::bases<GL::VertexBuffer, RM::GCObject>(),
            "new", sol::factories(VertexFactory1<GL::VertexBuffer1D>, VertexFactory2<GL::VertexBuffer1D>),
            "addVertex",       &GL::VertexBuffer1D::addVertex,
            "clear",           &GL::VertexBuffer1D::clear,
            "sendToGPU",       &GL::VertexBuffer1D::sendToGPU,
            "getNumVerticies", &GL::VertexBuffer1D::getNumVerticies
        );

        GL.new_usertype<GL::VertexBuffer2D>(
            "VertexBuffer2D",
            sol::base_classes, sol::bases<GL::VertexBuffer, RM::GCObject>(),
            "new", sol::factories(VertexFactory1<GL::VertexBuffer2D>, VertexFactory2<GL::VertexBuffer2D>),
            "addVertex",       &GL::VertexBuffer2D::addVertex,
            "clear",           &GL::VertexBuffer2D::clear,
            "sendToGPU",       &GL::VertexBuffer2D::sendToGPU,
            "getNumVerticies", &GL::VertexBuffer2D::getNumVerticies
        );

        GL.new_usertype<GL::VertexBuffer3D>(
            "VertexBuffer3D",
            sol::base_classes, sol::bases<GL::VertexBuffer>(),
            "new", sol::factories(VertexFactory1<GL::VertexBuffer3D>, VertexFactory2<GL::VertexBuffer3D>),
            "addVertex",       &GL::VertexBuffer3D::addVertex,
            "clear",           &GL::VertexBuffer3D::clear,
            "sendToGPU",       &GL::VertexBuffer3D::sendToGPU,
            "getNumVerticies", &GL::VertexBuffer3D::getNumVerticies
        );

        GL.new_usertype<GL::VertexBuffer4D>(
            "VertexBuffer4D",
            sol::base_classes, sol::bases<GL::VertexBuffer, RM::GCObject>(),
            "new", sol::factories(VertexFactory1<GL::VertexBuffer4D>, VertexFactory2<GL::VertexBuffer4D>),
            "addVertex",       &GL::VertexBuffer4D::addVertex,
            "clear",           &GL::VertexBuffer4D::clear,
            "sendToGPU",       &GL::VertexBuffer4D::sendToGPU,
            "getNumVerticies", &GL::VertexBuffer4D::getNumVerticies
        );

        GL.new_usertype<GL::VertexBufferArray>(
            "VertexBufferArray",
            "new", sol::factories(VertexArrayFactory1, VertexArrayFactory2),
            sol::meta_function::garbage_collect, sol::destructor( +[](GL::VertexBufferArray & array){} ),
            sol::base_classes, sol::bases<RM::BasicResource, RM::GCObject>(),
            "setBuffer", &GL::VertexBufferArray::setBuffer
        );
    }
}
