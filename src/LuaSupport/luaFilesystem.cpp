/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "luaFilesystem.hpp"

#include <string>

#include "Path.hpp"

#include <iostream>

#include "luaSupport.hpp"

void loadLuaFilesystemAPI(sol::state_view lua) {

    sol::constructors<RM::Path(), RM::Path(RM::Path&), RM::Path(std::string)> ctors;

    lua.new_usertype<RM::Path>("Path", ctors);

    sol::table path = lua["Path"];

    path[sol::meta_function::to_string] = &RM::Path::string;
    path[sol::meta_function::division] = sol::overload(
        &RM::Path::operator/,
        &RM::Path::extendString
    );
    path[sol::meta_function::addition] = sol::overload(
        &RM::Path::operator+,
        &RM::Path::appendString
    );

    path[sol::meta_function::concatenation] = sol::overload(+[](RM::Path & me, std::string them)
    {
        return me.string() + them;
    },
    +[](std::string them, RM::Path & me)
    {
        return them + me.string();
    });

    path["getExtension"] = &RM::Path::getExtension;
    path["setExtension"] = &RM::Path::setExtension;

    path["getName"] = &RM::Path::getName;

    path["getParent"] = &RM::Path::getParent;
    path["getAbsolute"] = &RM::Path::getAbsolute;
    path["getRelative"] = &RM::Path::getRelative;

    path["XXcopyTo"] = &RM::Path::copyTo;
    path["mkdirs"] = &RM::Path::mkdirs;
    path["exists"] = &RM::Path::exists;
    path["size"] = &RM::Path::size;
    path["isDirectory"] = &RM::Path::isDirectory;
    path["isEmpty"] = &RM::Path::isEmpty;
    path["isRegularFile"] = &RM::Path::isRegularFile;
    path["remove"] = &RM::Path::remove;
    path["contains"] = &RM::Path::contains;

    path["move"] = sol::resolve<bool(std::string)>(&RM::Path::move);
    path["move"] = sol::resolve<bool(RM::Path)>(&RM::Path::move);

    path["listContents"] = sol::resolve<sol::table(sol::this_state) const>(&RM::Path::listContents);

    path["open"] = &RM::Path::open;

    path["current"] = RM::Path::current;

    path.new_usertype<boost::filesystem::fstream>("filestream",
        "new", sol::no_constructor,
        "read", sol::overload(
            +[](boost::filesystem::fstream & self, std::string format) {
                std::string text;

                if (format == "a") {
                    std::streamoff start = self.tellg();
                    self.seekg(0, std::ios::end);
                    std::streamoff amount = self.tellg() - start;
                    self.seekg(start, std::ios::beg);

                    text.resize(amount);
                    self.read(&text[0], amount);
                } else if (format == "l") {
                    getline(self, text);
                } else {
                    throw std::runtime_error("Bad read format.");
                }

                return text;
            },
            +[](boost::filesystem::fstream & self, int amount) {
                if (amount >= 0) {
                    std::string text;
                    text.resize(amount);
                    self.read(&text[0], amount);

                    // Chop off extra data if we didn't read the whole thing.
                    text.resize(self.gcount());

                    return text;
                } else {
                    throw std::runtime_error("Can't read backwards.");
                }
            }
        ),
        "write", +[](boost::filesystem::fstream & self, std::string text)
        {
            self.write(&text[0], text.length());
        },
        "seek", +[](boost::filesystem::fstream & self, std::string mode, int offset)
        {
            if (mode == "set") {
                self.seekg (offset, std::ios::beg);
            } else if (mode == "cur") {
                self.seekg (offset, std::ios::cur);
            } else if (mode == "end") {
                self.seekg (offset, std::ios::end);
            } else {
                throw std::runtime_error("Unknown seek mode.");
            }
        },
        "tell", +[](boost::filesystem::fstream & self)
        {
            return self.tellg();
        },
        "close", &boost::filesystem::fstream::close
    );
}
