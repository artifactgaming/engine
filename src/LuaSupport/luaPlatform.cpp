/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include <glad.h>

#include "luaSupport.hpp"
#include "luaPlatform.hpp"
#include "globals.hpp"

#ifdef __linux__
    #define OS "Linux"
#else
    #ifdef __WIN32
        #define OS "Windows"
    #else
        #define OS "Unknown"
    #endif // __WIN32
#endif // __linux__

namespace LuaSupport {
    void loadLuaPlatformAPI(sol::state_view lua) {
        sol::table platform = lua.create_table();
        lua["platform"] = makeTableReadOnly(lua, platform);

        platform["name"] = +[]() {
            return OS;
        };

        platform["glVersion"] = +[]() {
            int major = GLVersion.major;
            int minor = GLVersion.minor;

            return std::make_tuple(major, minor);
        };

        platform["isDebug"] = +[]() {
            #ifdef DEBUG
            return true;
            #else
            return false;
            #endif // DEBUG
        };

        platform["isDeveloperMode"] = +[]() {
            return isDeveloperMode();
        };

        platform["libraryVersion"] = +[]() {
            return std::make_tuple(0, 2);
        };

        platform["formfactor"] = +[](){
            #ifdef FORMFACTOR_PC
            return "PC";
            #else
            return "Unknown";
            #endif // FORMFACTOR
        };
    }
}
