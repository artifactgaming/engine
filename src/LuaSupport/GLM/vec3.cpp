/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "vec3.hpp"
#include "luaGLM.hpp"

#include "AbsolutePanic.hpp"

namespace LuaGLM {
    sol::object vec3getter(glm::vec3 & self, sol::stack_object key, sol::this_state L) {

        // Check for string first.
        auto string_key = key.as<sol::optional<std::string>>();
        if (string_key) {
            // It's actually a string!
            const std::string& k = *string_key;

            unsigned int length = k.length();
            if (length > 4)
            {
                throw std::runtime_error("Max vector size is 4 components.");
            }

            std::vector<float> values;
            values.reserve(length);

            for (char c : k)
            {
                switch(c)
                {
                case 'x':
                case 'r':
                    values.push_back(self.x);
                    break;
                case 'y':
                case 'g':
                    values.push_back(self.y);
                    break;
                case 'z':
                case 'b':
                    values.push_back(self.z);
                    break;
                default:
                    std::string error = "Unknown vector component \"";
                    error += c;
                    error += "\" for type vec2.";
                    throw std::runtime_error(error);
                }
            }

            switch (length)
            {
            case 1:
                return sol::object(L, sol::in_place, values[0]);
            case 2:
                return sol::object(L, sol::in_place, glm::vec2(values[0], values[1]));
            case 3:
                return sol::object(L, sol::in_place, glm::vec3(values[0], values[1], values[2]));
            case 4:
                return sol::object(L, sol::in_place, glm::vec4(values[0], values[1], values[2], values[3]));

            default:
                ABSOLUTE_PANIC();
            }
        }

        // Okay, so it wasn't a string.
        // Check for a number then.
        auto numeric_key = key.as<sol::optional<int>>();
        if (numeric_key) {
            int n = *numeric_key;
            switch (n) {
            case 0:
                return sol::object(L, sol::in_place, self.x);
            case 1:
                return sol::object(L, sol::in_place, self.y);
            case 2:
                return sol::object(L, sol::in_place, self.z);
            default:
                break;
            }
        }

        // If we failed to find it, return nil.
        return sol::object(L, sol::in_place, sol::lua_nil);
    };

    void vec3setter(glm::vec3 & self, sol::stack_object key, sol::stack_object rawValue, sol::this_state) {
        // Check for string first.

        float value = rawValue.as<float>();

        auto string_key = key.as<sol::optional<std::string>>();
        if (string_key) {
            // It's actually a string!
            const std::string& k = *string_key;

            if (k == "x" || k == "r") {
                self.x = value;
                return;
            }

            if (k == "y" || k == "g") {
                self.y = value;
                return;
            }

            if (k == "z" || k == "b") {
                self.z = value;
                return;
            }
        }

        // Okay, so it wasn't a string.
        // Check for a number then.
        auto numeric_key = key.as<sol::optional<int>>();
        if (numeric_key) {
            int n = *numeric_key;
            switch (n) {
            case 0:
                self.x = value;
                return;
            case 1:
                self.y = value;
                return;
            case 2:
                self.z = value;
                return;
            }
        }
    };

    void loadVec3(sol::state_view lua) {
        sol::constructors<glm::vec3(), glm::vec3(float), glm::vec3(float, float, float),
                          glm::vec3(glm::vec3), glm::vec3(float, glm::vec2),
                          glm::vec3(glm::vec2, float)> vec3ctors;

        lua.new_usertype<glm::vec3>("vec3",
        sol::call_constructor, vec3ctors,
        "x", &glm::vec3::x,
        "y", &glm::vec3::y,
        "z", &glm::vec3::z,
        "r", &glm::vec3::r,
        "g", &glm::vec3::g,
        "b", &glm::vec3::b,

        sol::meta_function::index, &vec3getter
        );

        auto vec3 = lua["vec3"];

        loadGenericVectorFunctions<glm::vec3>(vec3);

        /*vec3["xx"] = &glm::vec3::xx;
        vec3["yx"] = &glm::vec3::yx;
        vec3["zx"] = &glm::vec3::zx;
        vec3["xy"] = &glm::vec3::xy;
        vec3["yy"] = &glm::vec3::yy;
        vec3["zy"] = &glm::vec3::zy;
        vec3["xz"] = &glm::vec3::xz;
        vec3["yz"] = &glm::vec3::yz;
        vec3["zz"] = &glm::vec3::zz;
        vec3["xxx"] = &glm::vec3::xxx;
        vec3["yxx"] = &glm::vec3::yxx;
        vec3["zxx"] = &glm::vec3::zxx;
        vec3["xyx"] = &glm::vec3::xyx;
        vec3["yyx"] = &glm::vec3::yyx;
        vec3["zyx"] = &glm::vec3::zyx;
        vec3["xzx"] = &glm::vec3::xzx;
        vec3["yzx"] = &glm::vec3::yzx;
        vec3["zzx"] = &glm::vec3::zzx;
        vec3["xxy"] = &glm::vec3::xxy;
        vec3["yxy"] = &glm::vec3::yxy;
        vec3["zxy"] = &glm::vec3::zxy;
        vec3["xyy"] = &glm::vec3::xyy;
        vec3["yyy"] = &glm::vec3::yyy;
        vec3["zyy"] = &glm::vec3::zyy;
        vec3["xzy"] = &glm::vec3::xzy;
        vec3["yzy"] = &glm::vec3::yzy;
        vec3["zzy"] = &glm::vec3::zzy;
        vec3["xxz"] = &glm::vec3::xxz;
        vec3["yxz"] = &glm::vec3::yxz;
        vec3["zxz"] = &glm::vec3::zxz;
        vec3["xyz"] = &glm::vec3::xyz;
        vec3["yyz"] = &glm::vec3::yyz;
        vec3["zyz"] = &glm::vec3::zyz;
        vec3["xzz"] = &glm::vec3::xzz;
        vec3["yzz"] = &glm::vec3::yzz;
        vec3["zzz"] = &glm::vec3::zzz;
        vec3["xxxx"] = &glm::vec3::xxxx;
        vec3["yxxx"] = &glm::vec3::yxxx;
        vec3["zxxx"] = &glm::vec3::zxxx;
        vec3["xyxx"] = &glm::vec3::xyxx;
        vec3["yyxx"] = &glm::vec3::yyxx;
        vec3["zyxx"] = &glm::vec3::zyxx;
        vec3["xzxx"] = &glm::vec3::xzxx;
        vec3["yzxx"] = &glm::vec3::yzxx;
        vec3["zzxx"] = &glm::vec3::zzxx;
        vec3["xxyx"] = &glm::vec3::xxyx;
        vec3["yxyx"] = &glm::vec3::yxyx;
        vec3["zxyx"] = &glm::vec3::zxyx;
        vec3["xyyx"] = &glm::vec3::xyyx;
        vec3["yyyx"] = &glm::vec3::yyyx;
        vec3["zyyx"] = &glm::vec3::zyyx;
        vec3["xzyx"] = &glm::vec3::xzyx;
        vec3["yzyx"] = &glm::vec3::yzyx;
        vec3["zzyx"] = &glm::vec3::zzyx;
        vec3["xxzx"] = &glm::vec3::xxzx;
        vec3["yxzx"] = &glm::vec3::yxzx;
        vec3["zxzx"] = &glm::vec3::zxzx;
        vec3["xyzx"] = &glm::vec3::xyzx;
        vec3["yyzx"] = &glm::vec3::yyzx;
        vec3["zyzx"] = &glm::vec3::zyzx;
        vec3["xzzx"] = &glm::vec3::xzzx;
        vec3["yzzx"] = &glm::vec3::yzzx;
        vec3["zzzx"] = &glm::vec3::zzzx;
        vec3["xxxy"] = &glm::vec3::xxxy;
        vec3["yxxy"] = &glm::vec3::yxxy;
        vec3["zxxy"] = &glm::vec3::zxxy;
        vec3["xyxy"] = &glm::vec3::xyxy;
        vec3["yyxy"] = &glm::vec3::yyxy;
        vec3["zyxy"] = &glm::vec3::zyxy;
        vec3["xzxy"] = &glm::vec3::xzxy;
        vec3["yzxy"] = &glm::vec3::yzxy;
        vec3["zzxy"] = &glm::vec3::zzxy;
        vec3["xxyy"] = &glm::vec3::xxyy;
        vec3["yxyy"] = &glm::vec3::yxyy;
        vec3["zxyy"] = &glm::vec3::zxyy;
        vec3["xyyy"] = &glm::vec3::xyyy;
        vec3["yyyy"] = &glm::vec3::yyyy;
        vec3["zyyy"] = &glm::vec3::zyyy;
        vec3["xzyy"] = &glm::vec3::xzyy;
        vec3["yzyy"] = &glm::vec3::yzyy;
        vec3["zzyy"] = &glm::vec3::zzyy;
        vec3["xxzy"] = &glm::vec3::xxzy;
        vec3["yxzy"] = &glm::vec3::yxzy;
        vec3["zxzy"] = &glm::vec3::zxzy;
        vec3["xyzy"] = &glm::vec3::xyzy;
        vec3["yyzy"] = &glm::vec3::yyzy;
        vec3["zyzy"] = &glm::vec3::zyzy;
        vec3["xzzy"] = &glm::vec3::xzzy;
        vec3["yzzy"] = &glm::vec3::yzzy;
        vec3["zzzy"] = &glm::vec3::zzzy;
        vec3["xxxz"] = &glm::vec3::xxxz;
        vec3["yxxz"] = &glm::vec3::yxxz;
        vec3["zxxz"] = &glm::vec3::zxxz;
        vec3["xyxz"] = &glm::vec3::xyxz;
        vec3["yyxz"] = &glm::vec3::yyxz;
        vec3["zyxz"] = &glm::vec3::zyxz;
        vec3["xzxz"] = &glm::vec3::xzxz;
        vec3["yzxz"] = &glm::vec3::yzxz;
        vec3["zzxz"] = &glm::vec3::zzxz;
        vec3["xxyz"] = &glm::vec3::xxyz;
        vec3["yxyz"] = &glm::vec3::yxyz;
        vec3["zxyz"] = &glm::vec3::zxyz;
        vec3["xyyz"] = &glm::vec3::xyyz;
        vec3["yyyz"] = &glm::vec3::yyyz;
        vec3["zyyz"] = &glm::vec3::zyyz;
        vec3["xzyz"] = &glm::vec3::xzyz;
        vec3["yzyz"] = &glm::vec3::yzyz;
        vec3["zzyz"] = &glm::vec3::zzyz;
        vec3["xxzz"] = &glm::vec3::xxzz;
        vec3["yxzz"] = &glm::vec3::yxzz;
        vec3["zxzz"] = &glm::vec3::zxzz;
        vec3["xyzz"] = &glm::vec3::xyzz;
        vec3["yyzz"] = &glm::vec3::yyzz;
        vec3["zyzz"] = &glm::vec3::zyzz;
        vec3["xzzz"] = &glm::vec3::xzzz;
        vec3["yzzz"] = &glm::vec3::yzzz;
        vec3["zzzz"] = &glm::vec3::zzzz;

        vec3["rr"] = &glm::vec3::rr;
        vec3["gr"] = &glm::vec3::gr;
        vec3["br"] = &glm::vec3::br;
        vec3["rg"] = &glm::vec3::rg;
        vec3["gg"] = &glm::vec3::gg;
        vec3["bg"] = &glm::vec3::bg;
        vec3["rb"] = &glm::vec3::rb;
        vec3["gb"] = &glm::vec3::gb;
        vec3["bb"] = &glm::vec3::bb;
        vec3["rrr"] = &glm::vec3::rrr;
        vec3["grr"] = &glm::vec3::grr;
        vec3["brr"] = &glm::vec3::brr;
        vec3["rgr"] = &glm::vec3::rgr;
        vec3["ggr"] = &glm::vec3::ggr;
        vec3["bgr"] = &glm::vec3::bgr;
        vec3["rbr"] = &glm::vec3::rbr;
        vec3["gbr"] = &glm::vec3::gbr;
        vec3["bbr"] = &glm::vec3::bbr;
        vec3["rrg"] = &glm::vec3::rrg;
        vec3["grg"] = &glm::vec3::grg;
        vec3["brg"] = &glm::vec3::brg;
        vec3["rgg"] = &glm::vec3::rgg;
        vec3["ggg"] = &glm::vec3::ggg;
        vec3["bgg"] = &glm::vec3::bgg;
        vec3["rbg"] = &glm::vec3::rbg;
        vec3["gbg"] = &glm::vec3::gbg;
        vec3["bbg"] = &glm::vec3::bbg;
        vec3["rrb"] = &glm::vec3::rrb;
        vec3["grb"] = &glm::vec3::grb;
        vec3["brb"] = &glm::vec3::brb;
        vec3["rgb"] = &glm::vec3::rgb;
        vec3["ggb"] = &glm::vec3::ggb;
        vec3["bgb"] = &glm::vec3::bgb;
        vec3["rbb"] = &glm::vec3::rbb;
        vec3["gbb"] = &glm::vec3::gbb;
        vec3["bbb"] = &glm::vec3::bbb;
        vec3["rrrr"] = &glm::vec3::rrrr;
        vec3["grrr"] = &glm::vec3::grrr;
        vec3["brrr"] = &glm::vec3::brrr;
        vec3["rgrr"] = &glm::vec3::rgrr;
        vec3["ggrr"] = &glm::vec3::ggrr;
        vec3["bgrr"] = &glm::vec3::bgrr;
        vec3["rbrr"] = &glm::vec3::rbrr;
        vec3["gbrr"] = &glm::vec3::gbrr;
        vec3["bbrr"] = &glm::vec3::bbrr;
        vec3["rrgr"] = &glm::vec3::rrgr;
        vec3["grgr"] = &glm::vec3::grgr;
        vec3["brgr"] = &glm::vec3::brgr;
        vec3["rggr"] = &glm::vec3::rggr;
        vec3["gggr"] = &glm::vec3::gggr;
        vec3["bggr"] = &glm::vec3::bggr;
        vec3["rbgr"] = &glm::vec3::rbgr;
        vec3["gbgr"] = &glm::vec3::gbgr;
        vec3["bbgr"] = &glm::vec3::bbgr;
        vec3["rrbr"] = &glm::vec3::rrbr;
        vec3["grbr"] = &glm::vec3::grbr;
        vec3["brbr"] = &glm::vec3::brbr;
        vec3["rgbr"] = &glm::vec3::rgbr;
        vec3["ggbr"] = &glm::vec3::ggbr;
        vec3["bgbr"] = &glm::vec3::bgbr;
        vec3["rbbr"] = &glm::vec3::rbbr;
        vec3["gbbr"] = &glm::vec3::gbbr;
        vec3["bbbr"] = &glm::vec3::bbbr;
        vec3["rrrg"] = &glm::vec3::rrrg;
        vec3["grrg"] = &glm::vec3::grrg;
        vec3["brrg"] = &glm::vec3::brrg;
        vec3["rgrg"] = &glm::vec3::rgrg;
        vec3["ggrg"] = &glm::vec3::ggrg;
        vec3["bgrg"] = &glm::vec3::bgrg;
        vec3["rbrg"] = &glm::vec3::rbrg;
        vec3["gbrg"] = &glm::vec3::gbrg;
        vec3["bbrg"] = &glm::vec3::bbrg;
        vec3["rrgg"] = &glm::vec3::rrgg;
        vec3["grgg"] = &glm::vec3::grgg;
        vec3["brgg"] = &glm::vec3::brgg;
        vec3["rggg"] = &glm::vec3::rggg;
        vec3["gggg"] = &glm::vec3::gggg;
        vec3["bggg"] = &glm::vec3::bggg;
        vec3["rbgg"] = &glm::vec3::rbgg;
        vec3["gbgg"] = &glm::vec3::gbgg;
        vec3["bbgg"] = &glm::vec3::bbgg;
        vec3["rrbg"] = &glm::vec3::rrbg;
        vec3["grbg"] = &glm::vec3::grbg;
        vec3["brbg"] = &glm::vec3::brbg;
        vec3["rgbg"] = &glm::vec3::rgbg;
        vec3["ggbg"] = &glm::vec3::ggbg;
        vec3["bgbg"] = &glm::vec3::bgbg;
        vec3["rbbg"] = &glm::vec3::rbbg;
        vec3["gbbg"] = &glm::vec3::gbbg;
        vec3["bbbg"] = &glm::vec3::bbbg;
        vec3["rrrb"] = &glm::vec3::rrrb;
        vec3["grrb"] = &glm::vec3::grrb;
        vec3["brrb"] = &glm::vec3::brrb;
        vec3["rgrb"] = &glm::vec3::rgrb;
        vec3["ggrb"] = &glm::vec3::ggrb;
        vec3["bgrb"] = &glm::vec3::bgrb;
        vec3["rbrb"] = &glm::vec3::rbrb;
        vec3["gbrb"] = &glm::vec3::gbrb;
        vec3["bbrb"] = &glm::vec3::bbrb;
        vec3["rrgb"] = &glm::vec3::rrgb;
        vec3["grgb"] = &glm::vec3::grgb;
        vec3["brgb"] = &glm::vec3::brgb;
        vec3["rggb"] = &glm::vec3::rggb;
        vec3["gggb"] = &glm::vec3::gggb;
        vec3["bggb"] = &glm::vec3::bggb;
        vec3["rbgb"] = &glm::vec3::rbgb;
        vec3["gbgb"] = &glm::vec3::gbgb;
        vec3["bbgb"] = &glm::vec3::bbgb;
        vec3["rrbb"] = &glm::vec3::rrbb;
        vec3["grbb"] = &glm::vec3::grbb;
        vec3["brbb"] = &glm::vec3::brbb;
        vec3["rgbb"] = &glm::vec3::rgbb;
        vec3["ggbb"] = &glm::vec3::ggbb;
        vec3["bgbb"] = &glm::vec3::bgbb;
        vec3["rbbb"] = &glm::vec3::rbbb;
        vec3["gbbb"] = &glm::vec3::gbbb;
        vec3["bbbb"] = &glm::vec3::bbbb;*/

        // Setter has to be set last otherwise we'll segfault while setting up the functions.
        vec3[sol::meta_function::new_index] = &vec3setter;
    }
}
