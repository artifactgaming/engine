/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "vec4.hpp"
#include "luaGLM.hpp"

#include "AbsolutePanic.hpp"

namespace LuaGLM {
    sol::object vec4getter(glm::vec4 & self, sol::stack_object key, sol::this_state L) {

        // Check for string first.
        auto string_key = key.as<sol::optional<std::string>>();
        if (string_key) {
            // It's actually a string!
            const std::string& k = *string_key;

            unsigned int length = k.length();
            if (length > 4)
            {
                throw std::runtime_error("Max vector size is 4 components.");
            }

            std::vector<float> values;
            values.reserve(length);

            for (char c : k)
            {
                switch(c)
                {
                case 'x':
                case 'r':
                    values.push_back(self.x);
                    break;
                case 'y':
                case 'g':
                    values.push_back(self.y);
                    break;
                case 'z':
                case 'b':
                    values.push_back(self.z);
                    break;
                case 'w':
                case 'a':
                    values.push_back(self.w);
                    break;
                default:
                    std::string error = "Unknown vector component \"";
                    error += c;
                    error += "\" for type vec2.";
                    throw std::runtime_error(error);
                }
            }

            switch (length)
            {
            case 1:
                return sol::object(L, sol::in_place, values[0]);
            case 2:
                return sol::object(L, sol::in_place, glm::vec2(values[0], values[1]));
            case 3:
                return sol::object(L, sol::in_place, glm::vec3(values[0], values[1], values[2]));
            case 4:
                return sol::object(L, sol::in_place, glm::vec4(values[0], values[1], values[2], values[3]));

            default:
                ABSOLUTE_PANIC();
            }
        }

        // Okay, so it wasn't a string.
        // Check for a number then.
        auto numeric_key = key.as<sol::optional<int>>();
        if (numeric_key) {
            int n = *numeric_key;
            switch (n) {
            case 0:
                return sol::object(L, sol::in_place, self.x);
            case 1:
                return sol::object(L, sol::in_place, self.y);
            case 2:
                return sol::object(L, sol::in_place, self.z);
            case 3:
                return sol::object(L, sol::in_place, self.w);
            default:
                break;
            }
        }

        // If we failed to find it, return nil.
        return sol::object(L, sol::in_place, sol::lua_nil);
    };

    void vec4setter(glm::vec4 & self, sol::stack_object key, sol::stack_object rawValue, sol::this_state) {
        // Check for string first.

        float value = rawValue.as<float>();

        auto string_key = key.as<sol::optional<std::string>>();
        if (string_key) {
            // It's actually a string!
            const std::string& k = *string_key;

            if (k == "x" || k == "r") {
                self.x = value;
                return;
            }

            if (k == "y" || k == "g") {
                self.y = value;
                return;
            }

            if (k == "z" || k == "b") {
                self.z = value;
                return;
            }

            if (k == "w" || k == "a") {
                self.w = value;
                return;
            }
        }

        // Okay, so it wasn't a string.
        // Check for a number then.
        auto numeric_key = key.as<sol::optional<int>>();
        if (numeric_key) {
            int n = *numeric_key;
            switch (n) {
            case 0:
                self.x = value;
                return;
            case 1:
                self.y = value;
                return;
            case 2:
                self.z = value;
            case 3:
                self.w = value;
                return;
            }
        }
    };

    void loadVec4(sol::state_view lua) {
        sol::constructors<glm::vec4(), glm::vec4(float),
                          glm::vec4(float, float, float, float),
                          glm::vec4(float, float, glm::vec2),
                          glm::vec4(glm::vec2, float, float),
                          glm::vec4(float, glm::vec2, float),
                          glm::vec4(glm::vec2, glm::vec2),
                          glm::vec4(glm::vec3, float),
                          glm::vec4(float, glm::vec3)> vec4ctors;

        lua.new_usertype<glm::vec4>("vec4",
        sol::call_constructor, vec4ctors,
        "x", &glm::vec4::x,
        "y", &glm::vec4::y,
        "z", &glm::vec4::z,
        "w", &glm::vec4::w,
        "r", &glm::vec4::r,
        "g", &glm::vec4::g,
        "b", &glm::vec4::b,
        "a", &glm::vec4::a,

        sol::meta_function::index, &vec4getter);

        auto vec4 = lua["vec4"];

        loadGenericVectorFunctions<glm::vec4>(vec4);

        /*vec4["xx"] = &glm::vec4::xx;
        vec4["yx"] = &glm::vec4::yx;
        vec4["zx"] = &glm::vec4::zx;
        vec4["wx"] = &glm::vec4::wx;
        vec4["xy"] = &glm::vec4::xy;
        vec4["yy"] = &glm::vec4::yy;
        vec4["zy"] = &glm::vec4::zy;
        vec4["wy"] = &glm::vec4::wy;
        vec4["xz"] = &glm::vec4::xz;
        vec4["yz"] = &glm::vec4::yz;
        vec4["zz"] = &glm::vec4::zz;
        vec4["wz"] = &glm::vec4::wz;
        vec4["xw"] = &glm::vec4::xw;
        vec4["yw"] = &glm::vec4::yw;
        vec4["zw"] = &glm::vec4::zw;
        vec4["ww"] = &glm::vec4::ww;
        vec4["xxx"] = &glm::vec4::xxx;
        vec4["yxx"] = &glm::vec4::yxx;
        vec4["zxx"] = &glm::vec4::zxx;
        vec4["wxx"] = &glm::vec4::wxx;
        vec4["xyx"] = &glm::vec4::xyx;
        vec4["yyx"] = &glm::vec4::yyx;
        vec4["zyx"] = &glm::vec4::zyx;
        vec4["wyx"] = &glm::vec4::wyx;
        vec4["xzx"] = &glm::vec4::xzx;
        vec4["yzx"] = &glm::vec4::yzx;
        vec4["zzx"] = &glm::vec4::zzx;
        vec4["wzx"] = &glm::vec4::wzx;
        vec4["xwx"] = &glm::vec4::xwx;
        vec4["ywx"] = &glm::vec4::ywx;
        vec4["zwx"] = &glm::vec4::zwx;
        vec4["wwx"] = &glm::vec4::wwx;
        vec4["xxy"] = &glm::vec4::xxy;
        vec4["yxy"] = &glm::vec4::yxy;
        vec4["zxy"] = &glm::vec4::zxy;
        vec4["wxy"] = &glm::vec4::wxy;
        vec4["xyy"] = &glm::vec4::xyy;
        vec4["yyy"] = &glm::vec4::yyy;
        vec4["zyy"] = &glm::vec4::zyy;
        vec4["wyy"] = &glm::vec4::wyy;
        vec4["xzy"] = &glm::vec4::xzy;
        vec4["yzy"] = &glm::vec4::yzy;
        vec4["zzy"] = &glm::vec4::zzy;
        vec4["wzy"] = &glm::vec4::wzy;
        vec4["xwy"] = &glm::vec4::xwy;
        vec4["ywy"] = &glm::vec4::ywy;
        vec4["zwy"] = &glm::vec4::zwy;
        vec4["wwy"] = &glm::vec4::wwy;
        vec4["xxz"] = &glm::vec4::xxz;
        vec4["yxz"] = &glm::vec4::yxz;
        vec4["zxz"] = &glm::vec4::zxz;
        vec4["wxz"] = &glm::vec4::wxz;
        vec4["xyz"] = &glm::vec4::xyz;
        vec4["yyz"] = &glm::vec4::yyz;
        vec4["zyz"] = &glm::vec4::zyz;
        vec4["wyz"] = &glm::vec4::wyz;
        vec4["xzz"] = &glm::vec4::xzz;
        vec4["yzz"] = &glm::vec4::yzz;
        vec4["zzz"] = &glm::vec4::zzz;
        vec4["wzz"] = &glm::vec4::wzz;
        vec4["xwz"] = &glm::vec4::xwz;
        vec4["ywz"] = &glm::vec4::ywz;
        vec4["zwz"] = &glm::vec4::zwz;
        vec4["wwz"] = &glm::vec4::wwz;
        vec4["xxw"] = &glm::vec4::xxw;
        vec4["yxw"] = &glm::vec4::yxw;
        vec4["zxw"] = &glm::vec4::zxw;
        vec4["wxw"] = &glm::vec4::wxw;
        vec4["xyw"] = &glm::vec4::xyw;
        vec4["yyw"] = &glm::vec4::yyw;
        vec4["zyw"] = &glm::vec4::zyw;
        vec4["wyw"] = &glm::vec4::wyw;
        vec4["xzw"] = &glm::vec4::xzw;
        vec4["yzw"] = &glm::vec4::yzw;
        vec4["zzw"] = &glm::vec4::zzw;
        vec4["wzw"] = &glm::vec4::wzw;
        vec4["xww"] = &glm::vec4::xww;
        vec4["yww"] = &glm::vec4::yww;
        vec4["zww"] = &glm::vec4::zww;
        vec4["www"] = &glm::vec4::www;
        vec4["xxxx"] = &glm::vec4::xxxx;
        vec4["yxxx"] = &glm::vec4::yxxx;
        vec4["zxxx"] = &glm::vec4::zxxx;
        vec4["wxxx"] = &glm::vec4::wxxx;
        vec4["xyxx"] = &glm::vec4::xyxx;
        vec4["yyxx"] = &glm::vec4::yyxx;
        vec4["zyxx"] = &glm::vec4::zyxx;
        vec4["wyxx"] = &glm::vec4::wyxx;
        vec4["xzxx"] = &glm::vec4::xzxx;
        vec4["yzxx"] = &glm::vec4::yzxx;
        vec4["zzxx"] = &glm::vec4::zzxx;
        vec4["wzxx"] = &glm::vec4::wzxx;
        vec4["xwxx"] = &glm::vec4::xwxx;
        vec4["ywxx"] = &glm::vec4::ywxx;
        vec4["zwxx"] = &glm::vec4::zwxx;
        vec4["wwxx"] = &glm::vec4::wwxx;
        vec4["xxyx"] = &glm::vec4::xxyx;
        vec4["yxyx"] = &glm::vec4::yxyx;
        vec4["zxyx"] = &glm::vec4::zxyx;
        vec4["wxyx"] = &glm::vec4::wxyx;
        vec4["xyyx"] = &glm::vec4::xyyx;
        vec4["yyyx"] = &glm::vec4::yyyx;
        vec4["zyyx"] = &glm::vec4::zyyx;
        vec4["wyyx"] = &glm::vec4::wyyx;
        vec4["xzyx"] = &glm::vec4::xzyx;
        vec4["yzyx"] = &glm::vec4::yzyx;
        vec4["zzyx"] = &glm::vec4::zzyx;
        vec4["wzyx"] = &glm::vec4::wzyx;
        vec4["xwyx"] = &glm::vec4::xwyx;
        vec4["ywyx"] = &glm::vec4::ywyx;
        vec4["zwyx"] = &glm::vec4::zwyx;
        vec4["wwyx"] = &glm::vec4::wwyx;
        vec4["xxzx"] = &glm::vec4::xxzx;
        vec4["yxzx"] = &glm::vec4::yxzx;
        vec4["zxzx"] = &glm::vec4::zxzx;
        vec4["wxzx"] = &glm::vec4::wxzx;
        vec4["xyzx"] = &glm::vec4::xyzx;
        vec4["yyzx"] = &glm::vec4::yyzx;
        vec4["zyzx"] = &glm::vec4::zyzx;
        vec4["wyzx"] = &glm::vec4::wyzx;
        vec4["xzzx"] = &glm::vec4::xzzx;
        vec4["yzzx"] = &glm::vec4::yzzx;
        vec4["zzzx"] = &glm::vec4::zzzx;
        vec4["wzzx"] = &glm::vec4::wzzx;
        vec4["xwzx"] = &glm::vec4::xwzx;
        vec4["ywzx"] = &glm::vec4::ywzx;
        vec4["zwzx"] = &glm::vec4::zwzx;
        vec4["wwzx"] = &glm::vec4::wwzx;
        vec4["xxwx"] = &glm::vec4::xxwx;
        vec4["yxwx"] = &glm::vec4::yxwx;
        vec4["zxwx"] = &glm::vec4::zxwx;
        vec4["wxwx"] = &glm::vec4::wxwx;
        vec4["xywx"] = &glm::vec4::xywx;
        vec4["yywx"] = &glm::vec4::yywx;
        vec4["zywx"] = &glm::vec4::zywx;
        vec4["wywx"] = &glm::vec4::wywx;
        vec4["xzwx"] = &glm::vec4::xzwx;
        vec4["yzwx"] = &glm::vec4::yzwx;
        vec4["zzwx"] = &glm::vec4::zzwx;
        vec4["wzwx"] = &glm::vec4::wzwx;
        vec4["xwwx"] = &glm::vec4::xwwx;
        vec4["ywwx"] = &glm::vec4::ywwx;
        vec4["zwwx"] = &glm::vec4::zwwx;
        vec4["wwwx"] = &glm::vec4::wwwx;
        vec4["xxxy"] = &glm::vec4::xxxy;
        vec4["yxxy"] = &glm::vec4::yxxy;
        vec4["zxxy"] = &glm::vec4::zxxy;
        vec4["wxxy"] = &glm::vec4::wxxy;
        vec4["xyxy"] = &glm::vec4::xyxy;
        vec4["yyxy"] = &glm::vec4::yyxy;
        vec4["zyxy"] = &glm::vec4::zyxy;
        vec4["wyxy"] = &glm::vec4::wyxy;
        vec4["xzxy"] = &glm::vec4::xzxy;
        vec4["yzxy"] = &glm::vec4::yzxy;
        vec4["zzxy"] = &glm::vec4::zzxy;
        vec4["wzxy"] = &glm::vec4::wzxy;
        vec4["xwxy"] = &glm::vec4::xwxy;
        vec4["ywxy"] = &glm::vec4::ywxy;
        vec4["zwxy"] = &glm::vec4::zwxy;
        vec4["wwxy"] = &glm::vec4::wwxy;
        vec4["xxyy"] = &glm::vec4::xxyy;
        vec4["yxyy"] = &glm::vec4::yxyy;
        vec4["zxyy"] = &glm::vec4::zxyy;
        vec4["wxyy"] = &glm::vec4::wxyy;
        vec4["xyyy"] = &glm::vec4::xyyy;
        vec4["yyyy"] = &glm::vec4::yyyy;
        vec4["zyyy"] = &glm::vec4::zyyy;
        vec4["wyyy"] = &glm::vec4::wyyy;
        vec4["xzyy"] = &glm::vec4::xzyy;
        vec4["yzyy"] = &glm::vec4::yzyy;
        vec4["zzyy"] = &glm::vec4::zzyy;
        vec4["wzyy"] = &glm::vec4::wzyy;
        vec4["xwyy"] = &glm::vec4::xwyy;
        vec4["ywyy"] = &glm::vec4::ywyy;
        vec4["zwyy"] = &glm::vec4::zwyy;
        vec4["wwyy"] = &glm::vec4::wwyy;
        vec4["xxzy"] = &glm::vec4::xxzy;
        vec4["yxzy"] = &glm::vec4::yxzy;
        vec4["zxzy"] = &glm::vec4::zxzy;
        vec4["wxzy"] = &glm::vec4::wxzy;
        vec4["xyzy"] = &glm::vec4::xyzy;
        vec4["yyzy"] = &glm::vec4::yyzy;
        vec4["zyzy"] = &glm::vec4::zyzy;
        vec4["wyzy"] = &glm::vec4::wyzy;
        vec4["xzzy"] = &glm::vec4::xzzy;
        vec4["yzzy"] = &glm::vec4::yzzy;
        vec4["zzzy"] = &glm::vec4::zzzy;
        vec4["wzzy"] = &glm::vec4::wzzy;
        vec4["xwzy"] = &glm::vec4::xwzy;
        vec4["ywzy"] = &glm::vec4::ywzy;
        vec4["zwzy"] = &glm::vec4::zwzy;
        vec4["wwzy"] = &glm::vec4::wwzy;
        vec4["xxwy"] = &glm::vec4::xxwy;
        vec4["yxwy"] = &glm::vec4::yxwy;
        vec4["zxwy"] = &glm::vec4::zxwy;
        vec4["wxwy"] = &glm::vec4::wxwy;
        vec4["xywy"] = &glm::vec4::xywy;
        vec4["yywy"] = &glm::vec4::yywy;
        vec4["zywy"] = &glm::vec4::zywy;
        vec4["wywy"] = &glm::vec4::wywy;
        vec4["xzwy"] = &glm::vec4::xzwy;
        vec4["yzwy"] = &glm::vec4::yzwy;
        vec4["zzwy"] = &glm::vec4::zzwy;
        vec4["wzwy"] = &glm::vec4::wzwy;
        vec4["xwwy"] = &glm::vec4::xwwy;
        vec4["ywwy"] = &glm::vec4::ywwy;
        vec4["zwwy"] = &glm::vec4::zwwy;
        vec4["wwwy"] = &glm::vec4::wwwy;
        vec4["xxxz"] = &glm::vec4::xxxz;
        vec4["yxxz"] = &glm::vec4::yxxz;
        vec4["zxxz"] = &glm::vec4::zxxz;
        vec4["wxxz"] = &glm::vec4::wxxz;
        vec4["xyxz"] = &glm::vec4::xyxz;
        vec4["yyxz"] = &glm::vec4::yyxz;
        vec4["zyxz"] = &glm::vec4::zyxz;
        vec4["wyxz"] = &glm::vec4::wyxz;
        vec4["xzxz"] = &glm::vec4::xzxz;
        vec4["yzxz"] = &glm::vec4::yzxz;
        vec4["zzxz"] = &glm::vec4::zzxz;
        vec4["wzxz"] = &glm::vec4::wzxz;
        vec4["xwxz"] = &glm::vec4::xwxz;
        vec4["ywxz"] = &glm::vec4::ywxz;
        vec4["zwxz"] = &glm::vec4::zwxz;
        vec4["wwxz"] = &glm::vec4::wwxz;
        vec4["xxyz"] = &glm::vec4::xxyz;
        vec4["yxyz"] = &glm::vec4::yxyz;
        vec4["zxyz"] = &glm::vec4::zxyz;
        vec4["wxyz"] = &glm::vec4::wxyz;
        vec4["xyyz"] = &glm::vec4::xyyz;
        vec4["yyyz"] = &glm::vec4::yyyz;
        vec4["zyyz"] = &glm::vec4::zyyz;
        vec4["wyyz"] = &glm::vec4::wyyz;
        vec4["xzyz"] = &glm::vec4::xzyz;
        vec4["yzyz"] = &glm::vec4::yzyz;
        vec4["zzyz"] = &glm::vec4::zzyz;
        vec4["wzyz"] = &glm::vec4::wzyz;
        vec4["xwyz"] = &glm::vec4::xwyz;
        vec4["ywyz"] = &glm::vec4::ywyz;
        vec4["zwyz"] = &glm::vec4::zwyz;
        vec4["wwyz"] = &glm::vec4::wwyz;
        vec4["xxzz"] = &glm::vec4::xxzz;
        vec4["yxzz"] = &glm::vec4::yxzz;
        vec4["zxzz"] = &glm::vec4::zxzz;
        vec4["wxzz"] = &glm::vec4::wxzz;
        vec4["xyzz"] = &glm::vec4::xyzz;
        vec4["yyzz"] = &glm::vec4::yyzz;
        vec4["zyzz"] = &glm::vec4::zyzz;
        vec4["wyzz"] = &glm::vec4::wyzz;
        vec4["xzzz"] = &glm::vec4::xzzz;
        vec4["yzzz"] = &glm::vec4::yzzz;
        vec4["zzzz"] = &glm::vec4::zzzz;
        vec4["wzzz"] = &glm::vec4::wzzz;
        vec4["xwzz"] = &glm::vec4::xwzz;
        vec4["ywzz"] = &glm::vec4::ywzz;
        vec4["zwzz"] = &glm::vec4::zwzz;
        vec4["wwzz"] = &glm::vec4::wwzz;
        vec4["xxwz"] = &glm::vec4::xxwz;
        vec4["yxwz"] = &glm::vec4::yxwz;
        vec4["zxwz"] = &glm::vec4::zxwz;
        vec4["wxwz"] = &glm::vec4::wxwz;
        vec4["xywz"] = &glm::vec4::xywz;
        vec4["yywz"] = &glm::vec4::yywz;
        vec4["zywz"] = &glm::vec4::zywz;
        vec4["wywz"] = &glm::vec4::wywz;
        vec4["xzwz"] = &glm::vec4::xzwz;
        vec4["yzwz"] = &glm::vec4::yzwz;
        vec4["zzwz"] = &glm::vec4::zzwz;
        vec4["wzwz"] = &glm::vec4::wzwz;
        vec4["xwwz"] = &glm::vec4::xwwz;
        vec4["ywwz"] = &glm::vec4::ywwz;
        vec4["zwwz"] = &glm::vec4::zwwz;
        vec4["wwwz"] = &glm::vec4::wwwz;
        vec4["xxxw"] = &glm::vec4::xxxw;
        vec4["yxxw"] = &glm::vec4::yxxw;
        vec4["zxxw"] = &glm::vec4::zxxw;
        vec4["wxxw"] = &glm::vec4::wxxw;
        vec4["xyxw"] = &glm::vec4::xyxw;
        vec4["yyxw"] = &glm::vec4::yyxw;
        vec4["zyxw"] = &glm::vec4::zyxw;
        vec4["wyxw"] = &glm::vec4::wyxw;
        vec4["xzxw"] = &glm::vec4::xzxw;
        vec4["yzxw"] = &glm::vec4::yzxw;
        vec4["zzxw"] = &glm::vec4::zzxw;
        vec4["wzxw"] = &glm::vec4::wzxw;
        vec4["xwxw"] = &glm::vec4::xwxw;
        vec4["ywxw"] = &glm::vec4::ywxw;
        vec4["zwxw"] = &glm::vec4::zwxw;
        vec4["wwxw"] = &glm::vec4::wwxw;
        vec4["xxyw"] = &glm::vec4::xxyw;
        vec4["yxyw"] = &glm::vec4::yxyw;
        vec4["zxyw"] = &glm::vec4::zxyw;
        vec4["wxyw"] = &glm::vec4::wxyw;
        vec4["xyyw"] = &glm::vec4::xyyw;
        vec4["yyyw"] = &glm::vec4::yyyw;
        vec4["zyyw"] = &glm::vec4::zyyw;
        vec4["wyyw"] = &glm::vec4::wyyw;
        vec4["xzyw"] = &glm::vec4::xzyw;
        vec4["yzyw"] = &glm::vec4::yzyw;
        vec4["zzyw"] = &glm::vec4::zzyw;
        vec4["wzyw"] = &glm::vec4::wzyw;
        vec4["xwyw"] = &glm::vec4::xwyw;
        vec4["ywyw"] = &glm::vec4::ywyw;
        vec4["zwyw"] = &glm::vec4::zwyw;
        vec4["wwyw"] = &glm::vec4::wwyw;
        vec4["xxzw"] = &glm::vec4::xxzw;
        vec4["yxzw"] = &glm::vec4::yxzw;
        vec4["zxzw"] = &glm::vec4::zxzw;
        vec4["wxzw"] = &glm::vec4::wxzw;
        vec4["xyzw"] = &glm::vec4::xyzw;
        vec4["yyzw"] = &glm::vec4::yyzw;
        vec4["zyzw"] = &glm::vec4::zyzw;
        vec4["wyzw"] = &glm::vec4::wyzw;
        vec4["xzzw"] = &glm::vec4::xzzw;
        vec4["yzzw"] = &glm::vec4::yzzw;
        vec4["zzzw"] = &glm::vec4::zzzw;
        vec4["wzzw"] = &glm::vec4::wzzw;
        vec4["xwzw"] = &glm::vec4::xwzw;
        vec4["ywzw"] = &glm::vec4::ywzw;
        vec4["zwzw"] = &glm::vec4::zwzw;
        vec4["wwzw"] = &glm::vec4::wwzw;
        vec4["xxww"] = &glm::vec4::xxww;
        vec4["yxww"] = &glm::vec4::yxww;
        vec4["zxww"] = &glm::vec4::zxww;
        vec4["wxww"] = &glm::vec4::wxww;
        vec4["xyww"] = &glm::vec4::xyww;
        vec4["yyww"] = &glm::vec4::yyww;
        vec4["zyww"] = &glm::vec4::zyww;
        vec4["wyww"] = &glm::vec4::wyww;
        vec4["xzww"] = &glm::vec4::xzww;
        vec4["yzww"] = &glm::vec4::yzww;
        vec4["zzww"] = &glm::vec4::zzww;
        vec4["wzww"] = &glm::vec4::wzww;
        vec4["xwww"] = &glm::vec4::xwww;
        vec4["ywww"] = &glm::vec4::ywww;
        vec4["zwww"] = &glm::vec4::zwww;
        vec4["wwww"] = &glm::vec4::wwww;

        vec4["rr"] = &glm::vec4::rr;
        vec4["gr"] = &glm::vec4::gr;
        vec4["br"] = &glm::vec4::br;
        vec4["ar"] = &glm::vec4::ar;
        vec4["rg"] = &glm::vec4::rg;
        vec4["gg"] = &glm::vec4::gg;
        vec4["bg"] = &glm::vec4::bg;
        vec4["ag"] = &glm::vec4::ag;
        vec4["rb"] = &glm::vec4::rb;
        vec4["gb"] = &glm::vec4::gb;
        vec4["bb"] = &glm::vec4::bb;
        vec4["ab"] = &glm::vec4::ab;
        vec4["ra"] = &glm::vec4::ra;
        vec4["ga"] = &glm::vec4::ga;
        vec4["ba"] = &glm::vec4::ba;
        vec4["aa"] = &glm::vec4::aa;
        vec4["rrr"] = &glm::vec4::rrr;
        vec4["grr"] = &glm::vec4::grr;
        vec4["brr"] = &glm::vec4::brr;
        vec4["arr"] = &glm::vec4::arr;
        vec4["rgr"] = &glm::vec4::rgr;
        vec4["ggr"] = &glm::vec4::ggr;
        vec4["bgr"] = &glm::vec4::bgr;
        vec4["agr"] = &glm::vec4::agr;
        vec4["rbr"] = &glm::vec4::rbr;
        vec4["gbr"] = &glm::vec4::gbr;
        vec4["bbr"] = &glm::vec4::bbr;
        vec4["abr"] = &glm::vec4::abr;
        vec4["rar"] = &glm::vec4::rar;
        vec4["gar"] = &glm::vec4::gar;
        vec4["bar"] = &glm::vec4::bar;
        vec4["aar"] = &glm::vec4::aar;
        vec4["rrg"] = &glm::vec4::rrg;
        vec4["grg"] = &glm::vec4::grg;
        vec4["brg"] = &glm::vec4::brg;
        vec4["arg"] = &glm::vec4::arg;
        vec4["rgg"] = &glm::vec4::rgg;
        vec4["ggg"] = &glm::vec4::ggg;
        vec4["bgg"] = &glm::vec4::bgg;
        vec4["agg"] = &glm::vec4::agg;
        vec4["rbg"] = &glm::vec4::rbg;
        vec4["gbg"] = &glm::vec4::gbg;
        vec4["bbg"] = &glm::vec4::bbg;
        vec4["abg"] = &glm::vec4::abg;
        vec4["rag"] = &glm::vec4::rag;
        vec4["gag"] = &glm::vec4::gag;
        vec4["bag"] = &glm::vec4::bag;
        vec4["aag"] = &glm::vec4::aag;
        vec4["rrb"] = &glm::vec4::rrb;
        vec4["grb"] = &glm::vec4::grb;
        vec4["brb"] = &glm::vec4::brb;
        vec4["arb"] = &glm::vec4::arb;
        vec4["rgb"] = &glm::vec4::rgb;
        vec4["ggb"] = &glm::vec4::ggb;
        vec4["bgb"] = &glm::vec4::bgb;
        vec4["agb"] = &glm::vec4::agb;
        vec4["rbb"] = &glm::vec4::rbb;
        vec4["gbb"] = &glm::vec4::gbb;
        vec4["bbb"] = &glm::vec4::bbb;
        vec4["abb"] = &glm::vec4::abb;
        vec4["rab"] = &glm::vec4::rab;
        vec4["gab"] = &glm::vec4::gab;
        vec4["bab"] = &glm::vec4::bab;
        vec4["aab"] = &glm::vec4::aab;
        vec4["rra"] = &glm::vec4::rra;
        vec4["gra"] = &glm::vec4::gra;
        vec4["bra"] = &glm::vec4::bra;
        vec4["ara"] = &glm::vec4::ara;
        vec4["rga"] = &glm::vec4::rga;
        vec4["gga"] = &glm::vec4::gga;
        vec4["bga"] = &glm::vec4::bga;
        vec4["aga"] = &glm::vec4::aga;
        vec4["rba"] = &glm::vec4::rba;
        vec4["gba"] = &glm::vec4::gba;
        vec4["bba"] = &glm::vec4::bba;
        vec4["aba"] = &glm::vec4::aba;
        vec4["raa"] = &glm::vec4::raa;
        vec4["gaa"] = &glm::vec4::gaa;
        vec4["baa"] = &glm::vec4::baa;
        vec4["aaa"] = &glm::vec4::aaa;
        vec4["rrrr"] = &glm::vec4::rrrr;
        vec4["grrr"] = &glm::vec4::grrr;
        vec4["brrr"] = &glm::vec4::brrr;
        vec4["arrr"] = &glm::vec4::arrr;
        vec4["rgrr"] = &glm::vec4::rgrr;
        vec4["ggrr"] = &glm::vec4::ggrr;
        vec4["bgrr"] = &glm::vec4::bgrr;
        vec4["agrr"] = &glm::vec4::agrr;
        vec4["rbrr"] = &glm::vec4::rbrr;
        vec4["gbrr"] = &glm::vec4::gbrr;
        vec4["bbrr"] = &glm::vec4::bbrr;
        vec4["abrr"] = &glm::vec4::abrr;
        vec4["rarr"] = &glm::vec4::rarr;
        vec4["garr"] = &glm::vec4::garr;
        vec4["barr"] = &glm::vec4::barr;
        vec4["aarr"] = &glm::vec4::aarr;
        vec4["rrgr"] = &glm::vec4::rrgr;
        vec4["grgr"] = &glm::vec4::grgr;
        vec4["brgr"] = &glm::vec4::brgr;
        vec4["argr"] = &glm::vec4::argr;
        vec4["rggr"] = &glm::vec4::rggr;
        vec4["gggr"] = &glm::vec4::gggr;
        vec4["bggr"] = &glm::vec4::bggr;
        vec4["aggr"] = &glm::vec4::aggr;
        vec4["rbgr"] = &glm::vec4::rbgr;
        vec4["gbgr"] = &glm::vec4::gbgr;
        vec4["bbgr"] = &glm::vec4::bbgr;
        vec4["abgr"] = &glm::vec4::abgr;
        vec4["ragr"] = &glm::vec4::ragr;
        vec4["gagr"] = &glm::vec4::gagr;
        vec4["bagr"] = &glm::vec4::bagr;
        vec4["aagr"] = &glm::vec4::aagr;
        vec4["rrbr"] = &glm::vec4::rrbr;
        vec4["grbr"] = &glm::vec4::grbr;
        vec4["brbr"] = &glm::vec4::brbr;
        vec4["arbr"] = &glm::vec4::arbr;
        vec4["rgbr"] = &glm::vec4::rgbr;
        vec4["ggbr"] = &glm::vec4::ggbr;
        vec4["bgbr"] = &glm::vec4::bgbr;
        vec4["agbr"] = &glm::vec4::agbr;
        vec4["rbbr"] = &glm::vec4::rbbr;
        vec4["gbbr"] = &glm::vec4::gbbr;
        vec4["bbbr"] = &glm::vec4::bbbr;
        vec4["abbr"] = &glm::vec4::abbr;
        vec4["rabr"] = &glm::vec4::rabr;
        vec4["gabr"] = &glm::vec4::gabr;
        vec4["babr"] = &glm::vec4::babr;
        vec4["aabr"] = &glm::vec4::aabr;
        vec4["rrar"] = &glm::vec4::rrar;
        vec4["grar"] = &glm::vec4::grar;
        vec4["brar"] = &glm::vec4::brar;
        vec4["arar"] = &glm::vec4::arar;
        vec4["rgar"] = &glm::vec4::rgar;
        vec4["ggar"] = &glm::vec4::ggar;
        vec4["bgar"] = &glm::vec4::bgar;
        vec4["agar"] = &glm::vec4::agar;
        vec4["rbar"] = &glm::vec4::rbar;
        vec4["gbar"] = &glm::vec4::gbar;
        vec4["bbar"] = &glm::vec4::bbar;
        vec4["abar"] = &glm::vec4::abar;
        vec4["raar"] = &glm::vec4::raar;
        vec4["gaar"] = &glm::vec4::gaar;
        vec4["baar"] = &glm::vec4::baar;
        vec4["aaar"] = &glm::vec4::aaar;
        vec4["rrrg"] = &glm::vec4::rrrg;
        vec4["grrg"] = &glm::vec4::grrg;
        vec4["brrg"] = &glm::vec4::brrg;
        vec4["arrg"] = &glm::vec4::arrg;
        vec4["rgrg"] = &glm::vec4::rgrg;
        vec4["ggrg"] = &glm::vec4::ggrg;
        vec4["bgrg"] = &glm::vec4::bgrg;
        vec4["agrg"] = &glm::vec4::agrg;
        vec4["rbrg"] = &glm::vec4::rbrg;
        vec4["gbrg"] = &glm::vec4::gbrg;
        vec4["bbrg"] = &glm::vec4::bbrg;
        vec4["abrg"] = &glm::vec4::abrg;
        vec4["rarg"] = &glm::vec4::rarg;
        vec4["garg"] = &glm::vec4::garg;
        vec4["barg"] = &glm::vec4::barg;
        vec4["aarg"] = &glm::vec4::aarg;
        vec4["rrgg"] = &glm::vec4::rrgg;
        vec4["grgg"] = &glm::vec4::grgg;
        vec4["brgg"] = &glm::vec4::brgg;
        vec4["argg"] = &glm::vec4::argg;
        vec4["rggg"] = &glm::vec4::rggg;
        vec4["gggg"] = &glm::vec4::gggg;
        vec4["bggg"] = &glm::vec4::bggg;
        vec4["aggg"] = &glm::vec4::aggg;
        vec4["rbgg"] = &glm::vec4::rbgg;
        vec4["gbgg"] = &glm::vec4::gbgg;
        vec4["bbgg"] = &glm::vec4::bbgg;
        vec4["abgg"] = &glm::vec4::abgg;
        vec4["ragg"] = &glm::vec4::ragg;
        vec4["gagg"] = &glm::vec4::gagg;
        vec4["bagg"] = &glm::vec4::bagg;
        vec4["aagg"] = &glm::vec4::aagg;
        vec4["rrbg"] = &glm::vec4::rrbg;
        vec4["grbg"] = &glm::vec4::grbg;
        vec4["brbg"] = &glm::vec4::brbg;
        vec4["arbg"] = &glm::vec4::arbg;
        vec4["rgbg"] = &glm::vec4::rgbg;
        vec4["ggbg"] = &glm::vec4::ggbg;
        vec4["bgbg"] = &glm::vec4::bgbg;
        vec4["agbg"] = &glm::vec4::agbg;
        vec4["rbbg"] = &glm::vec4::rbbg;
        vec4["gbbg"] = &glm::vec4::gbbg;
        vec4["bbbg"] = &glm::vec4::bbbg;
        vec4["abbg"] = &glm::vec4::abbg;
        vec4["rabg"] = &glm::vec4::rabg;
        vec4["gabg"] = &glm::vec4::gabg;
        vec4["babg"] = &glm::vec4::babg;
        vec4["aabg"] = &glm::vec4::aabg;
        vec4["rrag"] = &glm::vec4::rrag;
        vec4["grag"] = &glm::vec4::grag;
        vec4["brag"] = &glm::vec4::brag;
        vec4["arag"] = &glm::vec4::arag;
        vec4["rgag"] = &glm::vec4::rgag;
        vec4["ggag"] = &glm::vec4::ggag;
        vec4["bgag"] = &glm::vec4::bgag;
        vec4["agag"] = &glm::vec4::agag;
        vec4["rbag"] = &glm::vec4::rbag;
        vec4["gbag"] = &glm::vec4::gbag;
        vec4["bbag"] = &glm::vec4::bbag;
        vec4["abag"] = &glm::vec4::abag;
        vec4["raag"] = &glm::vec4::raag;
        vec4["gaag"] = &glm::vec4::gaag;
        vec4["baag"] = &glm::vec4::baag;
        vec4["aaag"] = &glm::vec4::aaag;
        vec4["rrrb"] = &glm::vec4::rrrb;
        vec4["grrb"] = &glm::vec4::grrb;
        vec4["brrb"] = &glm::vec4::brrb;
        vec4["arrb"] = &glm::vec4::arrb;
        vec4["rgrb"] = &glm::vec4::rgrb;
        vec4["ggrb"] = &glm::vec4::ggrb;
        vec4["bgrb"] = &glm::vec4::bgrb;
        vec4["agrb"] = &glm::vec4::agrb;
        vec4["rbrb"] = &glm::vec4::rbrb;
        vec4["gbrb"] = &glm::vec4::gbrb;
        vec4["bbrb"] = &glm::vec4::bbrb;
        vec4["abrb"] = &glm::vec4::abrb;
        vec4["rarb"] = &glm::vec4::rarb;
        vec4["garb"] = &glm::vec4::garb;
        vec4["barb"] = &glm::vec4::barb;
        vec4["aarb"] = &glm::vec4::aarb;
        vec4["rrgb"] = &glm::vec4::rrgb;
        vec4["grgb"] = &glm::vec4::grgb;
        vec4["brgb"] = &glm::vec4::brgb;
        vec4["argb"] = &glm::vec4::argb;
        vec4["rggb"] = &glm::vec4::rggb;
        vec4["gggb"] = &glm::vec4::gggb;
        vec4["bggb"] = &glm::vec4::bggb;
        vec4["aggb"] = &glm::vec4::aggb;
        vec4["rbgb"] = &glm::vec4::rbgb;
        vec4["gbgb"] = &glm::vec4::gbgb;
        vec4["bbgb"] = &glm::vec4::bbgb;
        vec4["abgb"] = &glm::vec4::abgb;
        vec4["ragb"] = &glm::vec4::ragb;
        vec4["gagb"] = &glm::vec4::gagb;
        vec4["bagb"] = &glm::vec4::bagb;
        vec4["aagb"] = &glm::vec4::aagb;
        vec4["rrbb"] = &glm::vec4::rrbb;
        vec4["grbb"] = &glm::vec4::grbb;
        vec4["brbb"] = &glm::vec4::brbb;
        vec4["arbb"] = &glm::vec4::arbb;
        vec4["rgbb"] = &glm::vec4::rgbb;
        vec4["ggbb"] = &glm::vec4::ggbb;
        vec4["bgbb"] = &glm::vec4::bgbb;
        vec4["agbb"] = &glm::vec4::agbb;
        vec4["rbbb"] = &glm::vec4::rbbb;
        vec4["gbbb"] = &glm::vec4::gbbb;
        vec4["bbbb"] = &glm::vec4::bbbb;
        vec4["abbb"] = &glm::vec4::abbb;
        vec4["rabb"] = &glm::vec4::rabb;
        vec4["gabb"] = &glm::vec4::gabb;
        vec4["babb"] = &glm::vec4::babb;
        vec4["aabb"] = &glm::vec4::aabb;
        vec4["rrab"] = &glm::vec4::rrab;
        vec4["grab"] = &glm::vec4::grab;
        vec4["brab"] = &glm::vec4::brab;
        vec4["arab"] = &glm::vec4::arab;
        vec4["rgab"] = &glm::vec4::rgab;
        vec4["ggab"] = &glm::vec4::ggab;
        vec4["bgab"] = &glm::vec4::bgab;
        vec4["agab"] = &glm::vec4::agab;
        vec4["rbab"] = &glm::vec4::rbab;
        vec4["gbab"] = &glm::vec4::gbab;
        vec4["bbab"] = &glm::vec4::bbab;
        vec4["abab"] = &glm::vec4::abab;
        vec4["raab"] = &glm::vec4::raab;
        vec4["gaab"] = &glm::vec4::gaab;
        vec4["baab"] = &glm::vec4::baab;
        vec4["aaab"] = &glm::vec4::aaab;
        vec4["rrra"] = &glm::vec4::rrra;
        vec4["grra"] = &glm::vec4::grra;
        vec4["brra"] = &glm::vec4::brra;
        vec4["arra"] = &glm::vec4::arra;
        vec4["rgra"] = &glm::vec4::rgra;
        vec4["ggra"] = &glm::vec4::ggra;
        vec4["bgra"] = &glm::vec4::bgra;
        vec4["agra"] = &glm::vec4::agra;
        vec4["rbra"] = &glm::vec4::rbra;
        vec4["gbra"] = &glm::vec4::gbra;
        vec4["bbra"] = &glm::vec4::bbra;
        vec4["abra"] = &glm::vec4::abra;
        vec4["rara"] = &glm::vec4::rara;
        vec4["gara"] = &glm::vec4::gara;
        vec4["bara"] = &glm::vec4::bara;
        vec4["aara"] = &glm::vec4::aara;
        vec4["rrga"] = &glm::vec4::rrga;
        vec4["grga"] = &glm::vec4::grga;
        vec4["brga"] = &glm::vec4::brga;
        vec4["arga"] = &glm::vec4::arga;
        vec4["rgga"] = &glm::vec4::rgga;
        vec4["ggga"] = &glm::vec4::ggga;
        vec4["bgga"] = &glm::vec4::bgga;
        vec4["agga"] = &glm::vec4::agga;
        vec4["rbga"] = &glm::vec4::rbga;
        vec4["gbga"] = &glm::vec4::gbga;
        vec4["bbga"] = &glm::vec4::bbga;
        vec4["abga"] = &glm::vec4::abga;
        vec4["raga"] = &glm::vec4::raga;
        vec4["gaga"] = &glm::vec4::gaga;
        vec4["baga"] = &glm::vec4::baga;
        vec4["aaga"] = &glm::vec4::aaga;
        vec4["rrba"] = &glm::vec4::rrba;
        vec4["grba"] = &glm::vec4::grba;
        vec4["brba"] = &glm::vec4::brba;
        vec4["arba"] = &glm::vec4::arba;
        vec4["rgba"] = &glm::vec4::rgba;
        vec4["ggba"] = &glm::vec4::ggba;
        vec4["bgba"] = &glm::vec4::bgba;
        vec4["agba"] = &glm::vec4::agba;
        vec4["rbba"] = &glm::vec4::rbba;
        vec4["gbba"] = &glm::vec4::gbba;
        vec4["bbba"] = &glm::vec4::bbba;
        vec4["abba"] = &glm::vec4::abba;
        vec4["raba"] = &glm::vec4::raba;
        vec4["gaba"] = &glm::vec4::gaba;
        vec4["baba"] = &glm::vec4::baba;
        vec4["aaba"] = &glm::vec4::aaba;
        vec4["rraa"] = &glm::vec4::rraa;
        vec4["graa"] = &glm::vec4::graa;
        vec4["braa"] = &glm::vec4::braa;
        vec4["araa"] = &glm::vec4::araa;
        vec4["rgaa"] = &glm::vec4::rgaa;
        vec4["ggaa"] = &glm::vec4::ggaa;
        vec4["bgaa"] = &glm::vec4::bgaa;
        vec4["agaa"] = &glm::vec4::agaa;
        vec4["rbaa"] = &glm::vec4::rbaa;
        vec4["gbaa"] = &glm::vec4::gbaa;
        vec4["bbaa"] = &glm::vec4::bbaa;
        vec4["abaa"] = &glm::vec4::abaa;
        vec4["raaa"] = &glm::vec4::raaa;
        vec4["gaaa"] = &glm::vec4::gaaa;
        vec4["baaa"] = &glm::vec4::baaa;
        vec4["aaaa"] = &glm::vec4::aaaa;*/

        vec4[sol::meta_function::new_index] = &vec4setter;
    }
}
