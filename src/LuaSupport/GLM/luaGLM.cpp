/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "luaGLM.hpp"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/matrix_transform_2d.hpp>
#include <glm/gtx/matrix_decompose.hpp>

#include <string>
#include <sol.hpp>

#include <iostream>

#include "vec2.hpp"
#include "vec3.hpp"
#include "vec4.hpp"

namespace LuaGLM {

    template<typename mat>
    sol::object matGetter(mat & self, sol::stack_object rawKey, sol::this_state L) {
        auto maybe_key = rawKey.as<sol::optional<int>>();

        if (maybe_key) {
            int & key = *maybe_key;

            if (key >= 0 && key < self.length()) {
                return sol::object(L, sol::in_place, &self[key]);
            }
        }

        // If we failed to find it, return nil.
        return sol::object(L, sol::in_place, sol::lua_nil);
    };

    template<typename mat, typename vec>
    void matSetter(mat & self, sol::stack_object rawKey, sol::stack_object rawValue, sol::this_state) {
        auto maybe_value = rawValue.as<sol::optional<vec>>();
        auto maybe_key = rawKey.as<sol::optional<int>>();

        if (maybe_key && maybe_value) {
            int & key = *maybe_key;
            vec value = *maybe_value;

            if (key >= 0 && key < self.length()) {
                self[key] = value;
            }
        }
    }

    template<typename matrix>
    inline void specificMatTransforms(sol::table & mat) {
        // By default, a matrix won't get transformation functions.
    }

    template<>
    inline void specificMatTransforms<glm::mat3x3>(sol::table & mat) {
        mat["rotate"] = +[](glm::mat3x3 & mat, float angle) {
            return glm::rotate(mat, angle);
        };

        mat["scale"] = +[](glm::mat3x3 & mat, glm::vec2 scale) {
            return glm::scale(mat, scale);
        };

        mat["translate"] = +[](glm::mat3x3 & mat, glm::vec2 vec) {
            return glm::translate(mat, vec);
        };

        mat["shearX"] = +[](glm::mat3x3 & mat, float y) {
            return glm::shearX(mat, y);
        };

        mat["shearY"] = +[](glm::mat3x3 & mat, float x) {
            return glm::shearY(mat, x);
        };
    }

    template<>
    inline void specificMatTransforms<glm::mat4x4>(sol::table & mat) {
        mat["rotate"] = +[](glm::mat4x4 & mat, float angle, glm::vec3 vec) {
            return glm::rotate(mat, angle, vec);
        };

        mat["scale"] = +[](glm::mat4x4 & mat, glm::vec3 scale) {
            return glm::scale(mat, scale);
        };

        mat["translate"] = +[](glm::mat4x4 & mat, glm::vec3 vec) {
            return glm::translate(mat, vec);
        };
    }

    template<typename matrix, typename vector>
    std::string matrixToString(matrix & mat) {
        std::string str = "(";

        for (int i = 0; i < mat.length(); i++) {
            str += vecToString<vector>(mat[i]);
        }

        str += ')';

        return str;
    }

    template<typename matrix, int height, int width>
    inline bool insertNextValue(matrix & mat, int & x, int & y, float value) {
        mat[y][x++] = value;



        // Next line.
        if (x >= width) {
            x = 0;
            y++;

            // We've overfilled the matrix. Just ignore the rest of the inputs.
            if (y >= height) {
                return true;
            }
        }

        return false;
    }

    template<typename matrix, int height, int width>
    void matrixFactory(matrix & mat, sol::variadic_args args) {
        new (&mat) matrix();

        int x = 0;
        int y = 0;

        int index = 0;

        for (auto rawValue : args) {
            auto maybe_float = rawValue.as<sol::optional<float>>();
            if (maybe_float) {
                float & value = *maybe_float;
                index++;

                if (insertNextValue<matrix, height, width>(mat, x, y, value)) {
                    break;
                }

                continue;
            }

            auto maybe_vec2 = rawValue.as<sol::optional<glm::vec2>>();
            if (maybe_vec2) {
                glm::vec2 & value = *maybe_vec2;
                index += 2;

                if (insertNextValue<matrix, height, width>(mat, x, y, value.x)) {
                    break;
                }

                if (insertNextValue<matrix, height, width>(mat, x, y, value.y)) {
                    break;
                }

                continue;
            }

            auto maybe_vec3 = rawValue.as<sol::optional<glm::vec3>>();
            if (maybe_vec3) {
                glm::vec3 & value = *maybe_vec3;
                index += 3;

                if (insertNextValue<matrix, height, width>(mat, x, y, value.x)) {
                    break;
                }

                if (insertNextValue<matrix, height, width>(mat, x, y, value.y)) {
                    break;
                }

                if (insertNextValue<matrix, height, width>(mat, x, y, value.z)) {
                    break;
                }

                continue;
            }

            auto maybe_vec4 = rawValue.as<sol::optional<glm::vec4>>();
            if (maybe_vec4) {
                glm::vec4 & value = *maybe_vec4;
                index += 4;

                if (insertNextValue<matrix, height, width>(mat, x, y, value.x)) {
                    break;
                }

                if (insertNextValue<matrix, height, width>(mat, x, y, value.y)) {
                    break;
                }

                if (insertNextValue<matrix, height, width>(mat, x, y, value.z)) {
                    break;
                }

                if (insertNextValue<matrix, height, width>(mat, x, y, value.w)) {
                    break;
                }

                continue;
            }
        }

        if (index != width*height) {
            std::cout << "Index: " << index << std::endl;
            throw sol::error("Invalid number of arguments to build matrix.");
        }
    }

    template<typename matrix>
    void matrixIdentityFactory(matrix & mat) {
        new (&mat) matrix();
    }

    template<typename matrix>
    void matrixScaleFactory(matrix & mat, float value) {
        new (&mat) matrix(value);
    }

    template<typename matrix, typename vector, int height, int width>
    inline void loadMatAxB(sol::state_view lua, const char * name) {

        lua.new_usertype<matrix>(name,
                                 sol::call_constructor, sol::initializers(matrixIdentityFactory<matrix>, matrixScaleFactory<matrix>, matrixFactory<matrix, height, width>),
                                 sol::meta_function::index, &matGetter<matrix>);

        sol::table mat = lua[name];

        mat[sol::meta_function::length] = &matrix::length;

        mat[sol::meta_function::multiplication] = sol::overload(+[](matrix & me, float them) {
            return me * them;
        },
        +[](matrix & me, matrix & them) {
            return me * them;
        },
        +[](matrix & me, vector them) {
            return me * them;
        });

        mat[sol::meta_function::division] = +[](matrix & me, float them) {
            return me / them;
        };

        mat[sol::meta_function::addition] = sol::overload(+[](matrix & me, float them) {
            return me + them;
        },
        +[](matrix & me, matrix & them) {
            return me + them;
        });

        mat[sol::meta_function::subtraction] = sol::overload(+[](matrix & me, float them) {
            return me - them;
        },
        +[](matrix & me, matrix & them) {
            return me - them;
        });

        mat[sol::meta_function::to_string] = matrixToString<matrix, vector>;

        mat["determinant"] = +[](matrix & mat) {
            return glm::determinant(mat);
        };

        mat["inverse"] = +[](matrix & mat) {
            return glm::inverse(mat);
        };

        mat["transpose"] = +[](matrix & mat) {
            return glm::transpose(mat);
        };

        mat["matrixCompMult"] = +[](matrix & mat, matrix & mat2) {
            return glm::matrixCompMult(mat, mat2);
        };

        specificMatTransforms<matrix>(mat);

        // Setter has to be set last otherwise we'll segfault while setting up the functions.
        mat[sol::meta_function::new_index] = &matSetter<matrix, vector>;
    }

    inline void mat4x4builders(sol::state_view lua) {

        sol::table matrix = lua.create_table("mat4x4builders");

        matrix["frustum"] = +[](float left, float right, float bottom, float top, float near, float far) {
            return glm::frustum(left, right, bottom, top, near, far);
        };

        matrix["infinitePerspective"] = +[](float fovy, float aspect, float zNear) {
            return glm::infinitePerspective(fovy, aspect, zNear);
        };

        matrix["lookAt"] = +[](glm::vec3 eye, glm::vec3 center, glm::vec3 up) {
            return glm::lookAt(eye, center, up);
        };

        matrix["ortho"] = sol::overload(
            +[](float left, float right, float bottom, float top) {
                return glm::ortho(left, right, bottom, top);
            },
            +[](float left, float right, float bottom, float top, float near, float far) {
                return glm::ortho(left, right, bottom, top, near, far);
            }
        );

        matrix["perspective"] = +[](float fovy, float aspect, float zNear, float zFar) {
            return glm::perspective(fovy, aspect, zNear, zFar);
        };

        matrix["perspectiveFov"] = +[](float fov, float width, float height, float zNear, float zFar) {
                return glm::perspectiveFov(fov, width, height, zNear, zFar);
            };

        matrix["pickMatrix"] = +[](glm::vec2 center, glm::vec2 delta, glm::vec4 viewport) {
            return glm::pickMatrix(center, delta, viewport);
        };

        matrix["project"] = +[](glm::vec3 obj, glm::mat4 model, glm::mat4 proj, glm::vec4 viewport) {
            return glm::project(obj, model, proj, viewport);
        };

        matrix["rotation"] = +[] (float angle, glm::vec3 axis) {
            return glm::rotate(glm::mat4(1.0f), angle, axis);
        };

        matrix["scale"] = +[] (glm::vec3 axis) {
            return glm::scale(glm::mat4(1.0f), axis);
        };

        matrix["translate"] = +[] (glm::vec3 vec) {
            return glm::translate(glm::mat4(1.0f), vec);
        };
    }
}

void loadLuaGLMAPI(sol::state_view lua) {

    LuaGLM::loadVec2(lua);
    LuaGLM::loadVec3(lua);
    LuaGLM::loadVec4(lua);

    // Standard square sizes.
    LuaGLM::loadMatAxB<glm::mat2x2, glm::vec2, 2, 2>(lua, "mat2x2");
    LuaGLM::loadMatAxB<glm::mat3x3, glm::vec3, 3, 3>(lua, "mat3x3");
    LuaGLM::loadMatAxB<glm::mat4x4, glm::vec4, 4, 4>(lua, "mat4x4");

    LuaGLM::mat4x4builders(lua);
}
