/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "vec2.hpp"

#include "luaGLM.hpp"

#include "AbsolutePanic.hpp"

namespace LuaGLM {
    sol::object vec2getter(glm::vec2 & self, sol::stack_object key, sol::this_state L) {

        // Check for string first.
        auto string_key = key.as<sol::optional<std::string>>();
        if (string_key) {
            // It's actually a string!
            const std::string& k = *string_key;

            unsigned int length = k.length();
            if (length > 4)
            {
                throw std::runtime_error("Max vector size is 4 components.");
            }

            std::vector<float> values;
            values.reserve(length);

            for (char c : k)
            {
                switch(c)
                {
                case 'x':
                case 'r':
                    values.push_back(self.x);
                    break;
                case 'y':
                case 'g':
                    values.push_back(self.y);
                    break;
                default:
                    std::string error = "Unknown vector component \"";
                    error += c;
                    error += "\" for type vec2.";
                    throw std::runtime_error(error);
                }
            }

            switch (length)
            {
            case 1:
                return sol::object(L, sol::in_place, values[0]);
            case 2:
                return sol::object(L, sol::in_place, glm::vec2(values[0], values[1]));
            case 3:
                return sol::object(L, sol::in_place, glm::vec3(values[0], values[1], values[2]));
            case 4:
                return sol::object(L, sol::in_place, glm::vec4(values[0], values[1], values[2], values[3]));

            default:
                ABSOLUTE_PANIC();
            }
        }

        // Okay, so it wasn't a string.
        // Check for a number then.
        auto numeric_key = key.as<sol::optional<int>>();
        if (numeric_key) {
            int n = *numeric_key;
            switch (n) {
            case 0:
                return sol::object(L, sol::in_place, self.x);
            case 1:
                return sol::object(L, sol::in_place, self.y);
            default:
                break;
            }
        }

        // If we failed to find it, return nil.
        return sol::object(L, sol::in_place, sol::lua_nil);
    };

    void vec2setter(glm::vec2 & self, sol::stack_object key, sol::stack_object rawValue, sol::this_state) {
        // Check for string first.

        float value = rawValue.as<float>();

        auto string_key = key.as<sol::optional<std::string>>();
        if (string_key) {
            // It's actually a string!
            const std::string& k = *string_key;

            if (k == "x" || k == "r") {
                self.x = value;
                return;
            }

            if (k == "y" || k == "g") {
                self.y = value;
                return;
            }
        }

        // Okay, so it wasn't a string.
        // Check for a number then.
        auto numeric_key = key.as<sol::optional<int>>();
        if (numeric_key) {
            int n = *numeric_key;
            switch (n) {
            case 0:
                self.x = value;
                return;
            case 1:
                self.y = value;
                return;
            }
        }
    };

    void loadVec2(sol::state_view lua) {
    sol::constructors<glm::vec2(), glm::vec2(float), glm::vec2(float, float), glm::vec2(glm::vec2)> vec2ctors;

        lua.new_usertype<glm::vec2>("vec2",
        sol::call_constructor, vec2ctors,
        "x", &glm::vec2::x,
        "y", &glm::vec2::y,
        "r", &glm::vec2::r,
        "g", &glm::vec2::g,

        sol::meta_function::index, &vec2getter);

        sol::table vec2 = lua["vec2"];

        loadGenericVectorFunctions<glm::vec2>(vec2);

        /*vec2["xx"] = &glm::vec2::xx;
        vec2["yx"] = &glm::vec2::yx;
        vec2["xy"] = &glm::vec2::xy;
        vec2["yy"] = &glm::vec2::yy;
        vec2["xxx"] = &glm::vec2::xxx;
        vec2["yxx"] = &glm::vec2::yxx;
        vec2["xyx"] = &glm::vec2::xyx;
        vec2["yyx"] = &glm::vec2::yyx;
        vec2["xxy"] = &glm::vec2::xxy;
        vec2["yxy"] = &glm::vec2::yxy;
        vec2["xyy"] = &glm::vec2::xyy;
        vec2["yyy"] = &glm::vec2::yyy;
        vec2["xxxx"] = &glm::vec2::xxxx;
        vec2["yxxx"] = &glm::vec2::yxxx;
        vec2["xyxx"] = &glm::vec2::xyxx;
        vec2["yyxx"] = &glm::vec2::yyxx;
        vec2["xxyx"] = &glm::vec2::xxyx;
        vec2["yxyx"] = &glm::vec2::yxyx;
        vec2["xyyx"] = &glm::vec2::xyyx;
        vec2["yyyx"] = &glm::vec2::yyyx;
        vec2["xxxy"] = &glm::vec2::xxxy;
        vec2["yxxy"] = &glm::vec2::yxxy;
        vec2["xyxy"] = &glm::vec2::xyxy;
        vec2["yyxy"] = &glm::vec2::yyxy;
        vec2["xxyy"] = &glm::vec2::xxyy;
        vec2["yxyy"] = &glm::vec2::yxyy;
        vec2["xyyy"] = &glm::vec2::xyyy;
        vec2["yyyy"] = &glm::vec2::yyyy;

        vec2["rr"] = &glm::vec2::rr;
        vec2["gr"] = &glm::vec2::gr;
        vec2["rg"] = &glm::vec2::rg;
        vec2["gg"] = &glm::vec2::gg;
        vec2["rrr"] = &glm::vec2::rrr;
        vec2["grr"] = &glm::vec2::grr;
        vec2["rgr"] = &glm::vec2::rgr;
        vec2["ggr"] = &glm::vec2::ggr;
        vec2["rrg"] = &glm::vec2::rrg;
        vec2["grg"] = &glm::vec2::grg;
        vec2["rgg"] = &glm::vec2::rgg;
        vec2["ggg"] = &glm::vec2::ggg;
        vec2["rrrr"] = &glm::vec2::rrrr;
        vec2["grrr"] = &glm::vec2::grrr;
        vec2["rgrr"] = &glm::vec2::rgrr;
        vec2["ggrr"] = &glm::vec2::ggrr;
        vec2["rrgr"] = &glm::vec2::rrgr;
        vec2["grgr"] = &glm::vec2::grgr;
        vec2["rggr"] = &glm::vec2::rggr;
        vec2["gggr"] = &glm::vec2::gggr;
        vec2["rrrg"] = &glm::vec2::rrrg;
        vec2["grrg"] = &glm::vec2::grrg;
        vec2["rgrg"] = &glm::vec2::rgrg;
        vec2["ggrg"] = &glm::vec2::ggrg;
        vec2["rrgg"] = &glm::vec2::rrgg;
        vec2["grgg"] = &glm::vec2::grgg;
        vec2["rggg"] = &glm::vec2::rggg;
        vec2["gggg"] = &glm::vec2::gggg;*/

        // Setter has to be set last otherwise we'll segfault while setting up the functions.
        vec2[sol::meta_function::new_index] = &vec2setter;
    }
}
