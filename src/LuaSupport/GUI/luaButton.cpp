/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "luaButton.hpp"
#include "luaRM.hpp"

#include <iostream>
#include <glm/glm.hpp>
#include <nanogui/button.h>
using namespace nanogui;

#include "Icons.hpp"

namespace luaGUI {
    std::unordered_map<std::string, Button::IconPosition> buttonIconPositionMap(
    {
        {"left", Button::IconPosition::Left},
        {"left centered", Button::IconPosition::LeftCentered},
        {"right centered", Button::IconPosition::RightCentered},
        {"right", Button::IconPosition::Right}
    });

    void loadButton(sol::table & gui) {
        gui.new_usertype<Button>(
            "Button",
            "new", sol::factories(
                &luaObjectFactory<Button, Widget*, std::string>,
                &luaObjectFactory<Button, Widget*, std::string, int>
            ),
            sol::base_classes, sol::bases<Widget, RM::GCObject>(),

            "setCaption", &Button::setCaption,
            "getCaption", &Button::caption,

            "getBackgroundColor", &Button::backgroundColor,
            "setBackgroundColor", &Button::setBackgroundColor,

            "getTextColor", &Button::textColor,
            "setTextColor", &Button::setTextColor,

            "setIcon", +[](Button & button, std::string name) {
                button.setIcon(getIcon(name));
            },

            "getIcon", +[](Button & button) {
                return getIconName(button.icon());
            },

            "setIconPosition", +[](Button & button, std::string position) {
                std::unordered_map<std::string, Button::IconPosition>::const_iterator result = buttonIconPositionMap.find(position);
                if (result != buttonIconPositionMap.end()) {
                    button.setIconPosition(result->second);
                } else {
                    std::cout << "WARNING: unknown icon position requested \"" << position << "\". Choosing to ignore.";
                }
            },

            "getIconPosition", +[](Button & button) {
                switch (button.iconPosition()) {
                    case Button::IconPosition::Left: return "left";
                    case Button::IconPosition::LeftCentered: return "left centered";
                    case Button::IconPosition::RightCentered: return "right centered";
                    case Button::IconPosition::Right: return "right";
                    default: return "AAAAAAAAAA!";
                }
            },

            "isPushed", &Button::pushed,
            "setPushed", &Button::setPushed
        );
    }
}
