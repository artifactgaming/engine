/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "luaRM.hpp"
#include "luaWidget.hpp"

#include <iostream>
#include <glm/glm.hpp>
#include <nanogui/widget.h>
using namespace nanogui;

#include "globals.hpp"
#include "WidgetManager.hpp"

namespace luaGUI {

    void loadWidget(sol::table & gui) {

        gui.new_usertype<Widget>(
            "Widget",
            "new", sol::factories(&luaObjectFactory<Widget, Widget*>),
            sol::base_classes, sol::bases<RM::GCObject>(),

            "getParent", sol::resolve<Widget*()>(&Widget::parent),
            "getScreen", &Widget::screen,
            "setPosition", sol::overload(+[](Widget & widget, int x, int y) {
                widget.setPosition(glm::ivec2(x, y));
            },
            +[](Widget & widget, glm::vec2 vec) {
                widget.setPosition(glm::ivec2(vec));
            }),

            "getPosition", +[](Widget & widget) {
                glm::ivec2 vec = widget.position();
                return glm::vec2(vec);
            },

            "getAbsolutePosition", +[](Widget & widget) {
                glm::ivec2 vec = widget.absolutePosition();
                return glm::vec2(vec);
            },

            "getSize", +[](Widget & widget) {
                glm::ivec2 vec = widget.size();
                return glm::vec2(vec);
            },

            "setSize", sol::overload(+[](Widget & widget, int x, int y) {
                widget.setSize(glm::ivec2(x, y));
            },
            +[](Widget & widget, glm::vec2 vec) {
                widget.setSize(vec);
            }),

            "getFixedSize", +[](Widget & widget) {
                glm::ivec2 vec = widget.size();
                return glm::vec2(vec.x, vec.y);
            },

            "setFixedSize", sol::overload(+[](Widget & widget, int x, int y) {
                widget.setFixedSize(glm::ivec2(x, y));
            },
            +[](Widget & widget, glm::vec2 vec) {
                widget.setFixedSize(glm::ivec2(vec));
            }),

            "setWidth", &Widget::setWidth,
            "setHeight", &Widget::setHeight,
            "setFixedWidth", &Widget::setFixedWidth,
            "setFixedHeight", &Widget::setFixedHeight,

            "getWidth", +[](Widget & widget) {
                return widget.size().x;
            },

            "getHeight", +[](Widget & widget) {
                return widget.size().y;
            },

            "getFixedWidth", +[](Widget & widget) {
                return widget.fixedWidth();
            },

            "getFixedHeight", +[](Widget & widget) {
                return widget.fixedHeight();
            },

            "getVisible", &Widget::visible,
            "setVisible", &Widget::setVisible,

            "requestFocus", &Widget::requestFocus,

            "childAt", sol::resolve<Widget*(int)>(&Widget::childAt),
            "childIndex",&Widget::childIndex,
            "pushChildToFront", &Widget::pushChildToFront,
            "pushChildToBack", &Widget::pushChildToBack,
            "pushToBack", +[](Widget * widget) {
                widget->parent()->pushChildToBack(widget);
            },
            "pushToFront", +[](Widget * widget) {
                widget->parent()->pushChildToFront(widget);
            },

            "addResizeCallback", +[](Widget * widget, sol::function func)
            {
                return widget->addResizeCallback([func](const glm::vec2 & vec)
                {
                    func(vec);
                });
            },
            "removeResizeCallback", &Widget::removeResizeCallback,

            "dispose", +[](Widget * widget)
            {
                getWidgetManager()->queueWidgetToDispose(widget);
            }
        );
    }
}
