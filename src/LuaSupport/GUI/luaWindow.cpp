/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "luaWindow.hpp"

#include <glm/glm.hpp>
#include <nanogui/window.h>
using namespace nanogui;

#include "luaRM.hpp"
#include "luaWidget.hpp"

namespace luaGUI {

    void loadWindow(sol::table & gui) {

        gui.new_usertype<Window>(
            "Window",
            "new", sol::factories(&luaObjectFactory<Window, Widget*>, &luaObjectFactory<Window, Widget*, std::string>),
            sol::base_classes, sol::bases<Widget, RM::GCObject>()
        );

        sol::table window = gui["Window"];

        window["getTitle"] = &Window::title;
        window["setTitle"] = &Window::setTitle;

        window["center"] = &Window::center;

        window["addDisposeCallback"] = &Window::addDisposeCallback;
        window["removeDisposeCallback"] = &Window::removeDisposeCallback;
    }
}
