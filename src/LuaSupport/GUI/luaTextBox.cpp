/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "luaRM.hpp"
#include "luaTextBox.hpp"

#include <iostream>
#include <glm/glm.hpp>
#include <nanogui/textbox.h>
using namespace nanogui;

namespace luaGUI {
    void loadTextBox(sol::table & gui) {

        gui.new_usertype<TextBox>(
            "TextBox",
            "new", sol::factories(&luaObjectFactory<TextBox, Widget*>, &luaObjectFactory<TextBox, Widget*, std::string>),
            sol::base_classes, sol::bases<Widget, RM::GCObject>()
        );

        sol::table textbox = gui["TextBox"];

        textbox["isEditable"] = &TextBox::editable;
        textbox["setEditable"] = &TextBox::setEditable;
        textbox["getValue"] = &TextBox::value;
        textbox["setValue"] = &TextBox::setValue;
        textbox["getUnits"] = &TextBox::units;
        textbox["setUnits"] = &TextBox::setUnits;
        textbox["getPlaceholder"] = &TextBox::placeholder;
        textbox["setPlaceholder"] = &TextBox::setPlaceholder;
    }
}
