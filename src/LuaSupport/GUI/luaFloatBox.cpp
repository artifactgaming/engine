/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "luaFloatBox.hpp"
#include "luaRM.hpp"

#include <iostream>
#include <glm/glm.hpp>
#include <nanogui/textbox.h>
using namespace nanogui;

namespace luaGUI {
    void loadFloatBox(sol::table & gui) {
        gui.new_usertype<FloatBox<float>>(
            "FloatBox",
            "new", sol::factories(&luaObjectFactory<FloatBox<float>, Widget*>),
            sol::base_classes, sol::bases<Widget, RM::GCObject>()
        );

        sol::table floatbox = gui["FloatBox"];

        floatbox["isEditable"] = &FloatBox<float>::editable;
        floatbox["setEditable"] = &FloatBox<float>::setEditable;
        floatbox["getValue"] = &FloatBox<float>::value;
        floatbox["setValue"] = &FloatBox<float>::setValue;
        floatbox["getUnits"] = &FloatBox<float>::units;
        floatbox["setUnits"] = &FloatBox<float>::setUnits;
        floatbox["getPlaceholder"] = &FloatBox<float>::placeholder;
        floatbox["setPlaceholder"] = &FloatBox<float>::setPlaceholder;
    }
}
