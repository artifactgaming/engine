/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "luaRM.hpp"
#include "luaVScrollPanel.hpp"

#include <iostream>
#include <glm/glm.hpp>
#include <nanogui/vscrollpanel.h>
using namespace nanogui;

namespace luaGUI {
    void loadVScrollPanel(sol::table & gui) {
        gui.new_usertype<VScrollPanel>(
            "VScrollPanel",
            "new", sol::factories(&luaObjectFactory<VScrollPanel, Widget*>),
            sol::base_classes, sol::bases<Widget, RM::GCObject>()
        );

        sol::table panel = gui["VScrollPanel"];

        panel["getScroll"] = &VScrollPanel::scroll;
        panel["setScroll"] = &VScrollPanel::setScroll;
    }
}
