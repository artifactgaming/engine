/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "luaRM.hpp"
#include "luaSlider.hpp"

#include <iostream>
#include <glm/glm.hpp>
#include <nanogui/slider.h>
using namespace nanogui;

namespace luaGUI {
    void loadSlider(sol::table & gui) {

        gui.new_usertype<Slider>(
            "Slider",
            "new", sol::factories(&luaObjectFactory<Slider, Widget*>),
            sol::base_classes, sol::bases<Widget, RM::GCObject>()
        );

        sol::table slider = gui["Slider"];

        slider["getValue"] = &Slider::value;
        slider["setValue"] = &Slider::setValue;

        slider["setRange"] = +[](Slider & slider, float low, float high) {
            slider.setRange(std::pair<float, float>(low, high));
        };

        slider["getRange"] = +[](Slider & slider) {
            std::pair<float, float> range = slider.range();
            return std::tuple<float, float>(range.first, range.second);
        };
    }
}
