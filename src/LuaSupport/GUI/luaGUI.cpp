/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "luaRM.hpp"
#include "GUI/luaGUI.hpp"

#include "globals.hpp"
#include "WidgetManager.hpp"
#include "ResourceManager.hpp"

#include "JsonGUI.hpp"
#include "Icons.hpp"

#include "luaWidget.hpp"
#include "luaWindow.hpp"
#include "luaButton.hpp"
#include "luaPopupButton.hpp"
#include "luaCheckbox.hpp"
#include "luaLabel.hpp"
#include "luaSlider.hpp"
#include "luaTextBox.hpp"
#include "luaFloatBox.hpp"
#include "luaIntBox.hpp"
#include "luaVScrollPanel.hpp"
#include "luaScreen.hpp"

#include <nanogui/nanogui.h>

namespace luaGUI {
    static RM::ref<RM::JsonGUI> JsonGUIFactory1(const std::string path, sol::this_state lua) {
        sol::state_view view(lua);
        RM::ref<RM::JsonGUI> result = new RM::JsonGUI(nullptr, getWidgetManager()->getScreen(), view, sol::lua_nil, path);
        return result;
    }

    static RM::ref<RM::JsonGUI> JsonGUIFactory2(const std::string path, nanogui::Widget * parent, sol::this_state lua) {
        sol::state_view view(lua);
        RM::ref<RM::JsonGUI> result = new RM::JsonGUI(nullptr, parent, view, sol::lua_nil, path);
        return result;
    }

    static RM::ref<RM::JsonGUI> JsonGUIFactory3(const std::string path, nanogui::Widget * parent, sol::table args, sol::this_state lua) {
        sol::state_view view(lua);
        RM::ref<RM::JsonGUI> result = new RM::JsonGUI(nullptr, parent, view, args, path);
        return result;
    }

    static RM::ref<RM::JsonGUI> JsonGUIFactory4(const RM::Path & path, sol::this_state lua) {
        sol::state_view view(lua);
        RM::ref<RM::JsonGUI> result = new RM::JsonGUI(nullptr, getWidgetManager()->getScreen(), view, sol::lua_nil, path);
        return result;
    }

    static RM::ref<RM::JsonGUI> JsonGUIFactory5(const RM::Path & path, nanogui::Widget * parent, sol::this_state lua) {
        sol::state_view view(lua);
        RM::ref<RM::JsonGUI> result = new RM::JsonGUI(nullptr, parent, view, sol::lua_nil, path);
        return result;
    }

    static RM::ref<RM::JsonGUI> JsonGUIFactory6(const RM::Path & path, nanogui::Widget * parent, sol::table args, sol::this_state lua) {
        sol::state_view view(lua);
        RM::ref<RM::JsonGUI> result = new RM::JsonGUI(nullptr, parent, view, args, path);
        return result;
    }

    static RM::ref<RM::JsonGUI> JsonGUIFactory7(RM::ResourceGroup * group, const std::string path, sol::this_state lua) {
        sol::state_view view(lua);
        RM::ref<RM::JsonGUI> result = new RM::JsonGUI(nullptr, getWidgetManager()->getScreen(), view, sol::lua_nil, path);
        return result;
    }

    static RM::ref<RM::JsonGUI> JsonGUIFactory8(RM::ResourceGroup * group, const std::string path, nanogui::Widget * parent, sol::this_state lua) {
        sol::state_view view(lua);
        RM::ref<RM::JsonGUI> result = new RM::JsonGUI(nullptr, parent, view, sol::lua_nil, path);
        return result;
    }

    static RM::ref<RM::JsonGUI> JsonGUIFactory9(RM::ResourceGroup * group, const std::string path, nanogui::Widget * parent, sol::table args, sol::this_state lua) {
        sol::state_view view(lua);
        RM::ref<RM::JsonGUI> result = new RM::JsonGUI(nullptr, parent, view, args, path);
        return result;
    }

    static RM::ref<RM::JsonGUI> JsonGUIFactory10(RM::ResourceGroup * group, const RM::Path & path, sol::this_state lua) {
        sol::state_view view(lua);
        RM::ref<RM::JsonGUI> result = new RM::JsonGUI(nullptr, getWidgetManager()->getScreen(), view, sol::lua_nil, path);
        return result;
    }

    static RM::ref<RM::JsonGUI> JsonGUIFactory11(RM::ResourceGroup * group, const RM::Path & path, nanogui::Widget * parent, sol::this_state lua) {
        sol::state_view view(lua);
        RM::ref<RM::JsonGUI> result = new RM::JsonGUI(nullptr, parent, view, sol::lua_nil, path);
        return result;
    }

    static RM::ref<RM::JsonGUI> JsonGUIFactory12(RM::ResourceGroup * group, const RM::Path & path, nanogui::Widget * parent, sol::table args, sol::this_state lua) {
        sol::state_view view(lua);
        RM::ref<RM::JsonGUI> result = new RM::JsonGUI(nullptr, parent, view, args, path);
        return result;
    }

    void loadGUIWidgets(sol::table & gui) {
        loadWidget(gui);
        loadWindow(gui);
        loadButton(gui);
        loadPopupButton(gui);
        loadCheckbox(gui);
        loadLabel(gui);
        loadSlider(gui);

        loadTextBox(gui);
        loadFloatBox(gui);
        loadIntBox(gui);

        loadVScrollPanel(gui);

        loadScreen(gui);
    }

    void JsonGUISetter(RM::JsonGUI & self, sol::stack_object key, sol::stack_object rawValue, sol::this_state) {
        self.getENV()[key] = rawValue;
    }

    sol::object JsonGUIGetter(RM::JsonGUI & self, sol::stack_object key, sol::this_state L) {
        return sol::object(L, sol::in_place, self.getENV()[key]);
    }
}

void loadLuaGUIAPI(sol::table env) {
    sol::state_view lua = getResourceManager()->getStateOnly();

    sol::table gui = lua.create_table();
    env["GUI"] = gui;

    luaGUI::loadGUIWidgets(gui);

    // TODO we need to make it so trying to access this object in Lua is a 1:1 access to the JsonGUI environment.
    gui.new_usertype<RM::JsonGUI>(
        "json",
        sol::base_classes, sol::bases<RM::BasicResource, RM::GCObject>(),
        "new", sol::factories(
            &luaGUI::JsonGUIFactory1,
            &luaGUI::JsonGUIFactory2,
            &luaGUI::JsonGUIFactory3,
            &luaGUI::JsonGUIFactory4,
            &luaGUI::JsonGUIFactory5,
            &luaGUI::JsonGUIFactory6,
            &luaGUI::JsonGUIFactory7,
            &luaGUI::JsonGUIFactory8,
            &luaGUI::JsonGUIFactory9,
            &luaGUI::JsonGUIFactory10,
            &luaGUI::JsonGUIFactory11,
            &luaGUI::JsonGUIFactory12
        ),
        sol::meta_function::index, &luaGUI::JsonGUIGetter,
        sol::meta_function::new_index, &luaGUI::JsonGUISetter
    );

    gui.new_usertype<nanogui::Screen>(
        "Screen",
        "new", sol::no_constructor,
        sol::base_classes, sol::bases<nanogui::Widget, RM::GCObject>(),
        "breakFocus", +[](nanogui::Screen & screen)
        {
            screen.breakFocus();
        }
    );

    gui["getIcon"] = +[](std::string name) { return std::string(nanogui::utf8(getIcon(name)).data()); };
    gui["getIconName"] = &getIconName;

    gui["getScreen"] = +[]() {
        return RM::ref<nanogui::Screen>(getWidgetManager()->getScreen());
    };
}
