/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "luaCheckbox.hpp"
#include "luaRM.hpp"

#include <iostream>
#include <glm/glm.hpp>
#include <nanogui/checkbox.h>
using namespace nanogui;

namespace luaGUI {
    void loadCheckbox(sol::table & gui) {
        gui.new_usertype<CheckBox>(
            "CheckBox",
            "new", sol::factories(&luaObjectFactory<CheckBox, Widget*, std::string>),
            sol::base_classes, sol::bases<Widget, RM::GCObject>()
        );

        sol::table checkbox = gui["CheckBox"];

        checkbox["setCaption"] = &CheckBox::setCaption;
        checkbox["getCaption"] = &CheckBox::caption;

        checkbox["setChecked"] = &CheckBox::setChecked;
        checkbox["isChecked"] = &CheckBox::checked;
    }
}
