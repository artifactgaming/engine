/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "luaScreen.hpp"

#include <glm/glm.hpp>
#include <nanogui/screen.h>
using namespace nanogui;

#include "luaRM.hpp"
#include "luaWidget.hpp"

namespace luaGUI {

    void loadScreen(sol::table & gui) {

        gui.new_usertype<nanogui::Screen>(
            "Screen",
            "new", sol::no_constructor,
            sol::base_classes, sol::bases<Widget, RM::GCObject>()
        );
    }
}
