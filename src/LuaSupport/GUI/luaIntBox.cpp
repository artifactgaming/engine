/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "luaIntBox.hpp"
#include "luaRM.hpp"

#include <iostream>
#include <glm/glm.hpp>
#include <nanogui/textbox.h>
using namespace nanogui;

namespace luaGUI {
    void loadIntBox(sol::table & gui) {
        gui.new_usertype<IntBox<long>>(
            "IntBox",
            "new", sol::factories(&luaObjectFactory<IntBox<long>, Widget*>),
            sol::base_classes, sol::bases<Widget, RM::GCObject>()
        );

        sol::table intbox = gui["IntBox"];

        intbox["isEditable"] = &IntBox<long>::editable;
        intbox["setEditable"] = &IntBox<long>::setEditable;
        intbox["getValue"] = &IntBox<long>::value;
        intbox["setValue"] = &IntBox<long>::setValue;
        intbox["getUnits"] = &IntBox<long>::units;
        intbox["setUnits"] = &IntBox<long>::setUnits;
        intbox["getPlaceholder"] = &IntBox<long>::placeholder;
        intbox["setPlaceholder"] = &IntBox<long>::setPlaceholder;
    }
}
