/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "luaPopupButton.hpp"
#include "luaRM.hpp"

#include <iostream>
#include <glm/glm.hpp>
#include <nanogui/popupbutton.h>
using namespace nanogui;

namespace luaGUI {
    void loadPopupButton(sol::table & gui) {
        gui.new_usertype<PopupButton>(
            "PopupButton",
            "new", sol::factories(&luaObjectFactory<PopupButton, Widget*, std::string, int>),
            sol::base_classes, sol::bases<Button, RM::GCObject>()
        );
    }
}
