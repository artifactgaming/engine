/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "luaLabel.hpp"
#include "luaRM.hpp"

#include <iostream>
#include <glm/glm.hpp>
#include <nanogui/label.h>
using namespace nanogui;

namespace luaGUI {
    void loadLabel(sol::table & gui) {
        gui.new_usertype<Label>(
            "Label",
            "new", sol::factories(
                &luaObjectFactory<Label, Widget*, std::string>,
                &luaObjectFactory<Label, Widget*, std::string, std::string, int>
            ),
            sol::base_classes, sol::bases<Widget, RM::GCObject>()
        );

        sol::table label = gui["Label"];

        label["setCaption"] = &Label::setCaption;
        label["getCaption"] = &Label::caption;

        label["setFontSize"] = &Label::setFontSize;
        label["getFontSize"] = &Label::fontSize;
    }
}
