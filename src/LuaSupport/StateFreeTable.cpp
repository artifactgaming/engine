/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "StateFreeTable.hpp"
#include "AbsolutePanic.hpp"
#include "GCObject.hpp"
#include <glm/glm.hpp>
#include <memory>

#include "luaRM.hpp"

namespace luaSupport
{

    struct typeConverter
    {
        bool(*is)(sol::object);
        void(*set)(sol::object, StateFreeTable::Value&);
        bool(*stored_is)(StateFreeTable::Value&);
        sol::object(*get)(sol::state_view lua, StateFreeTable::Value&);
    };

    const int numTypeConverters = 4;

    template<class T>
    constexpr void addSupportForUsertype(std::array<typeConverter, numTypeConverters> & converterList, int & index)
    {
        converterList[index] = {
            +[](sol::object obj) { return obj.is<T>(); },
            +[](sol::object obj, StateFreeTable::Value & val) { val.value = obj.as<T>(); },
            +[](StateFreeTable::Value & value) {
                return value.value.type() == typeid(T); },
            +[](sol::state_view lua, StateFreeTable::Value & value) { return sol::object(lua, sol::in_place, std::any_cast<T>(value.value)); }
        };

        index++;
    }

    constexpr std::array<typeConverter, numTypeConverters> loadConverters()
    {
        std::array<typeConverter, numTypeConverters> converters = {};
        int index = 0;

        // Added in the order of most likely to be used.
        converters[index] = {
            +[](sol::object obj){ return obj.is<RM::GCObject>(); },
            +[](sol::object obj, StateFreeTable::Value & val){ val.value = RM::ref<RM::GCObject>(obj.as<RM::GCObject*>()); },
            +[](StateFreeTable::Value & value) { return value.value.type() == typeid(RM::ref<RM::GCObject>); },
            +[](sol::state_view lua, StateFreeTable::Value & value)
            {
                RM::ref<RM::GCObject> reference = std::any_cast<RM::ref<RM::GCObject>>(value.value);
                return sol::object(lua, sol::in_place, reference);
            }
        };
        index++;

        addSupportForUsertype<glm::vec3>(converters, index);
        addSupportForUsertype<glm::vec2>(converters, index);
        addSupportForUsertype<glm::vec4>(converters, index);

        return converters;
    }

    constexpr std::array<typeConverter, numTypeConverters> converters = loadConverters();

    StateFreeTable::StateFreeTable()
    {

    }

    StateFreeTable::StateFreeTable(sol::state_view lua, sol::table table)
    {
        table.for_each([this, &lua](sol::object key, sol::object value)
        {
            this->setValue(key, value, lua);
        });
    }

    StateFreeTable::~StateFreeTable()
    {

    }

    void StateFreeTable::setValue(sol::object key, sol::object value, sol::state_view lua)
    {
        setValue(key.as<LuaKey>(), value, lua);
    }

    sol::object StateFreeTable::getValue(sol::object key, sol::state_view lua)
    {
        return getValue(key.as<LuaKey>(), lua);
    }

    void StateFreeTable::setValue(LuaKey key, sol::object value, sol::state_view lua)
    {
        sol::type type = value.get_type();

        Value storageValue;

        switch (type)
        {
        case sol::type::none:
        case sol::type::nil:
            break;

        case sol::type::string:
            storageValue.value = value.as<std::string>();
            break;

        case sol::type::number:
            storageValue.value = value.as<double>();
            break;

        case sol::type::userdata:
        {
            sol::table as_table = value.as<sol::table>();
            sol::table metatable = as_table[sol::metatable_key];

            sol::object metatable_name = metatable["__name"];
            storageValue.metatable_name = metatable_name.as<std::string>();

            for (const typeConverter & converter : converters)
            {
                if (converter.is(value))
                {
                    converter.set(value, storageValue);

                    // I'm so sorry, I couldn't think of a better way that wouldn't slow things down.
                    goto found_value_type;
                }
            }

            ABSOLUTE_PANIC();

            found_value_type: {}
        }
        break;

        case sol::type::lightuserdata:
        {
            sol::table as_table = value.as<sol::table>();
            sol::table metatable = as_table[sol::metatable_key];
            storageValue.metatable_name = metatable["__name"];
            storageValue.value = value.as<RM::GCObject*>();
        }
        break;

        case sol::type::table:
        {
            RM::ref<StateFreeTable> store = new StateFreeTable();
            storageValue.value = store;
            sol::table table = value;

            table.for_each([&store, &lua](sol::object key, sol::object value)
            {
                store->setValue(key, value, lua);
            });
        }
        break;

        default:
            throw std::runtime_error("Unsupported type for state independent table.");
        }

        storageValue.type = type;
        data[key] = storageValue;
    }

    sol::object StateFreeTable::getValue(LuaKey key, sol::state_view lua)
    {
        std::map<LuaKey, Value>::iterator result;
        result = data.find(key);

        if (result != data.end())
        {
            Value & storageValue = data[key];
            sol::type type = storageValue.type;

            switch (type)
            {
            case sol::type::none:
            case sol::type::nil:
                return sol::object(lua, sol::in_place, sol::lua_nil);
                break;

            case sol::type::string:
                return sol::object(lua, sol::in_place, std::any_cast<std::string>(storageValue.value));
                break;

            case sol::type::number:
                return sol::object(lua, sol::in_place, std::any_cast<double>(storageValue.value));
                break;

            case sol::type::userdata:
            {
                sol::object data;

                for (const typeConverter & converter : converters)
                {
                    if (converter.stored_is(storageValue))
                    {
                        data = sol::object(lua, sol::in_place, converter.get(lua, storageValue));
                        goto found_value_type;
                    }
                }

                ABSOLUTE_PANIC();

                found_value_type: {}

                sol::stack::push(lua, data);
                luaL_setmetatable(lua, storageValue.metatable_name.c_str());

                // Some weird hickup, the __name value is sometimes not set, despite the fact that we actually found it using that name.
                sol::table as_table = sol::stack::pop<sol::table>(lua);
                sol::table metatable = as_table[sol::metatable_key];
                metatable["__name"] = storageValue.metatable_name;

                return data;
            }
            break;

            case sol::type::lightuserdata:
            {
                void ** ptr = static_cast<void**>(lua_newuserdata(lua, sizeof(void*)));
                *ptr = std::any_cast<void*>(storageValue.value);

                luaL_getmetatable(lua, storageValue.metatable_name.c_str());
                lua_setmetatable(lua, -2);

                return sol::stack::pop<sol::object>(lua);
            }
            break;

            case sol::type::table:
            {
                RM::ref<StateFreeTable> table = std::any_cast<RM::ref<StateFreeTable>>(storageValue.value);
                return sol::object(lua, sol::in_place, table);
            }

            default:
                ABSOLUTE_PANIC();
            }
        }
        else
        {
            return sol::object(lua, sol::in_place, sol::lua_nil);
        }
    }

    void StateFreeTable::removeValue(sol::object key)
    {
        removeValue(key.as<LuaKey>());
    }

    void StateFreeTable::removeValue(LuaKey key)
    {
        data.erase(key);
    }

    void loadStatefreeTable(sol::state_view lua)
    {
        lua.new_usertype<StateFreeTable>("StateFreeTable",
            sol::base_classes, sol::bases<RM::GCObject>(),

            sol::meta_function::index,
            +[](StateFreeTable & table, sol::stack_object key, sol::this_state lua)
            {
                return table.getValue(key, lua);
            },
            sol::meta_function::new_index,
            +[](StateFreeTable & table, sol::stack_object key, sol::stack_object value, sol::this_state lua)
            {
                table.setValue(key, value, lua);
            }
        );
    }
}
