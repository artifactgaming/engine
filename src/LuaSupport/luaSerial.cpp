/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "luaSerial.hpp"
#include "luaSupport.hpp"

#include <json/json.h>

#include <string>
#include <iostream>

#include "Path.hpp"

#define TABLE_DEPTH_LIMIT 64

namespace luaSerial {
    Json::Value tableToRawJSON(sol::table table, size_t depth);

    Json::Value luaObjectToJSON(sol::object object, size_t depth) {
        Json::Value value = Json::nullValue;

        if (object.is<bool>()) {
            value = object.as<bool>();
        } else if (object.is<int>()) {
            value = object.as<int>();
        } else if (object.is<double>() || object.is<float>()) {
            value = object.as<double>();
        } else if (object.is<std::string>()) {
            value = object.as<std::string>();
        } else if (object.is<sol::table>()) {
            value = tableToRawJSON(object.as<sol::table>(), depth);
        }

        return value;
    }

    Json::Value tableToRawJSON(sol::table table, size_t depth) {
        Json::Value root;

        depth++;
        if (depth > TABLE_DEPTH_LIMIT) {
            throw std::runtime_error("Max table depth exceeded.");
        }

        if (table[1] != sol::lua_nil) {
            for (auto v : table) {

                auto key = v.first;
                auto val = v.second;

                if (key.is<int>()) {
                    Json::Value value = luaObjectToJSON(val, depth);

                    if (value != Json::nullValue) {
                        root[key.as<int>()-1] = value;
                    }

                    continue;
                }

                if (key.is<std::string>()) {
                    throw std::runtime_error("You can not convert tables to JSON when they mix both numbers and strings for indexes.");
                }

                // I wish I could make this more useful.
                throw std::runtime_error("Key not compatible with JSON format.");
            }

        } else {
            for (auto v : table) {

                auto key = v.first;
                auto val = v.second;

                if (key.is<std::string>()) {
                    Json::Value value = luaObjectToJSON(val, depth);

                    if (value != Json::nullValue) {
                        root[key.as<std::string>()] = value;
                    }

                    continue;
                }

                if (key.is<int>()) {
                    throw std::runtime_error("You can not convert tables to JSON when they mix both numbers and strings for indexes.");
                }

                // I wish I could make this more useful.
                throw std::runtime_error("Key not compatible with JSON format.");
            }
        }

        return root;
    }

    std::string tableToJSON(sol::table table) {
        Json::Value json = tableToRawJSON(table, 0);

        Json::StreamWriterBuilder wbuilder;
        wbuilder["indentation"] = "    ";

        return Json::writeString(wbuilder, json);
    }

    sol::table RawJSONtoTable(sol::this_state lua, Json::Value root);

    sol::object JSONtoLuaObject(sol::this_state & lua, Json::Value obj) {
        switch (obj.type()) {
        case Json::booleanValue:
            return sol::object(lua, sol::in_place, obj.asBool());
            break;
        case Json::intValue:
            return sol::object(lua, sol::in_place, obj.asLargestInt());
            break;
        case Json::uintValue:
            return sol::object(lua, sol::in_place, obj.asLargestUInt());
            break;
        case Json::realValue:
            return sol::object(lua, sol::in_place, obj.asDouble());
            break;
        case Json::stringValue:
            return sol::object(lua, sol::in_place, obj.asString());
            break;
        case Json::arrayValue:
            return RawJSONtoTable(lua, obj);
            break;
        default:
            return sol::lua_nil;
        }
    }

    sol::table RawJSONtoTable(sol::this_state lua, Json::Value root) {
        sol::table table(lua, sol::create);

        if (root.type() == Json::objectValue) {
            Json::Value::Members members = root.getMemberNames();

            for (auto name : members) {
                table[name] = JSONtoLuaObject(lua, root[name]);
            }
        } else {
            unsigned int size = root.size();

            for (unsigned int i = 0; i < size; i++) {
                table[i + 1] = JSONtoLuaObject(lua, root[i]);
            }
        }

        return table;
    }

    sol::table JSONtoTable(sol::this_state lua, std::string input) {
        Json::Value root;
        Json::Reader reader;
        if (reader.parse(input, root)) {
            return RawJSONtoTable(lua, root);
        }

        std::string error = "Error while reading JSON string:\n";
        error += reader.getFormattedErrorMessages();

        throw std::runtime_error(error);
    }

    enum SerialType
    {
        integer = 0,
        double_float = 1,
        boolean = 2,
        string = 3,
        table = 4,
        userdata = 5
    };

    void serializeBlankTable(SerializedData & data)
    {
        const uint32_t entryCount = 0;
        insert(data, entryCount);
    }

    void serializeUserData(sol::object value, SerializedData & data)
    {
        sol::table table = value.as<sol::table>();
        sol::protected_function serialize = table["serialize"];
        sol::protected_function luaDeserializeFunctionPath = table["deserializeFunctionPath"];


        sol::protected_function_result functionPathResult = luaDeserializeFunctionPath();

        if (functionPathResult.valid())
        {
            std::string functionPath = functionPathResult;
            insert(data, functionPath);
            serialize(data); // FIXME No error handling has been done here.
        }
        else
        {
            sol::error err = functionPathResult;
            std::string what = "Error getting function path for deserializing an object.\n";
            what += "Object may not supported by the engine, a dependency may be missing, or this could just be a bug in a mod.\n";
            what += "Error message:\n";
            what += err.what();
            throw std::runtime_error(what);
        }
    }

    void serializeTableImpl(sol::table table, SerializedData & data, size_t & depth)
    {
        depth++;

        if (depth <= TABLE_DEPTH_LIMIT)
        {
            uint32_t entryCount = 0;
            size_t entryCountPoint = data.size();
            insert(data, entryCount);


            table.for_each([&entryCount, &data, &depth](sol::object key, sol::object value)
            {
                entryCount++;

                if (key.is<int32_t>())
                {
                    insert<uint8_t>(data, SerialType::integer); // Key type.
                    insert(data, key.as<int32_t>()); // The key.
                }
                else if (key.is<std::string>())
                {
                    insert<uint8_t>(data, SerialType::string); // Key type.
                    insert(data, key.as<std::string>()); // The key.
                }
                else
                {
                    throw std::runtime_error("Unsupported key type.");
                }

                if (value.is<double>())
                {
                    insert<uint8_t>(data, SerialType::double_float); // Value type.
                    insert(data, value.as<double>()); // The value.
                }
                else if (value.is<bool>())
                {
                    insert<uint8_t>(data, SerialType::boolean); // Value type.
                    insert(data, value.as<bool>()); // The value.
                }
                else if (value.is<std::string>())
                {
                    insert<uint8_t>(data, SerialType::string); // Value type.
                    insert(data, value.as<std::string>()); // The value.
                }
                else if (value.is<Serializable>())
                {
                    insert<uint8_t>(data, SerialType::userdata);
                    serializeUserData(value, data);
                }
                else if (value.is<sol::table>())
                {
                    sol::table table = value.as<sol::table>();

                    if (table["serialize"].valid())
                    {
                        insert<uint8_t>(data, SerialType::userdata);
                        serializeUserData(value, data);
                    }

                    insert<uint8_t>(data, SerialType::table); // Value type.
                    serializeTableImpl(table, data, depth); // The value.
                }
                else
                {
                    throw std::runtime_error("Unsupported value type.");
                }
            });

            replace(data, entryCountPoint, entryCount);
        }
        else
        {
            throw std::runtime_error("Table depth exceeded that which can be serialized.");
        }
    }

    void serializeTable(sol::table table, SerializedData & data)
    {
        size_t depth = 0;
        serializeTableImpl(table, data, depth);
    }

    template<class T>
    inline void read(const SerializedData & data, size_t & index, T & value)
    {
        const char * base = &data[index];
        index += sizeof(T);
        memcpy(&value, base, sizeof(T));
    }

    inline void read(const SerializedData & data, size_t & index, std::string & value)
    {
        uint32_t length;
        read(data, index, length);

        if ((data.size() - index) >= length)
        {
            value.resize(length);
            memcpy(&value[0], &data[index], length);
            index += length;
        }
        else
        {
            throw std::runtime_error("Not enough data left in serialized table to fill string.");
        }
    }

    void deserializeTableImpl(sol::state_view lua, sol::table & table, const SerializedData & data, size_t & index)
    {
        uint32_t entryCount;
        read(data, index, entryCount);

        for (;entryCount > 0; entryCount--)
        {
            uint8_t keyType;
            read(data, index, keyType);

            std::variant<int32_t, std::string> key;
            std::variant<double, bool, std::string, sol::table> value;

            switch (keyType)
            {
            case SerialType::integer:
                {
                    int32_t key_index;
                    read(data, index, key_index);
                    key = key_index;
                }
                break;
            case SerialType::string:
                {
                    std::string key_index;
                    read(data, index, key_index);
                    key = key_index;
                }
                break;
            default:
                throw std::runtime_error("Unknown key type specified in serialized table. Data stream may be corrupted.");
            }

            uint8_t valueType;
            read(data, index, valueType);

            switch (valueType)
            {
            case SerialType::double_float:
                {
                    double raw_value;
                    read(data, index, raw_value);
                    value = raw_value;
                }
                break;
            case SerialType::boolean:
                {
                    bool raw_value;
                    read(data, index, raw_value);
                    value = raw_value;
                }
                break;
            case SerialType::string:
                {
                    std::string raw_value;
                    read(data, index, raw_value);
                    value = raw_value;
                }
                break;
            case SerialType::table:
                {
                    sol::table sub_table = lua.create_table();
                    deserializeTableImpl(lua, sub_table, data, index);
                    value = sub_table;
                }
                break;
            }

            table[key] = value;
        }
    }

    sol::table deserializeTable(sol::state_view lua, const SerializedData & data)
    {
        size_t index = 0;
        sol::table table = lua.create_table();
        deserializeTableImpl(lua, table, data, index);

        return table;
    }

    void deserializeTable(sol::state_view lua, const SerializedData & data, sol::table & table)
    {
        size_t index = 0;
        deserializeTableImpl(lua, table, data, index);
    }

    void saveBin(SerializedData & data, RM::Path path)
    {
        if (path.isLegal())
        {
            boost::filesystem::ofstream file(path.getBoostPath());
            file.write(&data[0], data.size());
            file.close();
        }
        else
        {
            throw std::runtime_error("Path is outside of sandbox.");
        }
    }

    void loadBin(SerializedData & data, RM::Path path)
    {
        if (path.isLegal())
        {
            boost::filesystem::ifstream file(path.getBoostPath());
            file.seekg(0, std::ios::end);
            size_t length = file.tellg();
            data.reserve(length);
            file.seekg(0, std::ios::beg);
            file.read(&data[0], length);
            file.close();
        }
        else
        {
            throw std::runtime_error("Path is outside of sandbox.");
        }
    }
}

void loadLuaSerialAPI(sol::state_view lua) {
    sol::table serial = lua.create_table();
    lua["serial"] = LuaSupport::makeTableReadOnly(lua, serial);

    serial["tableToJSON"] = &luaSerial::tableToJSON;
    serial["JSONToTable"] = &luaSerial::JSONtoTable;
    serial["tableToBin"] = +[](sol::table table)
    {
        luaSerial::SerializedData data;
        luaSerial::serializeTable(table, data);
        return data;
    };

    serial["binToTable"] = +[](sol::this_state lua, luaSerial::SerializedData & data) { return luaSerial::deserializeTable(lua, data); };

    serial.new_usertype<luaSerial::SerializedData>(
        "SerializedData",
        "save", sol::overload(
            &luaSerial::saveBin,
            +[](luaSerial::SerializedData & data, std::string path)
                { luaSerial::saveBin(data, path); }
        ),
        "load", sol::overload(
            &luaSerial::loadBin,
            +[](luaSerial::SerializedData & data, std::string path)
                { luaSerial::loadBin(data, path); }
        )
    );
}
