/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "AsyncLuaTester.hpp"

AsyncLuaTester::AsyncLuaTester(RM::ResourceGroup * group, sol::function func, sol::function syncfunc, sol::variadic_args args, std::string name):
    TrueConcurrency(name, "Concurrent Lua Task", group)
{
    asyncFuncDump = dumpFunction(func);
    this->syncfunc = syncfunc;

    for (auto v: args) {
        switch (v.get_type())
        {
        case sol::type::nil:
            this->jobArgs.push_back(nullptr);
            break;
        case sol::type::string:
            this->jobArgs.push_back(v.as<std::string>());
            break;
        case sol::type::number:
            this->jobArgs.push_back(v.as<float>());
            break;
        case sol::type::boolean:
            this->jobArgs.push_back(v.as<bool>());
            break;
        case sol::type::userdata:
            break;
        default:
            // TODO put the resource name here.
            throw std::runtime_error("Unsupported argument type to concurrent Lua job.");
        }
    }

    this->setResourceStepsToLoad(1);
    this->startLoad();
}

AsyncLuaTester::~AsyncLuaTester()
{
    //dtor
}

void AsyncLuaTester::async_load()
{
    boost::mutex::scoped_lock lock(mutex);

    sol::state_view lua = manager->getStateOnly();

    sol::environment env(lua, sol::create, lua.globals());
    sol::function func = loadString(lua, asyncFuncDump);
    sol::set_environment(env, func);

    try
    {
        sol::protected_function_result result = func(sol::as_args(jobArgs));

        if (result.valid())
        {
            for (auto i : result)
            {
                if (!(i.get_type() == sol::type::nil))
                {
                    jobResults.push_back(i);
                }
                else
                {
                    jobResults.push_back(nullptr);
                }

            }
        }
        else
        {
            sol::error err = result;
            std::string what = err.what();
            // TODO put the resource name here.
            std::cerr << "Error in thread:\n" << what << std::endl;
        }
    } catch (sol::error & e) {
        std::cerr << e.what() << std::endl;
    }

    incrementResourceProgress(1);
}

void AsyncLuaTester::sync_load()
{
    boost::mutex::scoped_lock lock(mutex);
    sol::protected_function_result result = syncfunc(sol::as_args(jobResults));

    if (!result.valid())
    {
        sol::error err = result;
        std::string what = err.what();
        // TODO put the resource name here.
        std::cerr << "Error in thread sync callback:\n" << what << std::endl;
    }
}
