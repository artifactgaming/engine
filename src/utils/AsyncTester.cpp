/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "AsyncTester.hpp"

#include <boost/chrono.hpp>

AsyncTester::AsyncTester(RM::ResourceGroup * group, unsigned int wait):
    AsyncResource("TestTimer", "Async Resource Test", group), wait(wait)
{
    this->setResourceStepsToLoad(wait);
    this->startLoad();
}

AsyncTester::AsyncTester(unsigned int wait) : AsyncResource("TestTimer", "Async Resource Test", nullptr), wait(wait)
{
    this->setResourceStepsToLoad(wait);
    this->startLoad();
}

AsyncTester::~AsyncTester()
{

}

void AsyncTester::async_load() {
    for (unsigned int i = 0; i < wait; i++) {
        boost::chrono::seconds duration(1);
        boost::this_thread::sleep_for(duration);

        incrementResourceProgress(1);
    }
}

void AsyncTester::sync_load() {

}
