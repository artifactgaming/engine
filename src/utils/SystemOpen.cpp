/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "SystemOpen.hpp"

#ifdef __WIN32__
#include <windows.h>
#endif // __WIN32__

#include <iostream>

namespace SystemOpen {
    bool open(std::string url) {
        std::cout << "System Open URL " << url << std::endl;

#       ifdef __linux__
        std::string command = "";
        command += "xdg-open \"";
        command += url;
        command += "\"";
        int result = system(command.c_str());
        if (result) {
            return false;
        } else {
            return true;
        }
#       endif // __linux__

#       ifdef __WIN32__
        HINSTANCE result = ShellExecuteA(GetDesktopWindow(), "open", url.c_str(), NULL, NULL, SW_SHOW);

        if (reinterpret_cast<uint64_t>(result) > 32) {
            return true;
        } else {
            return false;
        }
#       endif // __WIN32__

#       ifdef __OSX__
        std::string command = "";
        command += "open \"";
        command += url;
        command += "\"";
        int result = system(command.c_str());
        if (result) {
            return false;
        } else {
            return true;
        }

#       endif // __OSX__
    }
}
