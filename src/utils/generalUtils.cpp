/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "generalUtils.hpp"

glm::vec3 toGLM(btVector3 input) {
    return glm::vec3(input.x(), input.y(), input.z());
}

btVector3 toBullet(glm::vec3 input) {
    return btVector3(input.x, input.y, input.z);
}

/*btClosestNotMeConvexResultCallback::btClosestNotMeConvexResultCallback(btCollisionObject* me, const btVector3& fromA, const btVector3& toA, btOverlappingPairCache* pairCache, btDispatcher* dispatcher):
    btCollisionWorld::ClosestConvexResultCallback(fromA,toA), m_me(me), m_allowedPenetration(0.0f), m_pairCache(pairCache), m_dispatcher(dispatcher)
{
}

btScalar btClosestNotMeConvexResultCallback::addSingleResult(btCollisionWorld::LocalConvexResult & convexResult, bool normalInWorldSpace) {
    if (convexResult.m_hitCollisionObject == m_me)
        return 1.0f;

    //ignore result if there is no contact response
    if(!convexResult.m_hitCollisionObject->hasContactResponse())
        return 1.0f;

    btVector3 linVelA,linVelB;
    linVelA = m_convexToWorld - m_convexFromWorld;
    linVelB = btVector3(0, 0, 0);//toB.getOrigin()-fromB.getOrigin();

    btVector3 relativeVelocity = (linVelA-linVelB);
    //don't report time of impact for motion away from the contact normal (or causes minor penetration)
    if (convexResult.m_hitNormalLocal.dot(relativeVelocity) >= -m_allowedPenetration)
        return 1.f;

    return ClosestConvexResultCallback::addSingleResult(convexResult, normalInWorldSpace);
}

bool btClosestNotMeConvexResultCallback::needsCollision(btBroadphaseProxy* proxy0) const
{
    //don't collide with itself
    if (proxy0->m_clientObject == m_me)
        return false;

    ///don't do CCD when the collision filters are not matching
    if (!ClosestConvexResultCallback::needsCollision(proxy0))
        return false;

    btCollisionObject* otherObj = (btCollisionObject*) proxy0->m_clientObject;

    //call needsResponse, see http://code.google.com/p/bullet/issues/detail?id=179
    if (m_dispatcher->needsResponse(m_me,otherObj))
    {
        return true;
    }

    return false;
}*/
