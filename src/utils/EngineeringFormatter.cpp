/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "EngineeringFormatter.hpp"

#include <iomanip>
#include <sstream>

namespace ENGFormatting {
    char metricEndings[][3] = {
        {"a"}, // Atto
        {"f"}, // Femto
        {"p"}, // Pico
        {"n"}, // Nano
        {"μ"}, // Micro
        {"m"}, // Milli
        {"" }, // None
        {"k"}, // Kilo
        {"M"}, // Mega
        {"G"}, // Giga
        {"T"}, // Tera
        {"P"}, // Peta
        {"E"}  // Exa
    };

    double metricScales[] = {
        0.000000000000000001, // Atto
        0.000000000000001,    // Femto
        0.000000000001,       // Pico
        0.000000001,          // Nano
        0.000001,             // Micro
        0.001,                // Milli
        1,                    // None
        1000,                 // Kilo
        1000000,              // Mega
        1000000000,           // Giga
        1000000000000,        // Tera
        1000000000000000,     // Peta
        1000000000000000000   // Exa
    };

    std::string format(double value) {
        int index = 0; // Start on Atto.
        if (value != 0) {
            while (abs(value) > metricScales[index + 1]) {
                index++;
            }
        } else {
            index = 6; // 0 is a weird edge case where we go 1:1.
        }


        std::stringstream stream;
        stream << std::fixed << std::setprecision(1) << (value / metricScales[index]) << " " << metricEndings[index];

        return stream.str();
    }

    std::string format(double value, char ** units) {
        int index = 0; // Start on Atto.
        if (value != 0) {
            while (abs(value) > metricScales[index + 1]) {
                index++;
            }
        } else {
            index = 6; // 0 is a weird edge case where we go 1:1.
        }

        std::stringstream stream;
        stream << std::fixed << std::setprecision(1) << (value / metricScales[index]);

        *units = &metricEndings[index][0];

        return stream.str();
    }
}
