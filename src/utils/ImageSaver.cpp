/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "ImageSaver.hpp"
#include <lodepng.h>

namespace RM {
    ImageSaver::ImageSaver(ResourceGroup * group, const Path & path, GLubyte * pixels, int width, int height) : AsyncResource(path.getName(), "ImageSaver", group)
    {
        this->pixels = pixels;
        this->path = path;
        this->width = width;
        this->height = height;

        setResourceStepsToLoad(1);
        startLoad();
    }

    ImageSaver::~ImageSaver()
    {
        delete pixels;
    }

    void ImageSaver::async_load() {
        //We have to flip the y axis, so we'll copy to this array.
        GLubyte * flippedImage = new GLubyte[width * height * 4];

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                int sourceOffset = y * width * 4 + x * 4;
                int targetOffset = (height - y - 1) * width * 4 + x * 4;

                flippedImage[targetOffset    ] = pixels[sourceOffset    ];//R
                flippedImage[targetOffset + 1] = pixels[sourceOffset + 1];//G
                flippedImage[targetOffset + 2] = pixels[sourceOffset + 2];//B
                flippedImage[targetOffset + 3] = 255;//A There should be no transparency in a screen-shot.
            }
        }

        lodepng::encode(path.string(), flippedImage, width, height);
        delete[] flippedImage;
        incrementResourceProgress(1);
    }

    void ImageSaver::sync_load() {
        std::cout << "Finished saving image: " << path.string() << std::endl;
    }

    void saveImage(const Path & path, GLubyte * pixels, int width, int height) {
        new ImageSaver(nullptr, path, pixels, width, height);
    }
}
