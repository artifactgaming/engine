/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "SystemStats.hpp"

#ifdef __linux__
#   include <sys/time.h>
#   include <sys/resource.h>
#endif // __linux__

#ifdef __WIN32
#   include "windows.h"
#   include "psapi.h"
#endif // __WIN32

#include <boost/thread/thread.hpp>
#include <iostream>

namespace SystemStats {

    float coreCompensation = 1.0f / boost::thread::hardware_concurrency();

    float getCoreCompensation() {
        return coreCompensation;
    }

#   ifdef __linux__
    timeval lastCPUTime = {0, 0};
    timeval lastTime = {0, 0};
#   endif // __WIN32

#   ifdef __WIN32
    ULARGE_INTEGER lastCPU;
    ULARGE_INTEGER lastSysCPU;
    ULARGE_INTEGER lastUserCPU;
#   endif // __WIN32

    void getSystemUsageStats(float & CPUusage, long & memoryUsage) {
        CPUusage = 0;
        memoryUsage = 0;

#       ifdef __linux__
        rusage usage;

        getrusage(RUSAGE_SELF, &usage);

        memoryUsage = usage.ru_maxrss * 1000;

        timeval userTime = usage.ru_utime;
        timeval kernelTime = usage.ru_stime;

        timeval totalCPUtime;
        totalCPUtime.tv_sec = userTime.tv_sec + kernelTime.tv_sec;
        totalCPUtime.tv_usec = userTime.tv_usec + kernelTime.tv_usec;

        timeval now;
        gettimeofday(&now, nullptr);

        float timeDelta = (now.tv_sec - lastTime.tv_sec) + static_cast<float>(now.tv_usec - lastTime.tv_usec) / 1000000;
        float cpuTimeDelta = (totalCPUtime.tv_sec - lastCPUTime.tv_sec) + static_cast<float>(totalCPUtime.tv_usec - lastCPUTime.tv_usec) / 1000000;

        CPUusage = (cpuTimeDelta / timeDelta) * coreCompensation;

        lastCPUTime = totalCPUtime;
        lastTime = now;

#       endif

#       ifdef __WIN32
        PROCESS_MEMORY_COUNTERS pmc;
        GetProcessMemoryInfo(GetCurrentProcess(), &pmc, sizeof(pmc));
        memoryUsage = pmc.WorkingSetSize;

        union {
            FILETIME ftime;
            ULARGE_INTEGER now;
        };

        union {
            FILETIME fsys;
            ULARGE_INTEGER sys;
        };

        union {
            FILETIME fuser;
            ULARGE_INTEGER user;
        };

        GetSystemTimeAsFileTime(&ftime);
        GetProcessTimes(GetCurrentProcess(), &ftime, &ftime, &fsys, &fuser);

        CPUusage = (sys.QuadPart - lastSysCPU.QuadPart) + (user.QuadPart - lastUserCPU.QuadPart);
        CPUusage /= (now.QuadPart - lastCPU.QuadPart);
        CPUusage *= coreCompensation;

        lastCPU = now;
        lastUserCPU = user;
        lastSysCPU = sys;

#       endif // __WIN32

    }
}
