/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "ControllerManager.hpp"

#include "JSONBlockController.hpp"
#include "JSONDynamicEntityController.hpp"

namespace World {
    ControllerManager::ControllerManager()
    {

    }

    ControllerManager::~ControllerManager() {
        if (!block_controllers.empty()) {
            std::cout << "Warning: Not all block controllers were disposed before shutdown. This is an engine level bug. Please find a way to reproduce it and report it." << std::endl;
        }

        if (!dynamic_entity_controllers.empty()) {
            std::cout << "Warning: Not all dynamic entity controllers were disposed before shutdown. This is an engine level bug. Please find a way to reproduce it and report it." << std::endl;
        }
    }

    RM::ref<BlockController> ControllerManager::getBlockController(RM::Path path) {
        std::string key = path.getAbsolute().string();
        return this->getBlockController(key);
    }

    RM::ref<BlockController> ControllerManager::getBlockController(std::string key) {
        RM::ref<BlockController> controller;

        if (!key.empty() && key[0] == '@') {
            // TODO grab a builtin block.
        } else {
            std::map<std::string, BlockController*>::const_iterator result = block_controllers.find(key);
            if (result == block_controllers.end()) {
                controller = new JSONBlockController(key);
                block_controllers[key] = controller;
            } else {
                controller = result->second;
            }
        }

        return controller;
    }

    RM::ref<DynamicEntityController> ControllerManager::getDynamicEntityController(RM::Path path) {
        std::string key = path.getAbsolute().string();
        return this->getDynamicEntityController(key);
    }

    RM::ref<DynamicEntityController> ControllerManager::getDynamicEntityController(std::string key) {
        RM::ref<DynamicEntityController> controller;

        if (!key.empty() && key[0] == '@') {
            // TODO grab a builtin dynamic entity.
        } else {
            std::map<std::string, DynamicEntityController*>::const_iterator result = dynamic_entity_controllers.find(key);
            if (result == dynamic_entity_controllers.end()) {
                controller = new JSONDynamicEntityController(key);
                dynamic_entity_controllers[key] = controller;
            } else {
                controller = result->second;
            }
        }

        return controller;
    }

    void ControllerManager::removeBlockController(std::string path) {
        block_controllers.erase(path);
    }

    void ControllerManager::removeDynamicEntityController(std::string path)
    {
        dynamic_entity_controllers.erase(path);
    }
}
