/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "JSONControllerCore.hpp"

#include <iostream>
#include <boost/algorithm/string/predicate.hpp>

#include "globals.hpp"

#include "TrueConcurrency.hpp"
#include "luaWorld.hpp"

namespace World
{
    JSONControllerCore::JSONControllerCore(RM::Path jsonFile, const char * expectedEnding, const char * typeName, std::function<void(Json::Value&, Json::Reader&)> loadJSON, std::string resourceName)
    {
        if (!jsonFile.exists()) {
            std::string message = "File does not exist: ";
            message += jsonFile.string();
            throw std::runtime_error(message);
        }

        std::cout << "Building JSON " << typeName << " controller from " << jsonFile.string() << std::endl;
        if (!boost::algorithm::ends_with(jsonFile.string(), expectedEnding)) {
            std::cout << "Warning: Json files containing " << typeName << " controllers should have names ending with \"" << expectedEnding << "\"." << std::endl;
        }

        std::string input;

        boost::filesystem::ifstream file(jsonFile.getBoostPath(), std::ios::in | std::ios::binary);
        if (!file) {
            throw std::runtime_error("Failed to open file.");
        }

        file.seekg(0, std::ios::end);
        input.resize(file.tellg());
        file.seekg(0, std::ios::beg);
        file.read(&input[0], input.size());
        file.close();

        Json::Value root;
        Json::Reader reader;
        if (!reader.parse(input, root)) {
            std::string error = "Error while reading JSON string:\n";
            error += reader.getFormattedErrorMessages();

            throw std::runtime_error(error);
        }

        loadJSON(root, reader);

        funcTableName = "Controller:" + resourceName;

        getResourceManager()->registerEnvironment(funcTableName, [this](sol::state_view lua)
        {
            sol::environment env = this->loadFunctionTable(lua);
            this->addSelfFunctions(env);
            return env;
        });
    }

    JSONControllerCore::~JSONControllerCore()
    {
        getResourceManager()->removeEnvironment(funcTableName);
    }

    sol::table JSONControllerCore::getFunctions()
    {
        sol::environment env;
        sol::state_view lua = getResourceManager()->getEnvironment(funcTableName, env);

        return env;
    }

    void JSONControllerCore::loadFuncTableBuilder(Json::Value functions, Json::Reader & reader, std::string name)
    {
        // Load up the source code.
        std::string source;

        if (functions.isArray())
        {
            for (Json::Value line : functions)
            {
                source += line.asString() + '\n';
            }
        }
        else if (functions.isString())
        {
            source = functions.asString();
        }
        else
        {
            throw JsonError(functions, "Expected string or array of strings for function builder.");
        }

        // Now pre-compile it for all of the other threads that will need it.
        int line, column;
        reader.getValueLineAndColumn(functions, line, column);

        sol::state_view lua = getResourceManager()->getStateOnly();

        sol::protected_function loader = lua.load(source, name + " line: " + std::to_string(line) + " column: " + std::to_string(column));

        funcTableBuilder = LuaSupport::TrueConcurrency::dumpFunction(loader);
    }

    sol::environment JSONControllerCore::loadFunctionTable(sol::state_view lua)
    {
        sol::environment env(lua, sol::create);
        sol::protected_function loader = LuaSupport::TrueConcurrency::loadString(lua, funcTableBuilder);

        sol::set_environment(env, loader);
        loader();

        sol::environment env_core;
        getResourceManager()->getEnvironment("Terrain Management", env_core);

        // Check and make sure everything in there is a function.
        for (auto keyPair : env)
        {
            const sol::object & value = keyPair.second;

            if (!value.is<sol::function>())
            {
                std::string message = "All values defined in the functions section must be functions. Problematic value name: ";
                message += keyPair.first.as<std::string>();
                throw std::runtime_error(message);
            }

            sol::environment env(lua, sol::create, env_core);
            sol::set_environment(env, value);
        }

        return env;
    }
}

