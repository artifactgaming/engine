/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "DynamicEntity.hpp"

#include "World.hpp"

#include "BinaryFileTools.hpp"
#include "HexFormatter.hpp"

#include "globals.hpp"
#include "ControllerManager.hpp"
#include "ControlManager.hpp"

namespace World
{
    DynamicEntity::DynamicEntity(RM::ref<DynamicEntityController> controller, World * world, glm::vec3 startingPosition, sol::table args):
        controller(controller),
        world(world),
        center(startingPosition)
    {
        userdata = new luaSupport::StateFreeTable();
        chunk = world->getChunkBlockCoords_safe(startingPosition.x, startingPosition.y, startingPosition.z);

        ID = chunk->getWorld()->generateDynamicEntityID(this->file);
        timeOfCreation = chunk->getWorld()->getTime();

        userdata->setValue("args", args, getResourceManager()->getStateOnly());
        controller->init(this);
        save();

        world->addDynamicEntity(this);
    }

    DynamicEntity::DynamicEntity(World * world, RM::Path file_path, uint64_t ID):
        ID(ID),
        file(file_path),
        world(world)
    {
        userdata = new luaSupport::StateFreeTable();
        boost::filesystem::ifstream file(this->file.getBoostPath());

        if (!file)
        {
            throw std::runtime_error("Failed to open dynamic entity file for read.");
        }

        BinTools::readStruct(file, timeOfCreation);

        uint64_t chunkHash;
        BinTools::readStruct(file, chunkHash);
        chunk = world->getChunkHash_safe(chunkHash);

        std::string controller_name;
        BinTools::readStruct(file, controller_name);
        controller = getControllerManager()->getDynamicEntityController(controller_name);

        BinTools::readStruct(file, center.x);
        BinTools::readStruct(file, center.y);
        BinTools::readStruct(file, center.z);

        luaSerial::SerializedData data;
        BinTools::readStruct(file, data);

        controller->init(this);
        controller->load(this, data);

        file.close();

        world->addDynamicEntity(this);
    }

    DynamicEntity::~DynamicEntity()
    {
        RM::ControlManager * cm = getControlManager();
        for (std::tuple<std::string, std::string, RM::ref<nanogui::ListenerReference<void(bool pushed)>>> control : registeredBooleanControls)
        {
            cm->removeBooleanCallback(std::get<0>(control), std::get<1>(control), std::get<2>(control));
        }

        for (std::tuple<std::string, std::string, RM::ref<nanogui::ListenerReference<void(float delta)>>> control : registeredAnalogControls)
        {
            cm->removeAnalogCallback(std::get<0>(control), std::get<1>(control), std::get<2>(control));
        }

        world->removeDynamicEntity(this);
    }

    void DynamicEntity::save()
    {
        boost::filesystem::ofstream file(this->file.getBoostPath(), std::ios::binary);

        BinTools::writeStruct(file, timeOfCreation);
        BinTools::writeStruct(file, chunk->getHashCode());
        BinTools::writeStruct(file, controller->resourceName());
        BinTools::writeStruct(file, center.x);
        BinTools::writeStruct(file, center.y);
        BinTools::writeStruct(file, center.z);

        luaSerial::SerializedData data;
        controller->save(this, data);
        BinTools::writeStruct(file, data);

        // TODO add in physics objects.

        file.close();
    }

    bool DynamicEntity::isForceLoad()
    {
        return controller->forceLoad(this);
    }
}
