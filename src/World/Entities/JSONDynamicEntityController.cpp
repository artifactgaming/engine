/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "JSONDynamicEntityController.hpp"
#include "luaSerial.hpp"

#include "globals.hpp"
#include "ControlManager.hpp"

namespace World
{
    JSONDynamicEntityController::JSONDynamicEntityController(RM::ResourceGroup * group, RM::Path jsonFile):
        DynamicEntityController(group, jsonFile.getAbsolute().string()),
        JSONControllerCore(jsonFile, "-dynamic_entity.json", "dynamic entity", [this](Json::Value & root, Json::Reader & reader) { this->loadJSON(root, reader); }, jsonFile.getAbsolute().string())
    {

    }

    JSONDynamicEntityController::JSONDynamicEntityController(RM::Path jsonFile):
        JSONDynamicEntityController(nullptr, jsonFile)
    {

    }

    JSONDynamicEntityController::~JSONDynamicEntityController()
    {

    }

    void JSONDynamicEntityController::init(DynamicEntity * entity)
    {
        sol::table functions = this->getFunctions();
        sol::protected_function init = functions["init"];

        if (init != sol::lua_nil)
        {
            sol::state_view lua = getResourceManager()->getStateOnly();
            sol::object args = entity->userdata->getValue("args", lua);
            entity->userdata->removeValue("args");

            sol::protected_function_result result = init(entity, args);
            if (!result.valid())
            {
                sol::error err = result;
                std::string what = "Error in dynamic entity's init function loaded from ";
                what += resourceName();
                what += ".\n";
                what += err.what();
                throw std::runtime_error(what);
            }
        }
        else
        {
            std::cout << "WARNING: No init function defined for dynamic entity \"" << resourceName() << "\"." << std::endl;
        }
    }

    void JSONDynamicEntityController::save(DynamicEntity * entity, luaSerial::SerializedData & data)
    {
        sol::table functions = this->getFunctions();
        sol::protected_function save = functions["save"];

        if (save != sol::lua_nil)
        {
            sol::protected_function_result result = save(entity);
            if (result.valid())
            {
                sol::table table = result;
                luaSerial::serializeTable(table, data);
            }
            else
            {
                sol::error err = result;
                std::string what = "Error in dynamic entity's save function loaded from ";
                what += resourceName();
                what += ".\n";
                what += err.what();
                throw std::runtime_error(what);
            }
        }
        else
        {
            throw std::runtime_error("Save function was not defined for entity.");
        }
    }

    void JSONDynamicEntityController::load(DynamicEntity * entity, luaSerial::SerializedData & data)
    {
        sol::table functions = this->getFunctions();
        sol::protected_function load = functions["load"];

        if (load != sol::lua_nil)
        {
            sol::state_view lua = getResourceManager()->getStateOnly();
            sol::table table = luaSerial::deserializeTable(lua, data);

            sol::protected_function_result result = load(entity, table);

            if (!result.valid())
            {
                sol::error err = result;
                std::string what = "Error in dynamic entity's load function loaded from ";
                what += resourceName();
                what += ".\n";
                what += err.what();
                throw std::runtime_error(what);
            }
        }
        else
        {
            throw std::runtime_error("Load function was not defined for entity.");
        }
    }

    bool JSONDynamicEntityController::forceLoad(DynamicEntity * entity)
    {
        sol::table functions = this->getFunctions();
        sol::protected_function forceLoad = functions["forceLoadOnWorldLoad"];

        if (forceLoad != sol::lua_nil)
        {
            sol::protected_function_result result = forceLoad(entity);
            if (!result.valid())
            {
                sol::error err = result;
                std::string what = "Error in dynamic entity's forceLoad function loaded from ";
                what += resourceName();
                what += ".\n";
                what += err.what();
                throw std::runtime_error(what);
            }

            bool result_boolean = result;
            return result_boolean;
        }
        else
        {
            return false;
        }
    }

    void JSONDynamicEntityController::loadJSON(Json::Value & root, Json::Reader & reader)
    {
        setResourceStepsToLoad(1);

        try {
            loadFuncTableBuilder(root["functions"], reader, resourceName());

        } catch (JsonError & error) {
            reader.pushError(error.value, error.message);
            throw std::runtime_error("Bad Value in JSON block: " + reader.getFormatedErrorMessages());
        }

        addSelfFunctions = [this](sol::environment & env)
        {
            env["addBooleanControlCallback"] = [this](DynamicEntity * self, std::string group, std::string name, sol::function func)
            {

                RM::ControlManager * cm = getControlManager();

                sol::bytecode bytecode = func.dump();

                RM::ref<nanogui::ListenerReference<void(bool pushed)>> callback = cm->addBooleanCallback(group, name, [self, bytecode, this](bool pressed)
                {
                    sol::state_view lua = getResourceManager()->getStateOnly();

                    sol::table table = this->getFunctions();
                    sol::function func = lua.load(bytecode.as_string_view());

                    sol::protected_function_result result = func(self, pressed);
                    if (!result.valid())
                    {
                        sol::error err = result;
                        std::string what = "Error in control callback: ";
                        what += err.what();
                        throw std::runtime_error(what);
                    }
                });

                self->registeredBooleanControls.emplace_back(group, name, callback);
            };

            env["addAnalogControlCallback"] = [this](DynamicEntity * self, std::string group, std::string name, sol::function func)
            {
                RM::ControlManager * cm = getControlManager();

                sol::bytecode bytecode = func.dump();

                RM::ref<nanogui::ListenerReference<void(float delta)>> callback = cm->addAnalogCallback(group, name, [self, bytecode, this](float delta)
                {
                    sol::state_view lua = getResourceManager()->getStateOnly();

                    sol::table table = this->getFunctions();
                    sol::function func = lua.load(bytecode.as_string_view());

                    sol::protected_function_result result = func(self, delta);
                    if (!result.valid())
                    {
                        sol::error err = result;
                        std::string what = "Error in control callback: ";
                        what += err.what();
                        throw std::runtime_error(what);
                    }
                });

                self->registeredAnalogControls.emplace_back(group, name, callback);
            };
        };

        incrementResourceProgress(1);
    }
}
