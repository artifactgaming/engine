/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "DynamicEntityController.hpp"

#include "globals.hpp"
#include "ControllerManager.hpp"

namespace World
{
    DynamicEntityController::DynamicEntityController(RM::ResourceGroup * group, const std::string & name):
        RM::BasicResource(name, "Dynamic Entity Controller", group)
    {
        //ctor
    }

    DynamicEntityController::~DynamicEntityController()
    {
        getControllerManager()->removeDynamicEntityController(resourceName());
    }
}

