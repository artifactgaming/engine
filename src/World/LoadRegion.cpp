/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "LoadRegion.hpp"

LoadRegion::LoadRegion(int16_t min_x, int16_t min_y, int16_t min_z, int16_t max_x, int16_t max_y, int16_t max_z):
        min_x(min_x),
        min_y(min_y),
        min_z(min_z),

        max_x(max_x),
        max_y(max_y),
        max_z(max_z)
{

}

LoadRegion::~LoadRegion()
{
    //dtor
}
