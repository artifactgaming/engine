/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "PhysicsDebugRenderer.hpp"

#include <iostream>

namespace World {

    struct PhysicsDebugRenderShader: public GL::InternalShaderProgramSource
    {
        std::string getName() const
        {
            return "Physics Debug Shader Program";
        }

        const std::vector<const char *> getShaderStrings() const
        {
            return {
R"(Physics Debug Fragment
FRAGMENT
#version 330

in vec3 vTexCoord;

// Inputs.
in vec3 colorIn;

// Outputs
out vec4 colorOut;

void main(void)
{
    colorOut = vec4(colorIn, 1.0);
})",
R"(Physics Debug Vertex
VERTEX
#version 330

//Inputs
layout (location = 0) in vec3 vertex;
layout (location = 1) in vec3 color;

uniform mat4 matrix;

//Outputs
out vec3 vTexCoord;
out vec3 colorIn;

void main(void)
{
    gl_Position = matrix * vec4(vertex, 1.0);
    colorIn = color;
})"
            };
        }
    } internalDebugRenderShader;

    PhysicsDebugRenderer::PhysicsDebugRenderer():
        program(internalDebugRenderShader), // TODO we need a way to make some assets built in.
        vbarray("Physics Debug VBA", &program),
        verticies("Physics Debug Verticies", GL_STREAM_DRAW),
        colors("Physics Debug Colors", GL_STREAM_DRAW)
    {
        // Can't let this stuff get disposed until we dispose.
        program.incRef();
        vbarray.incRef();
        verticies.incRef();
        colors.incRef();

        vbarray.setBuffer("vertex", &verticies);
        vbarray.setBuffer("color", &colors);

        program.setPolygonMode(GL_LINES);
    }

    PhysicsDebugRenderer::~PhysicsDebugRenderer()
    {
        // Notice we don't decRef anything.
        // That's because they will dispose with us.
    }

    void PhysicsDebugRenderer::drawLine(const btVector3& from,const btVector3& to,const btVector3& color) {
        verticies.addVertex(glm::vec3(from.getX(), from.getY(), from.getZ()));
        verticies.addVertex(glm::vec3(to.getX(), to.getY(), to.getZ()));

        // You have to add it to both ends of the line.
        colors.addVertex(glm::vec3(color.getX(), color.getY(), color.getZ()));
        colors.addVertex(glm::vec3(color.getX(), color.getY(), color.getZ()));
    }

    void PhysicsDebugRenderer::drawContactPoint(const btVector3 & PointOnB,const btVector3 & normalOnB, btScalar distance, int lifeTime, const btVector3& color) {
        btVector3 to = PointOnB + normalOnB * distance;
        const btVector3 & from = PointOnB;

        drawLine(from, to, color);
    }

    void PhysicsDebugRenderer::reportErrorWarning(const char* warningString) {
        std::cout << warningString << std::endl;
    }

    void PhysicsDebugRenderer::draw3dText(const btVector3& location,const char* textString) {
        // TODO we need a 3D text renderer.
    }

    void PhysicsDebugRenderer::render(GL::RenderTarget * target, GL::View & view, btDiscreteDynamicsWorld & dynamicsWorld) {
        dynamicsWorld.debugDrawWorld();

        verticies.sendToGPU();
        colors.sendToGPU();

        {
            GL::ShaderProgramScope scope(target, &program);
            scope.setMatrix4x4("matrix", view.getCombinedMatrix());
            scope.drawVertexBuffers(vbarray);
        }
    }
}

