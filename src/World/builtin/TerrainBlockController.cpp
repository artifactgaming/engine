/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "TerrainBlockController.hpp"

namespace World {
    TerrainBlockController::TerrainBlockController(RM::ResourceGroup * group):
        BlockController(group, "@BaseTerrainBlock")
    {
        //ctor
    }

    TerrainBlockController::~TerrainBlockController()
    {
        //dtor
    }

    void TerrainBlockController::spawn(Block * block, BlockEntity * entity, uint16_t metadata) {

    }

    void TerrainBlockController::spawn(Block * block, BlockEntity * entity, sol::table & args) {

    }

    void TerrainBlockController::remove(Block * block, BlockEntity * entity) {

    }

    void TerrainBlockController::rightClick(Block * block, BlockEntity * entity, sol::table & args) {

    }

    void TerrainBlockController::leftClick(Block * block, BlockEntity * entity, sol::table & args) {

    }

    void TerrainBlockController::openGUI(Block * block, BlockEntity * entity, sol::table & args) {

    }

    void TerrainBlockController::update(Block * block, BlockEntity * entity, float timedelta) {

    }

    void TerrainBlockController::renderGhost(Block * block, BlockEntity * entity) {

    }

    void TerrainBlockController::render(Block * block, BlockEntity * entity) {

    }
}
