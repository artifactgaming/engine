/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "TerrainVisualMesh.hpp"

#include "World.hpp"
#include "BlockContainer.hpp"

namespace World
{
    TerrainVisualMesh::TerrainVisualMesh(std::string name, World * world, BlockContainer * blockContainer):
        AsyncResource(name, "Terrain Visual Mesh", nullptr),
        vertices('~' + name + ":vertices", GL_STATIC_DRAW),
        textureCoords('~' + name + ":texture_coordinates", GL_STATIC_DRAW),
        normals('~' + name + ":normals", GL_STATIC_DRAW),
        tangents('~' + name + ":tangents", GL_STATIC_DRAW),
        bitangents('~' + name + ":bi_tangents", GL_STATIC_DRAW)
    {
        // We will handle the garbage collection. This prevents it from auto-happening.
        vertices.incRef();
        textureCoords.incRef();
        normals.incRef();
        tangents.incRef();
        bitangents.incRef();

        this->blockContainer = blockContainer;
        this->world = world;

        loaded = false;
    }

    TerrainVisualMesh::~TerrainVisualMesh()
    {
        //dtor
    }

    void TerrainVisualMesh::async_load()
    {
        this->setResourceStepsToLoad(BlockContainer::block_array_length);
        World * world = this->world;
        blockContainer->forEach([world, this](long x, long y, long z, Block * block)
        {
            // std::cout << "Chunk: (" << this->blockContainer->getX() << ", " << this->blockContainer->getY() << ", " << this->blockContainer->getZ() << ") Block: (" << x << ", " << y << ", " << z << ") ID: " << block->ID << std::endl;
            BlockController * controller = world->getBlockController(block->ID);
            controller->renderToTerrain(*block, nullptr, x, y, z, this);
            this->incrementResourceProgress(1);
        });

        for (Face & face : faces)
        {
            vertices.addVertex(face.v0);
            vertices.addVertex(face.v1);
            vertices.addVertex(face.v2);
        }
    }

    void TerrainVisualMesh::sync_load()
    {
        vertices.sendToGPU();
        vertices.clear();
        faces.clear();

        loaded = true;
    }

    void TerrainVisualMesh::render(GL::View & view, GL::ShaderProgramScope & scope)
    {
        if (loaded)
        {
            GL::ShaderProgram * program = view.getProgram();
            GL::VertexBufferArray * array;

            std::unordered_map<GL::ShaderProgram*, GL::VertexBufferArray>::iterator result;
            result = bufferArrays.find(program);

            if (result != bufferArrays.end())
            {
                array = &result->second;
            }
            else
            {
                array = &bufferArrays.emplace(std::piecewise_construct, std::forward_as_tuple(program), std::forward_as_tuple("Terrain Mesh VBA", program)).first->second;

                array->setBuffer("vertexPosition", &vertices);
            }

            scope.drawVertexBuffers(*array);
        }
    }

    void TerrainVisualMesh::addFace(glm::vec3 v0, glm::vec3 v1, glm::vec3 v2)
    {
        faces.emplace_back(0, v0, v1, v2);
    }
}

