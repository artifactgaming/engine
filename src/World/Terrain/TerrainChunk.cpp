/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "TerrainChunk.hpp"

#include "TerrainProvider.hpp"
#include "HexFormatter.hpp"

#include "World.hpp"

namespace World {
    TerrainChunk::TerrainChunk(int16_t x, int16_t y, int16_t z, World * world, TerrainProvider * terrainProvider):
        BlockContainer("Chunk: " + std::to_string(x) + " " + std::to_string(y) + " " + std::to_string(z), z, y, z),
        world(world),
        terrainProvider(terrainProvider)
    {
        blockRefs = 0;

        this->x = x;
        this->y = y;
        this->z = z;

        hashCode = generateHashCode(x, y, z);
        file = world->getTerrainFolder() / formatAsHex(hashCode);
    }

    TerrainChunk::~TerrainChunk()
    {

    }

    bool TerrainChunk::isPhysicsLoaded()
    {
        return false;
    }

    bool TerrainChunk::isGraphicsLoaded()
    {
        return false;
    }

    bool TerrainChunk::isDynamicEntitiesLoaded()
    {
        return false;
    }

    void TerrainChunk::update(float timedelta, double time)
    {

    }

    void TerrainChunk::renderTerrain(GL::View & view, GL::ShaderProgramScope & scope)
    {
        if (mesh)
        {
            mesh->render(view, scope);
        }
    }

    void TerrainChunk::initializeBlockArray()
    {
        this->BlockContainer::initializeBlockArray();
        terrainProvider->generateBlocks(this);
    }

    void TerrainChunk::incBlockRef()
    {
        blockRefs++;

        if (!blocks)
        {
            this->initializeBlockArray();
        }
    }

    void TerrainChunk::decBlockRef()
    {
        blockRefs--;

        // FIXME causes segfault.
        //if (blockRefs == 0)
        //{
        //    saveTerrain();
        //}
    }

    void TerrainChunk::incVisualRef()
    {
        incBlockRef(); // We need blocks to be spawned.
        visualRefs++;
        if (!mesh)
        {
            mesh = new TerrainVisualMesh(std::to_string(x) + " " + std::to_string(y) + " " + std::to_string(z), world, this);
            mesh->incRef();
        }
    }

    void TerrainChunk::decVisualRef()
    {
        decBlockRef();
        visualRefs--;

        if (mesh && visualRefs == 0)
        {
            delete mesh;
            mesh = nullptr;
        }
    }

    void TerrainChunk::saveTerrain()
    {
        terrainProvider->saveBlocks(this);
    }

    TerrainVisualRef TerrainChunk::getVisualRef()
    {
        return TerrainVisualRef(this);
    }
}
