/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "World.hpp"

#include <iostream>
#include <json/json.h>
#include <boost/filesystem.hpp>

#include "main.hpp"

#include "TerrainProvider.hpp"
#include "TerrainChunk.hpp"

#include "BinaryFileTools.hpp"
#include "HexFormatter.hpp"

namespace World {

    World::World(RM::Path folder, const std::string & name,
        std::vector<RM::ref<BlockController>> & blockControllers,
        TerrainProvider * terrainProvider
    ):
        World::World(nullptr, folder, name, blockControllers,  terrainProvider)
    {

    }

    World::World(RM::ResourceGroup * group, RM::Path folder, const std::string & name,
        std::vector<RM::ref<BlockController>> & blockControllers,
        TerrainProvider * terrainProvider
    ):
        RM::BasicResource(name, "World", group),
        dispatcher(&collisionConfiguration),
        dynamicsWorld(&dispatcher, &overlappingPairCache, &solver, &collisionConfiguration),
        terrainProvider(terrainProvider),
        dynamicEntityIDDistrobution(0, static_cast<uint64_t>(~0)),
        folder(folder),
        name(name)
    {

        userdata = new luaSupport::StateFreeTable();

        chunksFolder = folder / "chunks";
        dynamicChunksFolder = folder / "dynamic_chunks";
        terrainFolder = folder / "terrain";
        blockEntitiesFolder = folder / "block_entities";
        dynamicEntitiesFolder = folder / "dynamic_entities";

        this->blockControllersByID.push_back(nullptr);

        folder.mkdirs();
        chunksFolder.mkdirs();
        terrainFolder.mkdirs();
        dynamicChunksFolder.mkdirs();
        blockEntitiesFolder.mkdirs();
        dynamicEntitiesFolder.mkdirs();

        // You have a new world here.
        if (!folder.exists())
        {
            std::cout << "Creating world \"" << name << "\" in folder: " << folder.string() << std::endl;

            // Allocate space.
            this->blockControllersByID.resize(blockControllers.size()+1);

            // We start at block id index 1 because 0 is reserved for empty space.
            uint16_t block_id_index = 1;
            for (RM::ref<BlockController> controller : blockControllers)
            {
                this->blockControllersByID[block_id_index] = controller;
                this->blockControllersAndIDsByName[controller->resourceName()] = std::pair<BlockController*, uint16_t>(controller, block_id_index);
                std::cout << "Block \"" << controller->resourceName() << "\" has been assigned ID: " << block_id_index << std::endl;
                block_id_index++;
            }

            time = 0;
            save();
        }
        else
        {
            readHeader();
            readBlockList();

            // We do not load this list. Why? Because we need to give Lua scripts a chance to setup the globals first.
            // readDynamicEntityForceLoadList();
        }

        // Setup physics engine.
        debugRenderer.setDebugMode(btIDebugDraw::DBG_DrawWireframe | btIDebugDraw::DBG_DrawAabb);
        dynamicsWorld.setDebugDrawer(&debugRenderer);

        // FIXME all of this leaks memory. Just a test is all it is.
        btCollisionShape* groundShape = new btStaticPlaneShape(btVector3(0, 1, 0), 1);

        btCollisionShape* fallShape = new btSphereShape(1);


        btDefaultMotionState* groundMotionState = new btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), btVector3(0, -2, 0)));
        btRigidBody::btRigidBodyConstructionInfo
                groundRigidBodyCI(0, groundMotionState, groundShape, btVector3(0, 0, 0));
        btRigidBody* groundRigidBody = new btRigidBody(groundRigidBodyCI);
        dynamicsWorld.addRigidBody(groundRigidBody);


        btDefaultMotionState* fallMotionState =
                new btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), btVector3(0, 4, -4)));
        btScalar mass = 1;
        btVector3 fallInertia(0, 0, 0);
        fallShape->calculateLocalInertia(mass, fallInertia);
        btRigidBody::btRigidBodyConstructionInfo fallRigidBodyCI(mass, fallMotionState, fallShape, fallInertia);
        btRigidBody* fallRigidBody = new btRigidBody(fallRigidBodyCI);
        dynamicsWorld.addRigidBody(fallRigidBody);

        timescale = 1;
        getWorldManager()->addWorld(this);
    }

    World::~World()
    {
        getWorldManager()->removeWorld(this);
        /*boost::mutex::scoped_lock lock(dynamicEntityMutex);
        {
            for (std::pair<uint64_t, DynamicEntity*> forceloadPair : forceLoadEntities)
            {
                forceloadPair.second->decRef();
            }
        }*/

        while (!forceLoadEntities.empty())
        {
            std::unordered_map<uint64_t, DynamicEntity*>::iterator pair = forceLoadEntities.begin();
            pair->second->decRef();
        }

        for (std::pair<uint64_t, TerrainChunk*> chunk_pair : chunks)
        {
            TerrainChunk * chunk = chunk_pair.second;
            delete chunk;
        }

        delete this->terrainProvider;
    }

    Block World::createBlock(std::string name)
    {
        std::pair<BlockController*, uint16_t> thePair = blockControllersAndIDsByName[name];
        BlockController * controller = thePair.first;

        if (controller)
        {
            return controller->buildBlock(thePair.second);
        }
        else
        {
            std::string message = "Attempted to spawn a block without a known block controller: ";
            message += name;
            throw std::runtime_error(message);
        }
    }

    RM::ref<DynamicEntity> World::spawnDynamicEntity(std::string name, glm::vec3 location, sol::table args)
    {
        RM::ref<DynamicEntityController> controller = getControllerManager()->getDynamicEntityController(name);
        RM::ref<DynamicEntity> entity = new DynamicEntity(controller, this, location, args);

        return entity;
    }

    void World::save()
    {
        writeHeader();
        writeBlockList();
        writeTerrain();
        writeDynamicEntityForceLoadList();
    }

    void World::loadEntities()
    {
        readDynamicEntityForceLoadList();
    }

    void World::writeHeader()
    {

        RM::Path headerFile = folder / "header.bin";

        // If it exists, we keep a backup of the old header just in case things go wrong.
        if (headerFile.exists())
        {
            RM::Path headerBackup = folder / "header.bin-backup";
            if (headerBackup.exists())
            {
                headerBackup.remove();
            }

            headerFile.move(headerBackup);
        }

        boost::filesystem::ofstream file(headerFile.getBoostPath(), std::ios::binary);
        if (file)
        {
            BinTools::writeStruct(file, world_format_version);
            BinTools::writeStruct(file, name);
            BinTools::writeStruct(file, time);

            file.close();
        }
        else
        {
            std::cout << "Warning: Failed to open header file \"" << headerFile.string() << "\" for write." << std::endl;
            restoreHeaderBackup();
        }
    }

    void World::readHeader()
    {
        RM::Path headerFile = folder / "header.bin";

        // If it exists, we keep a backup of the old header just in case things go wrong.
        if (!headerFile.exists())
        {
            std::cout << "Warning: Could not find world's header file." << std::endl;
            restoreHeaderBackup();
        }

        std::string input;

        boost::filesystem::ifstream file(headerFile.getBoostPath(), std::ios::in | std::ios::binary);
        if (file)
        {
            try
            {
                uint16_t version;
                BinTools::readStruct(file, version);
                if (version != world_format_version)
                {
                    throw std::runtime_error("Incompatible world version format.");
                }

                BinTools::readStruct(file, name);
                BinTools::readStruct(file, time);
            }
            catch (BinTools::BinReadError & e)
            {
                std::cout << "Error reading header file: " << e.what() << std::endl;
                restoreHeaderBackup();
                readHeader();
            }
        }
        else
        {
            throw std::runtime_error("Failed to open world header file.");
        }
    }

    void World::restoreHeaderBackup()
    {
        RM::Path file = folder / "header.bin";
        RM::Path backup = folder / "header.bin-backup";

        // If it exists, we keep a backup of the old header just in case things go wrong.
        if (backup.exists())
        {
            if (file.exists())
            {
                file.remove();
            }

            backup.move(file);
            std::cout << "Header file backup restored." << std::endl;
        }
        else
        {
            std::cout << "Could not find a header file backup." << std::endl;
        }
    }

    void World::writeBlockList()
    {
        RM::Path blockListFile = terrainFolder / "block_ids.bin";

        // If it exists, we keep a backup of the old block list just in case things go wrong.
        if (blockListFile.exists())
        {
            RM::Path blockListBackup = terrainFolder / "block_ids.bin-backup";
            if (blockListBackup.exists())
            {
                blockListBackup.remove();
            }

            blockListFile.move(blockListBackup);
        }

        boost::filesystem::ofstream file(blockListFile.getBoostPath(), std::ios::binary);
        if (file)
        {
            for (auto controller : blockControllersByID)
            {
                if (controller)
                {
                    std::string name = controller->resourceName();
                    BinTools::writeStruct(file, name);
                }
            }

            file.close();
        }
        else
        {
            restoreBlockListBackup();
            throw std::runtime_error("Failed to open block list file for write.");
        }
    }

    void World::readBlockList()
    {
        RM::Path blockListFile = terrainFolder / "block_ids.bin";

        // If it exists, we keep a backup of the old header just in case things go wrong.
        if (!blockListFile.exists())
        {
            std::cout << "Warning: Could not find world's block ID file." << std::endl;
            restoreBlockListBackup();
        }

        std::string input;

        boost::filesystem::ifstream file(blockListFile.getBoostPath(), std::ios::in | std::ios::binary);
        if (file)
        {
            file.seekg(0, std::ios::end);
            int endOfFile = file.tellg();
            file.seekg(0, std::ios::beg);

            ControllerManager * blockControllerManager = getControllerManager();

            while (file.tellg() != endOfFile)
            {
                std::string blockPath;
                BinTools::readStruct(file, blockPath);

                RM::ref<BlockController> controller = blockControllerManager->getBlockController(blockPath);

                this->blockControllersByID.push_back(controller);

                uint16_t blockID = blockControllersByID.size() - 1;

                this->blockControllersAndIDsByName[controller->resourceName()] = std::pair<BlockController*, uint16_t>(controller, blockID);
                std::cout << "Block \"" << controller->resourceName() << "\" has been assigned ID: " << blockID << std::endl;

            }
        }
        else
        {
            throw std::runtime_error("Failed to open world block ID file.");
        }
    }

    void World::restoreBlockListBackup()
    {
        RM::Path file = terrainFolder / "block_ids.bin";
        RM::Path backup = terrainFolder / "block_ids.bin-backup";

        // If it exists, we keep a backup of the old header just in case things go wrong.
        if (backup.exists())
        {
            if (file.exists())
            {
                file.remove();
            }

            backup.move(file);
            std::cout << "Block ID file backup restored." << std::endl;
        }
        else
        {
            std::cout << "Could not find a block ID file backup." << std::endl;
        }
    }

    void World::writeTerrain()
    {
        for (std::pair<uint64_t, TerrainChunk*> chunk_pair : chunks)
        {
            TerrainChunk * chunk = chunk_pair.second;
            chunk->saveTerrain();
        }
    }

    void World::writeDynamicEntityForceLoadList()
    {
        RM::Path entityListFile = dynamicEntitiesFolder / "forceload.bin";

        // If it exists, we keep a backup of the old block list just in case things go wrong.
        if (entityListFile.exists())
        {
            RM::Path entityListBackup = dynamicEntitiesFolder / "forceload.bin-backup";
            if (entityListBackup.exists())
            {
                entityListBackup.remove();
            }

            entityListFile.move(entityListBackup);
        }

        boost::filesystem::ofstream file(entityListFile.getBoostPath(), std::ios::binary);
        if (file)
        {
            for (std::pair<uint64_t, DynamicEntity*> forceloadPair : forceLoadEntities)
            {
                BinTools::writeStruct(file, forceloadPair.second->getID());
            }

            file.close();
        }
        else
        {
            restoreDynamicEntityForceLoadList();
            throw std::runtime_error("Failed to open dynamic entity force load list file for write.");
        }
    }

    void World::readDynamicEntityForceLoadList()
    {
        RM::Path entityListFile = dynamicEntitiesFolder / "forceload.bin";

        // If it exists, we keep a backup of the old header just in case things go wrong.
        if (!entityListFile.exists())
        {
            std::cout << "Warning: Could not find world's force load entity list file." << std::endl;
            restoreDynamicEntityForceLoadList();
        }

        boost::filesystem::ifstream file(entityListFile.getBoostPath(), std::ios::in | std::ios::binary);
        if (file)
        {
            file.seekg(0, std::ios::end);
            int endOfFile = file.tellg();
            file.seekg(0, std::ios::beg);

            while (file.tellg() != endOfFile) {
                uint64_t entityID;
                BinTools::readStruct(file, entityID);

                RM::Path file = dynamicEntitiesFolder / formatAsHex(entityID);
                new DynamicEntity(this, file, entityID);
            }
        }
        else
        {
            throw std::runtime_error("Failed to open world force load entity list file.");
        }
    }

    void World::restoreDynamicEntityForceLoadList()
    {
        RM::Path file = dynamicEntitiesFolder / "forceload.bin";
        RM::Path backup = dynamicEntitiesFolder / "forceload.bin-backup";

        // If it exists, we keep a backup of the old header just in case things go wrong.
        if (backup.exists())
        {
            if (file.exists())
            {
                file.remove();
            }

            backup.move(file);
            std::cout << "Dynamic Entity force load list file backup restored." << std::endl;
        }
        else
        {
            std::cout << "Could not find a Dynamic Entity force load list file backup." << std::endl;
        }
    }

    void World::update(float elapsed)
    {
        time += elapsed * timescale;

        chunksToUpdate.clear();
        chunksToUpdate.reserve(chunks.size());

        // First we make a copy of our list of chunks to update. We do this so that chunks can inject
        // new chunks while we are working without running the risk of messing up this loop.
        // TODO update terrain during this step.
        for (std::pair<uint64_t, TerrainChunk*> chunk_pair : chunks)
        {
            TerrainChunk * chunk = chunk_pair.second;
            chunksToUpdate.push_back(chunk);
        }


        for (TerrainChunk * chunk : chunksToUpdate)
        {
            // Now update the chunk.
            chunk->update(elapsed, time);
        }

        dynamicsWorld.stepSimulation(elapsed, 10);
    }

    void World::renderTerrain(GL::View & view, GL::ShaderProgramScope & programScope)
    {
        // It might be best to move this to an outside renderer, especially when we implement a pipelined renderer.
        if (isPhysicsDebugEnabled())
        {
            debugRenderer.render(programScope.getRenderTarget(), view, dynamicsWorld);
        }

        programScope.setMatrix4x4("matrix", view.getCombinedMatrix());

        for (std::pair<uint64_t, TerrainChunk*> chunk : chunks)
        {
            chunk.second->renderTerrain(view, programScope);
        }
    }

    uint64_t World::generateDynamicEntityID(RM::Path & path)
    {
        RM::Path file;
        uint64_t ID;
        boost::mutex::scoped_lock lock(dynamicEntityMutex);

        do
        {
            ID = dynamicEntityIDDistrobution(dynamicEntityIDGenerator);
            file = dynamicEntitiesFolder / formatAsHex(ID);

        } while (file.exists());

        file.createFile();
        path = file;
        return ID;
    }

    DynamicEntity * World::getDynamicEntity(DynamicEntityLockon entity_lock)
    {
        std::unordered_map<uint64_t, DynamicEntity*>::iterator result;
        {
            boost::mutex::scoped_lock lock(dynamicEntityMutex);
            result = loadedDynamicEntities.find(entity_lock.ID);
        }

        if (result != loadedDynamicEntities.end())
        {
            // The entity with this ID is already loaded!
            DynamicEntity * entity = result->second;

            // Check if it was created at the right time, otherwise it may be just a reused ID.
            if (entity->getTimeOfCreation() == entity_lock.timeOfCreation)
            {
                return entity;
            }
            else
            {
                return nullptr;
            }
        }
        else
        {
            // If this entity exists, it's not loaded. We need to find it.

            RM::Path file_path = dynamicEntitiesFolder / formatAsHex(entity_lock.ID);

            if (file_path.exists())
            {
                boost::filesystem::ifstream file(file_path.getBoostPath());

                double timeOfCreation;
                BinTools::readStruct(file, timeOfCreation);

                file.close();

                if (timeOfCreation == entity_lock.timeOfCreation)
                {
                    return new DynamicEntity(this, file_path, entity_lock.ID);
                }
            }
        }

        return nullptr;
    }
}
