/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "JSONBlockController.hpp"
#include "main.hpp"

namespace World
{
    JSONBlockController::JSONBlockController(RM::ResourceGroup * group, RM::Path jsonFile):
        BlockController(group, jsonFile.getAbsolute().string()),
        JSONControllerCore(jsonFile, "-block.json", "block", [this](Json::Value & root, Json::Reader & reader) { this->loadJSON(root, reader); }, jsonFile.getAbsolute().string())
    {

    }

    JSONBlockController::JSONBlockController(RM::Path jsonFile):
        JSONBlockController(nullptr, jsonFile)
    {

    }

    JSONBlockController::~JSONBlockController()
    {

    }

    unsigned JSONBlockController::getHorizontalRotation(Block block)
    {
        if (hRotation)
        {
            return getMetaValue(block, hRotation);
        }
        else
        {
            return 0;
        }
    }

    unsigned JSONBlockController::getVerticalRotation(Block block)
    {
        if (vRotation)
        {
            return getMetaValue(block, vRotation);
        }
        else
        {
            return 0;
        }
    }

    BlockController::Shape JSONBlockController::getShape(Block block)
    {
        if (shape)
        {
            return static_cast<BlockController::Shape>(getMetaValue(block, shape));
        }
        else
        {
            return defaultShape;
        }
    }

    void JSONBlockController::renderToTerrain(Block block, BlockEntity * entity, long x, long y, long z, TerrainVisualMesh * mesh)
    {
        if (useDefaultRenderer)
        {
            Shape shape = getShape(block);
            switch (shape)
            {
            case Cube:
                mesh->addFace(
                    glm::vec3(x    , y + 1, z + 1),
                    glm::vec3(x + 1, y + 1, z + 1),
                    glm::vec3(x + 1, y + 1, z    )
                );
                break;
            case Slope:
            case Corner:
            case InvertedCorner:
            case Pyramid:
            case Cylinder:
            case Sphere:
            case Bullet:
            default:
                std::cout << "Warning: Currently blocks can only be rendered as cubes." << std::endl;
            }
        }
    }

    void JSONBlockController::loadMetaManipulators(Json::Value meta)
    {
        if (meta.isArray())
        {
            uint16_t defaultMeta = 0;

            for (Json::Value item : meta)
            {
                std::string name = item["name"].asString();
                std::string bits = item["bits"].asString();
                uint16_t value   = item["default value"].asInt();

                if (bits.size() == 0)
                {
                    throw JsonError(item["bits"], "Bit range not specified.");
                }

                uint16_t shift = getBitIndex(bits[0]);
                unsigned width = -1;
                unsigned lastBit = shift - 1;
                for (char c : bits)
                {
                    unsigned int bitIndex = getBitIndex(c);

                    if (lastBit + 1 == bitIndex)
                    {
                        lastBit = bitIndex;
                        width++;
                    }
                    else
                    {
                        throw JsonError(item["bits"], "Bits for bit range must be arranged in a consecutive order.");
                    }
                }

                uint16_t mask = static_cast<uint16_t>(static_cast<int16_t>(1 << 15) >> width) >> (15 - shift - width);

                defaultMeta |= (value << shift) & mask;

                metaDataManipulators.try_emplace(name, shift, mask);
            }

            this->defaultMeta = defaultMeta;

            // A data manipulator that accomplishes nothing which is handy for indicating you don't actually want to do anything.
            metaDataManipulators.try_emplace("NULL", 0, 0);
        }
        else
        {
            throw JsonError(meta, "Error while loading meta data specification for block controller: Meta values list is not an array.");
        }
    }

    unsigned int JSONBlockController::getBitIndex(char bitHex)
    {
        if (bitHex <= '9' && bitHex >= '0')
        {
            return bitHex - '0';
        }
        else if (bitHex <= 'F' && bitHex >= 'A')
        {
            return bitHex - ('A' - 10);
        }
        else
        {
            std::string message = "Unknown bit index: ";
            message += bitHex;
            message += "\nRemember that letters must be capitalized.";
            throw std::runtime_error(message);
        }
    }

    void JSONBlockController::loadJSON(Json::Value & root, Json::Reader & reader)
    {
        setResourceStepsToLoad(1);

        vRotation = nullptr;
        hRotation = nullptr;
        shape = nullptr;
        useDefaultRenderer = false;

        try {
            bhasEntity = root["has entity"].asBool();
            loadMetaManipulators(root["meta"]);
            loadFuncTableBuilder(root["functions"], reader, resourceName());

            if (root["is direction"].asBool())
            {
                {
                    std::map<std::string, MetaManipulator>::iterator result = metaDataManipulators.find("direction horizontal");
                    if (result != metaDataManipulators.end())
                    {
                        hRotation = &result->second;
                    }
                    else
                    {
                        throw std::runtime_error("Block is indicated as being directional, but no bits are set to indicate horizontal direction.");
                    }
                }

                {
                    std::map<std::string, MetaManipulator>::iterator result = metaDataManipulators.find("direction vertical");
                    if (result != metaDataManipulators.end())
                    {
                        vRotation = &result->second;
                    }
                    else
                    {
                        throw std::runtime_error("Block is indicated as being directional, but no bits are set to indicate vertical direction.");
                    }
                }

            }

            std::string shapeName = root["shape"].asString();

            if (shapeName == "dynamic")
            {
                std::map<std::string, MetaManipulator>::iterator result = metaDataManipulators.find("shape");
                if (result != metaDataManipulators.end())
                {
                    shape = &result->second;
                }
                else
                {
                    throw std::runtime_error("Block is indicated as having a dynamic shape, but no bits are set to indicate the shape.");
                }
            }
            else if (shapeName == "cube")
            {
                defaultShape = Cube;
            }
            else if (shapeName == "slope")
            {
                defaultShape = Slope;
            }
            else if (shapeName == "corner")
            {
                defaultShape = Corner;
            }
            else if (shapeName == "inverted corner")
            {
                defaultShape = InvertedCorner;
            }
            else if (shapeName == "pyramid")
            {
                defaultShape = Pyramid;
            }
            else if (shapeName == "cylinder")
            {
                defaultShape = Cylinder;
            }
            else if (shapeName == "sphere")
            {
                defaultShape = Sphere;
            }
            else if (shapeName == "bullet")
            {
                defaultShape = Bullet;
            }
            else
            {
                std::string message = "Unknown shape: ";
                message += shapeName;
                throw std::runtime_error(message);
            }

            Json::Value renderer = root["renderer"];

            if (renderer.isString())
            {
                std::string rendererName = renderer.asString();
                if (rendererName == "default")
                {
                    useDefaultRenderer = true;
                }
            }
            else if (renderer.isNull())
            {
                // Cool then we just don't do anything.
            }
            else
            {
                throw std::runtime_error("Unsupported renderer type.");
            }

        } catch (JsonError & error) {
            reader.pushError(error.value, error.message);
            throw std::runtime_error("Bad Value in JSON block: " + reader.getFormatedErrorMessages());
        }

        addSelfFunctions = [](sol::environment & env)
        {

        };

        incrementResourceProgress(1);
    }
}
