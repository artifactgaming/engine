/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "BlockController.hpp"

#include "globals.hpp"
#include "ControllerManager.hpp"

namespace World {
    BlockController::BlockController(RM::ResourceGroup * group, const std::string & name):
        RM::BasicResource(name, "BlockController", group)
    {
        defaultMeta = 0;
    }

    BlockController::~BlockController()
    {
        getControllerManager()->removeBlockController(resourceName());
    }

    Block BlockController::buildBlock(uint16_t ID)
    {
        return Block{
            ID,
            defaultMeta
        };
    }

    Block BlockController::setMetaValue(Block block, std::string name, uint16_t value)
    {
        std::map<std::string, MetaManipulator>::iterator result = metaDataManipulators.find(name);
        if (result != metaDataManipulators.end())
        {
            uint16_t metadata = block.metadata;

            MetaManipulator & manipulator = result->second;
            uint16_t mask = manipulator.mask;
            uint16_t shift = manipulator.shift;

            metadata &= ~mask; // Clear out old value.
            metadata |= (value << shift) & mask; // Set to new value.

            return Block{block.ID, metadata};
        }
        else
        {
            throw std::runtime_error("Attempted to set unknown metadata value on block.");
        }
    }

    uint16_t BlockController::getMetaValue(Block block, std::string name)
    {
        std::map<std::string, MetaManipulator>::iterator result = metaDataManipulators.find(name);
        if (result != metaDataManipulators.end())
        {
            uint16_t metadata = block.metadata;
            MetaManipulator & manipulator = result->second;
            metadata = (metadata & manipulator.mask) >> manipulator.shift;

            return metadata;
        }
        else
        {
            throw std::runtime_error("Attempted to get unknown metadata value from block.");
        }
    }

    Block BlockController::setMetaValue(Block block, MetaManipulator * manipulator, uint16_t value)
    {
        uint16_t metadata = block.metadata;

        uint16_t mask = manipulator->mask;
        uint16_t shift = manipulator->shift;

        metadata &= ~mask; // Clear out old value.
        metadata |= (value << shift) & mask; // Set to new value.

        return Block{block.ID, metadata};
    }

    uint16_t BlockController::getMetaValue(Block block, MetaManipulator * manipulator)
    {
        uint16_t metadata = block.metadata;
        metadata = (metadata & manipulator->mask) >> manipulator->shift;

        return metadata;
    }
}
