/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "BlockContainer.hpp"

#include <gzip_cpp.hpp>

namespace World {
    BlockContainer::BlockContainer(std::string name, int16_t x, int16_t y, int16_t z):
        x(x),
        y(y),
        z(z)
    {
        blocks = nullptr;
        terrainGenerated = false;

        mesh = nullptr;
    }

    BlockContainer::~BlockContainer()
    {
        if (mesh)
        {
            delete mesh;
        }

        // Delete all block entities.
        for (auto pair: blockEntities)
        {
            delete pair.second;
        }

        // If blocks are loaded, delete them too.
        if (blocks)
        {
            delete[] blocks;
        }
    }

    void BlockContainer::saveTerrainToFile() noexcept
    {
        // Saving is only attempted if blocks are loaded.
        if (blocks && terrainGenerated)
        {
            // If it exists, we keep a backup of the old file just in case things go wrong.
            if (file.exists())
            {
                RM::Path backup = file + "-backup";
                if (backup.exists())
                {
                    backup.remove();
                }

                file.move(backup);
            }

            boost::filesystem::ofstream output(file.getBoostPath(), std::ios::binary);
            if (output)
            {
                gzip::Comp comp(gzip::Comp::Level::Max);

                if (comp.IsSucc())
                {
                    gzip::DataList compressed_chunk = comp.Process(reinterpret_cast<char*>(blocks), block_array_length * sizeof(Block), true);
                    gzip::DataList::iterator it = compressed_chunk.begin();

                    while (it != compressed_chunk.end())
                    {
                        gzip::Data data = *it;
                        output.write(data->ptr, data->size);
                        it++;
                    }
                }
                else
                {
                    std::cout << "Could not start data compressor for terrain file: " << file.string() << std::endl;
                    restoreBackup();
                }

                output.close();
            }
            else
            {
                std::cout << "Warning: Failed to open terrain file for write: " << file.string() << std::endl;
                restoreBackup();
            }
        }
    }

    bool BlockContainer::loadTerrainFromFile() noexcept
    {
        if (!blocks)
        {
            // Notice, we don't need to zero them out because we are going to initialize them ourselves.
            blocks = new Block[block_array_length];
        }

        if (file.exists())
        {
            gzip::Decomp decomp;

            if (decomp.IsSucc())
            {
                boost::filesystem::ifstream input(file.getBoostPath(), std::ios::binary);
                if (input)
                {
                    input.seekg(0, std::ios::end);
                    gzip::Data dataRead = gzip::AllocateData(input.tellg());
                    input.seekg(0, std::ios::beg);
                    input.read(dataRead->ptr, dataRead->size);
                    input.close();

                    bool worked;
                    gzip::DataList decompressed_data;
                    std::tie(worked, decompressed_data) = decomp.Process(dataRead);
                    if (worked) {
                        char * blockBytes = reinterpret_cast<char*>(blocks);
                        unsigned int bytesWritten = 0;

                        gzip::DataList::iterator it = decompressed_data.begin();

                        while (it != decompressed_data.end())
                        {
                            gzip::Data data = *it;

                            size_t size = data->size;

                            // Always check your bounds!
                            bytesWritten += size;
                            if (bytesWritten > block_array_length * sizeof(Block))
                            {
                                std::cout << "Warning: Terrain file had more bytes in it than it should.\n"
                                          << "This file may be corrupted or modified for malicious purposes." << std::endl;


                                // Assume nothing is safe! Clear out all that data!
                                std::memset(blocks, 0, sizeof(Block) * block_array_length);
                                return false;
                            }

                            std::memcpy(blockBytes, data->ptr, size);

                            blockBytes += size;
                            it++;
                        }

                        if (bytesWritten < block_array_length * sizeof(Block))
                        {
                            std::cout << "Warning: Terrain file had fewer bytes in it than it should.\n"
                                      << "This file may be corrupted or modified for malicious purposes." << std::endl;


                            // Assume nothing is safe! Clear out all that data!
                            std::memset(blocks, 0, sizeof(Block) * block_array_length);
                            return false;
                        }
                    }
                    else
                    {
                        std::cout << "Warning: Could not decompress terrain file. Chunk file may be corrupted. File: " << file.string() << std::endl;
                        restoreBackup();
                        return loadTerrainFromFile();
                    }
                }
                else
                {
                    std::cout << "Warning: Failed to open terrain file for read. File: " << file.string() << std::endl;
                    restoreBackup();
                    return loadTerrainFromFile();
                }
            }
            else
            {
                std::cout << "Warning: Could not start data decompresser for terrain chunk. File: " << file.string() << std::endl;
                restoreBackup();
                return loadTerrainFromFile();
            }

            queueForStaticMeshUpdate();
            return true;
        }
        else
        {
            return false;
        }
    }

    void BlockContainer::restoreBackup()
    {
        // Delete our possibly corrupt file.
        if (file.exists())
        {
            file.remove();
        }

        // Find and restore the backup.
        RM::Path backup = file + "-backup";
        if (backup.exists())
        {
            backup.move(file);
            std::cout << "Terrain backup file has been restored." << std::endl;
        }
        else
        {
            std::cout << "Terrain backup file has does not exist. It has not been restored." << std::endl;
        }
    }

    void BlockContainer::initializeBlockArray()
    {
        // Allocate memory and set everything to be air.
        blocks = new Block[block_array_length];
        std::memset(blocks, 0, sizeof(Block) * block_array_length);
    }
}
