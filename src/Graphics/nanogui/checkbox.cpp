/*
    src/checkbox.cpp -- Two-state check box widget

    NanoGUI was developed by Wenzel Jakob <wenzel.jakob@epfl.ch>.
    The widget drawing code is based on the NanoVG demo application
    by Mikko Mononen.

    All rights reserved. Use of this source code is governed by a
    BSD-style license that can be found in the LICENSE.txt file.
*/

#include <nanogui/checkbox.h>
#include <nanogui/opengl.h>
#include <nanogui/theme.h>
#include <nanogui/serializer/core.h>

NAMESPACE_BEGIN(nanogui)

CheckBox::CheckBox(Widget *parent, const std::string &caption)
    : Widget(parent), mCaption(caption), mPushed(false), mChecked(false){

    mIconExtraScale = 1.2f;// widget override
}

bool CheckBox::mouseButtonEvent(const glm::ivec2 &p, int button, bool down,
                                int modifiers) {
    Widget::mouseButtonEvent(p, button, down, modifiers);
    if (!mEnabled)
        return false;

    if (button == GLFW_MOUSE_BUTTON_1) {
        if (down) {
            mPushed = true;
        } else if (mPushed) {
            if (contains(p)) {
                mChecked = !mChecked;
                mCallback(mChecked);
            }
            mPushed = false;
        }
        return true;
    }
    return false;
}

glm::ivec2 CheckBox::preferredSize(NVGcontext *ctx) const {
    if (mFixedSize != glm::ivec2(0))
        return mFixedSize;
    nvgFontSize(ctx, fontSize());
    nvgFontFace(ctx, "sans");
    return glm::ivec2(
        nvgTextBounds(ctx, 0, 0, mCaption.c_str(), nullptr, nullptr) +
            1.8f * fontSize(),
        fontSize() * 1.3f);
}

void CheckBox::draw(NVGcontext *ctx) {
    Widget::draw(ctx);

    nvgFontSize(ctx, fontSize());
    nvgFontFace(ctx, "sans");
    nvgFillColor(ctx,
                 convertToNVGcolor(mEnabled ? mTheme->mTextColor : mTheme->mDisabledTextColor));
    nvgTextAlign(ctx, NVG_ALIGN_LEFT | NVG_ALIGN_MIDDLE);
    nvgText(ctx, mPos.x + 1.6f * fontSize(), mPos.y + mSize.y * 0.5f,
            mCaption.c_str(), nullptr);

    NVGpaint bg = nvgBoxGradient(ctx, mPos.x + 1.5f, mPos.y + 1.5f,
                                 mSize.y - 2.0f, mSize.y - 2.0f, 3, 3,
                                 convertToNVGcolor(mPushed ? glm::vec4(0, 0, 0, 0.4f) : glm::vec4(0, 0, 0, 0.13)),
                                 convertToNVGcolor(glm::vec4(0, 0, 0, 0.71)));

    nvgBeginPath(ctx);
    nvgRoundedRect(ctx, mPos.x + 1.0f, mPos.y + 1.0f, mSize.y - 2.0f,
                   mSize.y - 2.0f, 3);
    nvgFillPaint(ctx, bg);
    nvgFill(ctx);

    if (mChecked) {
        nvgFontSize(ctx, mSize.y * icon_scale());
        nvgFontFace(ctx, "icons");
        nvgFillColor(ctx, convertToNVGcolor(mEnabled ? mTheme->mIconColor
                                                     : mTheme->mDisabledTextColor));
        nvgTextAlign(ctx, NVG_ALIGN_CENTER | NVG_ALIGN_MIDDLE);
        nvgText(ctx, mPos.x + mSize.y * 0.5f + 1,
                mPos.y + mSize.y * 0.5f, utf8(mTheme->mCheckBoxIcon).data(),
                nullptr);
    }
}

void CheckBox::save(Serializer &s) const {
    Widget::save(s);
    s.set("caption", mCaption);
    s.set("pushed", mPushed);
    s.set("checked", mChecked);
}

bool CheckBox::load(Serializer &s) {
    if (!Widget::load(s)) return false;
    if (!s.get("caption", mCaption)) return false;
    if (!s.get("pushed", mPushed)) return false;
    if (!s.get("checked", mChecked)) return false;
    return true;
}

void CheckBox::dispose() {
    this->Widget::dispose();
    mCallback.clear();
}

NAMESPACE_END(nanogui)
