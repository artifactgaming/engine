/*
    src/graph.cpp -- Simple graph widget for showing a function plot

    NanoGUI was developed by Wenzel Jakob <wenzel.jakob@epfl.ch>.
    The widget drawing code is based on the NanoVG demo application
    by Mikko Mononen.

    All rights reserved. Use of this source code is governed by a
    BSD-style license that can be found in the LICENSE.txt file.
*/

#include <nanogui/graph.h>
#include <nanogui/theme.h>
#include <nanogui/opengl.h>
#include <nanogui/serializer/core.h>

NAMESPACE_BEGIN(nanogui)

Graph::Graph(Widget *parent, const std::string &caption)
    : Widget(parent), mCaption(caption) {
    mBackgroundColor = glm::vec4(20/255.0f, 20/255.0f, 20/255.0f, 128/255.0f);
    mForegroundColor = glm::vec4(255/255.0f, 192/255.0f, 0/255.0f, 128/255.0f);
    mTextColor = glm::vec4(240/255.0f, 240/255.0f, 240/255.0f, 192/255.0f);
}

glm::ivec2 Graph::preferredSize(NVGcontext *) const {
    return glm::ivec2(180, 45);
}

void Graph::draw(NVGcontext *ctx) {
    Widget::draw(ctx);

    nvgBeginPath(ctx);
    nvgRect(ctx, mPos.x, mPos.y, mSize.x, mSize.y);
    nvgFillColor(ctx, convertToNVGcolor(mBackgroundColor));
    nvgFill(ctx);

    if (mValues.size() < 2)
        return;

    nvgBeginPath(ctx);
    nvgMoveTo(ctx, mPos.x, mPos.y+mSize.y);
    for (size_t i = 0; i < (size_t) mValues.size(); i++) {
        float value = mValues[i];
        float vx = mPos.x + i * mSize.x / (float) (mValues.size() - 1);
        float vy = mPos.y + (1-value) * mSize.y;
        nvgLineTo(ctx, vx, vy);
    }

    nvgLineTo(ctx, mPos.x + mSize.x, mPos.y + mSize.y);
    nvgStrokeColor(ctx, convertToNVGcolor(glm::vec4(100/255.0f, 100/255.0f, 100/255.0f, 255/255.0f)));
    nvgStroke(ctx);
    nvgFillColor(ctx, convertToNVGcolor(mForegroundColor));
    nvgFill(ctx);

    nvgFontFace(ctx, "sans");

    if (!mCaption.empty()) {
        nvgFontSize(ctx, 14.0f);
        nvgTextAlign(ctx, NVG_ALIGN_LEFT | NVG_ALIGN_TOP);
        nvgFillColor(ctx, convertToNVGcolor(mTextColor));
        nvgText(ctx, mPos.x + 3, mPos.y + 1, mCaption.c_str(), NULL);
    }

    if (!mHeader.empty()) {
        nvgFontSize(ctx, 18.0f);
        nvgTextAlign(ctx, NVG_ALIGN_RIGHT | NVG_ALIGN_TOP);
        nvgFillColor(ctx, convertToNVGcolor(mTextColor));
        nvgText(ctx, mPos.x + mSize.x - 3, mPos.y + 1, mHeader.c_str(), NULL);
    }

    if (!mFooter.empty()) {
        nvgFontSize(ctx, 15.0f);
        nvgTextAlign(ctx, NVG_ALIGN_RIGHT | NVG_ALIGN_BOTTOM);
        nvgFillColor(ctx, convertToNVGcolor(mTextColor));
        nvgText(ctx, mPos.x + mSize.x - 3, mPos.y + mSize.y - 1, mFooter.c_str(), NULL);
    }

    nvgBeginPath(ctx);
    nvgRect(ctx, mPos.x, mPos.y, mSize.x, mSize.y);
    nvgStrokeColor(ctx, convertToNVGcolor(glm::vec4(100/255.0f, 100/255.0f, 100/255.0f, 255/255.0f)));
    nvgStroke(ctx);
}

void Graph::save(Serializer &s) const {
    Widget::save(s);
    s.set("caption", mCaption);
    s.set("header", mHeader);
    s.set("footer", mFooter);
    s.set("backgroundColor", mBackgroundColor);
    s.set("foregroundColor", mForegroundColor);
    s.set("textColor", mTextColor);
    s.set("values", mValues);
}

bool Graph::load(Serializer &s) {
    if (!Widget::load(s)) return false;
    if (!s.get("caption", mCaption)) return false;
    if (!s.get("header", mHeader)) return false;
    if (!s.get("footer", mFooter)) return false;
    if (!s.get("backgroundColor", mBackgroundColor)) return false;
    if (!s.get("foregroundColor", mForegroundColor)) return false;
    if (!s.get("textColor", mTextColor)) return false;
    if (!s.get("values", mValues)) return false;
    return true;
}

NAMESPACE_END(nanogui)
