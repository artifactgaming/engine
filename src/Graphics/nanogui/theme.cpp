/*
    src/theme.cpp -- Storage class for basic theme-related properties

    The text box widget was contributed by Christian Schueller.

    NanoGUI was developed by Wenzel Jakob <wenzel.jakob@epfl.ch>.
    The widget drawing code is based on the NanoVG demo application
    by Mikko Mononen.

    All rights reserved. Use of this source code is governed by a
    BSD-style license that can be found in the LICENSE.txt file.
*/

#include <nanogui/theme.h>
#include <nanogui/opengl.h>
#include <nanogui/entypo.h>
#include <nanogui_resources.h>

NAMESPACE_BEGIN(nanogui)

Theme::Theme(NVGcontext *ctx) {
    mStandardFontSize                 = 16;
    mButtonFontSize                   = 20;
    mTextBoxFontSize                  = 20;
    mIconScale                        = 0.77f;

    mWindowCornerRadius               = 2;
    mWindowHeaderHeight               = 30;
    mWindowDropShadowSize             = 10;
    mButtonCornerRadius               = 2;
    mTabBorderWidth                   = 0.75f;
    mTabInnerMargin                   = 5;
    mTabMinButtonWidth                = 20;
    mTabMaxButtonWidth                = 160;
    mTabControlWidth                  = 20;
    mTabButtonHorizontalPadding       = 10;
    mTabButtonVerticalPadding         = 2;

    mDropShadow                       = glm::vec4(255/255.0f, 125/255.0f, 209/255.0f, 128/255.0f);
    mTransparent                      = glm::vec4(0/255.0f, 0/255.0f, 0/255.0f, 0/255.0f);
    mBorderDark                       = glm::vec4(72/255.0f, 33/255.0f, 103/255.0f, 255/255.0f);
    mBorderLight                      = glm::vec4(158/255.0f, 72/255.0f, 225/255.0f, 255/255.0f);
    mBorderMedium                     = glm::vec4(132/255.0f, 60/255.0f, 188/255.0f, 255/255.0f);
    mTextColor                        = glm::vec4(255/255.0f, 255/255.0f, 255/255.0f, 160/255.0f);
    mDisabledTextColor                = glm::vec4(255/255.0f, 255/255.0f, 255/255.0f, 80/255.0f);
    mTextColorShadow                  = glm::vec4(0/255.0f, 0/255.0f, 0/255.0f, 160/255.0f);
    mIconColor                        = mTextColor;

    mButtonGradientTopFocused         = glm::vec4(166/255.0f, 76/255.0f, 237/255.0f, 255/255.0f);
    mButtonGradientBotFocused         = glm::vec4(54/255.0f, 125/255.0f, 209/255.0f, 255/255.0f);
    mButtonGradientTopUnfocused       = glm::vec4(119/255.0f, 55/255.0f, 170/255.0f, 255/255.0f);
    mButtonGradientBotUnfocused       = glm::vec4(51/255.0f, 23/255.0f, 73/255.0f, 255/255.0f);
    mButtonGradientTopPushed          = glm::vec4(51/255.0f, 23/255.0f, 73/255.0f, 255/255.0f);
    mButtonGradientBotPushed          = glm::vec4(51/255.0f, 23/255.0f, 73/255.0f, 255/255.0f);

    /* Window-related */
    mWindowFillUnfocused              = glm::vec4(51/255.0f, 23/255.0f, 73/255.0f, 230/255.0f);
    mWindowFillFocused                = glm::vec4(81/255.0f, 37/255.0f, 115/255.0f, 230/255.0f);
    mWindowTitleUnfocused             = glm::vec4(220/255.0f, 220/255.0f, 220/255.0f, 160/255.0f);
    mWindowTitleFocused               = glm::vec4(255/255.0f, 255/255.0f, 255/255.0f, 190/255.0f);

    mWindowHeaderGradientTop          = mButtonGradientTopUnfocused;
    mWindowHeaderGradientBot          = mButtonGradientBotUnfocused;
    mWindowHeaderSepTop               = mBorderLight;
    mWindowHeaderSepBot               = mBorderDark;

    mWindowPopup                      = mWindowFillFocused;
    mWindowPopupTransparent           = glm::vec4(50/255.0f, 50/255.0f, 50/255.0f, 0/255.0f);

    mCheckBoxIcon                     = ENTYPO_ICON_CHECK;
    mMessageInformationIcon           = ENTYPO_ICON_INFO_WITH_CIRCLE;
    mMessageQuestionIcon              = ENTYPO_ICON_HELP_WITH_CIRCLE;
    mMessageWarningIcon               = ENTYPO_ICON_WARNING;
    mMessageAltButtonIcon             = ENTYPO_ICON_CIRCLE_WITH_CROSS;
    mMessagePrimaryButtonIcon         = ENTYPO_ICON_CHECK;
    mPopupChevronRightIcon            = ENTYPO_ICON_CHEVRON_RIGHT;
    mPopupChevronLeftIcon             = ENTYPO_ICON_CHEVRON_LEFT;
    mTabHeaderLeftIcon                = ENTYPO_ICON_ARROW_BOLD_LEFT;
    mTabHeaderRightIcon               = ENTYPO_ICON_ARROW_BOLD_RIGHT;
    mTextBoxUpIcon                    = ENTYPO_ICON_CHEVRON_UP;
    mTextBoxDownIcon                  = ENTYPO_ICON_CHEVRON_DOWN;

    mFontNormal = nvgCreateFontMem(ctx, "sans", roboto_regular_ttf,
                                   roboto_regular_ttf_size, 0);
    mFontBold = nvgCreateFontMem(ctx, "sans-bold", roboto_bold_ttf,
                                 roboto_bold_ttf_size, 0);
    mFontIcons = nvgCreateFontMem(ctx, "icons", entypo_ttf,
                                  entypo_ttf_size, 0);
    if (mFontNormal == -1 || mFontBold == -1 || mFontIcons == -1)
        throw std::runtime_error("Could not load fonts!");
}

NAMESPACE_END(nanogui)
