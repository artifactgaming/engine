/*
    src/colorpicker.cpp -- push button with a popup to tweak a color value

    This widget was contributed by Christian Schueller.

    NanoGUI was developed by Wenzel Jakob <wenzel.jakob@epfl.ch>.
    The widget drawing code is based on the NanoVG demo application
    by Mikko Mononen.

    All rights reserved. Use of this source code is governed by a
    BSD-style license that can be found in the LICENSE.txt file.
*/

#include <nanogui/colorpicker.h>
#include <nanogui/layout.h>
#include <nanogui/colorwheel.h>

NAMESPACE_BEGIN(nanogui)

ColorPicker::ColorPicker(Widget *parent, const glm::vec4& color) : PopupButton(parent, "") {
    setBackgroundColor(color);
    Popup *popup = this->popup();
    popup->setLayout(new GroupLayout());

    // set the color wheel to the specified color
    mColorWheel = new ColorWheel(popup, color);

    // set the pick button to the specified color
    mPickButton = new Button(popup, "Pick");
    mPickButton->setBackgroundColor(color);
    mPickButton->setTextColor(contrastingColor(color));
    mPickButton->setFixedSize(glm::ivec2(100, 20));

    // set the reset button to the specified color
    mResetButton = new Button(popup, "Reset");
    mResetButton->setBackgroundColor(color);
    mResetButton->setTextColor(contrastingColor(color));
    mResetButton->setFixedSize(glm::ivec2(100, 20));

    PopupButton::addChangeCallback([&](bool) {
        if (this->mPickButton->pushed()) {
            setColor(backgroundColor());
            mFinalCallback(backgroundColor());
        }
    });

    mColorWheel->addChangeCallback([&](const glm::vec4 &value) {
        mPickButton->setBackgroundColor(value);
        mPickButton->setTextColor(contrastingColor(value));
        mCallback(value);
    });

    mPickButton->addPushCallback([this]() {
        if (mPushed) {
            glm::vec4 value = mColorWheel->color();
            setPushed(false);
            setColor(value);
            mFinalCallback(value);
        }
    });

    mResetButton->addPushCallback([this]() {
        glm::vec4 bg = this->mResetButton->backgroundColor();
        glm::vec4 fg = this->mResetButton->textColor();

        mColorWheel->setColor(bg);
        mPickButton->setBackgroundColor(bg);
        mPickButton->setTextColor(fg);

        mCallback(bg);
        mFinalCallback(bg);
    });
}

glm::vec4 ColorPicker::color() const {
    return backgroundColor();
}

void ColorPicker::setColor(const glm::vec4& color) {
    /* Ignore setColor() calls when the user is currently editing */
    if (!mPushed) {
        glm::vec4 fg = contrastingColor(color);
        setBackgroundColor(color);
        setTextColor(fg);
        mColorWheel->setColor(color);

        mPickButton->setBackgroundColor(color);
        mPickButton->setTextColor(fg);

        mResetButton->setBackgroundColor(color);
        mResetButton->setTextColor(fg);
    }
}
void ColorPicker::dispose() {
    this->Widget::dispose();
    mCallback.clear();
    mFinalCallback.clear();
}

NAMESPACE_END(nanogui)
