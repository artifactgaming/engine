/*
    nanogui/imageview.cpp -- Widget used to display images.

    The image view widget was contributed by Stefan Ivanov.

    NanoGUI was developed by Wenzel Jakob <wenzel.jakob@epfl.ch>.
    The widget drawing code is based on the NanoVG demo application
    by Mikko Mononen.

    All rights reserved. Use of this source code is governed by a
    BSD-style license that can be found in the LICENSE.txt file.
*/

#include <nanogui/imageview.h>
#include <nanogui/window.h>
#include <nanogui/screen.h>
#include <nanogui/theme.h>
#include <cmath>

NAMESPACE_BEGIN(nanogui)

namespace {
    std::vector<std::string> tokenize(const std::string &string,
                                      const std::string &delim = "\n",
                                      bool includeEmpty = false) {
        std::string::size_type lastPos = 0, pos = string.find_first_of(delim, lastPos);
        std::vector<std::string> tokens;

        while (lastPos != std::string::npos) {
            std::string substr = string.substr(lastPos, pos - lastPos);
            if (!substr.empty() || includeEmpty)
                tokens.push_back(std::move(substr));
            lastPos = pos;
            if (lastPos != std::string::npos) {
                lastPos += 1;
                pos = string.find_first_of(delim, lastPos);
            }
        }

        return tokens;
    }

    struct DefaultImageViewShader: public GL::InternalShaderProgramSource
    {
        std::string getName() const
        {
            return "Default Image View";
        }

        const std::vector<const char *> getShaderStrings() const
        {
            return {
R"(Default Image View Vertex Shader
VERTEX
#version 330
uniform vec2 scaleFactor;
uniform vec2 position;
in vec2 vertex;
out vec2 uv;
void main() {
    uv = vertex;
    vec2 scaledVertex = (vertex * scaleFactor) + position;
    gl_Position  = vec4(2.0*scaledVertex.x - 1.0,
                        1.0 - 2.0*scaledVertex.y,
                        0.0, 1.0);

})",
R"(Default Image Fragment Shader.
FRAGMENT
#version 330
uniform sampler2D image;
out vec4 color;
in vec2 uv;
void main() {
    color = texture(image, uv);
})"
            };
        }
    } defaultImageViewShader;

}

ImageView::ImageView(Widget* parent, GLuint imageID)
    : Widget(parent), mShader(defaultImageViewShader), vba("Image View Shader", &mShader),
    indices("Image View Indices", GL_STATIC_DRAW), vertices("Image View Vertices", GL_STATIC_DRAW),
    mImageID(imageID), mScale(1.0f), mOffset(glm::vec2(0)),
    mFixedScale(false), mFixedOffset(false), mPixelInfoCallback(nullptr) {
    updateImageParameters();

    indices.addVertex(glm::vec3(0, 1, 2));
    indices.addVertex(glm::vec3(2, 3, 1));

    vertices.addVertex(glm::vec2(0, 0));
    vertices.addVertex(glm::vec2(1, 0));
    vertices.addVertex(glm::vec2(0, 1));
    vertices.addVertex(glm::vec2(1, 1));

    vba.setBuffer("indices", &indices);
    vba.setBuffer("vertex", &vertices);
}

ImageView::~ImageView() {

}

void ImageView::bindImage(GLuint imageId) {
    mImageID = imageId;
    updateImageParameters();
    fit();
}

glm::vec2 ImageView::imageCoordinateAt(const glm::vec2& position) const {
    glm::vec2 imagePosition = position - mOffset;
    return imagePosition / mScale;
}

glm::vec2 ImageView::clampedImageCoordinateAt(const glm::vec2& position) const {

    glm::vec2 imageCoordinate = imageCoordinateAt(position);
    return cwiseMax(imageCoordinate, cwiseMin(glm::vec2(0), imageSizeF()));
}

glm::vec2 ImageView::positionForCoordinate(const glm::vec2& imageCoordinate) const {
    return mScale*imageCoordinate + mOffset;
}

void ImageView::setImageCoordinateAt(const glm::vec2& position, const glm::vec2& imageCoordinate) {
    // Calculate where the new offset must be in order to satisfy the image position equation.
    // Round the floating point values to balance out the floating point to integer conversions.
    mOffset = position - (imageCoordinate * mScale);

    // Clamp offset so that the image remains near the screen.
    mOffset = cwiseMin(cwiseMin(mOffset, sizeF()), -scaledImageSizeF());
}

void ImageView::center() {
    mOffset = (sizeF() - scaledImageSizeF()) * 0.5f;
}

void ImageView::fit() {
    // Calculate the appropriate scaling factor.
    mScale = glm::compMin(cwiseQuotient(sizeF(), imageSizeF()));
    center();
}

void ImageView::setScaleCentered(float scale) {
    auto centerPosition = sizeF() * 0.5f;
    auto p = imageCoordinateAt(centerPosition);
    mScale = scale;
    setImageCoordinateAt(centerPosition, p);
}

void ImageView::moveOffset(const glm::vec2& delta) {
    // Apply the delta to the offset.
    mOffset += delta;

    // Prevent the image from going out of bounds.
    auto scaledSize = scaledImageSizeF();
    if (mOffset.x + scaledSize.x < 0)
        mOffset.x = -scaledSize.x;
    if (mOffset.x > sizeF().x)
        mOffset.x = sizeF().x;
    if (mOffset.y + scaledSize.y < 0)
        mOffset.y = -scaledSize.y;
    if (mOffset.y > sizeF().y)
        mOffset.y = sizeF().y;
}

void ImageView::zoom(int amount, const glm::vec2& focusPosition) {
    auto focusedCoordinate = imageCoordinateAt(focusPosition);
    float scaleFactor = std::pow(mZoomSensitivity, amount);
    mScale = std::max(0.01f, scaleFactor * mScale);
    setImageCoordinateAt(focusPosition, focusedCoordinate);
}

bool ImageView::mouseDragEvent(const glm::ivec2& p, const glm::ivec2& rel, int button, int /*modifiers*/) {
    if ((button & (1 << GLFW_MOUSE_BUTTON_LEFT)) != 0 && !mFixedOffset) {
        setImageCoordinateAt(glm::vec2(p + rel), imageCoordinateAt(glm::vec2(p)));
        return true;
    }
    return false;
}

bool ImageView::gridVisible() const {
    return (mGridThreshold != -1) && (mScale > mGridThreshold);
}

bool ImageView::pixelInfoVisible() const {
    return mPixelInfoCallback && (mPixelInfoThreshold != -1) && (mScale > mPixelInfoThreshold);
}

bool ImageView::helpersVisible() const {
    return gridVisible() || pixelInfoVisible();
}

bool ImageView::scrollEvent(const glm::ivec2& p, const glm::vec2& rel) {
    if (mFixedScale)
        return false;
    float v = rel.y;
    if (std::abs(v) < 1)
        v = std::copysign(1.f, v);
    zoom(v, glm::vec2(p - position()));
    return true;
}

bool ImageView::keyboardEvent(int key, int /*scancode*/, int action, int modifiers) {
    if (action) {
        switch (key) {
        case GLFW_KEY_LEFT:
            if (!mFixedOffset) {
                if (GLFW_MOD_CONTROL & modifiers)
                    moveOffset(glm::vec2(30, 0));
                else
                    moveOffset(glm::vec2(10, 0));
                return true;
            }
            break;
        case GLFW_KEY_RIGHT:
            if (!mFixedOffset) {
                if (GLFW_MOD_CONTROL & modifiers)
                    moveOffset(glm::vec2(-30, 0));
                else
                    moveOffset(glm::vec2(-10, 0));
                return true;
            }
            break;
        case GLFW_KEY_DOWN:
            if (!mFixedOffset) {
                if (GLFW_MOD_CONTROL & modifiers)
                    moveOffset(glm::vec2(0, -30));
                else
                    moveOffset(glm::vec2(0, -10));
                return true;
            }
            break;
        case GLFW_KEY_UP:
            if (!mFixedOffset) {
                if (GLFW_MOD_CONTROL & modifiers)
                    moveOffset(glm::vec2(0, 30));
                else
                    moveOffset(glm::vec2(0, 10));
                return true;
            }
            break;
        }
    }
    return false;
}

bool ImageView::keyboardCharacterEvent(unsigned int codepoint) {
    switch (codepoint) {
    case '-':
        if (!mFixedScale) {
            zoom(-1, sizeF() * 0.5f);
            return true;
        }
        break;
    case '+':
        if (!mFixedScale) {
            zoom(1, sizeF() * 0.5f);
            return true;
        }
        break;
    case 'c':
        if (!mFixedOffset) {
            center();
            return true;
        }
        break;
    case 'f':
        if (!mFixedOffset && !mFixedScale) {
            fit();
            return true;
        }
        break;
    case '1': case '2': case '3': case '4': case '5':
    case '6': case '7': case '8': case '9':
        if (!mFixedScale) {
            setScaleCentered(1 << (codepoint - '1'));
            return true;
        }
        break;
    default:
        return false;
    }
    return false;
}

glm::ivec2 ImageView::preferredSize(NVGcontext* /*ctx*/) const {
    return mImageSize;
}

void ImageView::performLayout(NVGcontext* ctx) {
    Widget::performLayout(ctx);
    center();
}

void ImageView::draw(NVGcontext* ctx) {
    Widget::draw(ctx);
    nvgEndFrame(ctx); // Flush the NanoVG draw stack, not necessary to call nvgBeginFrame afterwards.

    drawImageBorder(ctx);

    // Calculate several variables that need to be send to OpenGL in order for the image to be
    // properly displayed inside the widget.
    const Screen* screen = dynamic_cast<const Screen*>(this->window()->parent());
    assert(screen);
    glm::vec2 screenSize = screen->size();
    glm::vec2 scaleFactor = cwiseQuotient(mScale * imageSizeF(), screenSize);
    glm::vec2 positionInScreen = absolutePosition();
    glm::vec2 positionAfterOffset = positionInScreen + mOffset;
    glm::vec2 imagePosition = cwiseQuotient(positionAfterOffset, screenSize);
    glEnable(GL_SCISSOR_TEST);
    float r = screen->pixelRatio();
    glScissor(positionInScreen.x * r,
              (screenSize.y - positionInScreen.y - size().y) * r,
              size().x * r, size().y * r);

    {
        GL::ShaderProgramScope scope(&mShader);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, mImageID);
        scope.setFloat("image", 0);
        scope.setVec2("scaleFactor", scaleFactor);
        scope.setVec2 ("position", imagePosition);

        scope.drawVertexBuffers(vba);
    }

    glDisable(GL_SCISSOR_TEST);

    if (helpersVisible())
        drawHelpers(ctx);

    drawWidgetBorder(ctx);
}

void ImageView::updateImageParameters() {
    // Query the width of the OpenGL texture.
    glBindTexture(GL_TEXTURE_2D, mImageID);
    GLint w, h;
    glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, &w);
    glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, &h);
    mImageSize = glm::ivec2(w, h);
}

void ImageView::drawWidgetBorder(NVGcontext* ctx) const {
    nvgBeginPath(ctx);
    nvgStrokeWidth(ctx, 1);
    nvgRoundedRect(ctx, mPos.x + 0.5f, mPos.y + 0.5f, mSize.x - 1,
                   mSize.y - 1, 0);
    nvgStrokeColor(ctx, convertToNVGcolor(mTheme->mWindowPopup));
    nvgStroke(ctx);

    nvgBeginPath(ctx);
    nvgRoundedRect(ctx, mPos.x + 0.5f, mPos.y + 0.5f, mSize.x - 1,
                   mSize.y - 1, mTheme->mButtonCornerRadius);
    nvgStrokeColor(ctx, convertToNVGcolor(mTheme->mBorderDark));
    nvgStroke(ctx);
}

void ImageView::drawImageBorder(NVGcontext* ctx) const {
    nvgSave(ctx);
    nvgBeginPath(ctx);
    nvgScissor(ctx, mPos.x, mPos.y, mSize.x, mSize.y);
    nvgStrokeWidth(ctx, 1.0f);
    glm::ivec2 borderPosition = mPos + glm::ivec2(mOffset);
    glm::ivec2 borderSize = scaledImageSizeF();
    nvgRect(ctx, borderPosition.x - 0.5f, borderPosition.y - 0.5f,
            borderSize.x + 1, borderSize.y + 1);
    nvgStrokeColor(ctx, convertToNVGcolor(glm::vec4(1.0f, 1.0f, 1.0f, 1.0f)));
    nvgStroke(ctx);
    nvgResetScissor(ctx);
    nvgRestore(ctx);
}

void ImageView::drawHelpers(NVGcontext* ctx) const {
    // We need to apply mPos after the transformation to account for the position of the widget
    // relative to the parent.
    glm::vec2 upperLeftCorner = positionForCoordinate(glm::vec2(0)) + positionF();
    glm::vec2 lowerRightCorner = positionForCoordinate(imageSizeF()) + positionF();
    if (gridVisible())
        drawPixelGrid(ctx, upperLeftCorner, lowerRightCorner, mScale);
    if (pixelInfoVisible())
        drawPixelInfo(ctx, mScale);
}

void ImageView::drawPixelGrid(NVGcontext* ctx, const glm::vec2& upperLeftCorner,
                              const glm::vec2& lowerRightCorner, float stride) {
    nvgBeginPath(ctx);

    // Draw the vertical grid lines
    float currentX = upperLeftCorner.x;
    while (currentX <= lowerRightCorner.x) {
        nvgMoveTo(ctx, std::round(currentX), std::round(upperLeftCorner.y));
        nvgLineTo(ctx, std::round(currentX), std::round(lowerRightCorner.y));
        currentX += stride;
    }

    // Draw the horizontal grid lines
    float currentY = upperLeftCorner.y;
    while (currentY <= lowerRightCorner.y) {
        nvgMoveTo(ctx, std::round(upperLeftCorner.x), std::round(currentY));
        nvgLineTo(ctx, std::round(lowerRightCorner.x), std::round(currentY));
        currentY += stride;
    }

    nvgStrokeWidth(ctx, 1.0f);
    nvgStrokeColor(ctx, convertToNVGcolor(glm::vec4(1.0f, 1.0f, 1.0f, 0.2f)));
    nvgStroke(ctx);
}

void ImageView::drawPixelInfo(NVGcontext* ctx, float stride) const {
    // Extract the image coordinates at the two corners of the widget.
    glm::ivec2 topLeft = clampedImageCoordinateAt(glm::vec2(0));
    topLeft = glm::ivec2(std::floor(topLeft.x), std::floor(topLeft.y));

    glm::ivec2 bottomRight = clampedImageCoordinateAt(sizeF());
    bottomRight = glm::ivec2(std::ceil(bottomRight.x), std::ceil(bottomRight.y));

    // Extract the positions for where to draw the text.
    glm::vec2 currentCellPosition =
        (positionF() + positionForCoordinate(topLeft));

    float xInitialPosition = currentCellPosition.x;
    int xInitialIndex = topLeft.x;

    // Properly scale the pixel information for the given stride.
    auto fontSize = stride * mFontScaleFactor;
    static constexpr float maxFontSize = 30.0f;
    fontSize = fontSize > maxFontSize ? maxFontSize : fontSize;
    nvgBeginPath(ctx);
    nvgFontSize(ctx, fontSize);
    nvgTextAlign(ctx, NVG_ALIGN_CENTER | NVG_ALIGN_TOP);
    nvgFontFace(ctx, "sans");
    while (topLeft.y != bottomRight.y) {
        while (topLeft.x != bottomRight.x) {
            writePixelInfo(ctx, currentCellPosition, topLeft, stride, fontSize);
            currentCellPosition.x += stride;
            ++topLeft.x;
        }
        currentCellPosition.x = xInitialPosition;
        currentCellPosition.y += stride;
        ++topLeft.y;
        topLeft.x = xInitialIndex;
    }
}

void ImageView::writePixelInfo(NVGcontext* ctx, const glm::vec2& cellPosition,
                               const glm::ivec2& pixel, float stride, float fontSize) const {
    auto pixelData = mPixelInfoCallback(pixel);
    auto pixelDataRows = tokenize(pixelData.first);

    // If no data is provided for this pixel then simply return.
    if (pixelDataRows.empty())
        return;

    nvgFillColor(ctx, convertToNVGcolor(pixelData.second));
    float yOffset = (stride - fontSize * pixelDataRows.size()) / 2;
    for (size_t i = 0; i != pixelDataRows.size(); ++i) {
        nvgText(ctx, cellPosition.x + stride / 2, cellPosition.y + yOffset,
                pixelDataRows[i].data(), nullptr);
        yOffset += fontSize;
    }
}

NAMESPACE_END(nanogui)
