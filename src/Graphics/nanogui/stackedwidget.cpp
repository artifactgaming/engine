/*
    nanogui/stackedwidget.cpp -- Widget used to stack widgets on top
    of each other. Only the active widget is visible.

    The stacked widget was contributed by Stefan Ivanov.

    NanoGUI was developed by Wenzel Jakob <wenzel.jakob@epfl.ch>.
    The widget drawing code is based on the NanoVG demo application
    by Mikko Mononen.

    All rights reserved. Use of this source code is governed by a
    BSD-style license that can be found in the LICENSE.txt file.
*/

#include <nanogui/stackedwidget.h>

NAMESPACE_BEGIN(nanogui)

StackedWidget::StackedWidget(nanogui::Widget *parent)
    : Widget(parent) { }

void StackedWidget::setSelectedIndex(int index) {
    assert(index < childCount());
    if (mSelectedIndex >= 0)
        mChildren[mSelectedIndex]->setVisible(false);
    mSelectedIndex = index;
    mChildren[mSelectedIndex]->setVisible(true);
}

int StackedWidget::selectedIndex() const {
    return mSelectedIndex;
}

void StackedWidget::performLayout(NVGcontext *ctx) {
    for (auto child : mChildren) {
        child->setPosition(glm::ivec2(0));
        child->setSize(mSize);
        child->performLayout(ctx);
    }
}

glm::ivec2 StackedWidget::preferredSize(NVGcontext *ctx) const {
    glm::ivec2 size = glm::ivec2(0);
    for (auto child : mChildren)
    {
        size = cwiseMax(size, child->preferredSize(ctx));
    }
    return size;
}

void StackedWidget::addChild(int index, Widget *widget) {
    if (mSelectedIndex >= 0)
        mChildren[mSelectedIndex]->setVisible(false);
    Widget::addChild(index, widget);
    widget->setVisible(true);
    setSelectedIndex(index);
}

NAMESPACE_END(nanogui)
