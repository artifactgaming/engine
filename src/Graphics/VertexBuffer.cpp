/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include <glm/glm.hpp>

#include "VertexBuffer.hpp"
#include <iostream>

namespace GL {
    template<class D>
    VertexBufferIMP<D>::VertexBufferIMP(std::string name, GLuint bufferType) : VertexBufferIMP(nullptr, name, bufferType) {

    }

    template<class D>
    VertexBufferIMP<D>::VertexBufferIMP(RM::ResourceGroup * group, std::string name, GLuint bufferType):
        VertexBuffer(name, "VertexBuffer", group),
        bufferType(bufferType)
    {
        switch (bufferType) {
        case GL_STATIC_DRAW:
        case GL_DYNAMIC_DRAW:
        case GL_STREAM_DRAW:
            break; // This is fine.
        default:
            throw std::runtime_error("Invalid VertexBuffer type.");
        }

        numVerticies = 0;
        glGenBuffers(1, &id);

        stepsCompleted = 1;
        stepsToLoad = 1;

    }

    template<class D>
    VertexBufferIMP<D>::~VertexBufferIMP() {
        glDeleteBuffers(1, &id);
    }

    template<class D>
    void VertexBufferIMP<D>::addVertex(D vertex) {
        verticies.push_back(vertex);
        numVerticies = verticies.size();
    }

    template<class D>
    void VertexBufferIMP<D>::clear() {
        verticies.clear();
    }

    template<class D>
    void VertexBufferIMP<D>::sendToGPU() {
        numVerticies = verticies.size();

        size_t bufferSize = sizeof(D) * numVerticies;

        glBindBuffer(GL_ARRAY_BUFFER, id);
        glBufferData(GL_ARRAY_BUFFER, bufferSize, verticies.data(), bufferType);

        verticies.clear();
        verticies.shrink_to_fit();
    }

    template<class D>
    unsigned int VertexBufferIMP<D>::getNumVerticies() {
        return numVerticies;
    }

    template<class D>
    void VertexBufferIMP<D>::bindToAttribute(GLuint attribute) {
        if (verticies.empty()) {
            const unsigned size = sizeof(D) / sizeof(float);

            glBindBuffer(GL_ARRAY_BUFFER, id);
            glVertexAttribPointer(
                attribute, // Attribute of the shader to be loaded as.
                size, // Size of a single unit.
                GL_FLOAT, // Type.
                GL_FALSE, // Do we want this to be normalized for us?
                size * sizeof(float), // Distance to next index.
                nullptr // Array buffer offset.
            );
        } else {
            throw std::runtime_error("Vertex Buffer was never sent to the GPU.");
        }
    }

    template class VertexBufferIMP<float>;
    template class VertexBufferIMP<glm::vec2>;
    template class VertexBufferIMP<glm::vec3>;
    template class VertexBufferIMP<glm::vec4>;

    void computeTangentsAndBitangents(std::vector<glm::vec3> & tangents, std::vector<glm::vec3> & bitangents, std::vector<glm::vec3> & verticies, std::vector<glm::vec3> & texCoords) {
        size_t numVerticies = verticies.size();

        //Here we compute tangents and bitangents.
        size_t numTriangles = numVerticies / 3;

        //The number of triangles is multiplied by 3 so that we have a float for each axis.
        tangents.resize(numVerticies);
        bitangents.resize(numVerticies);

        for (unsigned int i = 0; i < numTriangles; i++) {
            unsigned int index = i * 3;

            // Shortcuts for verticies
            glm::vec3 v0 = verticies[index];
            glm::vec3 v1 = verticies[index + 1];
            glm::vec3 v2 = verticies[index + 2];

            // Shortcuts for UVs
            glm::vec2 uv0 = texCoords[index].xy();
            glm::vec2 uv1 = texCoords[index + 1].xy();
            glm::vec2 uv2 = texCoords[index + 2].xy();

            // Edges of the triangle : position delta
            glm::vec3 deltaPos1 = v1-v0;
            glm::vec3 deltaPos2 = v2-v0;

            // UV delta
            glm::vec2 deltaUV1 = uv1-uv0;
            glm::vec2 deltaUV2 = uv2-uv0;

            float r = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV1.y * deltaUV2.x);
            glm::vec3 tangent = r * (deltaPos1 * deltaUV2.y   - deltaPos2 * deltaUV1.y);
            glm::vec3 bitangent = r * (deltaPos2 * deltaUV1.x   - deltaPos1 * deltaUV2.x);

            tangent = glm::normalize(tangent);
            bitangent = glm::normalize(bitangent);

            tangents[index    ] = tangent;
            tangents[index + 1] = tangent;
            tangents[index + 2] = tangent;

            bitangents[index    ] = bitangent;
            bitangents[index + 1] = bitangent;
            bitangents[index + 2] = bitangent;
        }
    }
}
