/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "RenderTarget.hpp"

using namespace nanogui;

namespace GL {

    RenderTarget::RenderTarget(Widget * parent, const std::function<void(RenderTarget * target)> & renderGeometry) :
        GLCanvas(parent) {
        this->renderGeometry = renderGeometry;
    }

    RenderTarget::~RenderTarget() {
    }

    void RenderTarget::drawGL() {
        renderGeometry(this);
    }

    void RenderTarget::dispose() {
        renderGeometry = [](RenderTarget*){};
    }
}
