/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "DeferredRenderTarget.hpp"

namespace GL {
    DeferredRenderTarget::DeferredRenderTarget(Widget * parent, std::function<void(RenderTarget * target)> & renderGeometry, std::function<void(RenderTarget * target)> & setupSecondStage):
        DeferredRenderTarget(parent, renderGeometry)
    {
        this->setupSecondStage = setupSecondStage;
    }

    DeferredRenderTarget::DeferredRenderTarget(Widget * parent, std::function<void(RenderTarget * target)> & renderGeometry):
        RenderTarget(parent, renderGeometry)
    {

    }

    DeferredRenderTarget::~DeferredRenderTarget()
    {
        //dtor
    }

    void DeferredRenderTarget::drawGL() {
        renderGeometry(this);
        if (setupSecondStage) {
            setupSecondStage(this);
        }
        // TODO do the second stage rendering.
    }
}
