/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "Shader.hpp"

#include <boost/filesystem/fstream.hpp>
#include "globals.hpp"

namespace GL {

    Shader::Shader(RM::Path path) :
        path(path)
    {
        manager = getShaderManager();
        internal = false;
        manager->addShader(path.getAbsolute().string(), this);

        std::cout << "Loading shader: " << path.getAbsolute().string() << std::endl;

        if (path.exists()) {
            std::string ext = path.getExtension();

            if (ext == ".vert") {
                type = GL_VERTEX_SHADER;
                std::cout << "Shader type: VERTEX" << std::endl;
            } else if (ext == ".frag") {
                type = GL_FRAGMENT_SHADER;
                std::cout << "Shader type: FRAGMENT" << std::endl;
            } else if (ext == ".geom") {
                type = GL_GEOMETRY_SHADER;
                std::cout << "Shader type: GEOMETRY" << std::endl;
            } else if (ext == ".comp") {
                if (HardwareAccelleration::supportComputeShaders()) {
                    type = GL_COMPUTE_SHADER;
                    std::cout << "Shader type: COMPUTE" << std::endl;
                } else {
                    throw std::runtime_error("Graphics driver does not support compute shaders: " + path.string());
                }
            } else {
                throw std::runtime_error("Failed to determine shader type from extension: " + path.string());
            }

            boost::filesystem::ifstream file(path.getBoostPath(), std::ios::in);
            if (file.fail()) {
                throw std::runtime_error("Failed to open shader file: " + path.string());
            }

            std::string line;

            // First we just load the first line. It should be a "#version 330.
            getline(file, line);
            if (line != "#version 330") {
                throw std::runtime_error("Grid Locked only supports GLSL shader version 330.");
            }

            while (getline(file, line)) {
                code += line + '\n';
            }

            file.close();

            compileShader(path.getAbsolute().string());
        } else {
            throw std::runtime_error("Could not find shader file: " + path.string());
        }
    }

    Shader::Shader(const char * internalCode) :
        internalCode(internalCode)
    {
        manager = getShaderManager();
        internal = true;
        manager->addInternalShader(internalCode, this);

        std::istringstream file(internalCode);

        std::string name;
        std::getline(file, name); // First line holds the shader name.

        std::cout << "Loading internal shader: " << name << std::endl;


        std::string line;
        std::getline(file, line); // Second line holds the shader type.
        if (line == "VERTEX") {
            type = GL_VERTEX_SHADER;
            std::cout << "Shader type: VERTEX" << std::endl;
        } else if (line == "FRAGMENT") {
            type = GL_FRAGMENT_SHADER;
            std::cout << "Shader type: FRAGMENT" << std::endl;
        } else if (line == "GEOMETRY") {
            type = GL_GEOMETRY_SHADER;
            std::cout << "Shader type: GEOMETRY" << std::endl;
        } else if (line == "COMPUTE") {
            if (HardwareAccelleration::supportComputeShaders()) {
                type = GL_COMPUTE_SHADER;
                std::cout << "Shader type: COMPUTE" << std::endl;
            } else {
                throw std::runtime_error("Graphics driver does not support compute shaders.");
            }
        } else {
            throw std::runtime_error("Unsupported internal shader type: " + line);
        }

        // First we just load the first  of actual code. It should be a "#version 330.
        getline(file, line);
        if (line != "#version 330") {
            throw std::runtime_error("Grid Locked only supports GLSL shader version 330.");
        }

        while (getline(file, line)) {
            code += line + '\n';
        }

        compileShader(name);
    }

    Shader::~Shader() {
        if (!internal) {
            manager->removeShader(path.getAbsolute().string());
            std::cout << "Deleted shader: " << name << std::endl;
        } else {
            manager->removeInternalShader(internalCode);
            std::cout << "Deleted internal shader: " << name << std::endl;
        }

        glDeleteShader(shader);
    }

    void Shader::compileShader(std::string name) {
        this->name = name;
        std::string code = "#version 330\n";

        code += this->code;

        // It's ugly, but it's the only way I can figure out to make this thing
        // cast the pointer right.
        GLchar const * codePointer = code.c_str();

        shader = glCreateShader(type);
        glShaderSource(shader, 1, &codePointer, NULL);
        glCompileShader(shader);

        GLint result = GL_FALSE;
        GLint logLength;

        // This will leave result as false in the case of a failure.
        glGetShaderiv(shader, GL_COMPILE_STATUS, &result);
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLength);

        if (result != GL_TRUE) { // We failed to compile. Throw an error.
            std::string log = "Error compiling shader script \"";
            log += name;
            log += "\": \n";

            size_t logStart = log.length();

            log.insert(logStart, logLength, ' ');
            glGetShaderInfoLog(shader, logLength, NULL, &log[logStart]);

            throw std::runtime_error(log);
        } else if (logLength > 0){ // Everything compiled fine, but we got some kind of message from the driver.
                                   // It could be a simple success message, but it could also be a warning.
                                   // It's a good idea to log it.
            std::string log = "A message was returned from the shader script compiler \"";
            log += name;
            log += "\": ";

            size_t logStart = log.length();

            log.insert(logStart, logLength, ' ');
            glGetShaderInfoLog(shader, logLength, NULL, &log[logStart]);

            std::cout << log << std::endl;
        }

        std::cout << "Shader loaded." << std::endl;
    }

    GLuint Shader::getShader() {
        return shader;
    }

    GLuint Shader::getType() {
        return type;
    }
}
