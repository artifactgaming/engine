/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "TextureArray.hpp"
#include "LocalFile.hpp"

#include <nanogui/object.h>

namespace GL {
    static unsigned int closestPower(unsigned int v) {
        unsigned int r = 0; // r will be lg(v)

        while (v >>= 1) {// unroll for more speed...
            r++;
        }

        return 1 << r;
    }

    TextureArray::TextureArray(unsigned int width, unsigned int height) {
        this->width = closestPower(width);
        if (this->width != width) {
            throw std::runtime_error("Width and height of textures must be an exponent of two.");
        }

        this->height = closestPower(height);
        if (this->height != height) {
            throw std::runtime_error("Width and height of textures must be an exponent of two.");
        }

        textureCount = 0;
        glGenTextures(1, &textureID);
    }

    TextureArray::~TextureArray() {
       glDeleteTextures(1, &textureID);
    }

    void TextureArray::addFile(boost::filesystem::path path, std::string name) {
        sources.push_back(new LocalFileTextureSource(path, name));
    }

    void TextureArray::pushToGPU() {
        indexes.clear();//If we're re-uploading, we need to clear out the old stuff.

        size_t numSources = sources.size();

        for (size_t s = 0; s < numSources; s++) {
            TextureSource * source = sources[s];
            // textureCount += source->getDepth();
        }

        glBindTexture(GL_TEXTURE_2D_ARRAY, textureID);

        glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_R, GL_REPEAT);

        glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_RGBA8, this->width, this->height, textureCount, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);

        size_t textureIndex = 0;

        for (size_t i = 0; i < sources.size(); i++) {

            TextureSource * source = sources[i];

            if (source->getWidth() != this->width || source->getHeight() != this->height) {
                std::string message = "Error loading Texture: ";
                message += source->getName();
                message += "\nDimensions did not match texture array size of ";
                message += this->width;
                message += " x ";
                message += this->height;
                message += "\n";
                throw std::runtime_error(message);
            }

            //source->sendToGPU(textureIndex);

            //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

            indexes[source->getName()] = textureIndex;
            //source->setIndex(textureIndex);

            //textureIndex += source->getDepth();
        }
    }

    float TextureArray::getTextureIndex(std::string name) {
        try {
            return indexes.at(name);
        } catch (std::out_of_range e) {
            return -1;//We failed to find it.
        }
    }

    unsigned int TextureArray::getTextureCount() {
        return textureCount;
    }
}
