/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "Sampler2D.hpp"

#include "AbsolutePanic.hpp"

namespace GL {
    Sampler2D::Sampler2D(TextureSource::TextureSource * source, std::string name): Sampler2D(nullptr, source, name) {

    }

    Sampler2D::Sampler2D(RM::ResourceGroup * group, TextureSource::TextureSource * source, std::string name):
        Sampler(name, "Sampler2D", group),
        source(source) {
        glGenTextures(1, &ID);

        // FIXME currently you can only have one sampler with one textures source like this.
        source->setCompletionCallback([this, source]() {
            glBindTexture(GL_TEXTURE_2D, this->ID);

            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, source->getWidth(), source->getHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, source->getData());
            glGenerateMipmap(GL_TEXTURE_2D);
        });
    }

    Sampler2D::~Sampler2D() {
        glDeleteTextures(1, &ID);
    }

    void Sampler2D::bind(GLint binding) {
        glActiveTexture(GL_TEXTURE0 + binding);
        glBindTexture(GL_TEXTURE_2D, ID);
    }
}
