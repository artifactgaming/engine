/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "LocalFile.hpp"

#include "lodepng.h"
#include <glad.h>

namespace GL {
    namespace TextureSource {
        LocalFile::LocalFile(RM::Path path, std::string name): LocalFile(nullptr, path, name) {

        }

        LocalFile::LocalFile(RM::ResourceGroup * group, RM::Path path, std::string name): TextureSource(name, "LocalFileTextureSource", group) {
            this->path = path;

            width = -1;
            height = -1;

            this->setResourceStepsToLoad(1);
            startLoad();
        }

        LocalFile::~LocalFile() {

        }

        void LocalFile::reload() {
            if (colors.empty()) {
                startLoad();
            } else {
                std::cout << "Warning: Requested reload of Local File texture source " << resourceName() << " which is already loaded." << std::endl;

            }
        }

        void LocalFile::clear() {
            if (!colors.empty()) {
                colors.clear();
            } else {
                std::cout << "Warning: Requested clearing Local File texture source " << resourceName() << " which is already cleared." << std::endl;
            }
        }

        bool LocalFile::isLoaded() {
            return !colors.empty();
        }

        void LocalFile::async_load() {
            unsigned error = lodepng::decode(colors, width, height, path.string());

            if (error) {
                std::string message = "Error while loading texture from file \"" + path.string() + "\":\n";
                message += lodepng_error_text(error);
                throw std::runtime_error(message);
            }

            incrementResourceProgress(1);
        }

        void LocalFile::sync_load() {
            std::cout << "Loaded texture to memory: " << resourceName() << std::endl;
        }

        unsigned int LocalFile::getWidth() {
            return width;
        }

        unsigned int LocalFile::getHeight() {
            return height;
        }

        unsigned int * LocalFile::getData() {
            return reinterpret_cast<unsigned int*>(colors.data());
        }

        unsigned int LocalFile::getResourceStepsToLoad() {
            return 1;
        }

        unsigned int LocalFile::getResourceStepsCompleted() {
            return (colors.size() == getHeight() * getWidth());
        }

        void LocalFile::openPreview(nanogui::Screen * parent) {

        }
    }
}
