/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "View.hpp"

// Why'd I do this again?
namespace GL {
    View::View(GL::ShaderProgram & program):
        viewMatrix(1),
        projectionMatrix(1),

        scale(0),
        rotation(glm::vec3(1, 0, 0)),
        translation(0),
        skew(0),
        perspective(0),
        program(&program)
    {

    }

    View::~View()
    {

    }

    bool View::inViewport(glm::vec3 & point, float radius)
    {
        glm::vec3 closest_point = (center_vector * glm::dot(point, center_vector.xyz())).xyz();
        return (radius <= glm::distance(closest_point, point)) // Within the cone?
            && (radius <= glm::distance(-translation, closest_point)); // In front of the camera?
    }
}
