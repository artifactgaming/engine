/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include <iostream>

#include "VertexBufferArray.hpp"
#include "AbsolutePanic.hpp"

namespace GL {
    VertexBufferArray::VertexBufferArray(std::string name, GL::ShaderProgram * program) : VertexBufferArray(nullptr, name, program) {

    }

    VertexBufferArray::VertexBufferArray(RM::ResourceGroup * group, std::string name, GL::ShaderProgram * program) : RM::BasicResource(name, "VertexBufferArray", group) {
        glGenVertexArrays(1, &ID);
        this->program = program;
    }

    VertexBufferArray::~VertexBufferArray() {
        glDeleteVertexArrays(1, &ID);
    }

    void VertexBufferArray::setBuffer(std::string name, GL::VertexBuffer * buffer) {

        BufferType type;

        if (dynamic_cast<VertexBuffer1D*>(buffer)) {
            type = BufferType::Scalar;
        } else if (dynamic_cast<VertexBuffer2D*>(buffer)) {
            type = BufferType::Vec2;
        } else if (dynamic_cast<VertexBuffer3D*>(buffer)) {
            type = BufferType::Vec3;
        } else if (dynamic_cast<VertexBuffer4D*>(buffer)) {
            type = BufferType::Vec4;
        } else {
            ABSOLUTE_PANIC();
        }

        std::map<std::string, BufferBinding>::iterator result = bufferMap.find(name);
        if (result != bufferMap.end()) {
            BufferBinding * bufferBinding = &result->second;
            bufferBinding->buffer = buffer;
            buffer->bindToAttribute(bufferBinding->attribute);
            bufferBinding->type = type;
        } else {
            GLuint attribute = program->getAttributeLocation(name);

            bufferMap.emplace(std::piecewise_construct,
                              std::forward_as_tuple(name),
                              std::forward_as_tuple(buffer, attribute, type));

            glBindVertexArray(ID);
            glEnableVertexAttribArray(attribute);
            buffer->bindToAttribute(attribute);
        }
    }

    size_t VertexBufferArray::shortestBufferLength() {
        size_t shortest = ~0; // Max value possible.
        size_t changes = 0;

        for (auto index: bufferMap) {
            VertexBuffer * buffer = index.second.buffer;
            if (buffer->getNumVerticies() < shortest) {
                shortest = buffer->getNumVerticies();
                changes++;
            }
        }

        if (changes == 1) {
            return shortest;
        } else {
            std::cout << "Warning: Not all buffers in vertex buffer array " << this->resourceName() << " are the same length." << std::endl;
            return shortest;
        }
    }

    void VertexBufferArray::bind() {
        glBindVertexArray(ID);
    }
}
