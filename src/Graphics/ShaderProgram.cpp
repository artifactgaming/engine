/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include "ShaderProgram.hpp"
#include "globals.hpp"
#include <glm/gtc/type_ptr.hpp>

namespace GL {
    ShaderProgram::ShaderProgram(std::string name, const ShaderFileList & shaderPaths):
        ShaderProgram(nullptr, name, shaderPaths)
    {

    }

    ShaderProgram::ShaderProgram(RM::ResourceGroup * group, std::string name, const ShaderFileList & shaderPaths):
        BasicResource(name, "Shader Program", group),
        polygonMode(GL_TRIANGLES)
    {

        if (shaderPaths.size() == 0) {
            throw std::runtime_error("No shader source files provided.");
        }

        manager = getShaderManager();
        programID = glCreateProgram();
        lastTextureBinding = 0;

        for (RM::Path path : shaderPaths) {
            RM::ref<Shader> shader = manager->getShader(path);
            shaders.push_back(shader);
            glAttachShader(programID, shader->getShader());
        }

        glLinkProgram(programID);

        // Check the program
        GLint result = GL_FALSE;
        int logLength;

        glGetProgramiv(programID, GL_LINK_STATUS, &result);
        glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &logLength);
        if (result != GL_TRUE){
            std::string log = "Error while linking shader program.\n";
            log += "\nError Message:\n";

            int logStart = log.length();
            log.insert(logStart, logLength, ' ');

            glGetProgramInfoLog(programID, logLength, NULL, &log[logStart]);

            throw std::runtime_error(log);
        }

        std::cout << "Shader Program Linked." << std::endl;
    }

    ShaderProgram::ShaderProgram(const InternalShaderProgramSource & internalShaders): ShaderProgram(nullptr, internalShaders)
    {

    }

    ShaderProgram::ShaderProgram(RM::ResourceGroup * group, const InternalShaderProgramSource & internalShaders):
        BasicResource(internalShaders.getName(), "Internal Shader Program", group),
        polygonMode(GL_TRIANGLES)
    {
        manager = getShaderManager();
        programID = glCreateProgram();
        lastTextureBinding = 0;

        for (const char * shaderString : internalShaders.getShaderStrings()) {
            RM::ref<Shader> shader = manager->getInternalShader(shaderString);
            shaders.push_back(shader);
            glAttachShader(programID, shader->getShader());
        }

        glLinkProgram(programID);

        // Check the program
        GLint result = GL_FALSE;
        int logLength;

        glGetProgramiv(programID, GL_LINK_STATUS, &result);
        glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &logLength);
        if (result != GL_TRUE){
            std::string log = "Error while linking shader program.\n";
            log += "\nError Message:\n";

            int logStart = log.length();
            log.insert(logStart, logLength, ' ');

            glGetProgramInfoLog(programID, logLength, NULL, &log[logStart]);

            throw std::runtime_error(log);
        }

        std::cout << "Shader Program Linked." << std::endl;
    }

    ShaderProgram::~ShaderProgram()
    {
        glDeleteProgram(programID);
    }

    GLint ShaderProgram::getUniformLocation(std::string name) {
        GLint location;

        try {
            location = uniformLocations.at(name);
        } catch (std::out_of_range & e)  {
            location = glGetUniformLocation(programID, name.c_str());
            uniformLocations[name] = location;//We store these things so we don't have to get them from the graphics driver every time.

            if (location == -1) {
                std::cout << "Warning: Uniform \"" << name << "\" ether does not exist or was optimized out by the shader compiler.\n";
            }
        }

        return location;
    }

    GLint ShaderProgram::getAttributeLocation(std::string name) {
        GLint location;
        try {
            location = attributeLocations.at(name);
        } catch (std::out_of_range & e)  {
            location = glGetAttribLocation(programID, name.c_str());
            attributeLocations[name] = location;//We store these things so we don't have to get them from the graphics driver every time.

            if (location != -1) {

            } else {
                std::cout << "Warning: Attribute \"" << name << "\" ether does not exist or was optimized out by the shader compiler.\n";
            }
        }

        return location;
    }

////////////////////////////////////////////////////////////////////////////////

    ShaderProgramScope::ShaderProgramScope(RenderTarget * target, ShaderProgram * program):
        programStack(&target->shaderStack),
        target(target)
    {
        this->program = program;
        GLuint programID = program->programID;

        programStack->push(programID);
        glUseProgram(programID);
    }

    ShaderProgramScope::ShaderProgramScope(ShaderProgram * program):
        programStack(nullptr)
    {
        this->program = program;
        GLuint programID = program->programID;
        glUseProgram(programID);
    }

    ShaderProgramScope::~ShaderProgramScope() {
        if (programStack)
        {
            programStack->pop();

            if (!programStack->empty()) {
                glUseProgram(programStack->top());
            } else {
                glUseProgram(0);
            }
        }
        else
        {
            glUseProgram(0);
        }
    }

    void ShaderProgramScope::drawVertexBuffers(GL::VertexBufferArray & array) {
        size_t numVerticies = array.shortestBufferLength();
        if (numVerticies) {
            array.bind();
            glDrawArrays(program->polygonMode, 0, numVerticies);
        }
    }

    void ShaderProgramScope::setFloat(std::string name, float value) {
        GLint location = program->getUniformLocation(name);
        glUniform1f(location, value);
    }

    void ShaderProgramScope::setVec2(std::string name, glm::vec2 value) {
        GLint location = program->getUniformLocation(name);
        glUniform2f(location, value.x, value.y);
    }

    void ShaderProgramScope::setVec3(std::string name, glm::vec3 value) {
        GLint location = program->getUniformLocation(name);
        glUniform3f(location, value.x, value.y, value.z);
    }

    void ShaderProgramScope::setVec4(std::string name, glm::vec4 value) {
        GLint location = program->getUniformLocation(name);
        glUniform4f(location, value.x, value.y, value.z, value.w);
    }

    void ShaderProgramScope::setMatrix2x2(std::string name, glm::mat2x2 value) {
        GLint location = program->getUniformLocation(name);
        glUniformMatrix2fv(location, 1, GL_FALSE, glm::value_ptr(value));
    }

    void ShaderProgramScope::setMatrix3x3(std::string name, glm::mat3x3 value) {
        GLint location = program->getUniformLocation(name);
        glUniformMatrix3fv(location, 1, GL_FALSE, glm::value_ptr(value));
    }

    void ShaderProgramScope::setMatrix4x4(std::string name, glm::mat4x4 value) {
        GLint location = program->getUniformLocation(name);
        glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(value));
    }

    void ShaderProgramScope::setSampler(std::string name, GL::Sampler * sampler) {

        std::pair<GLint, RM::ref<GL::Sampler>> * binding;

        try {
            binding = &program->textureBindings.at(name);
        } catch (std::out_of_range & e)  {
            GLint location = program->getUniformLocation(name);

            binding = &(
                //We store these things so we don't have to get them from the graphics driver every time.
                program->textureBindings[name] = std::pair<GLint, RM::ref<GL::Sampler>>(program->lastTextureBinding++, sampler)
            );
            glUniform1i(location, binding->second);
        }

        if (sampler) {
            sampler->bind(binding->second);
        } else {
            glActiveTexture(GL_TEXTURE0 + binding->second);
            glBindTexture(GL_TEXTURE_2D, 0);
        }
    }

    std::string getGLTypename(GLint type) {
        switch (type) {
            case GL_FLOAT:
                return "Float";
            case GL_FLOAT_VEC2:
                return "Float Vec2";
            case GL_FLOAT_VEC3:
                return "Float Vec3";
            case GL_FLOAT_VEC4:
                return "Float Vec4";
            case GL_FLOAT_MAT2:
                return "Float Mat2x2";
            case GL_FLOAT_MAT3:
                return "Float Mat3x3";
            case GL_FLOAT_MAT4:
                return "Float Mat4x4";
            case GL_FLOAT_MAT2x3:
                return "Float Mat2x3";
            case GL_FLOAT_MAT2x4:
                return "Float Mat2x4";
            case GL_FLOAT_MAT3x2:
                return "Float Mat3x2";
            case GL_FLOAT_MAT3x4:
                return "Float Mat3x4";
            case GL_FLOAT_MAT4x2:
                return "Float Mat4x2";
            case GL_FLOAT_MAT4x3:
                return "Float Mat4x3";
            case GL_INT:
                return "Integer";
            case GL_INT_VEC2:
                return "Integer Vec2";
            case GL_INT_VEC3:
                return "Integer Vec3";
            case GL_INT_VEC4:
                return "Integer Vec4";
            case GL_UNSIGNED_INT:
                return "Unsigned Integer";
            case GL_UNSIGNED_INT_VEC2:
                return "Unsigned Integer Vec2";
            case GL_UNSIGNED_INT_VEC3:
                return "Unsigned Integer Vec3";
            case GL_UNSIGNED_INT_VEC4:
                return "Unsigned Integer Vec4";
            case GL_DOUBLE:
                return "Double";
            default:
                return "UNKNOWN";
        }
    }

    void ShaderProgramScope::printDebug() {
        GLint count;

        GLint size;
        GLenum type;

        const GLsizei bufSize = 256;
        GLchar name[bufSize];
        GLsizei length;

        GLuint programID = program->programID;

        glGetProgramiv(programID, GL_ACTIVE_ATTRIBUTES, &count);
        std::cout << "\nActive Attributes: " << count << std::endl;

        for (GLint i = 0; i < count; i++) {
            glGetActiveAttrib(programID, (GLuint)i, bufSize, &length, &size, &type, name);

            std::cout << "Attribute #: " << i << " Type: " << getGLTypename(type) << " Name: " << name << std::endl;
        }

        glGetProgramiv(programID, GL_ACTIVE_UNIFORMS, &count);

        std::cout << "\nActive Uniforms: " << count << std::endl;

        for (GLint i = 0; i < count; i++) {
            glGetActiveUniform(programID, (GLuint)i, bufSize, &length, &size, &type, name);

            std::cout << "Attribute #: " << i << " Type: " << getGLTypename(type) << " Name: " << name << std::endl;
        }
    }
}
