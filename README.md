Grid Locked uses the Code::Blocks build system. You must mingw setup to build the engine for Windows, and GCC installed to build for Linux.
I know cygwin includes mingw, but I do not know if it includes GCC. Automation scripts are written in Python3 in hopes to make this project
portable.

Note that all binaries are placed in folders under bin/. Each folder also includes binaries that the engine depends on. The packaging script
that creates zip and tar files with the engine grab those dependencies along with the actual executable during packaging time. Note that
the folder enviorment/ is used to run the engine when you run it from within codeblocks. The enviorment/ folder is also included as the root
directory in the zip and tar files.

If building on Linux, you will need to install the following packages to resolve dependencies:
libgl1-mesa-dev
libxxf86vm-dev
libxrandr-dev
libxcursor-dev
libxinerama-dev
