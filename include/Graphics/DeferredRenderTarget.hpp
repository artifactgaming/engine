/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#ifndef DEFERREDRENDERTARGET_H
#define DEFERREDRENDERTARGET_H

#include "RenderTarget.hpp"

namespace GL {
    class DeferredRenderTarget: public RenderTarget
    {
        public:
            DeferredRenderTarget(Widget * parent, std::function<void(RenderTarget * target)> & renderGeometry, std::function<void(RenderTarget * target)> & setupSecondStage);
            DeferredRenderTarget(Widget * parent, std::function<void(RenderTarget * target)> & renderGeometry);
            virtual ~DeferredRenderTarget();

            void drawGL();

        protected:
            std::function<void(RenderTarget * target)> setupSecondStage;
        private:
    };
}

#endif // DEFERREDRENDERTARGET_H
