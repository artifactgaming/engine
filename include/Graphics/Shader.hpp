/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#ifndef SHADER_HPP_INCLUDED
#define SHADER_HPP_INCLUDED

#include <glad.h>
#include "ShaderManager.hpp"
#include "Path.hpp"
#include "GCObject.hpp"

namespace GL {
    class Shader : public RM::GCObject {
    public:
        Shader(RM::Path path);
        Shader(const char * internalCode);
        virtual ~Shader();

        void compileShader(std::string name);

        GLuint getShader();
        GLuint getType();
    private:
        std::string code;
        std::string name;

        RM::Path path;
        const char * internalCode;

        ShaderManager * manager;

        GLuint type;
        GLuint shader;
        bool internal;

    };
}

#endif // SHADER_HPP_INCLUDED
