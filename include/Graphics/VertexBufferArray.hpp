/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#ifndef VERTEXBUFFERARRAY_H
#define VERTEXBUFFERARRAY_H

#include <glad.h>
#include "BasicResource.hpp"
#include "VertexBuffer.hpp"
#include "ShaderProgram.hpp"

namespace GL {
    class ShaderProgram;
    class ShaderProgramScope;
    class VertexBuffer;

    class VertexBufferArray: public RM::BasicResource
    {
        public:
            VertexBufferArray(std::string name, GL::ShaderProgram * program);
            VertexBufferArray(RM::ResourceGroup * group, std::string name, GL::ShaderProgram * program);
            virtual ~VertexBufferArray();

            void setBuffer(std::string name, GL::VertexBuffer * buffer);
            size_t shortestBufferLength();

            GL::ShaderProgram * getProgram() { return program; }

        protected:
            GLuint ID;
            GL::ShaderProgram * program;

            enum BufferType {
                Scalar,
                Vec2,
                Vec3,
                Vec4
            };

            struct BufferBinding {
                BufferBinding(RM::ref<GL::VertexBuffer> buffer, GLuint attribute, BufferType type):
                    buffer(buffer),
                    attribute(attribute),
                    type(type) {

                }

                RM::ref<GL::VertexBuffer> buffer;
                GLuint attribute;
                BufferType type;
            };

            std::map<std::string, BufferBinding> bufferMap;

            friend ShaderProgramScope;
            void bind();
        private:
    };
}
#endif // VERTEXBUFFERARRAY_H
