/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include <glm/glm.hpp>

namespace GL {
    template<class D>
    class VertexBufferIMP;

    typedef VertexBufferIMP<float> VertexBuffer1D;
    typedef VertexBufferIMP<glm::vec2> VertexBuffer2D;
    typedef VertexBufferIMP<glm::vec3> VertexBuffer3D;
    typedef VertexBufferIMP<glm::vec4> VertexBuffer4D;
}

#ifndef VERTEXBUFFER_H
#define VERTEXBUFFER_H

#include <string>
#include <vector>
#include <glad.h>
#include <exception>

#include "BasicResource.hpp"
#include "VertexBufferArray.hpp"

namespace GL {

    class VertexBufferArray;

    class VertexBuffer: public RM::BasicResource {
        public:
            VertexBuffer(std::string name, std::string type, RM::ResourceGroup * group) : RM::BasicResource(name, type, group) {};
            virtual ~VertexBuffer() {};

            virtual void clear() = 0;

            virtual void sendToGPU() = 0;
            virtual unsigned int getNumVerticies() = 0;

        protected:
            friend VertexBufferArray;
            virtual void bindToAttribute(GLuint attribute) = 0;
    };

    template<class D>
    class VertexBufferIMP: public VertexBuffer
    {
        public:
            VertexBufferIMP(std::string name, GLuint bufferType);
            VertexBufferIMP(RM::ResourceGroup * group, std::string name, GLuint bufferType);
            virtual ~VertexBufferIMP();

            void addVertex(D vertex);
            void clear();

            void reserve(size_t size)
            {
                verticies.reserve(size);
            }

            void sendToGPU();
            unsigned int getNumVerticies();

        protected:
            std::vector<D> verticies;
            GLuint id;
            GLuint bufferType;
            size_t numVerticies;

            void bindToAttribute(GLuint attribute);

        private:
    };

    typedef VertexBufferIMP<float> VertexBuffer1D;
    typedef VertexBufferIMP<glm::vec2> VertexBuffer2D;
    typedef VertexBufferIMP<glm::vec3> VertexBuffer3D;
    typedef VertexBufferIMP<glm::vec4> VertexBuffer4D;

    void computeTangentsAndBitangents(std::vector<glm::vec3> & tangents, std::vector<glm::vec3> & bitangents, std::vector<glm::vec3> & verticies, std::vector<glm::vec3> & texCoords);
}

#endif // VERTEXBUFFER_H
