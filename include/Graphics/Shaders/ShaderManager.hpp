/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#ifndef SHADERMANAGER_H
#define SHADERMANAGER_H

#include <string>
#include <vector>
#include <map>
#include <unordered_map>

#include "GCObject.hpp"
#include "Path.hpp"

namespace GL {

    class Shader;

    class ShaderManager
    {
        public:
            ShaderManager();
            virtual ~ShaderManager();

            RM::ref<Shader> getShader(RM::Path path);
            RM::ref<Shader> getInternalShader(const char * shaderString);
        protected:
            friend Shader;

            void addShader(std::string path, Shader * shader);
            void removeShader(std::string path);

            void addInternalShader(const char * shaderString, Shader * shader);
            void removeInternalShader(const char * shaderString);

            std::map<std::string, Shader*> shaders;
            std::unordered_map<const char *, Shader*> internalShaders;
        private:
    };
}

#endif // SHADERMANAGER_H
