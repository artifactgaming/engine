/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#ifndef VIEW_H
#define VIEW_H

#include <LinearMath/btTransform.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/matrix_decompose.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>

#include "ShaderProgram.hpp"

namespace GL {
    class View: public RM::GCObject
    {
        public:
            View(GL::ShaderProgram & program);
            virtual ~View();

            inline void setProjectionMatrix(glm::mat4 & projectionMatrix) {
                this->projectionMatrix = projectionMatrix;
                updateDecompose();
            }

            inline void setViewMatrix(glm::mat4 & viewMatrix) {
                this->viewMatrix = viewMatrix;
                updateDecompose();
            }

            inline void setViewMAtrix(btTransform & transform) {
                transform.getOpenGLMatrix(glm::value_ptr(viewMatrix));
                updateDecompose();
            }

            inline glm::mat4 getProjectionMatrix() { return projectionMatrix; }
            inline glm::mat4 getViewMatrix() { return viewMatrix; }
            inline glm::mat4 getCombinedMatrix() { return viewMatrix * projectionMatrix; }

            inline glm::vec3 getScale() {
                return scale;
            }

            inline glm::quat getRotation() {
                return rotation;
            }

            inline glm::vec3 getTranslation() {
                return translation;
            }

            inline glm::vec3 getSkew() {
                return skew;
            }

            inline glm::vec4 getPerspective() {
                return perspective;
            }

            RM::ref<GL::ShaderProgram> getProgram() { return program; }

            /// Not perfectly accurate, but it will return true when the object is out of view rather than false when it is in view.
            bool inViewport(glm::vec3 & point, float radius);
        protected:
            inline void updateDecompose() {
                glm::decompose(viewMatrix * projectionMatrix, scale, rotation, translation, skew, perspective);
                center_vector = glm::inverse(getCombinedMatrix()) * glm::vec4(0, 0, 1, 1);
            }

        private:
            glm::mat4 viewMatrix;
            glm::mat4 projectionMatrix;

            glm::vec3 scale;
            glm::quat rotation;
            glm::vec3 translation;
            glm::vec3 skew;
            glm::vec4 perspective;

            // A normalized vector pointing towards where we are looking.
            glm::vec4 center_vector;

            RM::ref<GL::ShaderProgram> program;
    };
}

#endif // VIEW_H
