/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

namespace GL {
    class TextureArray;
}

#ifndef TEXTUREARRAY_H
#define TEXTUREARRAY_H

#include <boost/filesystem.hpp>
#include <glad.h>
#include <string>
#include <vector>
#include <exception>
#include <unordered_map>

#include "Shader.hpp"
#include "TextureSource.hpp"

namespace GL {
    class TextureArray
    {
        public:
            TextureArray(unsigned int width, unsigned int height);
            virtual ~TextureArray();

            void addFile(boost::filesystem::path path, std::string name);
            // VectorFont * addFont(boost::filesystem::path path, std::string name, unsigned int qualityLevel);
            void pushToGPU();

            float getTextureIndex(std::string name);

            unsigned int getTextureCount();

        protected:
            friend Shader;
            std::unordered_map<std::string, unsigned int> indexes;
            std::vector<nanogui::ref<TextureSource>> sources;
            unsigned int width;
            unsigned int height;
            unsigned int textureCount;

            GLuint textureID;
        private:
    };
}

#endif // TEXTUREARRAY_H
