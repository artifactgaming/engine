/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#ifndef TEXTURESOURCE_H
#define TEXTURESOURCE_H

#include <string>

#include "AsyncResource.hpp"

namespace GL {
    namespace TextureSource {
        class TextureSource: public RM::AsyncResource
        {
            public:
                TextureSource(std::string name, std::string type, RM::ResourceGroup * group);

                virtual unsigned int getWidth() = 0;
                virtual unsigned int getHeight() = 0;

                virtual unsigned int * getData() = 0;

                virtual void reload() = 0;
                virtual void clear() = 0;
                virtual bool isLoaded() = 0;

            protected:

            private:
        };
    }
}

#endif // TEXTURESOURCE_H
