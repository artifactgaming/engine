/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#ifndef LOCALFILETEXTURESOURCE_H
#define LOCALFILETEXTURESOURCE_H

#include <boost/filesystem.hpp>

#include "AsyncResource.hpp"
#include "TextureSource.hpp"
#include "Path.hpp"

namespace GL {
    namespace TextureSource {
        class LocalFile: public TextureSource
        {
            public:
                LocalFile(RM::Path path, std::string name);
                LocalFile(RM::ResourceGroup * group, RM::Path path, std::string name);
                virtual ~LocalFile();

                void reload();
                void clear();
                bool isLoaded();

                void async_load();
                void sync_load();

                unsigned int getWidth();
                unsigned int getHeight();
                unsigned int * getData();

                unsigned int getResourceStepsToLoad();
                unsigned int getResourceStepsCompleted();

                void openPreview(nanogui::Screen * parent);

            protected:

            private:
                RM::Path path;

                std::vector<unsigned char> colors;
                unsigned int width;
                unsigned int height;
        };
    }
}

#endif // LOCALFILETEXTURESOURCE_H
