/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#ifndef RENDERTARGET_H
#define RENDERTARGET_H

#include "ShaderProgram.hpp"

#include <nanogui/glcanvas.h>

namespace GL {
    class ShaderProgramScope;
    typedef std::stack<GLuint> ShaderStack;

    class RenderTarget: public nanogui::GLCanvas
    {
        public:
            RenderTarget(Widget * parent, const std::function<void(RenderTarget * target)> & renderGeometry);
            virtual ~RenderTarget();

            virtual void drawGL();

            void dispose();

        protected:
            std::function<void(RenderTarget * target)> renderGeometry;

            friend ShaderProgramScope;
            ShaderStack shaderStack;

        private:

    };
}

#endif // RENDERTARGET_H
