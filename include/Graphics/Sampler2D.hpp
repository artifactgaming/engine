/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#ifndef SAMPLER2D_H
#define SAMPLER2D_H

#include <glad.h>
#include "BasicResource.hpp"
#include "TextureSource.hpp"
#include "Sampler.hpp"

namespace GL {
    class Sampler2D: public GL::Sampler
    {
        public:
            Sampler2D(RM::ResourceGroup * group, TextureSource::TextureSource * source, std::string name);
            Sampler2D(TextureSource::TextureSource * source, std::string name);
            virtual ~Sampler2D();

            void bind(GLint binding);

        protected:
            RM::ref<TextureSource::TextureSource> source;
            GLuint ID;
        private:
    };
}

#endif // SAMPLER2D_H
