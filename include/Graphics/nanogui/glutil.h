/*
    nanogui/glutil.h -- Convenience classes for accessing OpenGL >= 3.x

    NanoGUI was developed by Wenzel Jakob <wenzel.jakob@epfl.ch>.
    The widget drawing code is based on the NanoVG demo application
    by Mikko Mononen.

    All rights reserved. Use of this source code is governed by a
    BSD-style license that can be found in the LICENSE.txt file.
*/
/** \file */

#pragma once

#include <nanogui/opengl.h>

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/norm.hpp>
#include <glm/gtx/component_wise.hpp>

#include <map>

#ifndef DOXYGEN_SHOULD_SKIP_THIS
namespace half_float { class half; }
#endif

#if !defined(GL_HALF_FLOAT) || defined(DOXYGEN_DOCUMENTATION_BUILD)
    /// Ensures that ``GL_HALF_FLOAT`` is defined properly for all platforms.
    #define GL_HALF_FLOAT 0x140B
#endif

NAMESPACE_BEGIN(nanogui)

//  ----------------------------------------------------

/**
 * \struct Arcball glutil.h nanogui/glutil.h
 *
 * \brief Arcball helper class to interactively rotate objects on-screen.
 *
 * The Arcball class enables fluid interaction by representing rotations using
 * a quaternion, and is setup to be used in conjunction with the existing
 * mouse callbacks defined in \ref nanogui::Widget.  The Arcball operates by
 * maintaining an "active" state which is typically controlled using a mouse
 * button click / release.  A click pressed would call \ref Arcball::button
 * with ``down = true``, and a click released with ``down = false``.  The high
 * level mechanics are:
 *
 * 1. The Arcball is made active by calling \ref Arcball::button with a
 *    specified click location, and ``down = true``.
 * 2. As the user holds the mouse button down and drags, calls to
 *    \ref Arcball::motion are issued.  Internally, the Arcball keeps track of
 *    how far the rotation is from the start click.  During the active state,
 *    \ref mQuat is not updated, call \ref Arcball::matrix to get the current
 *    rotation for use in drawing updates.  Receiving the rotation as a matrix
 *    will usually be more convenient for traditional pipelines, however you
 *    can also acquire the active rotation using \ref Arcball::activeState.
 * 3. The user releases the mouse button, and a call to \ref Arcball::button
 *    with ``down = false``.  The Arcball is no longer active, and its internal
 *    \ref mQuat is updated.
 *
 * A very simple \ref nanogui::Screen derived class to illustrate usage:
 *
 * \rst
 * .. code-block:: cpp
 *
 *    class ArcballScreen : public nanogui::Screen {
 *    public:
 *        // Creating a 400x400 window
 *        ArcballScreen() : nanogui::Screen({400, 400}, "ArcballDemo") {
 *            mArcball.setSize(mSize);// Note 1
 *        }
 *
 *        virtual bool mouseButtonEvent(const glm::ivec2 &p, int button, bool down, int modifiers) override {
 *            // In this example, we are using the left mouse button
 *            // to control the arcball motion
 *            if (button == GLFW_MOUSE_BUTTON_1) {
 *                mArcball.button(p, down);// Note 2
 *                return true;
 *            }
 *            return false;
 *        }
 *
 *        virtual bool mouseMotionEvent(const glm::ivec2 &p, const glm::ivec2 &rel, int button, int modifiers) override {
 *            if (button == GLFW_MOUSE_BUTTON_1) {
 *                mArcball.motion(p);// Note 2
 *                return true;
 *            }
 *            return false;
 *        }
 *
 *        virtual void drawContents() override {
 *            // Option 1: acquire a 4x4 homogeneous rotation matrix
 *            Matrix4f rotation = mArcball.matrix();
 *            // Option 2: acquire an equivalent quaternion
 *            Quaternionf rotation = mArcball.activeState();
 *            // ... do some drawing with the current rotation ...
 *        }
 *
 *    protected:
 *        nanogui::Arcball mArcball;
 *    };
 *
 * **Note 1**
 *     The user is responsible for setting the size with
 *     :func:`Arcball::setSize <nanogui::Arcball::setSize>`, this does **not**
 *     need to be the same as the Screen dimensions (e.g., you are using the
 *     Arcball to control a specific ``glViewport``).
 *
 * **Note 2**
 *     Be aware that the input vector ``p`` to
 *     :func:`Widget::mouseButtonEvent <nanogui::Widget::mouseButtonEvent>`
 *     and :func:`Widget::mouseMotionEvent <nanogui::Widget::mouseMotionEvent>`
 *     are in the coordinates of the Screen dimensions (top left is ``(0, 0)``,
 *     bottom right is ``(width, height)``).  If you are using the Arcball to
 *     control a subregion of the Screen, you will want to transform the input
 *     ``p`` before calling :func:`Arcball::button <nanogui::Arcball::button>`
 *     or :func:`Arcball::motion <nanogui::Arcball::motion>`.  For example, if
 *     controlling the right half of the screen, you might create
 *     ``glm::ivec2 adjusted_click(p.x - (mSize.x / 2), p.y)``, and then
 *     call ``mArcball.motion(adjusted_click)``.
 * \endrst
 */
struct Arcball {
    /**
     * \brief The default constructor.
     *
     * \rst
     * .. note::
     *
     *    Make sure to call :func:`Arcball::setSize <nanogui::Arcball::setSize>`
     *    after construction.
     * \endrst
     *
     * \param speedFactor
     *     The speed at which the Arcball rotates (default: ``2.0``).  See also
     *     \ref mSpeedFactor.
     */
    Arcball(float speedFactor = 2.0f)
        : mActive(false), mLastPos(glm::ivec2(0)), mSize(glm::ivec2(0)),
          mQuat(glm::fquat()),
          mIncr(glm::fquat()),
          mSpeedFactor(speedFactor) { }

    /**
     * Constructs an Arcball based off of the specified rotation.
     *
     * \rst
     * .. note::
     *
     *    Make sure to call :func:`Arcball::setSize <nanogui::Arcball::setSize>`
     *    after construction.
     * \endrst
     */
    Arcball(const glm::fquat &quat)
        : mActive(false), mLastPos(glm::ivec2(0)), mSize(glm::ivec2(0)),
          mQuat(quat),
          mIncr(glm::fquat()),
          mSpeedFactor(2.0f) { }

    /**
     * \brief The internal rotation of the Arcball.
     *
     * Call \ref Arcball::matrix for drawing loops, this method will not return
     * any updates while \ref mActive is ``true``.
     */
    glm::fquat &state() { return mQuat; }

    /// ``const`` version of \ref Arcball::state.
    const glm::fquat &state() const { return mQuat; }

    /// Sets the rotation of this Arcball.  The Arcball will be marked as **not** active.
    void setState(const glm::fquat &state) {
        mActive = false;
        mLastPos = glm::ivec2(0);
        mQuat = state;
        mIncr = glm::fquat();
    }

    /**
     * \brief Sets the size of this Arcball.
     *
     * The size of the Arcball and the positions being provided in
     * \ref Arcball::button and \ref Arcball::motion are directly related.
     */
    void setSize(glm::ivec2 size) { mSize = size; }

    /// Returns the current size of this Arcball.
    const glm::ivec2 &size() const { return mSize; }

    /// Sets the speed at which this Arcball rotates.  See also \ref mSpeedFactor.
    void setSpeedFactor(float speedFactor) { mSpeedFactor = speedFactor; }

    /// Returns the current speed at which this Arcball rotates.
    float speedFactor() const { return mSpeedFactor; }

    /// Returns whether or not this Arcball is currently active.
    bool active() const { return mActive; }

    /**
     * \brief Signals a state change from active to non-active, or vice-versa.
     *
     * \param pos
     *     The click location, should be in the same coordinate system as
     *     specified by \ref mSize.
     *
     * \param pressed
     *     When ``true``, this Arcball becomes active.  When ``false``, this
     *     Arcball becomes non-active, and its internal \ref mQuat is updated
     *     with the final rotation.
     */
    void button(glm::ivec2 pos, bool pressed) {
        mActive = pressed;
        mLastPos = pos;
        if (!mActive)
            mQuat = glm::normalize(mIncr * mQuat);
        mIncr = glm::fquat();
    }

    /**
     * \brief When active, updates \ref mIncr corresponding to the specified
     *        position.
     *
     * \param pos
     *     Where the mouse has been dragged to.
     */
    bool motion(glm::ivec3 pos) {
        if (!mActive)
            return false;

        /* Based on the rotation controller from AntTweakBar */
        float invMinDim = 1.0f / glm::compMin(mSize);
        float w = (float) mSize.x, h = (float) mSize.y;

        float ox = (mSpeedFactor * (2*mLastPos.x   - w) + w) - w - 1.0f;
        float tx = (mSpeedFactor * (2*pos.x        - w) + w) - w - 1.0f;
        float oy = (mSpeedFactor * (h - 2*mLastPos.y  ) + h) - h - 1.0f;
        float ty = (mSpeedFactor * (h - 2*pos.y)        + h) - h - 1.0f;

        ox *= invMinDim; oy *= invMinDim;
        tx *= invMinDim; ty *= invMinDim;

        glm::fvec3 v0(ox, oy, 1.0f), v1(tx, ty, 1.0f);
        if (glm::length2(v0) > 1e-4f && glm::length2(v1) > 1e-4f) {
            glm::normalize(v0); glm::normalize(v1);
            glm::fvec3 axis = glm::cross(v0, v1);
            float sa = std::sqrt(glm::dot(axis, axis)),
                  ca = glm::dot(v0, v1),
                  angle = std::atan2(sa, ca);
            if (tx*tx + ty*ty > 1.0f)
                angle *= 1.0f + 0.2f * (std::sqrt(tx*tx + ty*ty) - 1.0f);
            mIncr = glm::angleAxis(angle, glm::normalize(axis));
            if (!std::isfinite(glm::length(mIncr)))
                mIncr = glm::quat();
        }
        return true;
    }

    /**
     * Returns the current rotation *including* the active motion, suitable for
     * use with typical homogeneous matrix transformations.  The upper left 3x3
     * block is the rotation matrix, with 0-0-0-1 as the right-most column /
     * bottom row.
     */
    glm::mat4 matrix() const {
        return glm::toMat4(mIncr * mQuat);
    }

    /// Returns the current rotation *including* the active motion.
    glm::fquat activeState() const { return mIncr * mQuat; }

    /**
     * \brief Interrupts the current Arcball motion by calling
     *        \ref Arcball::button with ``(0, 0)`` and ``false``.
     *
     * Use this method to "close" the state of the Arcball when a mouse release
     * event is not available.  You would use this method if you need to stop
     * the Arcball from updating its internal rotation, but the event stopping
     * the rotation does **not** come from a mouse release.  For example, you
     * have a callback that created a \ref nanogui::MessageDialog which will now
     * be in focus.
     */
    void interrupt() { button(glm::ivec2(0), false); }

protected:
    /// Whether or not this Arcball is currently active.
    bool mActive;

    /// The last click position (which triggered the Arcball to be active / non-active).
    glm::ivec2 mLastPos;

    /// The size of this Arcball.
    glm::ivec2 mSize;

    /**
     * The current stable state.  When this Arcball is active, represents the
     * state of this Arcball when \ref Arcball::button was called with
     * ``down = true``.
     */
    glm::fquat mQuat;

    /// When active, tracks the overall update to the state.  Identity when non-active.
    glm::fquat mIncr;

    /**
     * The speed at which this Arcball rotates.  Smaller values mean it rotates
     * more slowly, higher values mean it rotates more quickly.
     */
    float mSpeedFactor;

public:
};

NAMESPACE_END(nanogui)
