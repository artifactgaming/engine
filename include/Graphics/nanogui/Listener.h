/*
    NanoGUI was developed by Wenzel Jakob <wenzel.jakob@epfl.ch>.
    The widget drawing code is based on the NanoVG demo application
    by Mikko Mononen.

    All rights reserved. Use of this source code is governed by a
    BSD-style license that can be found in the LICENSE.txt file.
*/

#pragma once

#include "GCObject.hpp"
#include <deque>
#include <functional>
#include <nanogui/common.h>

NAMESPACE_BEGIN(nanogui)

template<class Func>
class ListenerReference: public RM::GCObject
{
    public:
        ListenerReference(const std::function<Func> & func) {
            this->func = func;
        }

        template<typename... Args>
        inline void call(Args ... args) {
            func(args...);
        }

        bool operator==(ListenerReference * other)
        {
            return other == this;
        }

        bool operator==(ListenerReference & other)
        {
            return &other == this;
        }


    protected:
        std::function<Func> func;
};

template<class Func>
class ListenerQueue
{
    public:

        ~ListenerQueue() {

        };

        inline RM::ref<ListenerReference<Func>> addListener(const std::function<Func> & func) {
            ListenerReference<Func> * reference = new ListenerReference<Func>(func);

            functions.push_back(reference);
            return reference;
        }

        inline void removeListener(RM::ref<ListenerReference<Func>> reference) {
            functions.erase(std::remove(functions.begin(), functions.end(), reference), functions.end());
        }

        template<typename... Args>
        inline void operator()(Args ... args) {
            for (ListenerReference<Func> * func: functions) {
                func->call(args...);
            }
        }

        void clear()
        {
            functions.clear();
        }

    protected:
        std::deque<RM::ref<ListenerReference<Func>>> functions;
};

NAMESPACE_END(nanogui)
