/*
    nanogui/imagepanel.h -- Image panel widget which shows a number of
    square-shaped icons

    NanoGUI was developed by Wenzel Jakob <wenzel.jakob@epfl.ch>.
    The widget drawing code is based on the NanoVG demo application
    by Mikko Mononen.

    All rights reserved. Use of this source code is governed by a
    BSD-style license that can be found in the LICENSE.txt file.
*/
/** \file */

#pragma once

#include <nanogui/widget.h>
#include <nanogui/Listener.h>

NAMESPACE_BEGIN(nanogui)

/**
 * \class ImagePanel imagepanel.h nanogui/imagepanel.h
 *
 * \brief Image panel widget which shows a number of square-shaped icons.
 */
class NANOGUI_EXPORT ImagePanel : public Widget {
public:
    typedef std::vector<std::pair<int, std::string>> Images;
public:
    ImagePanel(Widget *parent);

    void setImages(const Images &data) { mImages = data; }
    const Images& images() const { return mImages; }

    ListenerReference<void(int)> * addClickCallback(const std::function<void(int)> & mCallback) {
        return this->mCallback.addListener(mCallback);
    }

    void removeClickCallback(ListenerReference<void(int)> * mCallback) {
        this->mCallback.removeListener(mCallback);
    }

    virtual bool mouseMotionEvent(const glm::ivec2 &p, const glm::ivec2 &rel, int button, int modifiers) override;
    virtual bool mouseButtonEvent(const glm::ivec2 &p, int button, bool down, int modifiers) override;
    virtual glm::ivec2 preferredSize(NVGcontext *ctx) const override;
    virtual void draw(NVGcontext* ctx) override;

    virtual void dispose();

protected:
    glm::ivec2 gridSize() const;
    int indexForPosition(const glm::ivec2 &p) const;
protected:
    Images mImages;
    ListenerQueue<void(int)> mCallback;
    int mThumbSize;
    int mSpacing;
    int mMargin;
    int mMouseIndex;
public:
};

NAMESPACE_END(nanogui)
