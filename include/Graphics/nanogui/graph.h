/*
    nanogui/graph.h -- Simple graph widget for showing a function plot

    NanoGUI was developed by Wenzel Jakob <wenzel.jakob@epfl.ch>.
    The widget drawing code is based on the NanoVG demo application
    by Mikko Mononen.

    All rights reserved. Use of this source code is governed by a
    BSD-style license that can be found in the LICENSE.txt file.
*/
/** \file */

#pragma once

#include <nanogui/widget.h>
#include <vector>

NAMESPACE_BEGIN(nanogui)

/**
 * \class Graph graph.h nanogui/graph.h
 *
 * \brief Simple graph widget for showing a function plot.
 */
class NANOGUI_EXPORT Graph : public Widget {
public:
    Graph(Widget *parent, const std::string &caption = "Untitled");

    const std::string &caption() const { return mCaption; }
    void setCaption(const std::string &caption) { mCaption = caption; }

    const std::string &header() const { return mHeader; }
    void setHeader(const std::string &header) { mHeader = header; }

    const std::string &footer() const { return mFooter; }
    void setFooter(const std::string &footer) { mFooter = footer; }

    const glm::vec4 &backgroundColor() const { return mBackgroundColor; }
    void setBackgroundColor(const glm::vec4 &backgroundColor) { mBackgroundColor = backgroundColor; }

    const glm::vec4 &foregroundColor() const { return mForegroundColor; }
    void setForegroundColor(const glm::vec4 &foregroundColor) { mForegroundColor = foregroundColor; }

    const glm::vec4 &textColor() const { return mTextColor; }
    void setTextColor(const glm::vec4 &textColor) { mTextColor = textColor; }

    const std::vector<float> &values() const { return mValues; }
    std::vector<float> &values() { return mValues; }
    void setValues(const std::vector<float> &values) { mValues = values; }

    virtual glm::ivec2 preferredSize(NVGcontext *ctx) const override;
    virtual void draw(NVGcontext *ctx) override;

    virtual void save(Serializer &s) const override;
    virtual bool load(Serializer &s) override;
protected:
    std::string mCaption, mHeader, mFooter;
    glm::vec4 mBackgroundColor, mForegroundColor, mTextColor;
    std::vector<float> mValues;
public:
};

NAMESPACE_END(nanogui)
