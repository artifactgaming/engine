/*
    nanogui/slider.h -- Fractional slider widget with mouse control

    NanoGUI was developed by Wenzel Jakob <wenzel.jakob@epfl.ch>.
    The widget drawing code is based on the NanoVG demo application
    by Mikko Mononen.

    All rights reserved. Use of this source code is governed by a
    BSD-style license that can be found in the LICENSE.txt file.
*/
/** \file */

#pragma once

#include <nanogui/widget.h>

NAMESPACE_BEGIN(nanogui)

/**
 * \class Slider slider.h nanogui/slider.h
 *
 * \brief Fractional slider widget with mouse control.
 */
class NANOGUI_EXPORT Slider : public Widget {
public:
    Slider(Widget *parent);

    float value() const { return mValue; }
    void setValue(float value) { mValue = value; }

    const glm::vec4 &highlightColor() const { return mHighlightColor; }
    void setHighlightColor(const glm::vec4 &highlightColor) { mHighlightColor = highlightColor; }

    std::pair<float, float> range() const { return mRange; }
    void setRange(std::pair<float, float> range) { mRange = range; }

    std::pair<float, float> highlightedRange() const { return mHighlightedRange; }
    void setHighlightedRange(std::pair<float, float> highlightedRange) { mHighlightedRange = highlightedRange; }

    ListenerReference<void(float)> * addChangeCallback(const std::function<void(float)> & mCallback) {
        return this->mCallback.addListener(mCallback);
    }

    void removeChangeback(ListenerReference<void(float)> * mCallback) {
        this->mCallback.removeListener(mCallback);
    }

    ListenerReference<void(float)> * addSelectCallback(const std::function<void(float)> & mCallback) {
        return this->mFinalCallback.addListener(mCallback);
    }

    void removeSelectback(ListenerReference<void(float)> * mCallback) {
        this->mFinalCallback.removeListener(mCallback);
    }

    virtual glm::ivec2 preferredSize(NVGcontext *ctx) const override;
    virtual bool mouseDragEvent(const glm::ivec2 &p, const glm::ivec2 &rel, int button, int modifiers) override;
    virtual bool mouseButtonEvent(const glm::ivec2 &p, int button, bool down, int modifiers) override;
    virtual void draw(NVGcontext* ctx) override;
    virtual void save(Serializer &s) const override;
    virtual bool load(Serializer &s) override;

    virtual void dispose();

protected:
    float mValue;
    ListenerQueue<void(float)> mCallback;
    ListenerQueue<void(float)> mFinalCallback;
    std::pair<float, float> mRange;
    std::pair<float, float> mHighlightedRange;
    glm::vec4 mHighlightColor;
public:
};

NAMESPACE_END(nanogui)
