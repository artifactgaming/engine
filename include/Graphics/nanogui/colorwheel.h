/*
    NanoGUI was developed by Wenzel Jakob <wenzel.jakob@epfl.ch>.
    The widget drawing code is based on the NanoVG demo application
    by Mikko Mononen.

    All rights reserved. Use of this source code is governed by a
    BSD-style license that can be found in the LICENSE.txt file.
*/
/**
 * \file nanogui/colorwheel.h
 *
 * \brief Fancy analog widget to select a color value.  This widget was
 *        contributed by Dmitriy Morozov.
 */

#pragma once

#include <nanogui/widget.h>
#include <nanogui/Listener.h>

NAMESPACE_BEGIN(nanogui)

/**
 * \class ColorWheel colorwheel.h nanogui/colorwheel.h
 *
 * \brief Fancy analog widget to select a color value.  This widget was
 *        contributed by Dmitriy Morozov.
 */
class NANOGUI_EXPORT ColorWheel : public Widget {
public:
    /**
     * Adds a ColorWheel to the specified parent.
     *
     * \param parent
     *     The Widget to add this ColorWheel to.
     *
     * \param color
     *     The initial color of the ColorWheel (default: Red).
     */
    ColorWheel(Widget *parent, const glm::vec4& color = glm::vec4(1.0f, 0.0f, 0.0f, 1.0f));


    /// Sets the callback to execute when a user changes the ColorWheel value.
    ListenerReference<void(const glm::vec4 &)> * addChangeCallback(const std::function<void(const glm::vec4 &)> & mCallback) {
        return this->mCallback.addListener(mCallback);
    }

    void removeChangeCallback(ListenerReference<void(const glm::vec4 &)> * mCallback) {
        this->mCallback.removeListener(mCallback);
    }

    /// The current Color this ColorWheel has selected.
    glm::vec4 color() const;

    /// Sets the current Color this ColorWheel has selected.
    void setColor(const glm::vec4& color);

    /// The preferred size of this ColorWheel.
    virtual glm::ivec2 preferredSize(NVGcontext *ctx) const override;

    /// Draws the ColorWheel.
    virtual void draw(NVGcontext *ctx) override;

    /// Handles mouse button click events for the ColorWheel.
    virtual bool mouseButtonEvent(const glm::ivec2 &p, int button, bool down, int modifiers) override;

    /// Handles mouse drag events for the ColorWheel.
    virtual bool mouseDragEvent(const glm::ivec2 &p, const glm::ivec2 &rel, int button, int modifiers) override;

    /// Saves the current state of this ColorWheel to the specified Serializer.
    virtual void save(Serializer &s) const override;

    /// Sets the state of this ColorWheel using the specified Serializer.
    virtual bool load(Serializer &s) override;

    virtual void dispose();

private:
    // Used to describe where the mouse is interacting
    enum Region {
        None = 0,
        InnerTriangle = 1,
        OuterCircle = 2,
        Both = 3
    };

    // Converts a specified hue (with saturation = value = 1) to RGB space.
    glm::vec4 hue2rgb(float h) const;

    // Manipulates the positioning of the different regions of the ColorWheel.
    Region adjustPosition(const glm::ivec2 &p, Region consideredRegions = Both);

protected:
    /// The current Hue in the HSV color model.
    float mHue;

    /**
     * The implicit Value component of the HSV color model.  See implementation
     * \ref nanogui::ColorWheel::color for its usage.  Valid values are in the
     * range ``[0, 1]``.
     */
    float mWhite;

    /**
     * The implicit Saturation component of the HSV color model.  See implementation
     * \ref nanogui::ColorWheel::color for its usage.  Valid values are in the
     * range ``[0, 1]``.
     */
    float mBlack;

    /// The current region the mouse is interacting with.
    Region mDragRegion;

    /// The current callback to execute when the color value has changed.
    ListenerQueue<void(const glm::vec4 &)> mCallback;

public:
};

NAMESPACE_END(nanogui)
