/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#ifndef SHADERPROGRAM_H
#define SHADERPROGRAM_H

#include <glad.h>
#include "GCObject.hpp"

#include <map>
#include <stack>
#include <vector>

#include <glm/glm.hpp>

#include "Path.hpp"
#include "Shader.hpp"
#include "ShaderManager.hpp"
#include "VertexBuffer.hpp"
#include "VertexBufferArray.hpp"

#include "BasicResource.hpp"
#include "ShaderProgram.hpp"
#include "Sampler.hpp"

#include "RenderTarget.hpp"

namespace GL {
    typedef std::vector<RM::Path> ShaderFileList;

    class ShaderProgramScope;
    class VertexBufferArray;
    class RenderTarget;

    struct InternalShaderProgramSource
    {
        virtual std::string getName() const = 0;
        virtual const std::vector<const char *> getShaderStrings() const = 0;
    };

    class ShaderProgram: public RM::BasicResource
    {
        public:
            ShaderProgram(std::string name, const ShaderFileList & shaders);
            ShaderProgram(RM::ResourceGroup * group, std::string name, const ShaderFileList & shaders);

            ShaderProgram(const InternalShaderProgramSource & shaders);
            ShaderProgram(RM::ResourceGroup * group, const InternalShaderProgramSource & shaders);

            virtual ~ShaderProgram();

            inline void setPolygonMode(GLuint polygonMode) {
                switch (polygonMode) {
                case GL_TRIANGLES:
                case GL_LINES:
                    break; // These are fine.
                default:
                    throw std::runtime_error("Invalid polygon mode.");
                }

                this->polygonMode = polygonMode;
            }

        protected:
            friend ShaderProgramScope;
            friend VertexBufferArray;

            std::vector<RM::ref<Shader>> shaders;
            std::map<std::string, GLint> uniformLocations;
            std::map<std::string, GLint> attributeLocations;
            std::map<std::string, std::pair<GLint, RM::ref<GL::Sampler>>> textureBindings;

            ShaderManager * manager;

            GLuint programID;
            GLint lastTextureBinding;

            GLuint polygonMode;

            GLint getAttributeLocation(std::string name);
            GLint getUniformLocation(std::string name);

        private:
    };

    typedef std::stack<GLuint> ShaderStack;

    class ShaderProgramScope: public RM::GCObject
    {
        public:
            ShaderProgramScope(RenderTarget * target, ShaderProgram * program);
            ShaderProgramScope(ShaderProgram * program);
            ~ShaderProgramScope();

            void drawVertexBuffers(GL::VertexBufferArray & array);

            // void setTextureArray(TextureArray * array, std::string name);

            void setFloat(std::string name, float value);
            void setVec2(std::string name, glm::vec2 value);
            void setVec3(std::string name, glm::vec3 value);
            void setVec4(std::string name, glm::vec4 value);
            void setMatrix2x2(std::string name, glm::mat2x2 value);
            void setMatrix3x3(std::string name, glm::mat3x3 value);
            void setMatrix4x4(std::string name, glm::mat4x4 value);

            void setSampler(std::string name, GL::Sampler * sampler);

            void printDebug();

            inline RenderTarget * getRenderTarget() {
                return target;
            }

        private:
            RM::ref<ShaderProgram> program;
            ShaderStack * programStack;
            RenderTarget * target;
    };
}

#endif // SHADERPROGRAM_H
