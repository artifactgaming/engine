/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#ifndef MAIN_HPP
#define MAIN_HPP

#include <deque>

#include <sol.hpp>
#include <nanogui/screen.h>

#include "Path.hpp"
#include "ResourceManager.hpp"
#include "DeveloperTerminal.hpp"
#include "ControlManager.hpp"
#include "ShaderManager.hpp"
#include "ControllerManager.hpp"
#include "WidgetManager.hpp"
#include "WorldManager.hpp"

// #include "World.hpp"
namespace World
{
    class World; // Forward declaration.
}

class GridLocked: public RM::WidgetManager, public World::WorldManager {
public:
    GridLocked(bool developerMode);
    ~GridLocked();

    void start();
    void shutdown();

    bool isDeveloperMode();

    nanogui::Screen * getScreen() { return screen; }
    RM::ResourceManager * getResourceManager() { return resourceManager; }
    RM::ControlManager * getControlManager() { return controlManager; }
    GL::ShaderManager * getShaderManager() { return shaderManager; }
    World::ControllerManager * getControllerManager() { return controllerManager; }

    bool openGLDebugEnabled() { return debugGlEnabled; }
    bool isPhysicsDebugEnabled()
    {
        return debugRendererEnabled;
    }

private:
    friend int main(int argc, char ** argv);
    friend class DevTools::DeveloperTerminal;
    void update(float elapsed);
    void render();
    void mainLoop();

    nanogui::Screen * screen;

    DevTools::DeveloperTerminal * terminal;
    RM::ControlManager * controlManager;
    RM::ResourceManager * resourceManager;
    GL::ShaderManager * shaderManager;
    World::ControllerManager * controllerManager;

    float minFrameTime;
    bool runGame;
    bool developerMode;
    bool debugGlEnabled;
    bool debugRendererEnabled;
};

GridLocked * getGameInstance();

#endif // MAIN_H
