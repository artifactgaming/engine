/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#ifndef DEVELOPERTERMINAL_H
#define DEVELOPERTERMINAL_H

#include <glad.h>
#include <nanogui/nanogui.h>

#include <deque>
#include <sol.hpp>

namespace DevTools {
    class DeveloperTerminal {
        public:
            DeveloperTerminal(nanogui::Screen * screen);
            ~DeveloperTerminal();

            void print(std::stringstream & stream);
            void clear();

            void setFPS(int fps);
            void setLuaMemoryUsage(int kbytes);
            void setSystemMemoryUsage(long bytes);
            void setSystemCPUUsage(float cpuUsage);
            void setNumberResources(long resources);
            void setNumberGroups(long groups);

            bool isVisible();

        protected:
            void updateTerminal();

            nanogui::TextBox * createStatWidget(nanogui::Widget * tab, std::string name, std::string defaultUnit);
            nanogui::CheckBox * createDebugSettingWidget(nanogui::Widget * tab, std::string name, const std::function< void(bool)> & callback, bool enabled);

        private:
            std::deque<std::string> lines;
            std::string lastCommand;

            nanogui::Window * window;
            nanogui::TabWidget * tabs;
            nanogui::VScrollPanel * terminal;
            nanogui::Label * terminalLabel;
            nanogui::TextBox * textBox;

            nanogui::TextBox * fps;
            nanogui::TextBox * luaMemoryUsage;
            nanogui::TextBox * systemMemoryUsage;
            nanogui::TextBox * systemCPUUsage;
            nanogui::TextBox * numResources;
            nanogui::TextBox * numGroups;

    }; // A global terminal for the whole application.
}

#endif // DEVELOPERTERMINAL_H
