#ifndef GLOBALS_HPP_INCLUDED
#define GLOBALS_HPP_INCLUDED

/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

// The includes here are commented out to prevent mass re-compiles.
// #include "ResourceManager.hpp"
// #include "WidgetManager.hpp"
// #include "ShaderManager.hpp"
// #include "ControlManager.hpp"

namespace RM
{
    class ResourceManager;
    class WidgetManager;
    class ControlManager;
}

namespace GL
{
    class ShaderManager;
}

RM::ResourceManager * getResourceManager();
RM::WidgetManager * getWidgetManager();
GL::ShaderManager * getShaderManager();
RM::ControlManager * getControlManager();

// Checks to determine if certain hardware acceleration features are provided by
// the hardware.
namespace HardwareAccelleration {
    bool supportComputeShaders();
}

bool isDeveloperMode();
bool isPhysicsDebugEnabled();
void shutdown();

// TODO These should be user defined.
// #include "ControllerManager.hpp"
// #include "WorldManager.hpp"

namespace World
{
    class ControllerManager;
    class WorldManager;
}

World::ControllerManager * getControllerManager();
World::WorldManager * getWorldManager();

#endif // GLOBALS_HPP_INCLUDED
