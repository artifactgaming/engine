/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#ifndef UPDATETARGET_H
#define UPDATETARGET_H

#include <vector>

class UpdateTarget;
typedef std::vector<UpdateTarget*> UpdateTargetList;

//#include "main.hpp"
// From main.hpp, but we can't use that because we get a nasty include paradox.
void setUpdateTargets(UpdateTargetList targets);

class UpdateTarget
{
    public:
        UpdateTarget();
        virtual ~UpdateTarget();

        virtual void update(float timedelta) = 0;

    protected:

    private:
        friend void setUpdateTargets(UpdateTargetList targets);
        UpdateTargetList * container;
};

#endif // UPDATETARGET_H
