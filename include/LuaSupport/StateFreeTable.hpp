/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#ifndef STATEFREETABLE_H
#define STATEFREETABLE_H

#include <variant>
#include <any>
#include <map>
#include <sol.hpp>

#include "GCObject.hpp"

namespace luaSupport
{
    class StateFreeTable: public RM::GCObject
    {
        public:
            typedef std::variant<std::string, int, double> LuaKey;

            StateFreeTable();
            StateFreeTable(sol::state_view lua, sol::table table);
            virtual ~StateFreeTable();

            void setValue(sol::object key, sol::object value, sol::state_view lua);
            sol::object getValue(sol::object key, sol::state_view lua);

            void setValue(LuaKey key, sol::object value, sol::state_view lua);
            sol::object getValue(LuaKey key, sol::state_view lua);

            void removeValue(sol::object key);
            void removeValue(LuaKey key);

            struct Value
            {
                std::any value;
                std::string metatable_name;
                sol::type type;
            };

            inline void forEach(sol::state_view lua, std::function<void(sol::object key, sol::object value)> func)
            {
                for (std::pair<LuaKey, Value> pair : data)
                {
                    sol::object key(lua, sol::in_place, pair.first);
                    sol::object value(lua, getValue(key, lua));
                    func(key, value);
                }
            }

            bool operator==(const StateFreeTable & ptr) const { return this == &ptr; }

        protected:
            std::map<LuaKey, Value> data;
        private:
    };

    void loadStatefreeTable(sol::state_view lua);
}

#endif // STATEFREETABLE_H
