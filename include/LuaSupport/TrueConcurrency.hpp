/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#ifndef TRUECONCURRENCY_H
#define TRUECONCURRENCY_H

#include "AsyncResource.hpp"

#include <stack>
#include <variant>
#include <boost/thread/mutex.hpp>
#include <memory>


namespace LuaSupport {
    class TrueConcurrency:
        public RM::AsyncResource
    {
        public:
            TrueConcurrency(std::string name, std::string type, RM::ResourceGroup * group):
                RM::AsyncResource(name, type, group)
            {

            }

            static inline std::string luaError(int errCode)
            {
                switch(errCode)
                {
                    case LUA_ERRSYNTAX: return "Invalid syntax (LUA_ERRSYNTAX)";
                    case LUA_ERRMEM:    return "Memory allocation error (LUA_ERRMEM)";
                    case LUA_ERRRUN:    return "Execution error (LUA_ERRRUN)";
                    case LUA_ERRGCMM:   return "Error in __gc method (LUA_ERRGCMM)";
                    case LUA_ERRERR:    return "Recursive error (LUA_ERRERR)";
                    default: return "Unknown (" + std::to_string(errCode) + ")";
                }
            }

            static inline int dumpMemoryWriter(lua_State*, const void* batch, size_t batchSize, void* storage)
            {
                if (storage == nullptr)
                    return 1;
                if (batchSize && batch) {
                    std::string& buff = *reinterpret_cast<std::string*>(storage);
                    const char* newData = reinterpret_cast<const char*>(batch);
                    buff.insert(buff.end(), newData, newData + batchSize);
                }
                return 0;
            }

            static inline std::string dumpFunction(const sol::function& f)
            {
                sol::state_view lua(f.lua_state());
                sol::stack::push(lua, f);
                std::string result;

                int ret = lua_dump(lua, dumpMemoryWriter, &result, 0);

                if (ret != LUA_OK)
                {
                    throw std::runtime_error("Unable to dump Lua function for thread transfer: " + luaError(ret));
                }

                sol::stack::remove(lua, -1, 1);
                return result;
            }

            static inline sol::function loadString(const sol::state_view& lua, const std::string& str,
                const sol::optional<std::string>& source = sol::nullopt)
            {
                int ret = luaL_loadbuffer(lua, str.c_str(), str.size(), source ? source.value().c_str() : nullptr);

                if (ret != LUA_OK)
                {
                    throw std::runtime_error("Unable to load function from string: " + luaError(ret));
                }

                return sol::stack::pop<sol::function>(lua);
            }

        protected:
            boost::mutex mutex;

        private:
    };
}

#endif // TRUECONCURRENCY_H
