/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#ifndef LUASUPPORT_HPP_INCLUDED
#define LUASUPPORT_HPP_INCLUDED

#include <string>
#include <sol.hpp>
#include "GCObject.hpp"

namespace LuaSupport {

    sol::table makeTableReadOnly(sol::state_view lua, sol::table & obj_metatable);

    std::string getStacktrace(lua_State * lua);

    sol::protected_function_result lua_error_handler(lua_State * lua, sol::protected_function_result pfr);

    void loadCoreAPI();
    void loadClientAPI();


    void startGame();
    void shutdownGame();

    void resetScriptTimeout(sol::state_view lua);

    template<typename T, typename... Args>
    auto WrappedConstructor(sol::types<T(Args...)>)
    {
        return +[](Args... args)
        {
            RM::ref<T> result = new T(std::forward<Args>(args)...);
            return result;
        };
    }

    template<typename... Args>
    auto WrappedConstructorList()
    {
        return sol::factories(WrappedConstructor(sol::types<Args>())...);
    }
}

#endif // LUASUPPORT_HPP_INCLUDED
