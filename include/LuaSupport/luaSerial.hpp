/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#ifndef LUASERIAL_H
#define LUASERIAL_H

#include <sol.hpp>

#include "Path.hpp"

#include <vector>
#include <string>

void loadLuaSerialAPI(sol::state_view lua);

namespace luaSerial
{
    typedef uint32_t LENGTH_HOLDER;
    typedef std::vector<char> SerializedData;

    void serializeBlankTable(SerializedData & data);

    void serializeTable(sol::table table, SerializedData & data);
    sol::table deserializeTable(sol::state_view lua, const SerializedData & data);
    void deserializeTable(sol::state_view lua, const SerializedData & data, sol::table & table);
    void saveBin(SerializedData & data, RM::Path path);
    void loadBin(SerializedData & data, RM::Path path);

    template<class T>
    inline void insert(SerializedData & data, T obj)
    {
        data.insert(data.end(), reinterpret_cast<char*>(&obj), reinterpret_cast<char*>(&obj) + sizeof(T)) - 1;
    }

    inline void insert(SerializedData & data, std::string str)
    {
        uint32_t length = str.size();
        insert(data, length);
        data.insert(data.end(), reinterpret_cast<char*>(&str[0]), reinterpret_cast<char*>(&str[length]));
    }

    template<class T>
    inline void insert(SerializedData & data, const std::vector<T> & vec)
    {
        // Tell them how long our string is.
        LENGTH_HOLDER length = vec.size();
        insert(data, length);

        for (T item : vec)
        {
            insert(data, item);
        }
    }

    template<class T>
    inline void replace(SerializedData & data, size_t point, T obj)
    {
        memcpy(&data[point], reinterpret_cast<char*>(&obj), sizeof(T));
    }

    struct Serializable
    {
        virtual void serialize(SerializedData & data) = 0;
        virtual void deserialize(SerializedData & data) = 0;
        virtual std::string luaDeserializeFunctionPath() = 0;
    };
}

#endif // LUASERIAL_H
