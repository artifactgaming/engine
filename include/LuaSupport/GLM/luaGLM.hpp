/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#ifndef LUAGLM_H
#define LUAGLM_H

#include <sol.hpp>
#include <glm/glm.hpp>

namespace LuaGLM {
    template<typename vector>
    std::string vecToString(vector & vec) {
        std::string str = "(";

        for (int i = 0; i < vec.length(); i++) {
            std::string num = std::to_string(vec[i]);
            num.erase (num.find_last_not_of('0') + 1, std::string::npos);
            if (num[num.length()-1] == '.') { // Chop off the decimal if there's no decimal to show.
                num.resize(num.length()-1);
            }
            str += num;
            str += ',';
        }

        // Cut off the extra comma.
        str.resize(str.length()-1);
        str += ')';

        return str;
    }

    template<typename vector>
    inline void loadGenericVectorFunctions(sol::table vec) {
        vec[sol::meta_function::length] = &vector::length;

        vec[sol::meta_function::unary_minus] = +[](vector & me) {
            return -me;
        };

        vec[sol::meta_function::addition] = sol::overload(+[](vector & me, vector & them) {
            return me + them;
        },
        +[](vector & me, float them) {
            return me + them;
        });

        vec[sol::meta_function::subtraction] = sol::overload(+[](vector & me, vector & them) {
            return me - them;
        }, +[](vector & me, float them) {
            return me - them;
        });

        // TODO matrix multiplication.
        vec[sol::meta_function::multiplication] = sol::overload(+[](vector & me, vector & them) {
            return me * them;
        }, +[](vector & me, float them) {
            return me * them;
        });

        vec[sol::meta_function::division] = sol::overload(+[](vector & me, vector & them) {
            return me / them;
        }, +[](vector & me, float them) {
            return me / them;
        });

        vec[sol::meta_function::to_string] = &vecToString<vector>;

        vec[sol::meta_function::concatenation] = sol::overload(+[](vector & vec, std::string str) {
            return vecToString<vector>(vec) + str;
        },
        +[](std::string str, vector & vec) {
            return str + vecToString<vector>(vec);
        });

        vec["normalize"] = +[] (vector & vec) {
            return glm::normalize(vec);
        };

        vec["length"] = +[](vector & vec) {
            return glm::length(vec);
        };
    }
}

void loadLuaGLMAPI(sol::state_view lua);

#endif // LUAGLM_H
