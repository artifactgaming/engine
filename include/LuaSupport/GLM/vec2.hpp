/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#ifndef VEC2_H
#define VEC2_H

#include <sol.hpp>

namespace LuaGLM {
    void loadVec2(sol::state_view lua);
}

#endif // VEC2_H
