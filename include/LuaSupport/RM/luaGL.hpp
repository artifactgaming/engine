/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#ifndef LUAGL_H
#define LUAGL_H

#include "luaRM.hpp"
#include <sol.hpp>

namespace luaGL
{
    void loadVBOs(sol::table & GL, sol::state_view lua);
}

void loadLuaGLAPI(sol::table lua);

#endif // LUAGL_H
