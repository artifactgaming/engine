/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#ifndef LUARM_H
#define LUARM_H

#include <sol.hpp>
#include "GCObject.hpp"
#include <iostream>

namespace sol {
    template <typename T>
    struct unique_usertype_traits<RM::ref<T>> {
        typedef T type;
        typedef RM::ref<T> actual_type;
        static const bool value = true;

        template <typename X>
        using rebind_base = RM::ref<X>;

        static bool is_null(actual_type& value) {
            return value == nullptr;
        }

        static type* get (actual_type& p) {
            return p.get();
        }
    };
}

template<typename T, typename... Args>
RM::ref<T> luaObjectFactory(Args... args) {
    RM::ref<T> result = new T(args...);
    return result;
}

namespace luaRM {
    std::string assumeNameForResource(sol::state_view lua);
}

void loadLuaResourceManagement(sol::state_view lua);

#endif // LUARM_H
