/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#ifndef PHYSICSDEBUGRENDERER_H
#define PHYSICSDEBUGRENDERER_H

#include <btBulletDynamicsCommon.h>
#include "VertexBufferArray.hpp"
#include "VertexBuffer.hpp"
#include "View.hpp"

namespace World {
    class PhysicsDebugRenderer: public btIDebugDraw
    {
        public:
            PhysicsDebugRenderer();
            virtual ~PhysicsDebugRenderer();

            void drawLine(const btVector3& from,const btVector3& to,const btVector3& color);
            void drawContactPoint(const btVector3& PointOnB,const btVector3& normalOnB,btScalar distance,int lifeTime,const btVector3& color);
            void reportErrorWarning(const char* warningString);
            void draw3dText(const btVector3& location,const char* textString);

            void setDebugMode(int debugMode) {
                this->debugMode = debugMode;
            }

            int getDebugMode() const {
                return debugMode;
            }

            void render(GL::RenderTarget * target, GL::View & view, btDiscreteDynamicsWorld & dynamicsWorld);

        protected:

        private:
            int debugMode;
            GL::ShaderProgram program;
            GL::VertexBufferArray vbarray;
            GL::VertexBuffer3D verticies;
            GL::VertexBuffer3D colors;

    };
}

#endif // PHYSICSDEBUGRENDERER_H
