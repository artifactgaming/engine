/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#ifndef TERRAINBLOCKCONTROLLER_H
#define TERRAINBLOCKCONTROLLER_H

#include "BlockController.hpp"

namespace World {
    class TerrainBlockController: public BlockController
    {
        public:
            TerrainBlockController(RM::ResourceGroup * group);
            virtual ~TerrainBlockController();

            void spawn(Block * block, BlockEntity * entity, sol::table & args);
            void spawn(Block * block, BlockEntity * entity, uint16_t metadata);
            void remove(Block * block, BlockEntity * entity);

            void rightClick(Block * block, BlockEntity * entity, sol::table & args);
            void leftClick(Block * block, BlockEntity * entity, sol::table & args);

            void openGUI(Block * block, BlockEntity * entity, sol::table & args);

            void update(Block * block, BlockEntity * entity, float timedelta);

            void renderGhost(Block * block, BlockEntity * entity);
            void render(Block * block, BlockEntity * entity);

            // The idea is that this entity needs to be very lightweight. Minimal data storage.
            bool usesEntity() { return false; };

            // Block base sides [2:0]:
            // Bottom: 0
            // North: 1
            // South: 2
            // East: 3
            // West: 4
            // Top: 5
            // Unused: 6
            // Unused: 7
            enum class BaseSide {
                Bottom = 0,
                North = 1,
                South = 2,
                East = 3,
                West = 4,
                Top = 5
            };

            // Block rotations (relative to base) [4:3]:
            // 0 degree: 0
            // 90 degrees: 1
            // 180 degrees: 2
            // 270 degrees: 3
            enum class Rotation {
                rot0 = 0,
                rot90 = 1,
                rot180 = 2,
                rot270 = 3
            };

            // Shape Type [7:5]:
            // Block: 0
            // Slope: 1
            // Corner Slope: 2
            // Inverted Corner Slope: 3
            // Steps: 4
            // Pyramid: 5
            // Unused: 6
            // Unused: 7
            enum class Shape {
                Block = 0,
                Slope = 1,
                CornerSlope = 2,
                InvertedCornerSlope = 3,
                Steps = 4,
                Pyramid = 6
            };

            // Bits [15:8] are free for the block to do whatever it wants.

        protected:
            inline void setBaseSide(Block * block, BaseSide side) {
                uint16_t metadata = block->metadata;
                uint16_t sidebits = static_cast<uint16_t>(side); // Store in local register.

                metadata &= 0xFFF8;   // Clear the base side bits.
                metadata |= sidebits; // Now set them.

                block->metadata = metadata; // Store.
            }

            inline void setRotation(Block * block, Rotation rotation) {
                uint16_t metadata = block->metadata;
                uint16_t sidebits = static_cast<uint16_t>(rotation) << 3; // Shift bits and store in local register.

                metadata &= 0xFFE7;   // Clear the rotation bits.
                metadata |= sidebits; // Now set them.

                block->metadata = metadata; // Store.
            }

            inline void setShape(Block * block, Shape shape) {
                uint16_t metadata = block->metadata;
                uint16_t sidebits = static_cast<uint16_t>(shape) << 5; // Shift bits and store in local register.

                metadata &= 0xFF1F;   // Clear the rotation bits.
                metadata |= sidebits; // Now set them.

                block->metadata = metadata; // Store.
            }

        private:
    };
}

#endif // TERRAINBLOCKCONTROLLER_H
