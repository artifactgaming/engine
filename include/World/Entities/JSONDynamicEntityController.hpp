/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#ifndef JSONDYNAMICENTITYCONTROLLER_H
#define JSONDYNAMICENTITYCONTROLLER_H

#include "Path.hpp"
#include "DynamicEntityController.hpp"
#include "JSONControllerCore.hpp"

namespace World
{
    class JSONDynamicEntityController: public DynamicEntityController, public JSONControllerCore
    {
        public:
            JSONDynamicEntityController(RM::ResourceGroup * group, RM::Path jsonFile);
            JSONDynamicEntityController(RM::Path jsonFile);
            virtual ~JSONDynamicEntityController();

            void init(DynamicEntity * entity);
            void save(DynamicEntity * entity, luaSerial::SerializedData & data);
            void load(DynamicEntity * entity, luaSerial::SerializedData & data);
            bool forceLoad(DynamicEntity * entity);

        protected:

            void loadJSON(Json::Value & root, Json::Reader & reader);

        private:
    };
}

#endif // JSONDYNAMICENTITYCONTROLLER_H
