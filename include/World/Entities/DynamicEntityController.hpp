/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#ifndef DYNAMICENTITYCONTROLLER_H
#define DYNAMICENTITYCONTROLLER_H

#include "TerrainChunk.hpp"
#include <vector>

#include "luaSerial.hpp"

#include "DynamicEntity.hpp"

namespace World
{
    class DynamicEntity;

    class DynamicEntityController: public RM::BasicResource
    {
        public:
            DynamicEntityController(RM::ResourceGroup * group, const std::string & name);
            virtual ~DynamicEntityController();

            virtual void init(DynamicEntity * entity) = 0;
            virtual void save(DynamicEntity * entity, luaSerial::SerializedData & data) = 0;
            virtual void load(DynamicEntity * entity, luaSerial::SerializedData & data) = 0;

            virtual bool forceLoad(DynamicEntity * entity) = 0;

        protected:

            std::vector<btCollisionShape> shapes;

        private:
    };
}

#endif // DYNAMICENTITYCONTROLLER_H
