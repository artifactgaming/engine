/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#ifndef ENTITY_H
#define ENTITY_H

#include <glm/glm.hpp>

#include <LinearMath/btMotionState.h>
#include <btBulletDynamicsCommon.h>

#include "DynamicEntityController.hpp"

#include <vector>

#include "luaSerial.hpp"
#include "TerrainChunk.hpp"

#include "GCObject.hpp"
#include "Path.hpp"

#include "StateFreeTable.hpp"

namespace World
{
    class DynamicEntityController;

    class DynamicEntity: public RM::GCObject {
        public:
            DynamicEntity(RM::ref<DynamicEntityController> controller, World * world, glm::vec3 startingPosition, sol::table args);
            DynamicEntity(World * world, RM::Path file, uint64_t ID);
            virtual ~DynamicEntity();

            void save();

            DynamicEntityController * getController()
            {
                return controller;
            }

            World * getWorld()
            {
                return world;
            }

            TerrainChunk * getTerrainChunk() { return chunk; }
            uint64_t getID() { return ID; }
            double getTimeOfCreation() { return timeOfCreation; }

            bool isForceLoad();

            RM::ref<luaSupport::StateFreeTable> userdata;
            std::vector<std::tuple<std::string, std::string, RM::ref<nanogui::ListenerReference<void(bool pushed)>>>> registeredBooleanControls;
            std::vector<std::tuple<std::string, std::string, RM::ref<nanogui::ListenerReference<void(float delta)>>>> registeredAnalogControls;

        protected:

            uint64_t ID;
            RM::Path file;
            RM::ref<DynamicEntityController> controller;

            World * world;
            TerrainChunk * chunk;
            bool physicsLoaded;
            double timeOfCreation;

            glm::vec3 center;
            btRigidBody * mainBody;
            std::vector<btRigidBody> bodyParts;
        private:
    };

    // A dynamic entity lock does not keep the entity loaded, it just insures that as long as the entity still exists, we can find it.
    class DynamicEntityLockon
    {
    public:
        DynamicEntityLockon(DynamicEntity * entity)
        {
            ID = entity->getID();
            timeOfCreation = entity->getTimeOfCreation();
        }
    private:
        friend World;

        uint64_t ID;
        double timeOfCreation;
    };
}

#endif // ENTITY_H
