/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#ifndef JSONCONTROLLERCORE_H
#define JSONCONTROLLERCORE_H

#include "Path.hpp"
#include <json/json.h>

namespace World
{
    struct JsonError {
        JsonError(const Json::Value & value, const std::string & message):
            value(value),
            message(message)
        {

        }

        Json::Value value;
        std::string message;
    };

    class JSONControllerCore
    {
        public:
            JSONControllerCore(RM::Path jsonFile, const char * expectedEnding, const char * typeName, std::function<void(Json::Value&, Json::Reader&)> loadJSON, std::string resourceName);
            virtual ~JSONControllerCore();

            sol::table getFunctions();

        protected:

            void loadFuncTableBuilder(Json::Value functions, Json::Reader & reader, std::string name);
            sol::environment loadFunctionTable(sol::state_view lua);

            std::function<void(sol::environment & env)> addSelfFunctions;
            std::string funcTableName;
            std::string funcTableBuilder;

        private:
    };
}

#endif // JSONCONTROLLERCORE_H
