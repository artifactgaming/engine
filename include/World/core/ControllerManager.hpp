/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#ifndef BLOCKCONTROLLERMANAGER_H
#define BLOCKCONTROLLERMANAGER_H

#include <nanogui.h>

#include "BlockController.hpp"
#include "DynamicEntityController.hpp"
#include "Path.hpp"

namespace World {
    class ControllerManager
    {
        public:
            ControllerManager();
            virtual ~ControllerManager();

            RM::ref<BlockController> getBlockController(RM::Path path);
            RM::ref<BlockController> getBlockController(std::string path);

            RM::ref<DynamicEntityController> getDynamicEntityController(RM::Path path);
            RM::ref<DynamicEntityController> getDynamicEntityController(std::string path);
        protected:
            friend BlockController;
            friend DynamicEntityController;

            void removeBlockController(std::string path);
            void removeDynamicEntityController(std::string path);

            std::map<std::string, BlockController*> block_controllers;
            std::map<std::string, DynamicEntityController*> dynamic_entity_controllers;
        private:
    };
}

#endif // BLOCKCONTROLLERMANAGER_H
