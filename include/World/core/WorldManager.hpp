/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#ifndef WORLDMANAGER_H
#define WORLDMANAGER_H

#include <deque>

namespace World
{
    // Forward declaration.
    class World;

    class WorldManager
    {
        public:
            WorldManager();
            virtual ~WorldManager();

        protected:
            friend World;

            inline void addWorld(World * world) {
                worlds.push_back(world);
            }

            inline void removeWorld(World * world) {
                worlds.erase(std::remove(worlds.begin(), worlds.end(), world), worlds.end());
            }

            void updateWorlds(float delta);

        private:
            std::deque<World*> worlds;
    };
}

#endif // WORLDMANAGER_H
