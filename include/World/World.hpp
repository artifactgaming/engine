/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#ifndef WORLD_H
#define WORLD_H

#include <vector>
#include <glm/glm.hpp>
#include <btBulletDynamicsCommon.h>

#include <BlockController.hpp>
#include <unordered_map>

#include <algorithm>

#include <boost/thread/shared_mutex.hpp>
#include <boost/thread/mutex.hpp>

#include "ShaderProgram.hpp"

#include "BasicResource.hpp"
#include "Path.hpp"

#include "TerrainProvider.hpp"
#include "TerrainChunk.hpp"
#include "Block.hpp"

#include "DynamicEntity.hpp"

#include "View.hpp"
#include "PhysicsDebugRenderer.hpp"

#include <boost/random/random_device.hpp>
#include <boost/random/uniform_int_distribution.hpp>

#include "StateFreeTable.hpp"

#include "globals.hpp"

#include "HexFormatter.hpp"

namespace World {
    class TerrainProvider;

    const uint16_t world_format_version = 0;

    class World: public RM::BasicResource
    {
        public:
            World(RM::Path folder, const std::string & name,
                  std::vector<RM::ref<BlockController>> & blockControllers,
                  TerrainProvider * terrainProvider
            );
            World(RM::ResourceGroup * group, RM::Path folder, const std::string & name,
                  std::vector<RM::ref<BlockController>> & blockControllers,
                  TerrainProvider * terrainProvider
            );
            virtual ~World();

            void update(float elapsed);
            void renderTerrain(GL::View & view, GL::ShaderProgramScope & programScope);

            inline void blockToChunkCoordinates(int32_t block_x, int32_t block_y, int32_t block_z, int16_t & local_x, int16_t & local_y, int16_t & local_z) {
                local_x = block_x  >> 4; // Chop off the block coordinates and shift to right.
                local_y = block_y  >> 4;
                local_z = block_z  >> 4;
            }

            inline TerrainChunk * getChunkNativeCoords_unsafe(int16_t x, int16_t y, int16_t z) {
                uint64_t code = TerrainChunk::generateHashCode(x, y, z);
                std::unordered_map<uint64_t, TerrainChunk*>::iterator result;
                {
                    boost::shared_lock<boost::shared_mutex> lock(chunkMutex);
                    result = chunks.find(code);
                }

                if (result != chunks.end())
                {
                    return result->second;
                }
                else
                {
                    return nullptr;
                }
            }

            inline TerrainChunk * getChunkBlockCoords_unsafe(int32_t x, int32_t y, int32_t z) {
                int16_t local_x;
                int16_t local_y;
                int16_t local_z;

                blockToChunkCoordinates(x, y, z, local_x, local_y, local_z);
                return getChunkNativeCoords_unsafe(local_x, local_y, local_z);
            }

            inline TerrainChunk * getChunkNativeCoords_safe(int16_t x, int16_t y, int16_t z)
            {
                TerrainChunk * chunk = getChunkNativeCoords_unsafe(x, y, z);

                if (chunk != nullptr)
                {
                    return chunk;
                }
                else
                {
                    chunk = new TerrainChunk(x, y, z, this, terrainProvider);
                    uint64_t code = TerrainChunk::generateHashCode(x, y, z);

                    {
                        boost::unique_lock<boost::shared_mutex> uniqueLock(chunkMutex);
                        chunks[code] = chunk;
                    }

                    return chunk;
                }
            }

            inline TerrainChunk * getChunkBlockCoords_safe(int32_t x, int32_t y, int32_t z)
            {
                int16_t local_x;
                int16_t local_y;
                int16_t local_z;

                blockToChunkCoordinates(x, y, z, local_x, local_y, local_z);
                return getChunkNativeCoords_safe(local_x, local_y, local_z);
            }

            inline TerrainChunk * getChunkHash_unsafe(uint64_t code) {
                std::unordered_map<uint64_t, TerrainChunk*>::iterator result;
                {
                    boost::shared_lock<boost::shared_mutex> lock(chunkMutex);
                    result = chunks.find(code);
                }

                if (result != chunks.end())
                {
                    return result->second;
                }
                else
                {
                    return nullptr;
                }
            }

            inline TerrainChunk * getChunkHash_safe(uint64_t code)
            {
                TerrainChunk * chunk = getChunkHash_unsafe(code);

                if (chunk != nullptr)
                {
                    return chunk;
                }
                else
                {
                    int16_t x, y, z;
                    TerrainChunk::reverseHashCode(code, x, y, z);

                    chunk = new TerrainChunk(x, y, z, this, terrainProvider);
                    {
                        boost::unique_lock<boost::shared_mutex> uniqueLock(chunkMutex);
                        chunks[code] = chunk;
                    }

                    return chunk;
                }
            }

            Block createBlock(std::string name);
            RM::ref<DynamicEntity> spawnDynamicEntity(std::string name, glm::vec3 location, sol::table args);

            void save();
            void loadEntities();

            RM::Path getFolder() { return folder; }
            RM::Path getChunksFolder() { return chunksFolder; }
            RM::Path getDynamicChunksFolder() { return dynamicChunksFolder; }
            RM::Path getTerrainFolder() { return terrainFolder; }
            RM::Path getBlockEntitiesFolder() { return blockEntitiesFolder; }
            RM::Path getDynamicEntitiesFolder() { return dynamicEntitiesFolder; }

            DynamicEntity * getDynamicEntity(DynamicEntityLockon lock);
            uint64_t generateDynamicEntityID(RM::Path & path);

            inline double getTime() { return time; }

            inline BlockController * getBlockController(uint16_t ID)
            {
                if (ID == 0)
                {
                    return nullptr;
                }

                RM::ref<BlockController> controller = blockControllersByID[ID];

                if (controller)
                {
                    return controller;
                }
                else
                {
                    throw std::runtime_error("Attempt to get block controller for an invalid block ID. Is the chunk file corrupt?");
                }
            }

            RM::ref<luaSupport::StateFreeTable> getUserdata() { return userdata; }

        protected:
            btDefaultCollisionConfiguration collisionConfiguration;
            btCollisionDispatcher dispatcher;
            btDbvtBroadphase overlappingPairCache;
            btSequentialImpulseConstraintSolver solver;
            btDiscreteDynamicsWorld dynamicsWorld;
            PhysicsDebugRenderer debugRenderer;

            TerrainProvider * terrainProvider;

            boost::shared_mutex chunkMutex;

            boost::random::random_device dynamicEntityIDGenerator;
            boost::random::uniform_int_distribution<uint64_t> dynamicEntityIDDistrobution;

            std::vector<RM::ref<BlockController>> blockControllersByID;
            std::map<std::string, std::pair<BlockController*, uint16_t>> blockControllersAndIDsByName;

            std::unordered_map<uint64_t, TerrainChunk*> chunks;
            std::vector<TerrainChunk*> chunksToUpdate;

            friend DynamicEntity;
            boost::mutex dynamicEntityMutex;
            std::unordered_map<uint64_t, DynamicEntity*> loadedDynamicEntities;
            std::unordered_map<uint64_t, DynamicEntity*> forceLoadEntities;

            inline void addDynamicEntity(DynamicEntity * entity)
            {
                boost::mutex::scoped_lock lock(dynamicEntityMutex);
                loadedDynamicEntities[entity->getID()] = entity;

                if (entity->isForceLoad())
                {
                    forceLoadEntities[entity->getID()] = entity; //FIXME needs to be removed from the list when unloaded.
                    entity->incRef();
                }
            }

            inline void removeDynamicEntity(DynamicEntity * entity)
            {
                boost::mutex::scoped_lock lock(dynamicEntityMutex);
                loadedDynamicEntities.erase(entity->getID());
                if (entity->isForceLoad())
                {
                    forceLoadEntities.erase(entity->getID());
                }
            }

            RM::Path folder;
            RM::Path chunksFolder;
            RM::Path dynamicChunksFolder;
            RM::Path terrainFolder;
            RM::Path blockEntitiesFolder;
            RM::Path dynamicEntitiesFolder;

            RM::ref<luaSupport::StateFreeTable> userdata;

            std::string name;
            double time;
            float timescale;

            void writeHeader();
            void readHeader();
            void restoreHeaderBackup();

            void writeBlockList();
            void readBlockList();
            void restoreBlockListBackup();

            void writeDynamicEntityForceLoadList();
            void readDynamicEntityForceLoadList();
            void restoreDynamicEntityForceLoadList();

            void writeTerrain();

        private:
    };
}

#endif // WORLD_H
