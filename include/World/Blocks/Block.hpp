/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#ifndef BLOCK_H
#define BLOCK_H

#include <stdint.h>

namespace World {

    // NO VIRTUAL FUNCTIONS PLEASE!
    // We don't want to be storing the v-pointer when saving this structure to file.
    struct Block
    {
        uint16_t ID;
        uint16_t metadata;
    };

    static_assert(sizeof(Block) == 4, "Block object must be 4 bytes in size!");
}

#endif // BLOCK_H
