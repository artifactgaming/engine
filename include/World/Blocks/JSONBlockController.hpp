/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#ifndef JSONBLOCKCONTROLLER_H
#define JSONBLOCKCONTROLLER_H

#include "BlockController.hpp"
#include "Path.hpp"

#include <sol.hpp>

#include "JSONControllerCore.hpp"

namespace World
{
    class JSONBlockController: public BlockController, public JSONControllerCore
    {
        public:
            JSONBlockController(RM::ResourceGroup * group, RM::Path jsonFile);
            JSONBlockController(RM::Path jsonFile);
            virtual ~JSONBlockController();

            virtual unsigned getHorizontalRotation(Block block);
            virtual unsigned getVerticalRotation(Block block);
            virtual Shape getShape(Block block);

            virtual void renderToTerrain(Block block, BlockEntity * entity, long x, long y, long z, TerrainVisualMesh * mesh);

        protected:
        private:

            void loadJSON(Json::Value & root, Json::Reader & reader);

            void loadMetaManipulators(Json::Value meta);
            static unsigned int getBitIndex(char bitHex);

            bool useDefaultRenderer;

            MetaManipulator * vRotation;
            MetaManipulator * hRotation;
            MetaManipulator * shape;
            Shape defaultShape;
    };
}

#endif // JSONBLOCKCONTROLLER_H
