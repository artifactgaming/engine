/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#ifndef BLOCKCONTROLLER_H
#define BLOCKCONTROLLER_H

#include <sol.hpp>
#include <json/json.h>

#include "Block.hpp"
#include "BlockEntity.hpp"
#include <map>

#include "BasicResource.hpp"
#include "TerrainVisualMesh.hpp"

namespace World {

    class BlockController: public RM::BasicResource
    {
        public:
            BlockController(RM::ResourceGroup * group, const std::string & name);
            virtual ~BlockController();

            enum Side
            {
                North = 0,
                East = 1,
                South = 2,
                West = 3,
                Up = 4,
                Down = 5
            };

            enum Shape
            {
                Cube = 0,
                Slope = 1,
                Corner = 2,
                InvertedCorner = 3,
                Pyramid = 4,
                Cylinder = 6,
                Sphere = 7,
                Bullet = 8
                // 9-15 are unused but reserved.
            };

            Block buildBlock(uint16_t ID);

            virtual void renderToTerrain(Block block, BlockEntity * entity, long x, long y, long z, TerrainVisualMesh * mesh) = 0;

            inline bool isOpaque(Block block, Side side) { return false; }
            inline bool hasEntity() { return bhasEntity; }

            Block setMetaValue(Block block, std::string name, uint16_t value);
            uint16_t getMetaValue(Block block, std::string name);

            virtual unsigned getHorizontalRotation(Block block) = 0;
            virtual unsigned getVerticalRotation(Block block) = 0;
            virtual Shape getShape(Block block) = 0;

        protected:
            std::string itemName;
            uint16_t defaultMeta;
            bool bhasEntity;

            bool bisDirectional;

            // Used to figure out what side is which in absolute coordinate no matter what direction the block is facing.
            static constexpr Side RotationTransformations[4][6] =
            {
                { // Vertical: 0
                    North, // True North
                    East,  // True East
                    South, // True South
                    West,  // True West
                    Up,    // True Up
                    Down   // True Down
                },
                { // Vertical: 90
                    Down,  // True North
                    East,  // True East
                    Up,    // True South
                    West,  // True West
                    North, // True Up
                    South  // True Down
                },
                { // Vertical: 180
                    South, // True North
                    East,  // True East
                    North, // True South
                    West,  // True West
                    Down,  // True Up
                    Up     // True Down
                },
                { // Vertical: 270
                    Up,    // True North
                    East,  // True East
                    Down,  // True South
                    West,  // True West
                    South, // True Up
                    North  // True Down
                }
            };

            inline Side getTrueSide(Block block, Side side)
            {
                const Side * sideBatch = RotationTransformations[getVerticalRotation(block)];

                switch (side)
                {
                case Side::North:
                case Side::East:
                case Side::South:
                case Side::West:
                    return sideBatch[(side + getHorizontalRotation(block)) & 0xF];
                default:
                    return sideBatch[side];
                }
            }

            struct MetaManipulator
            {
                MetaManipulator(uint16_t shift, uint16_t mask):
                    shift(shift),
                    mask(mask)
                {

                }

                uint16_t shift;
                uint16_t mask;
            };

            std::map<std::string, MetaManipulator> metaDataManipulators;

            Block setMetaValue(Block block, MetaManipulator * manipulator, uint16_t value);
            uint16_t getMetaValue(Block block, MetaManipulator * manipulator);

        private:
    };
}

#endif // BLOCKCONTROLLER_H
