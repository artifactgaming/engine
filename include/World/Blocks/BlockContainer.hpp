/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#ifndef BLOCKCONTAINER_H
#define BLOCKCONTAINER_H

#include "BlockEntity.hpp"
#include "Block.hpp"

#include <stdint.h>
#include <unordered_map>

#include "View.hpp"
#include "ShaderProgram.hpp"

#include "TerrainVisualMesh.hpp"

#define CHUNK_SIZE 16

namespace World {
    class BlockContainer
    {
        public:
            BlockContainer(std::string name, int16_t x, int16_t y, int16_t z);
            virtual ~BlockContainer();

            inline bool isTerrainLoaded() {
                return blocks != nullptr && terrainGenerated;
            }

            virtual bool isPhysicsLoaded() = 0;
            bool isGraphicsLoaded() { return mesh != nullptr; }
            virtual bool isDynamicEntitiesLoaded() = 0;

            inline int16_t getX() { return x; }
            inline int16_t getY() { return y; }
            inline int16_t getZ() { return z; }

            virtual void update(float timedelta, double time) = 0;
            virtual void renderTerrain(GL::View & view, GL::ShaderProgramScope & scope) = 0;

            inline BlockEntity * getBlockEntity(Block * block) {
                std::unordered_map<uint16_t, BlockEntity*>::iterator result = blockEntities.find(block - blocks);
                if (result != blockEntities.end()) {
                    return result->second;
                } else {
                    return nullptr;
                }
            }

            inline void localizeCooridnate(int32_t global_x, int32_t global_y, int32_t global_z,
                                           uint8_t & local_x, uint8_t  & local_y, uint8_t  & local_z)
            {

                local_x = global_x & 0x000F;
                uint8_t shift = local_x >> 8;
                local_x = ((0xF0 & shift) | local_x) + (16 & shift);

                local_y = global_y & 0x000F;
                shift = local_y >> 8;
                local_y = ((0xF0 & shift) | local_y) + (16 & shift);

                local_z = global_z & 0x000F;
                shift = local_z >> 8;
                local_z = ((0xF0 & shift) | local_z) + (16 & shift);

                /* // This is an example of the easier to read but less optimized version of this algorithm.
                // First grab only first 16 bits.
                local_x = global_x & 0x000F;
                if (global_x < 0) {
                    // Convert to positive coordinate.
                    // This is the equivalent of a subtraction.
                    local_x = 16 + (0xF0 | local_x);
                }*/
            }

            inline Block* getBlockLocal_unsafe(uint8_t x, uint8_t y, uint8_t z) {
                if (blocks) {
                    return indexBlockArray(x, y, z);
                } else {
                    return nullptr;
                }
            }

            inline Block* getBlockGlobal_unsafe(int32_t global_x, int32_t global_y, int32_t global_z) {

                uint8_t local_x;
                uint8_t local_y;
                uint8_t local_z;

                localizeCooridnate(global_x, global_y, global_z, local_x, local_y, local_z);
                return getBlockLocal_unsafe(local_x, local_y, local_z);
            }

            inline Block* getBlockLocal_safe(uint8_t x, uint8_t y, uint8_t z) {
                if (blocks) {
                    return indexBlockArray(x, y, z);
                } else {
                    initializeBlockArray();
                    return indexBlockArray(x, y, z);
                }
            }

            inline Block* getBlockGlobal_safe(int32_t global_x, int32_t global_y, int32_t global_z) {

                uint8_t local_x;
                uint8_t local_y;
                uint8_t local_z;

                localizeCooridnate(global_x, global_y, global_z, local_x, local_y, local_z);
                return getBlockLocal_safe(local_x, local_y, local_z);
            }

            inline void setBlockLocal_unsafe(uint8_t x, uint8_t y, uint8_t z, Block block)
            {
                Block * block_ptr = indexBlockArray(x, y, z);
                *block_ptr = block;
            }

            inline void setBlockLocal_safe(uint8_t x, uint8_t y, uint8_t z, Block block)
            {
                if (blocks)
                {
                    Block * block_ptr = indexBlockArray(x, y, z);
                    *block_ptr = block;
                }
                else
                {
                    initializeBlockArray();
                    Block * block_ptr = indexBlockArray(x, y, z);
                    *block_ptr = block;
                }
            }

            inline void setBlockGlobal_unsafe(int32_t global_x, int32_t global_y, int32_t global_z, Block block) {
                uint8_t local_x;
                uint8_t local_y;
                uint8_t local_z;

                if (blocks)
                {
                    localizeCooridnate(global_x, global_y, global_z, local_x, local_y, local_z);
                    setBlockLocal_unsafe(local_x, local_y, local_z, block);
                }
            }

            inline void setBlockGlobal_safe(int32_t global_x, int32_t global_y, int32_t global_z, Block block) {
                uint8_t local_x;
                uint8_t local_y;
                uint8_t local_z;

                localizeCooridnate(global_x, global_y, global_z, local_x, local_y, local_z);
                setBlockLocal_safe(local_x, local_y, local_z, block);
            }

            void saveTerrainToFile() noexcept;
            bool loadTerrainFromFile() noexcept;

            void setTerrainGenerated() { terrainGenerated = true; }

            inline void forEach(std::function<void(long x, long y, long z, Block * block)> func)
            {
                long globalX = this->x * 16;
                long globalY = this->y * 16;
                long globalZ = this->z * 16;

                Block * block = blocks;
                Block * end = blocks + block_array_length;
                while (block != end)
                {
                    unsigned int index = end - block;
                    char y =  index & 0x00F;
                    char x = (index & 0x0F0) >> 4;
                    char z = (index & 0xF00) >> 8;

                    func(globalX + x, globalY + y, globalZ + z, block);
                    block++;
                }
            }

            static const size_t block_array_length = CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE;

        protected:
            void queueForStaticMeshUpdate()
            {
                if (mesh)
                {
                    mesh->queueForUpdate();
                }
            }

            std::unordered_map<uint16_t, BlockEntity*> blockEntities;
            Block * blocks;
            RM::Path file;

            TerrainVisualMesh * mesh;

            // Limits world size to 65536^3 chunks in size. That's flippin massive.
            int16_t x;
            int16_t y;
            int16_t z;

            bool terrainGenerated;

            virtual void initializeBlockArray();

            inline Block* indexBlockArray(uint8_t x, uint8_t y, uint8_t z)
            {
                Block * address = blocks + CHUNK_SIZE * CHUNK_SIZE * z + CHUNK_SIZE * x + y;

                if (address < (blocks + block_array_length))
                {
                    return address;
                }
                else
                {
                    throw std::runtime_error("Attempted to index block outside of chunk.");
                }
            }

            void restoreBackup();
        private:
    };
}

#endif // BLOCKCONTAINER_H
