/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#ifndef LOADREGION_H
#define LOADREGION_H

#include <stdint.h>

class LoadRegion
{
    public:
        LoadRegion(int16_t min_x, int16_t min_y, int16_t min_z, int16_t max_x, int16_t max_y, int16_t max_z);
        virtual ~LoadRegion();

        inline bool isInRegion(int16_t x, int16_t y, int16_t z) {
            return ((min_x <= x) && (x <= max_x))
                && ((min_y <= y) && (y <= max_y))
                && ((min_z <= z) && (z <= max_z));
        }

        inline int16_t getMinX() { return min_x; }
        inline int16_t getMinY() { return min_y; }
        inline int16_t getMinZ() { return min_z; }

        inline int16_t getMaxX() { return max_x; }
        inline int16_t getMaxY() { return max_y; }
        inline int16_t getMaxZ() { return max_z; }
    protected:
        int16_t min_x;
        int16_t min_y;
        int16_t min_z;

        int16_t max_x;
        int16_t max_y;
        int16_t max_z;
    private:
};

#endif // LOADREGION_H
