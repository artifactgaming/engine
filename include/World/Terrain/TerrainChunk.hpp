/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#ifndef TERRAINCHUNK_H
#define TERRAINCHUNK_H

#include <vector>
#include <glm/glm.hpp>
#include <btBulletDynamicsCommon.h>

// #include "World.hpp"
// #include "TerrainProvider.hpp"
#include "BlockContainer.hpp"
#include "Block.hpp"

namespace World {
    class World;
    class TerrainProvider;
    class TerrainVisualRef;

    class TerrainChunk: public BlockContainer
    {
        public:
            TerrainChunk(int16_t x, int16_t y, int16_t z, World * world, TerrainProvider * terrainProvider);
            virtual ~TerrainChunk();

            inline uint64_t getHashCode() { return hashCode; };

            bool isPhysicsLoaded();
            bool isGraphicsLoaded();
            bool isTileEntitiesLoaded();
            bool isDynamicEntitiesLoaded();

            void update(float timedelta, double time);
            void renderTerrain(GL::View & view, GL::ShaderProgramScope & scope);

            static inline uint64_t generateHashCode(int16_t x, int16_t y, int16_t z) {
                // This code works, and is easier to understand, which is why I kept it here in a comment.
                /*uint64_t hashCode = static_cast<uint16_t>(z);
                hashCode <<= 16;
                hashCode |= static_cast<uint16_t>(y);
                hashCode <<= 16;
                hashCode |= static_cast<uint16_t>(x);
                return hashCode;*/

                // This code is better optimized for the job though.
                uint16_t a[4] = {0, static_cast<uint16_t>(x), static_cast<uint16_t>(y), static_cast<uint16_t>(z)};
                uint64_t res;
                memcpy(&res, a, sizeof(res));

                return res;
            }

            static inline void reverseHashCode(uint64_t hashCode, int16_t & x, int16_t & y, int16_t & z) {
                uint16_t a[4];
                memcpy(a, &hashCode, sizeof(hashCode));

                //Zero is unused, so we skipped that.
                x = a[1];
                y = a[2];
                z = a[3];
            }

            void initializeBlockArray();

            inline int16_t getNativeX() { return x; }
            inline int16_t getNativeY() { return y; }
            inline int16_t getNativeZ() { return z; }

            inline uint16_t getNativeX_unsigned() { return x; }
            inline uint16_t getNativeY_unsigned() { return y; }
            inline uint16_t getNativeZ_unsigned() { return z; }

            void saveTerrain();

            inline World * getWorld()
            {
                return world;
            }

            TerrainVisualRef getVisualRef();

        protected:

            friend TerrainProvider;

            World * world;
            TerrainProvider * terrainProvider;

            uint64_t hashCode;

            uint16_t x;
            uint16_t y;
            uint16_t z;

            friend class TerrainRef;
            friend class TerrainVisualRef;
            unsigned int blockRefs;
            unsigned int visualRefs;

            void incBlockRef();
            void decBlockRef();

            void incVisualRef();
            void decVisualRef();

        private:
    };

    // Use to have your object keep terrain loaded.
    class TerrainRef
    {
    public:
    /// Create a ``nullptr``-valued reference
    TerrainRef() { chunk = nullptr; }

    /// Construct a reference from a pointer
    TerrainRef(TerrainChunk * chunk) : chunk(chunk) {
        if (chunk)
        {
            chunk->incBlockRef();
        }
    }

    /// Copy constructor
    TerrainRef(const TerrainRef & r) : chunk(r.chunk) {
        if (chunk)
        {
            chunk->incBlockRef();
        }
    }

    /// Move constructor
    TerrainRef(TerrainRef && r) noexcept : chunk(r.chunk) {
        r.chunk = nullptr;
    }

    /// Destroy this reference
    ~TerrainRef() {
        if (chunk)
            chunk->decBlockRef();
    }

    /// Move another reference into the current one
    TerrainRef& operator=(TerrainRef && r) noexcept {
        if (&r != this) {
            if (chunk)
            {
                chunk->decBlockRef();
            }
            chunk = r.chunk;
            r.chunk = nullptr;
        }
        return *this;
    }

    /// Overwrite this reference with another reference
    TerrainRef& operator=(const TerrainRef & r) noexcept {
        if (chunk != r.chunk) {
            if (r.chunk)
            {
                r.chunk->incBlockRef();
            }

            if (chunk)
            {
                chunk->decBlockRef();
            }

            chunk = r.chunk;
        }
        return *this;
    }

    /// Overwrite this reference with a pointer to another object
    TerrainRef& operator=(TerrainChunk * chunk) noexcept {
        if (this->chunk != chunk) {
            if (chunk)
            {
                chunk->incBlockRef();
            }

            if (this->chunk)
            {
                this->chunk->decBlockRef();
            }

            this->chunk = chunk;
        }
        return *this;
    }

    bool operator==(const TerrainRef &r) const { return chunk == r.chunk; }
    bool operator!=(const TerrainRef &r) const { return chunk != r.chunk; }
    bool operator==(const TerrainChunk * chunk) const { return this->chunk == chunk; }
    bool operator!=(const TerrainChunk * chunk) const { return this->chunk != chunk; }
    TerrainChunk * operator->() { return chunk; }
    const TerrainChunk * operator->() const { return chunk; }
    TerrainChunk & operator*() { return *chunk; }
    const TerrainChunk& operator*() const { return *chunk; }
    operator TerrainChunk * () { return chunk; }
    TerrainChunk * get() { return chunk; }
    const TerrainChunk * get() const { return chunk; }
    operator bool() const { return chunk != nullptr; }
    protected:
    private:
        TerrainChunk * chunk;
    };

    class TerrainVisualRef
    {
    public:
        TerrainVisualRef(TerrainChunk * chunk) : chunk(chunk) {
            if (chunk)
            {
                chunk->incVisualRef();
            }
        }

        TerrainVisualRef(const TerrainVisualRef & r) : chunk(r.chunk) {
            if (chunk)
            {
                chunk->incVisualRef();
            }
        }

        TerrainVisualRef(TerrainVisualRef && r) noexcept : chunk(r.chunk) {
            r.chunk = nullptr;
        }

        ~TerrainVisualRef() {
            if (chunk)
            {
                chunk->decVisualRef();
            }
        }

    protected:
    private:
        TerrainChunk * chunk;
    };
}

#endif // TERRAINCHUNK_H
