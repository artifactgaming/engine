/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#ifndef TERRAINVISUALMESH_H
#define TERRAINVISUALMESH_H

#include "VertexBuffer.hpp"
#include "VertexBufferArray.hpp"
#include "View.hpp"

//#include "World.hpp"
//#include "BlockContainer.hpp"

#include <glm/glm.hpp>
#include <math.h>

#include "AsyncResource.hpp"

namespace World
{
    class World;
    class BlockContainer;

    class TerrainVisualMesh: public RM::AsyncResource
    {
        public:
            TerrainVisualMesh(std::string name, World * world, BlockContainer * blockContainer);
            virtual ~TerrainVisualMesh();

            inline void queueForUpdate()
            {
                startLoad();
            }

            void async_load();
            void sync_load();

            void addFace(glm::vec3 v0, glm::vec3 v1, glm::vec3 v2);

            void render(GL::View & view, GL::ShaderProgramScope & scope);

        protected:

            struct Face
            {
                Face(
                    unsigned int textureIndex,
                    glm::vec3 v0,
                    glm::vec3 v1,
                    glm::vec3 v2
                ):
                    textureIndex(textureIndex),
                    v0(v0),
                    v1(v1),
                    v2(v2)
                {

                }

                unsigned int textureIndex;

                glm::vec3 v0;
                glm::vec3 v1;
                glm::vec3 v2;
            };

            BlockContainer * blockContainer;
            World * world;
            std::vector<Face> faces;

            GL::VertexBuffer3D vertices;
            GL::VertexBuffer3D textureCoords;
            GL::VertexBuffer3D normals;
            GL::VertexBuffer3D tangents;
            GL::VertexBuffer3D bitangents;

            std::unordered_map<GL::ShaderProgram*, GL::VertexBufferArray> bufferArrays;

            bool loaded;

        private:
    };
}

#endif // TERRAINVISUALMESH_H
