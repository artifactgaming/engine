/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#ifndef PATH_H
#define PATH_H

#include <vector>

#include <boost/filesystem.hpp>
#include <sol.hpp>

namespace RM {
    class Path
    {
        public:
            Path();
            Path(const char * path);
            Path(std::string path);
            Path(const Path & path);
            virtual ~Path();

            Path operator/(const Path & that) const;
            Path extendString(const std::string & that) const;

            Path operator+(const Path & that) const;
            Path appendString(const std::string & that) const;

            std::string string() const;
            bool operator==(const Path & that) const;
            bool operator==(const std::string that) const;

            Path& operator=(const Path & path);

            std::string getExtension() const;
            void setExtension(const std::string ext);

            std::string getName() const;

            Path getParent() const;
            Path getAbsolute() const;
            Path getRelative(const Path & path) const;

            bool copyTo(Path & target) const;
            bool mkdirs();
            bool exists() const;
            size_t size() const;
            bool isDirectory() const;
            bool isEmpty() const;
            bool isRegularFile() const;
            size_t remove();
            bool contains(const Path & path) const;

            bool move(std::string newPath);
            bool move(Path newPath);

            sol::table listContents(sol::this_state lua) const;
            void listContents(std::vector<Path> * contents) const;

            bool isLegal() const;

            static Path root();
            static Path current(sol::this_state lua);

            void createFile();

            boost::filesystem::path getBoostPath() const;
            std::shared_ptr<boost::filesystem::fstream> open(const std::string & mode);

        protected:

        private:
            static boost::filesystem::path sroot;
            boost::filesystem::path bpath;

            bool isLegal(const boost::filesystem::path path) const;

            Path(boost::filesystem::path path);
    };
}

#endif // PATH_H
