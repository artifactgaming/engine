/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#ifndef RESOURCEGROUP_H
#define RESOURCEGROUP_H

#include "BasicResource.hpp"
#include <vector>

namespace RM {
    class BasicResource;
    class AsyncResource;

    class ResourceGroup : public RM::GCObject
    {
        public:
            ResourceGroup(std::string name);
            virtual ~ResourceGroup();

            void setProgressCallback(std::function<void(unsigned int)> onProgress);
            void setCompletionCallback(std::function<void(void)> onCompletion);

            unsigned int getStepsToLoad();
            unsigned int getStepsCompleted();

            std::string getName();

            size_t getNumMembers();
            std::vector<BasicResource*> & getMembers();

            template<typename ResourceClass, typename... Args>
            ResourceClass* add(const Args&... args) {
                return new ResourceClass(this, args...);
            }

        protected:
            friend BasicResource;
            friend AsyncResource;

            bool onProgressSet;
            bool onCompletionSet;

            void progress(unsigned int progress);

            std::function<void(unsigned int)> onProgress;
            std::function<void(void)> onCompletion;

            void addMember(BasicResource * member);
            void removeMember(BasicResource * member);

            std::vector<BasicResource*> members;
            std::string name;

            unsigned int stepsCompleted;
            unsigned int stepsToLoad;

        private:
    };
}

#endif // RESOURCEGROUP_H
