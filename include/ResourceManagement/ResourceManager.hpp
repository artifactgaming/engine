/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#ifndef RESOURCEMANAGER_H
#define RESOURCEMANAGER_H

#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/barrier.hpp>

#include <queue>
#include <deque>
#include <vector>
#include <map>

#include <sol.hpp>

#include "BasicResource.hpp"
#include "AsyncResource.hpp"
#include "ResourceGroup.hpp"

namespace RM {
    class AsyncResource;

    class ResourceManager
    {
        public:
            ResourceManager();
            virtual ~ResourceManager();

            void start();

            void registerEnvironment(const std::string name, const std::function<sol::environment(sol::state_view)> loader);
            void removeEnvironment(const std::string name);
            sol::state_view getEnvironment(const std::string envName, sol::environment & env);
            sol::state_view getStateOnly();

            void run();

            size_t getNumResources();
            void printResourceStats();

            size_t getNumGroups();
            void printGroupStats();

            void printLuaEnvironments();

            int getLuaMemoryUsage();

            void syncronusUpdate();
            void shutdown();

        protected:
        private:
            friend BasicResource;
            friend AsyncResource;
            friend ResourceGroup;

            void readyForSyncUpdate(AsyncResource * resource);

            struct ThreadLuaState
            {
                sol::state lua;
                std::map<std::string, sol::environment> envs;
                boost::mutex mutex;
            };

            void pushAsyncResource(AsyncResource * resource);
            RM::ref<AsyncResource> popAsyncResource();

            std::queue<RM::ref<AsyncResource>> queue;
            std::vector<boost::thread> threads;

            std::queue<RM::ref<AsyncResource>> syncUpdateQueue;

            std::deque<BasicResource*> resources;
            std::deque<ResourceGroup*> groups;

            boost::barrier initBarrier;
            boost::shared_mutex stateMutex;
            std::map<boost::thread::id, ThreadLuaState> luaStates;
            std::map<std::string, std::function<sol::environment(sol::state_view)>> envLoaders;

            ThreadLuaState & internalGetState(const boost::thread::id & id);
            std::function<sol::environment(sol::state_view)> getEnvLoader(std::string name);

            boost::mutex syncUpdateMutex;
            boost::mutex mutex;
            boost::condition_variable cond;

            bool alive;
    };
}

#endif // RESOURCEMANAGER_H
