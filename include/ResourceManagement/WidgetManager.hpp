/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#ifndef WIDGETMANAGER_H
#define WIDGETMANAGER_H

#include <queue>

#include "GCObject.hpp"
#include "nanogui.h"

namespace RM
{
    class WidgetManager
    {
        public:
            WidgetManager();
            virtual ~WidgetManager();

            void queueWidgetToDispose(RM::ref<nanogui::Widget> widget);

            virtual nanogui::Screen * getScreen() = 0;

        protected:

            void disposeWidgets();

        private:

            std::queue<RM::ref<nanogui::Widget>> widgetsToDispose;
    };
}

#endif // WIDGETMANAGER_H
