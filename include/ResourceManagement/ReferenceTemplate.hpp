/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#ifndef REFERENCETEMPLATE_H
#define REFERENCETEMPLATE_H

namespace ReferenceTemplateDetail
{
    typedef void(*Func)(void * obj);
}

template<class T, ReferenceTemplateDetail::Func incRef, ReferenceTemplateDetail::Func decRef>
class ReferenceTemplate
{
protected:
public:
    /// Create a ``nullptr``-valued reference
    ReferenceTemplate() { obj = nullptr; }

    /// Construct a reference from a pointer
    ReferenceTemplate(T * obj) : obj(obj) {
        if (obj)
        {
            incRef(obj);
        }
    }

    /// Copy constructor
    ReferenceTemplate(const ReferenceTemplate & r) : obj(r.obj) {
        if (obj)
        {
            incRef(obj);
        }
    }

    /// Move constructor
    ReferenceTemplate(ReferenceTemplate && r) noexcept : obj(r.obj) {
        r.obj = nullptr;
    }

    /// Destroy this reference
    ~ReferenceTemplate() {
        if (obj)
            decRef(obj);
    }

    /// Move another reference into the current one
    ReferenceTemplate& operator=(ReferenceTemplate && r) noexcept {
        if (&r != this) {
            if (obj)
            {
                decRef(obj);
            }
            obj = r.obj;
            r.obj = nullptr;
        }
        return *this;
    }

    /// Overwrite this reference with another reference
    ReferenceTemplate& operator=(const ReferenceTemplate & r) noexcept {
        if (obj != r.obj) {
            if (r.obj)
            {
                incRef(r.obj);
            }

            if (obj)
            {
                decRef(obj);
            }

            obj = r.obj;
        }
        return *this;
    }

    /// Overwrite this reference with a pointer to another object
    ReferenceTemplate& operator=(T * obj) noexcept {
        if (this->obj != obj) {
            if (obj)
            {
                incRef(obj);
            }

            if (this->obj)
            {
                decRef(this->obj);
            }

            this->obj = obj;
        }
        return *this;
    }

    bool operator==(const ReferenceTemplate &r) const { return obj == r.obj; }
    bool operator!=(const ReferenceTemplate &r) const { return obj != r.obj; }
    bool operator==(const T * obj) const { return this->obj == obj; }
    bool operator!=(const T * obj) const { return this->obj != obj; }
    T * operator->() { return obj; }
    const T * operator->() const { return obj; }
    T & operator*() { return *obj; }
    const T& operator*() const { return *obj; }
    operator T * () { return obj; }
    T * get() { return obj; }
    const T * get() const { return obj; }
    operator bool() const { return obj != nullptr; }

    private:
        T * obj;
};

#endif // REFERENCETEMPLATE_H
