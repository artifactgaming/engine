/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#ifndef GUIBUILDER_H
#define GUIBUILDER_H

#include <nanogui/nanogui.h>
#include <json/json.h>
#include <queue>

#include "BasicResource.hpp"
#include "Path.hpp"

namespace RM {
    class JsonGUI: public BasicResource {
        public:
            JsonGUI(ResourceGroup * group, nanogui::Widget * parent, sol::state_view & lua, const sol::table & args, const Path & path);
            virtual ~JsonGUI();

            sol::environment & getENV() { return env; }

        protected:
            inline sol::protected_function loadScript(Json::Value & scriptVal, Json::Reader & reader);

            void readBasicWidgetProperties(std::queue<sol::protected_function> & setupScripts, nanogui::Widget * widget, Json::Value & root, Json::Reader & reader);
            void addWidget(std::queue<sol::protected_function> & setupScripts, nanogui::Widget * window, Json::Value & root, Json::Reader & reader);

            void addWindow(std::queue<sol::protected_function> & setupScripts, nanogui::Widget * parent, Json::Value & root, Json::Reader & reader);
            void addButton(std::queue<sol::protected_function> & setupScripts, nanogui::Widget * parent, Json::Value & root, Json::Reader & reader);
            void addPopupButton(std::queue<sol::protected_function> & setupScripts, nanogui::Widget * parent, Json::Value & root, Json::Reader & reader);
            void addCheckBox(std::queue<sol::protected_function> & setupScripts, nanogui::Widget * parent, Json::Value & root, Json::Reader & reader);
            void addColorWheel(std::queue<sol::protected_function> & setupScripts, nanogui::Widget * parent, Json::Value & root, Json::Reader & reader);
            void addRenderTarget(std::queue<sol::protected_function> & setupScripts, nanogui::Widget * parent, Json::Value & root, Json::Reader & reader);
            void addGraph(std::queue<sol::protected_function> & setupScripts, nanogui::Widget * parent, Json::Value & root, Json::Reader & reader);
            void addImagePanel(std::queue<sol::protected_function> & setupScripts, nanogui::Widget * parent, Json::Value & root, Json::Reader & reader);
            void addImageView(std::queue<sol::protected_function> & setupScripts, nanogui::Widget * parent, Json::Value & root, Json::Reader & reader);
            void addLabel(std::queue<sol::protected_function> & setupScripts, nanogui::Widget * parent, Json::Value & root, Json::Reader & reader);
            void addProgressBar(std::queue<sol::protected_function> & setupScripts, nanogui::Widget * parent, Json::Value & root, Json::Reader & reader);
            void addSlider(std::queue<sol::protected_function> & setupScripts, nanogui::Widget * parent, Json::Value & root, Json::Reader & reader);
            void addStackedWidget(std::queue<sol::protected_function> & setupScripts, nanogui::Widget * parent, Json::Value & root, Json::Reader & reader);
            void addTabHeader(std::queue<sol::protected_function> & setupScripts, nanogui::Widget * parent, Json::Value & root, Json::Reader & reader);
            void addTabWidget(std::queue<sol::protected_function> & setupScripts, nanogui::Widget * parent, Json::Value & root, Json::Reader & reader);
            void addTextBox(std::queue<sol::protected_function> & setupScripts, nanogui::Widget * parent, Json::Value & root, Json::Reader & reader);
            void addFloatBox(std::queue<sol::protected_function> & setupScripts, nanogui::Widget * parent, Json::Value & root, Json::Reader & reader);
            void addIntBox(std::queue<sol::protected_function> & setupScripts, nanogui::Widget * parent, Json::Value & root, Json::Reader & reader);
            void addVScrollPanel(std::queue<sol::protected_function> & setupScripts, nanogui::Widget * parent, Json::Value & root, Json::Reader & reader);

            template<typename Widget>
            inline void bindToLua(Widget * parent, Json::Value & root);

            nanogui::Alignment getAlignment(Json::Value value);
            nanogui::Orientation getOrientation(Json::Value value);
            glm::vec4 getColor(Json::Value value);
            void setLayout(nanogui::Widget * widget, Json::Value layout);
            void addWidgetsToWidget(std::queue<sol::protected_function> & setupScripts, nanogui::Widget * widget, Json::Value & children, Json::Reader & reader);

            Path path;
            sol::state_view lua;
            sol::environment env;

            nanogui::Widget * parent;

        private:
    };
}

#endif // GUIBUILDER_H
