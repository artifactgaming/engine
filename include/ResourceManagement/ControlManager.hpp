/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#ifndef CONTROLMANAGER_H
#define CONTROLMANAGER_H

#include <boost/thread/mutex.hpp>
#include <boost/thread/recursive_mutex.hpp>

#include <vector>
#include <map>
#include <unordered_map>
#include <deque>

#include <nanogui/nanogui.h>
#include <glm/glm.hpp>
#include <GLFW/glfw3.h>

#include <nanogui/Listener.h>

#include <boost/filesystem.hpp>

#include <memory>

class GridLocked;

namespace RM {

    enum class KeyboardButtonID {
        Unknown         = GLFW_KEY_UNKNOWN,
        Space           = GLFW_KEY_SPACE,
        Apostrophe      = GLFW_KEY_APOSTROPHE,
        Comma           = GLFW_KEY_COMMA,
        Minus           = GLFW_KEY_MINUS,
        Period          = GLFW_KEY_PERIOD,
        Slash           = GLFW_KEY_SLASH,
        Button_0        = GLFW_KEY_0,
        Button_1        = GLFW_KEY_1,
        Button_2        = GLFW_KEY_2,
        Button_3        = GLFW_KEY_3,
        Button_4        = GLFW_KEY_4,
        Button_5        = GLFW_KEY_5,
        Button_6        = GLFW_KEY_6,
        Button_7        = GLFW_KEY_7,
        Button_8        = GLFW_KEY_8,
        Button_9        = GLFW_KEY_9,
        Semicolon       = GLFW_KEY_SEMICOLON,
        Equal           = GLFW_KEY_EQUAL,
        Button_A        = GLFW_KEY_A,
        Button_B        = GLFW_KEY_B,
        Button_C        = GLFW_KEY_C,
        Button_D        = GLFW_KEY_D,
        Button_E        = GLFW_KEY_E,
        Button_F        = GLFW_KEY_F,
        Button_G        = GLFW_KEY_G,
        Button_H        = GLFW_KEY_H,
        Button_I        = GLFW_KEY_I,
        Button_J        = GLFW_KEY_J,
        Button_K        = GLFW_KEY_K,
        Button_L        = GLFW_KEY_L,
        Button_M        = GLFW_KEY_M,
        Button_N        = GLFW_KEY_N,
        Button_O        = GLFW_KEY_O,
        Button_P        = GLFW_KEY_P,
        Button_Q        = GLFW_KEY_Q,
        Button_R        = GLFW_KEY_R,
        Button_S        = GLFW_KEY_S,
        Button_T        = GLFW_KEY_T,
        Button_U        = GLFW_KEY_U,
        Button_V        = GLFW_KEY_V,
        Button_W        = GLFW_KEY_W,
        Button_X        = GLFW_KEY_X,
        Button_Y        = GLFW_KEY_Y,
        Button_Z        = GLFW_KEY_Z,
        LeftBracket     = GLFW_KEY_LEFT_BRACKET,
        Backslash       = GLFW_KEY_BACKSLASH,
        RightBracket    = GLFW_KEY_RIGHT_BRACKET,
        GraveAccent     = GLFW_KEY_GRAVE_ACCENT,
        World_1         = GLFW_KEY_WORLD_1,
        World_2         = GLFW_KEY_WORLD_2,
        Escape          = GLFW_KEY_ESCAPE,
        Enter           = GLFW_KEY_ENTER,
        Tab             = GLFW_KEY_TAB,
        Backspace       = GLFW_KEY_BACKSPACE,
        Insert          = GLFW_KEY_INSERT,
        Delete          = GLFW_KEY_DELETE,
        Right           = GLFW_KEY_RIGHT,
        Left            = GLFW_KEY_LEFT,
        Down            = GLFW_KEY_DOWN,
        Up              = GLFW_KEY_UP,
        PageUp          = GLFW_KEY_PAGE_UP,
        PageDown        = GLFW_KEY_PAGE_DOWN,
        Home            = GLFW_KEY_HOME,
        End             = GLFW_KEY_END,
        CapsLock        = GLFW_KEY_CAPS_LOCK,
        ScrollLock      = GLFW_KEY_SCROLL_LOCK,
        NumLock         = GLFW_KEY_NUM_LOCK,
        PrintScreen     = GLFW_KEY_PRINT_SCREEN,
        KeyPause        = GLFW_KEY_PAUSE,
        Button_F1       = GLFW_KEY_F1,
        Button_F2       = GLFW_KEY_F2,
        Button_F3       = GLFW_KEY_F3,
        Button_F4       = GLFW_KEY_F4,
        Button_F5       = GLFW_KEY_F5,
        Button_F6       = GLFW_KEY_F6,
        Button_F7       = GLFW_KEY_F7,
        Button_F8       = GLFW_KEY_F8,
        Button_F9       = GLFW_KEY_F9,
        Button_F10      = GLFW_KEY_F10,
        Button_F11      = GLFW_KEY_F11,
        Button_F12      = GLFW_KEY_F12,
        Button_F13      = GLFW_KEY_F13,
        Button_F14      = GLFW_KEY_F14,
        Button_F15      = GLFW_KEY_F15,
        Button_F16      = GLFW_KEY_F16,
        Button_F17      = GLFW_KEY_F17,
        Button_F18      = GLFW_KEY_F18,
        Button_F19      = GLFW_KEY_F19,
        Button_F20      = GLFW_KEY_F20,
        Button_F21      = GLFW_KEY_F21,
        Button_F22      = GLFW_KEY_F22,
        Button_F23      = GLFW_KEY_F23,
        Button_F24      = GLFW_KEY_F24,
        Button_F25      = GLFW_KEY_F25,
        KeyPad_0        = GLFW_KEY_KP_0,
        KeyPad_1        = GLFW_KEY_KP_1,
        KeyPad_2        = GLFW_KEY_KP_2,
        KeyPad_3        = GLFW_KEY_KP_3,
        KeyPad_4        = GLFW_KEY_KP_4,
        KeyPad_5        = GLFW_KEY_KP_5,
        KeyPad_6        = GLFW_KEY_KP_6,
        KeyPad_7        = GLFW_KEY_KP_7,
        KeyPad_8        = GLFW_KEY_KP_8,
        KeyPad_9        = GLFW_KEY_KP_9,
        KeyPad_Decimal  = GLFW_KEY_KP_DECIMAL,
        KeyPad_Divide   = GLFW_KEY_KP_DIVIDE,
        KeyPad_Multiply = GLFW_KEY_KP_MULTIPLY,
        KeyPad_Subtract = GLFW_KEY_KP_SUBTRACT,
        KeyPad_Add      = GLFW_KEY_KP_ADD,
        KeyPad_Enter    = GLFW_KEY_KP_ENTER,
        KeyPad_Equal    = GLFW_KEY_KP_EQUAL,
        LeftShift       = GLFW_KEY_LEFT_SHIFT,
        LeftControl     = GLFW_KEY_LEFT_CONTROL,
        LeftAlt         = GLFW_KEY_LEFT_ALT,
        LeftSuper       = GLFW_KEY_LEFT_SUPER,
        RightShift      = GLFW_KEY_RIGHT_SHIFT,
        RightControl    = GLFW_KEY_RIGHT_CONTROL,
        RightAlt        = GLFW_KEY_RIGHT_ALT,
        RightSuper      = GLFW_KEY_RIGHT_SUPER,
        Menu            = GLFW_KEY_MENU,

        Mouse1          = GLFW_MOUSE_BUTTON_1,
        Mouse2          = GLFW_MOUSE_BUTTON_2,
        Mouse3          = GLFW_MOUSE_BUTTON_3,
        Mouse4          = GLFW_MOUSE_BUTTON_4,
        Mouse5          = GLFW_MOUSE_BUTTON_5,
        Mouse6          = GLFW_MOUSE_BUTTON_6,
        Mouse7          = GLFW_MOUSE_BUTTON_7,
        Mouse8          = GLFW_MOUSE_BUTTON_8,
        MouseLast       = GLFW_MOUSE_BUTTON_LAST,
        MouseLeft       = GLFW_MOUSE_BUTTON_LEFT,
        MouseRight      = GLFW_MOUSE_BUTTON_RIGHT,
        MouseMiddle     = GLFW_MOUSE_BUTTON_MIDDLE
    };

    struct EnumHash {
        template <typename T>
        std::size_t operator()(T t) const
        {
            return static_cast<std::size_t>(t);
        }
    };

    enum class MouseAnalogID {
        Unknown = 0,
        axisX   = 1,
        axisY   = 2,
        scrollH = 3,
        scrollV = 4
    };

    enum class ControlType {
        boolean = 0,
        analog  = 1
    };

    KeyboardButtonID getButtonID(std::string name);
    std::string getButtonName(KeyboardButtonID button);

    class ControlManager : public nanogui::Screen
    {
        friend GridLocked;
        public:
            ControlManager();
            virtual ~ControlManager();

            void updateEditors();

            void newBooleanControl(std::string group, std::string name, bool ignore_focus, KeyboardButtonID defaultButton);
            RM::ref<nanogui::ListenerReference<void(bool pushed)>> addBooleanCallback(std::string group, std::string name, std::function <void(bool pushed)> func);
            void removeBooleanCallback(std::string group, std::string name, RM::ref<nanogui::ListenerReference<void(bool pushed)>> reference);
            void changeBooleanBinding(std::string group, std::string name, KeyboardButtonID newButton);
            void changeBooleanBinding(std::string group, std::string name);

            void newAnalogControl(std::string group, std::string name, bool ignore_focus, MouseAnalogID defaultAnalog);
            RM::ref<nanogui::ListenerReference<void(float delta)>> addAnalogCallback(std::string group, std::string name, std::function <void(float delta)> func);
            void removeAnalogCallback(std::string group, std::string name, RM::ref<nanogui::ListenerReference<void(float delta)>> reference);
            void changeAnalogBinding(std::string group, std::string name, MouseAnalogID newAnalog);
            void changeAnalogBinding(std::string group, std::string name);

            void load();
            void save();

            nanogui::Window * openControlEditor(std::function<void(void)> onShutdown);

        protected:

            struct Control {
                Control(std::string group, std::string name, bool ignore_focus, KeyboardButtonID defaultID) :
                    group(group),
                    name(name),
                    ignore_focus(ignore_focus),
                    boolean_id(defaultID)
                {
                    std::cout << "Registered boolean control \"" << group << ":" << name << "\"." << std::endl;
                    type = ControlType::boolean;
                }

                Control(std::string group, std::string name, bool ignore_focus, MouseAnalogID defaultID) :
                    group(group),
                    name(name),
                    ignore_focus(ignore_focus),
                    analog_id(defaultID)
                {
                    std::cout << "Registered analog control \"" << group << ":" << name << "\"." << std::endl;
                    type = ControlType::analog;
                    sensitivity = 1.0;
                }

                std::string group;
                std::string name;
                ControlType type;

                bool ignore_focus;
                float sensitivity;

                union {
                    KeyboardButtonID boolean_id;
                    MouseAnalogID    analog_id;
                };

                nanogui::ListenerQueue<void(bool pushed)> booleanFunc;
                nanogui::ListenerQueue<void(float delta)> analogFunc;
            };

            virtual bool mouseMotionEvent(const glm::ivec2 &p, const glm::ivec2 &rel, int button, int modifiers) override;
            virtual bool mouseButtonEvent(const glm::ivec2 &p, int button, bool down, int modifiers) override;
            virtual bool keyboardEvent(int key, int scancode, int action, int modifiers) override;
            virtual bool scrollEvent(const glm::ivec2 &p, const glm::vec2 &rel) override;

            void takeScreenshot();
            void center();

            int getFramerate() { return mode->refreshRate; }

            static const boost::filesystem::path screenshotDirectory;
            boost::recursive_mutex mutex;
            std::vector<std::shared_ptr<Control>>                                       controls;
            std::map<std::string,           std::shared_ptr<Control>>                   namedControls;
            std::unordered_map<KeyboardButtonID,    std::shared_ptr<Control>, EnumHash> idedBoolControls;
            std::unordered_map<MouseAnalogID,       std::shared_ptr<Control>, EnumHash> idedAnalogControls;

            GLFWmonitor * monitor;
            const GLFWvidmode * mode;

            nanogui::Window * controlAssigner;
            std::shared_ptr<Control> toAssign;

            class ControlEditor: public nanogui::Window {
            public:
                ControlEditor(nanogui::Widget * parent, ControlManager * manager, std::function<void(void)> onShutdown);
                virtual ~ControlEditor();

                void update();

                virtual void dispose();

                struct ControlWidget : nanogui::Widget {
                    ControlWidget(nanogui::Widget * parent, ControlManager * manager, std::shared_ptr<ControlManager::Control> control, std::string group, std::string name);
                    ~ControlWidget();

                    std::string group;
                    std::string name;

                    nanogui::Label * label;
                    nanogui::PopupButton * edit;
                    nanogui::TextBox * textbox;

                    std::shared_ptr<ControlManager::Control> control;
                    ControlManager * manager;
                };

                std::map<std::string, nanogui::Widget*> tabs;
                std::map<std::string, ControlWidget*> controls;
                std::deque<nanogui::PopupButton*> editButtons;

            protected:
                std::function<void(void)> onShutdown;
                ControlManager * manager;
                nanogui::TabWidget * tabHeader;
            };

            std::deque<ControlEditor*> editors;

        private:
            static const unsigned int defaultWidth;
            static const unsigned int defaultHeight;
            static const char defaultWindowTitle[];
            static const bool defaultResizable;
            static const bool defaultFullscreen;
            static const int defaultColorBits;
            static const int defaultAlphaBits;
            static const int defaultDepthBits;
            static const int defaultStencilBits;
            static const int default_nSamples;
            static const unsigned int defaultGlMajor;
            static const unsigned int defaultGlMinor;
    };
}

#endif // CONTROLMANAGER_H
