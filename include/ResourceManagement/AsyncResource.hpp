/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#ifndef ASYNCRESOURCE_H
#define ASYNCRESOURCE_H

#include "ResourceManager.hpp"
#include "BasicResource.hpp"
#include "ResourceGroup.hpp"

#include <boost/thread/thread.hpp>
#include <boost/thread/recursive_mutex.hpp>

#include <sol.hpp>

namespace RM {
    class ResourceManager;
    class ResourceGroup;

    class AsyncResource: public BasicResource
    {
        public:
            AsyncResource(std::string name, std::string type, ResourceGroup * group);
            virtual ~AsyncResource();

            virtual void async_load() = 0;
            virtual void sync_load() = 0;

        protected:
            friend ResourceManager;

            virtual void setResourceStepsToLoad(unsigned int number);
            virtual void incrementResourceProgress(int number);

            void syncUpdate();

            void startLoad();

            ResourceManager * manager;

            boost::recursive_mutex mutex;
            boost::condition_variable cond;

        private:
            unsigned int progress;
            bool readyForUpdate;
            bool added;
    };
}

#endif // ASYNCRESOURCE_H
