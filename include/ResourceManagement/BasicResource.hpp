/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#ifndef BASICRESOURCE_H
#define BASICRESOURCE_H

#include <cstddef>
#include <string>

#include "GCObject.hpp"
#include <nanogui/screen.h>

#include "ResourceGroup.hpp"
#include <iostream>

namespace RM {
    class ResourceGroup;

    class BasicResource: public RM::GCObject
    {
        public:
            BasicResource(std::string name, std::string type, ResourceGroup * group);
            virtual ~BasicResource();

            std::string resourceName();
            std::string resourceType();

            void setProgressCallback(std::function<void(unsigned int)> onProgress);
            void setCompletionCallback(std::function<void(void)> onCompletion);

            ResourceGroup * getGroup();

            unsigned int getResourceStepsToLoad();
            unsigned int getResourceStepsCompleted();

        protected:
            bool onProgressSet;
            bool onCompletionSet;
            bool announce;

            std::function<void(unsigned int)> onProgress;
            std::function<void(void)> onCompletion;

            virtual void setResourceStepsToLoad(unsigned int number);
            virtual void incrementResourceProgress(int number);

            unsigned int stepsCompleted;
            unsigned int stepsToLoad;

            RM::ref<ResourceGroup> group;

            inline void changeName(std::string newName)
            {
                if (announce)
                {
                    std::cout << "Changed resource name \"" << this->name << "\" to \"" << newName << "\"." << std::endl;
                }

                this->name = newName;
            }

        private:
            std::string name;
            std::string type;
    };
}

#endif // BASICRESOURCE_H
