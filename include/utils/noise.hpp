/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#ifndef NOISE_HPP_INCLUDED
#define NOISE_HPP_INCLUDED

/*******************************************************************************
 * This is unlicensed code from copied from:
 * http://blog.kazade.co.uk/2014/05/a-public-domain-c11-1d2d3d-perlin-noise.html
 *
 * It is expressed on that page that the code is unlicensed and free to be used
 * for any purpose.
 * More information on what unlicensed means can be found at: http://unlicense.org/
 *
 * The author's name is Kazade.
 ******************************************************************************/

#include <random>
#include <array>

namespace noise {

    class Perlin {
        public:
            Perlin(uint32_t seed=0);

            double noise(double x) const { return noise(x, 0, 0); }
            double noise(double x, double y) const { return noise(x, y, 0); }
            double noise(double x, double y, double z) const;

        private:
            std::array<int, 512> p;
    };

    class PerlinOctave {
        public:
            PerlinOctave(int octaves, uint32_t seed=0);

            double noise(double x) const { return noise(x, 0, 0); }
            double noise(double x, double y) const { return noise(x, y, 0); }
            double noise(double x, double y, double z) const;

        private:
            Perlin perlin_;
            int octaves_;
    };
}

#endif // NOISE_HPP_INCLUDED
