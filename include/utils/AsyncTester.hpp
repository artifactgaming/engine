/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#ifndef ASYNCTESTER_H
#define ASYNCTESTER_H

#include "AsyncResource.hpp"

class AsyncTester: public RM::AsyncResource
{
    public:
        AsyncTester(RM::ResourceGroup * group, unsigned int wait);
        AsyncTester(unsigned int wait);
        virtual ~AsyncTester();

        void async_load();
        void sync_load();

    protected:
        unsigned int wait;

    private:
};

#endif // ASYNCTESTER_H
