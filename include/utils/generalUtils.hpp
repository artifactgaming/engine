/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#ifndef GENERALUTILS_HPP_INCLUDED
#define GENERALUTILS_HPP_INCLUDED

#include <btBulletDynamicsCommon.h>
#include <LinearMath/btVector3.h>
#include <glm/vec3.hpp>

glm::vec3 toGLM(btVector3 input);
btVector3 toBullet(glm::vec3 input);

/*class btClosestNotMeConvexResultCallback : public btCollisionWorld::ClosestConvexResultCallback
{
public:

        btCollisionObject* m_me;
        btScalar m_allowedPenetration;
        btOverlappingPairCache* m_pairCache;
        btDispatcher* m_dispatcher;

public:
        btClosestNotMeConvexResultCallback (btCollisionObject* me,const btVector3& fromA,const btVector3& toA,btOverlappingPairCache* pairCache,btDispatcher* dispatcher);

        virtual btScalar addSingleResult(btCollisionWorld::LocalConvexResult& convexResult,bool normalInWorldSpace);

        virtual bool needsCollision(btBroadphaseProxy* proxy0) const;
};*/

#endif // GENERALUTILS_HPP_INCLUDED
