/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#ifndef IMAGESAVER_H
#define IMAGESAVER_H

#include <glad.h>
#include "AsyncResource.hpp"
#include "Path.hpp"

namespace RM {
    class ImageSaver: public AsyncResource
    {
        public:
            ImageSaver(ResourceGroup * group, const Path & path, GLubyte * pixels, int width, int height);
            virtual ~ImageSaver();

        protected:
            void async_load();
            void sync_load();

            GLubyte * pixels;
            Path path;
            int width;
            int height;

        private:
    };

    void saveImage(const Path & path, GLubyte * pixels, int width, int height);
}

#endif // IMAGESAVER_H
