/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#ifndef NBOF_H
#define NBOF_H

#include <stdint.h>
#include <string>
#include <map>
#include <iostream>
#include <vector>

#include "BinaryFileTools.hpp"
#include "AbsolutePanic.hpp"

namespace BinTools
{
    enum Type
    {
        uint8 = 0,
        uint16 = 1,
        uint32 = 2,
        uint64 = 3,
        int8 = 4,
        int16 = 5,
        int32 = 6,
        int64 = 7,
        float_single = 8,
        float_double = 9,
        boolean = 10,
        string = 11,
        array = 12,
        object = 13,
        end_of_object = 14
    };

    const LENGTH_HOLDER lengths[] = {
        sizeof(uint8_t),
        sizeof(uint16_t),
        sizeof(uint32_t),
        sizeof(uint64_t),
        sizeof(int8_t),
        sizeof(int16_t),
        sizeof(int32_t),
        sizeof(int64_t),
        sizeof(float),
        sizeof(double),
        sizeof(bool)
    };

    static_assert(sizeof(char) == sizeof(uint8_t), "Char isn't the size expected on this platform.");

    constexpr Type typeValue(uint8_t & theThing) { return uint8; };
    constexpr Type typeValue(char & theThing) { return uint8; };
    constexpr Type typeValue(uint16_t & theThing) { return uint16; };
    constexpr Type typeValue(uint32_t & theThing) { return uint32; };
    constexpr Type typeValue(uint64_t & theThing) { return uint64; };
    constexpr Type typeValue(int8_t & theThing) { return int8; };
    constexpr Type typeValue(int16_t & theThing) { return int16; };
    constexpr Type typeValue(int32_t & theThing) { return int32; };
    constexpr Type typeValue(int64_t & theThing) { return int64; };
    constexpr Type typeValue(float & theThing) { return float_single; };
    constexpr Type typeValue(double & theThing) { return float_double; };
    constexpr Type typeValue(std::string & theThing) { return string; };
    constexpr Type typeValue(bool & theThing) { return boolean; };

    template<class T>
    constexpr Type typeValue(std::vector<T> theThing) { return array; };

    template<class fstream>
    struct ValueSetter
    {
        ValueSetter(const char * name, fstream & file):
            file(file),
            name(name)
        {

        }

        template<class T>
        void operator=(T value)
        {
            uint8_t type = typeValue(value);
            writeStruct(file, type);
            writeStruct(file, name);
            writeStruct(file, value);
        }

        // A workaround for what is apparently a bug in GCC.
        template<class T>
        inline void operator=(std::initializer_list<T> value)
        {
            ValueSetter & self = *this;
            std::vector<T> vec(value);
            self = vec;
        }

        template<class T>
        void operator=(std::vector<T> & value)
        {
            uint8_t type = array;

            T iHelpFigureOutTheType;
            uint8_t subtype = typeValue(iHelpFigureOutTheType);

            writeStruct(file, type);
            writeStruct(file, name);
            writeStruct(file, subtype);
            writeStruct(file, value);
        }

        fstream & file;
        std::string name;
    };

    // Named Binary Object File
    template<class fstream>
    class NBOFWriter
    {
        public:
            NBOFWriter(fstream & file):
                file(file)
            {

            }

            ValueSetter<fstream> operator[](const char * name)
            {
                return ValueSetter<fstream>(name, file);
            }


            NBOFWriter<fstream> createObject(std::string name)
            {
                uint8_t type = Type::object;
                writeStruct(file, type);
                writeStruct(file, name);
                return NBOFWriter(file);
            }

            void close()
            {
                uint8_t endMarker = end_of_object;
                writeStruct(file, endMarker);
            }

        protected:

            fstream & file;

        private:
    };

    template<class fstream>
    class NBOFReader
    {
        public:
            NBOFReader(fstream & file):
                file(file)
            {
                size_t startOfFile = file.tellg();
                file.seekg(0, std::ios::end);
                size_t endOfFile = file.tellg();
                file.seekg(startOfFile, std::ios::beg);

                bool endFound = false;

                size_t location;
                while ((location = file.tellg()) < endOfFile)
                {


                    uint8_t type;
                    readStruct(file, type);

                    if (type == end_of_object)
                    {
                        endFound = true;
                        break;
                    }

                    std::string name;
                    readStruct(file, name);
                    index[name] = location;

                    switch (type)
                    {
                    case uint8:
                    case uint16:
                    case uint32:
                    case uint64:
                    case int8:
                    case int16:
                    case int32:
                    case int64:
                    case float_single:
                    case float_double:
                    case boolean:
                        file.seekg(lengths[type], std::ios::cur);
                        break;

                    case string:
                    case array:
                        {
                            uint8_t subtype;
                            readStruct(file, subtype);
                            LENGTH_HOLDER subTypeLength = lengths[subtype];

                            if (subtype <= 11)
                            {
                                LENGTH_HOLDER length;
                                readStruct(file, length);
                                length *= subTypeLength;
                                file.seekg(length, std::ios::cur);
                            }
                            else if (subtype == array)
                            {
                                LENGTH_HOLDER num;
                                readStruct(file, num);

                                for (unsigned int i = 0; i < num; i++)
                                {
                                    LENGTH_HOLDER subLength;
                                    readStruct(file, subLength);

                                    file.seekg(subLength * subTypeLength, std::ios::cur);
                                }
                            }
                            else
                            {
                                throw std::runtime_error("Unsupported array subtype specified in file. File is likely corrupted.");
                            }
                        }
                        break;

                    case object:
                        {
                            children.emplace(name, file);;
                        }
                        break;

                    default:
                        throw std::runtime_error("Could not identify object type in file. File is likely corrupted.");
                    }
                }

                if (!endFound)
                {
                    std::cout << "Warning: No end of object marker found. File may be corrupt and missing data." << std::endl;
                }
            }

            Type getType(std::string name)
            {
                uint8_t type;
                file.seekg(index[name], std::ios::beg);
                readStruct(file, type);
                return static_cast<Type>(type);
            }


            template<class T>
            T get(std::string name)
            {
                file.seekg(index[name], std::ios::beg);

                T value;
                Type type = typeValue(value);
                uint8_t typeRead;
                readStruct(file, typeRead);

                if (typeRead == type)
                {
                    std::string nameRead;
                    readStruct(file, nameRead);
                    if (name == nameRead)
                    {
                        readStruct(file, value);
                    }
                    else
                    {
                        std::string message = "Name for value \"";
                        message += name;
                        message += "\" specified in binary file did not match expected value. File is likely corrupted.\n";
                        message += "Name given: \"";
                        message += nameRead;
                        message += "\".";

                        throw std::runtime_error(message);
                    }
                }
                else
                {
                    std::string message = "Type for value \"";
                    message += name;
                    message += "\" specified in binary file did not match expected type. File is likely corrupted.";

                    throw std::runtime_error(message);
                }

                return value;
            }

            NBOFReader & getObject(std::string name)
            {

                return children.at(name);
            }

            template<class T>
            std::vector<T> getArray(std::string name)
            {
                file.seekg(index[name], std::ios::beg);

                std::vector<T> value;
                Type type = typeValue(value);
                uint8_t typeRead;
                readStruct(file, typeRead);

                if (typeRead == type)
                {
                    std::string nameRead;
                    readStruct(file, nameRead);
                    if (name == nameRead)
                    {
                        uint8_t readSubtype;
                        readStruct(file, readSubtype);

                        T iHelpFigureOutTheType;
                        Type subtype = typeValue(iHelpFigureOutTheType);

                        if (readSubtype == subtype)
                        {
                            readStruct(file, value);
                        }
                        else
                        {
                            throw std::runtime_error("Subtype for array specified in binary file did not match expected type. File is likely corrupted.");
                        }
                    }
                    else
                    {
                        std::string message = "Name for value \"";
                        message += name;
                        message += "\" specified in binary file did not match expected value. File is likely corrupted.\n";
                        message += "Name given: \"";
                        message += nameRead;
                        message += "\".";

                        throw std::runtime_error(message);
                    }
                }
                else
                {
                    std::string message = "Type for value \"";
                    message += name;
                    message += "\" specified in binary file did not match expected type. File is likely corrupted.";

                    throw std::runtime_error(message);
                }

                return value;
            }

        protected:

            fstream & file;
            std::map<std::string, size_t> index;
            std::map<std::string, NBOFReader<fstream>> children;

        private:
    };
}

#endif // NBOF_H
