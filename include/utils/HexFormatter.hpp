/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#ifndef HEXFORMATTER_INCLUDED
#define HEXFORMATTER_INCLUDED

#include <iomanip>

template<typename T>
std::string formatAsHex(T i)
{
  std::stringstream stream;
  stream << std::setfill('0') << std::setw(sizeof(T) * 2) << std::hex << i;
  return stream.str();
}

uint64_t getFromHex(std::string value);

#endif // HEXFORMATTER_INCLUDED
