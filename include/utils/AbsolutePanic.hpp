/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#ifndef ABSOLUTEPANIC_HPP_INCLUDED
#define ABSOLUTEPANIC_HPP_INCLUDED

/// For those situations where it should never ever ever ever happen no matter what the
/// user does, because this screwup was in the engine.
#define ABSOLUTE_PANIC() throw std::runtime_error(std::string("AAAAAAAAAAAA!") + __FILE__ + std::to_string(__LINE__))

#endif // ABSOLUTEPANIC_HPP_INCLUDED
