/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#ifndef ASYNCLUATESTER_H
#define ASYNCLUATESTER_H

#include "TrueConcurrency.hpp"

class AsyncLuaTester: public LuaSupport::TrueConcurrency
{
    public:
        AsyncLuaTester(RM::ResourceGroup * group, sol::function func, sol::function syncfunc, sol::variadic_args args, std::string name);
        virtual ~AsyncLuaTester();

        void async_load();
        void sync_load();

    protected:

        sol::function syncfunc;
        std::string asyncFuncDump;
        std::vector<std::variant<void*, bool, float, std::string>> jobArgs;
        std::vector<std::variant<void*, bool, float, std::string>> jobResults;
    private:
};

#endif // ASYNCLUATESTER_H
