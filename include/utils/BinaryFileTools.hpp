#ifndef BINARYFILETOOLS_HPP_INCLUDED
#define BINARYFILETOOLS_HPP_INCLUDED

/**************************************************************************************
* Program: Grid Locked
* Author: James J Carl, jamescarl96@gmail.com
* Copyright (c) 2017-2019 James J Carl
* Summery:
* Grid Locked is a block based sandbox style game. It's key focus is in the
* construction of cool machines, operations of business, and making things look
* cool. There are a lot more details to the game that are not included here.
**************************************************************************************/

#include <stdexcept>
#include <vector>
#include <iostream>

namespace BinTools
{
    struct BinReadError : std::runtime_error
    {
        using std::runtime_error::runtime_error;
    };

    template<class fstream, class S>
    inline void writeStruct(fstream & output, const S & s)
    {
        output.write(reinterpret_cast<const char*>(&s), sizeof(S));
    }

    template<class fstream, class S>
    inline void readStruct(fstream & input, S & s)
    {
        input.read(reinterpret_cast<char*>(&s), sizeof(S));
        if (static_cast<size_t>(input.gcount()) < sizeof(S))
        {
            throw BinReadError("Not enough data left in file to fully fill struct.");
        }
    }

    typedef uint32_t LENGTH_HOLDER;

    template<class fstream>
    inline void writeStruct(fstream & output, const std::string & data)
    {
        // Tell them how long our string is.
        LENGTH_HOLDER length = data.size();
        output.write(reinterpret_cast<char*>(&length), sizeof(length));
        output.write(&data[0], length);
    }

    template<class fstream>
    inline void readStruct(fstream & input, std::string & data)
    {
        // Get length of the string.
        LENGTH_HOLDER length;
        input.read(reinterpret_cast<char*>(&length), sizeof(length));

        data.resize(length);

        input.read(&data[0], length);
        if (input.gcount() < length)
        {
            throw BinReadError("String in file was not as long as it claimed to be.");
        }
    }

    template<class fstream, class T>
    inline void writeStruct(fstream & output, const std::vector<T> & data)
    {
        // Tell them how long our string is.
        LENGTH_HOLDER length = data.size();
        writeStruct(output, length);

        for (T item : data)
        {
            writeStruct(output, item);
        }
    }

    template<class fstream, class T>
    inline void readStruct(fstream & input, std::vector<T> & data)
    {
        // Get length of the string.
        LENGTH_HOLDER length;
        input.read(reinterpret_cast<char*>(&length), sizeof(length));

        data.resize(length);

        for (unsigned int i = 0; i < length; i++)
        {
            readStruct(input, data[i]);
        }
    }
}

#endif // BINARYFILETOOLS_HPP_INCLUDED
