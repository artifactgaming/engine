print("Testing all platform functions.")
print()

print("Platform: "..platform.name())

local max, min = platform.glVersion()
print("GL Version: "..max.." "..min)

print("Developer mode: "..tostring(platform.isDeveloperMode()))

local max, min = platform.libraryVersion()
print("Library Version: "..max.." "..min)

print("Form Factor: "..platform.formfactor())
