
print("Testing Filesystem")

function script_path()
    if debug then
        local src = debug.getinfo(2, "S").source:sub(2)
        return src --:match("(.*/)")
    else
        print("Warning: This is not running in developer mode, so the path could not be derived using the debug library.")
        print("Assuming that the test API is located at \"tests/CoreAPI/\".")
        return "tests/CoreAPI/filesystem.lua"
    end
end

local function testConstructors()
    --Will crash on failure. Kinda lazy, but it works.
    local path = Path.new()
    local path = Path.new(path)
    local path = Path.new("")
    print("All constructors: [PASSED]")
end

local function testCurrentPath()
    local src = Path.new(script_path())

    print("Note: Current path determined to be: "..src)

    local output = Path.current()
    if output == src then
        print("Basic current path detection: [PASSED]")
    else
        print("Basic current path detection: [FAILED] --Returned \""..output.."\"")
    end

    local function func()
        return Path.current()
    end
    local output = func()

    if output == src then
        print("Current path detection from local function: [PASSED]")
    else
        print("Current path detection from local function: [FAILED] --Returned \""..output.."\"")
    end

    local func = loadstring("return Path.current()")
    local output = func()

    if output == src then
        print("Current path detection from loadstring: [PASSED]")
    else
        print("Current path detection from loadstring: [FAILED] --Returned \""..output.."\"")
    end

    local extPath = Path.current():getParent() / "filesystem/externalCurrentPath.lua"

    local output = dofile(tostring(extPath))

    if output == extPath then
        print("Current path detection from external file: [PASSED]")
    else
        print("Current path detection from external file: [FAILED] --Returned \""..output.."\"")
    end

    local output = loadfile(tostring(extPath))
    output = output()

    if output == extPath then
        print("Current path detection from external file (complicated load): [PASSED]")
    else
        print("Current path detection from external file (complicated load): [FAILED] --Returned \""..output.."\"")
    end
end

local function testContainment()
    --Exists on almost all Linux/Unix systems, but we shouldn't be able to see it.
    local path = Path.new("/etc")

    if path:exists() then
        print("Path containment for root directory: [FAILED]")
    else
        print("Path containment for root directory: [PASSED]")
    end

    if Path.new(""):getParent():exists() then
        print("Path containment for parent to root directory: [FAILED]")
    else
        print("Path containment for parent to root directory: [PASSED]")
    end

    local path = Path.new(""):getParent():getAbsolute()

    if tostring(path) == "" then
        print("Containment of absolute path: [PASSED]")
    else
        print("Containment of absolute path: [FAILED] --"..path)
    end
end

testConstructors()
testCurrentPath()
testContainment()

--path.new("path")
--path.current()
--path.root

