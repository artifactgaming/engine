print("Testing serial API")

local function areTablesEqual(tableA, tableB)
    --Can't be the same when we don't even have the same number of elements.
    if #tableA == #tableB then
        for i, v in pairs(tableA) do
            if type(v) == "table" then
                if not areTablesEqual(v, tableB[i]) then
                    return false
                end
            else
                if v ~= tableB[i] then
                    return false
                end
            end
        end

        return true
    end

    return false
end

local function testJSON()
    print("Testing JSON")

    local tbl = {}

    tbl["hello"] = 2
    tbl["table"] = {1, 2, 3, 4, 5}
    tbl["num"] = 2.5;

    local exampleString = [[{
    "hello" : 2,
    "num" : 2.5,
    "table" : 
    [
        1,
        2,
        3,
        4,
        5
    ]
}]]

    local text = serial.tableToJSON(tbl)

    if text == exampleString then
        print("Table to JSON string: [PASSED]")
    else
        print("Table to JSON string: [FAILED]")
    end

    local newTbl = serial.JSONToTable(exampleString)

    if areTablesEqual(tbl, newTbl) then
        print("JSON string back to table: [PASSED]")
    else
        print("JSON string back to table: [FAILED]")
    end

    --Paradox test. Should always cause a crash, which we will catch.
    local ok, ret = pcall(function()
        local tbl = {}
        tbl["paradox"] = tbl
        serial.tableToJSON(tbl)
    end)

    if ok then
        print("JSON Paradox: [FAILED]")
    else
        print("JSON Paradox: [PASSED]")
    end
end

function testBIN()
    print("Testing Binary Serialization")
    local tbl = {}

    tbl["hello"] = 2
    tbl["table"] = {1, 2, 3, 4, 5}
    tbl["num"] = 2.5
    tbl["some words"] = "Here's a bunch of test to test stuff with. BAH!"

    data = serial.tableToBin(tbl)

    local newTbl = serial.binToTable(data)

    if areTablesEqual(tbl, newTbl) then
        print("Binary stream back to table: [PASSED]")
    else
        print("Binary stream back to table: [FAILED]")
    end

    --Paradox test. Should always cause a crash, which we will catch.
    local ok, ret = pcall(function()
        print("testing paradox")
        local tbl = {}
        tbl["paradox"] = tbl
        serial.tableToBin(tbl)
    end)

    if ok then
        print("Binary stream Paradox: [FAILED]")
    else
        print("Binary stream Paradox: [PASSED]")
    end
end

testJSON()
testBIN()
