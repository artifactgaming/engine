print("Testing GLM")

--How close do numbers need to be to the pre-computed answer?
local PRECISION = 0.0000000000001

if doSwizzleTest == nil then
    --Set to true to enable the swizzle tests. Notice that it can be overriden by a calling script.
    doSwizzleTest = true
end

local function incrementDiget(tab, max, index)

    if not index then
        index = 1
    end

    local val = tab[index]

    val = val + 1
    if val > max then
        val = 1
        if #tab > index then
            incrementDiget(tab, max, index + 1)
        end
    end

    tab[index] = val

end

local function testSwizzleBatch(vec, vecName, length, glyphs)
    local func = {}

    for n = 1, length do
        table.insert(func, #glyphs)
    end

    local num = math.pow(#glyphs, length)

    for it = 1, num do
        incrementDiget(func, #glyphs)

        local text = ""

        for k, v in pairs(func) do
            text = text..glyphs[v]
        end

        if vec[text] then
            local cvec = vec[text]
            if type(cvec) == "function" then
                cvec = vec[text](vec)
            end

            local ver = it

            if length == 2 then
                ver = vec2(unpack(func))
            elseif length == 3 then
                ver = vec3(unpack(func))
            elseif length == 4 then
                ver = vec4(unpack(func))
            end

            if cvec == ver then
                print(text.." [PASSED]")
            else
                print(text.." [FAILED]")
            end
        else
            print(text.." [FAILED TO FIND FUNCTION]")
        end

        --The following was used to auto-generate the Lua bindings for the C++ side of the engine.
        --print(vecName.."[\""..text.."\"] = &glm::"..vecName.."::"..text..";")
    end
end

local function testSwizzle(sVecname, sVars)
    if doSwizzleTest then
        local num = #sVars
        local base = #sVars

        local glyphs = {}
        sVars:gsub(".", function(c) table.insert(glyphs,c) end)

        for n = 1, 4 do
            num = num + math.pow(base , n)
        end

        print("\nTesting swizzel for "..sVecname.." with "..num.." tests.")

        local init = {}
        for n = 1, base do
            table.insert(init, n)
        end

        local vec = _G[sVecname](unpack(init))
        
        for length = 1, 4 do
            testSwizzleBatch(vec, sVecname, length, glyphs)
        end
    else
        print("Swizzle testing has been disabled.")
    end

end

local function testConstructor(sConstructor, fTester)
    local con = loadstring("return "..sConstructor)

    local ok, ret = pcall(con)

    if ok then
        ok, ret = fTester(ret)
    end

    if ok then
        print("Constructor "..sConstructor..": [PASSED]")
    else
        print("Constructor "..sConstructor..": [FAILED]")
        print(ret)
    end
end

local function testVectorConstructor(sVecName, nComps)
    local vecX = _G[sVecName]

    local function testVector(vec, comp)
        for n = 0, #vec-1 do
            if vec[n] ~= comp[n+1] then
                return false
            end
        end

        return true
    end

    --Start by testing default constructor.
    local vec = vecX()

    local compTable = {}
    for n=1, nComps do
        table.insert(compTable, 0)
    end

    if testVector(vec, compTable) then
        print("Constructor "..sVecName.."() [PASSED]")
    else
        print("Constructor "..sVecName.."() [FAILED]")
    end

    --Test fill all constructor.
    local vec = vecX(5)

    local compTable = {}
    for n=1, nComps do
        table.insert(compTable, 5)
    end

    if testVector(vec, compTable) then
        print("Constructor "..sVecName.."(5) [PASSED]")
    else
        print("Constructor "..sVecName.."(5) [FAILED]")
    end

    local function constructionFromTable(name, tab)
        local testName = "("

        for n=1, #tab do
            testName = testName..tab[n]..","
        end

        return name..testName:sub(0, #testName-1)..")"
    end

    local function testConstructor(sConstructor, compTable)
        local con = loadstring("return "..sConstructor)
        
        local ok, ret = pcall(con)

        if ok then
            if testVector(ret, compTable) then
                print("Constructor "..sConstructor..": [PASSED]")
            else
                print("Constructor "..sConstructor..": [FAILED] --Vector was constructed wrong.")
            end
        else
            print("Constructor "..sConstructor..": [FAILED] --Constructor not found.")
            print(ret)
        end
    end

    --Now we test all the mishmosh of every constructor possible.
    local compTable = {}
    for n=1, nComps do
        table.insert(compTable, n)
    end

    --Start with scalars only.
    testConstructor(constructionFromTable(sVecName, compTable), compTable)

    --Now test with vec2s.
    local index = 1
    while index + 1 <= #compTable do
        local thisTable = {}

        for n=1, index-1 do
            table.insert(thisTable, compTable[n])
        end

        table.insert(thisTable, "vec2("..compTable[index]..","..compTable[index+1]..")")

        for n=index+2, #compTable do
            table.insert(thisTable, compTable[n])
        end

        testConstructor(constructionFromTable(sVecName, thisTable), compTable)
        index = index + 1
    end

    --Now test with 2 vec2s.
    if #compTable == 4 then --Need to be able to fit it.
        testConstructor("vec4(vec2(1,2), vec2(3,4))", compTable)
    end

    --Now test with vec3s.
    local index = 1
    while index + 2 <= #compTable do
        local thisTable = {}

        for n=1, index-1 do
            table.insert(thisTable, compTable[n])
        end

        table.insert(thisTable, "vec3("..compTable[index]..","..compTable[index+1]..","..compTable[index+2]..")")

        for n=index+3, #compTable do
            table.insert(thisTable, compTable[n])
        end

        testConstructor(constructionFromTable(sVecName, thisTable), compTable)
        index = index + 1
    end

    --Finally test vec4.
    if #compTable == 4 then --Need to be able to fit it.
        testConstructor("vec4(1,2,3,4)", compTable)
    end
end

local function testNegate(vecA, vecB)
    if -vecA == vecB then
        print("Operator NEGATE: [PASSED]")
    else
        print("Operator NEGATE: [FAILED]")
    end
end

local function testOperator(sOperator, sType, vecA, vecB, vecC)

    local func = loadstring("return function(vecA, vecB)\nreturn vecA "..sOperator.." vecB\nend")
    func = func() --Gotta get the function out of the string.

    local vec = func(vecA, vecB)

    if vec == vecC then
        print("Operator "..sOperator..sType..": [PASSED]")
    else
        print("Operator "..sOperator..sType..": [FAILED]")
    end
end

function testIndexing(vec, axis)
    local num = #axis

    for n = 1, num do
        vec[n-1] = n
    end

    local passed = true

    for n = 1, num do
        if vec[axis[n]] ~= n then
            passed = false
        end
    end

    if passed then
        print("Axis indexing (write): [PASSED]")
    else
        print("Axis indexing (write): [FAILED] "..vec)
    end

    local passed = true

    for n = 1, num do
        vec[axis[n]] = n
    end

    for n = 1, num do
        if vec[n-1] ~= n then
            passed = false
        end
    end

    if passed then
        print("Axis indexing (read): [PASSED]")
    else
        print("Axis indexing (read): [FAILED] "..vec)
    end
end

local function testVec2()
    print()
    print("Testing vec2")

    --Test the constructors.
    testVectorConstructor("vec2", 2)

    --Test basic operators.
    if #vec2() == 2 then
        print("Operator #: [PASSED]")
    else
        print("Operator #: [FAILED]")
    end

    testNegate(vec2(1, 2), vec2(-1, -2))

    testOperator("+", "number", vec2(1, 2), 2, vec2(3, 4))
    testOperator("+", "vec2", vec2(1, 2), vec2(1, 2), vec2(2, 4))

    testOperator("-", "number", vec2(1, 2), 2, vec2(-1, 0))
    testOperator("-", "vec2", vec2(2, 1), vec2(1, 2), vec2(1, -1))

    testOperator("*", "number", vec2(4, 2), 2, vec2(8, 4))
    testOperator("*", "vec2", vec2(2, 4), vec2(2, 3), vec2(4, 12))

    testOperator("/", "number", vec2(4, 12), 2, vec2(2, 6))
    testOperator("/", "vec2", vec2(4, 12), vec2(2, 3), vec2(2, 4))

    if tostring(vec2(2, 4)) == "(2,4)" then
        print("Conversion to string: [PASSED]")
    else
        print("Conversion to string: [FAILED]")
    end

    if "test"..vec2(2, 4) == "test(2,4)" and vec2(2, 4).."test" == "(2,4)test" then
        print("Concatenation with string: [PASSED]")
    else
        print("Concatenation with string: [FAILED]")
    end

    --Test the ability to access axis by index.
    testIndexing(vec2(), {"x", "y"})

    --Test basic functions.
    if math.abs(vec2(4, 1):normalize():length() - 1) < PRECISION then
        print("Normalization: [PASSED]")
    else
        print("Normalization: [FAILED]")
    end

    if math.abs(vec2(1, 1):length() - 1.4142135381699) < PRECISION then
        print("Calculate Length: [PASSED]")
    else
        print("Calculate Length: [FAILED]")
    end

    --Test for the existance of all the swizzle functions.
    testSwizzle("vec2", "xy")
    testSwizzle("vec2", "rg")
end


local function testVec3()
    print()
    print("Testing vec3")

    --Test the constructors.
    testVectorConstructor("vec3", 3)

    --Test basic operators.
    if #vec3() == 3 then
        print("Operator #: [PASSED]")
    else
        print("Operator #: [FAILED]")
    end

    testNegate(vec3(1, 2, 3), vec3(-1, -2, -3))

    testOperator("+", "number", vec3(1, 2, 3), 2, vec3(3, 4, 5))
    testOperator("+", "vec2", vec3(1, 2, 3), vec3(1, 2, 3), vec3(2, 4, 6))

    testOperator("-", "number", vec3(1, 2, 3), 2, vec3(-1, 0, 1))
    testOperator("-", "vec2", vec3(2, 1, 5), vec3(1, 2, 3), vec3(1, -1, 2))

    testOperator("*", "number", vec3(4, 2, 8), 2, vec3(8, 4, 16))
    testOperator("*", "vec2", vec3(2, 4, 5), vec3(2, 3, 4), vec3(4, 12, 20))

    testOperator("/", "number", vec3(4, 12, 24), 2, vec3(2, 6, 12))
    testOperator("/", "vec2", vec3(4, 12, 20), vec3(2, 3, 5), vec3(2, 4, 4))

    if tostring(vec3(2, 4, 8)) == "(2,4,8)" then
        print("Conversion to string: [PASSED]")
    else
        print("Conversion to string: [FAILED]")
    end

    if "test"..vec3(2, 4, 8) == "test(2,4,8)"
    and vec3(2, 4, 8).."test" == "(2,4,8)test" then
        print("Concatenation with string: [PASSED]")
    else
        print("Concatenation with string: [FAILED]")
    end

    --Test the ability to access axis by index.
    testIndexing(vec3(), {"x", "y", "z"})

    --Test basic functions.
    if math.abs(vec3(4, 1, 3):normalize():length() - 1) < PRECISION then
        print("Normalization: [PASSED]")
    else
        print("Normalization: [FAILED]")
    end

    if math.abs(vec3(1, 1, 1):length() - 1.7320507764816) < PRECISION then
        print("Calculate Length: [PASSED]")
    else
        print("Calculate Length: [FAILED]")
    end

    --Test for the existance of all the swizzle functions.
    testSwizzle("vec3", "xyz")
    testSwizzle("vec3", "rgb")
end

local function testVec4()
    print()
    print("Testing vec4")

    --Test the constructors.
    testVectorConstructor("vec4", 4)

    --Test basic operators.
    if #vec4() == 4 then
        print("Operator #: [PASSED]")
    else
        print("Operator #: [FAILED]")
    end

    testNegate(vec4(1, 2, 3, 4), vec4(-1, -2, -3, -4))

    testOperator("+", "number", vec4(1, 2, 3, 4), 2, vec4(3, 4, 5, 6))
    testOperator("+", "vec2", vec4(1, 2, 3, 4), vec4(1, 2, 3, 4), vec4(2, 4, 6, 8))

    testOperator("-", "number", vec4(1, 2, 3, 4), 2, vec4(-1, 0, 1, 2))
    testOperator("-", "vec2", vec4(2, 1, 5, 2), vec4(1, 2, 3, 4), vec4(1, -1, 2, -2))

    testOperator("*", "number", vec4(4, 2, 8, 16), 2, vec4(8, 4, 16, 32))
    testOperator("*", "vec2", vec4(2, 4, 5, 6), vec4(2, 3, 4, 5), vec4(4, 12, 20, 30))

    testOperator("/", "number", vec4(4, 12, 24, 48), 2, vec4(2, 6, 12, 24))
    testOperator("/", "vec2", vec4(4, 12, 20, 30), vec4(2, 3, 5, 3), vec4(2, 4, 4, 10))

    if tostring(vec4(2, 4, 8, 16)) == "(2,4,8,16)" then
        print("Conversion to string: [PASSED]")
    else
        print("Conversion to string: [FAILED]")
    end

    if "test"..vec4(2, 4, 8, 16) == "test(2,4,8,16)"
    and vec4(2, 4, 8, 16).."test" == "(2,4,8,16)test" then
        print("Concatenation with string: [PASSED]")
    else
        print("Concatenation with string: [FAILED]")
    end

    --Test the ability to access axis by index.
    testIndexing(vec4(), {"x", "y", "z", "w"})

    --Test basic functions.
    if math.abs(vec4(4, 1, 3, 1):normalize():length() - 1) < PRECISION then
        print("Normalization: [PASSED]")
    else
        print("Normalization: [FAILED]")
    end

    if math.abs(vec4(1, 1, 1, 1):length() - 2) < PRECISION then
        print("Calculate Length: [PASSED]")
    else
        print("Calculate Length: [FAILED]")
    end

    --Test for the existance of all the swizzle functions.
    testSwizzle("vec4", "xyzw")
    testSwizzle("vec4", "rgba")

end

local function testMatrixConstructors(matName, height, width)

    local function testMatrix(mat, tab)
        local index = 1

        for y=0, #mat-1 do
            local vec = mat[y]
            for x = 0, #vec-1 do
                if mat[y][x] ~= tab[index] then
                    return false
                end

                index = index + 1
            end
        end

        return true
    end

    local function constructionFromTable(name, tab)
        local testName = "("

        for n=1, #tab do
            testName = testName..tab[n]..","
        end

        return name..testName:sub(0, #testName-1)..")"
    end

    local function testConstructor(sConstructor, compTable)
        local con = loadstring("return "..sConstructor)
        
        local ok, ret = pcall(con)

        if ok then
            if testMatrix(ret, compTable) then
                print("Constructor "..sConstructor..": [PASSED]")
            else
                print("Constructor "..sConstructor..": [FAILED] --Matrix was constructed wrong.")
                print(ret)
            end
        else
            print("Constructor "..sConstructor..": [FAILED] --Constructor not found.")
            print(ret)
        end
    end

    local function testVecSet(matName, vec, height, width)

        local testTable = {}
        local num = 1

        for y=0, height-1 do
            for x=0, width-1 do
                table.insert(testTable, num)
                num = num + 1
            end
        end

        local function testWithVector(vecLen, matName, compTable)
            local vecName = "vec"..vecLen

            local index = 1
            while index + vecLen - 1 <= #compTable do
                local thisTable = {}

                for n=1, index-1 do
                    table.insert(thisTable, compTable[n])
                end

                local line = vecName.."("

                for n=0, vecLen-1 do
                    line = line..compTable[index + n]..","
                end

                line = line:sub(0, #line-1)..")"

                table.insert(thisTable, line)

                for n=index+vecLen, #compTable do
                    table.insert(thisTable, compTable[n])
                end

                testConstructor(constructionFromTable(matName, thisTable), compTable)
                index = index + 1
            end
        end

        testConstructor(constructionFromTable(matName, testTable), testTable)
        testWithVector(2, matName, testTable)
        testWithVector(3, matName, testTable)
        testWithVector(4, matName, testTable)
    end

    local function buildIdentityTable(num)
        local testTable = {}
        local mid = 0

        for y=0, height-1 do
            for x=0, width-1 do
                if x ~= mid then
                    table.insert(testTable, 0)
                else
                    table.insert(testTable, num)
                end
            end
            mid = mid + 1
        end

        return testTable
    end

    local testTable = buildIdentityTable(1)
    testConstructor(matName.."()", testTable)

    local testTable = buildIdentityTable(5)
    testConstructor(matName.."(5)", testTable)

    testVecSet(matName, 2, height, width)
    testVecSet(matName, 3, height, width)
    testVecSet(matName, 4, height, width)

end

function testMatrix(height, width)
    local matName = "mat"..height.."x"..width
    local matSource = _G[matName]
    local vecSource = _G["vec"..width]

    testMatrixConstructors(matName, height, width)

    local mat = matSource()

    if #mat == height then
        print("Operator #: [PASSED]")
    else
        print("Operator #: [FAILED]")
    end

    for y = 1, #mat do
        local vec = mat[y-1]
        for x = 1, #vec do
            vec[x-1] = x*y
        end
    end

    local passed = true

    for y = 1, #mat do
        local vec = mat[y-1]
        for x = 1, #vec do
            if vec[x-1] ~= x*y then
                passed = false
            end
        end
    end

    if passed then
        print("Indirect indexing test: [PASSED]")
    else
        print("Indirect indexing test: [FAILED]")
    end

    for y = 1, #mat do
        mat[y-1] = vecSource(y)
    end

    local passed = true

    for y = 1, #mat do
        local vec = mat[y-1]
        for x = 1, #vec do
            if vec[x-1] ~= y then
                passed = false
            end
        end
    end

    if passed then
        print("Direct indexing test: [PASSED]")
    else
        print("Direct indexing test: [FAILED]")
    end
end

function testMat2x2()
    print()
    print("Testing mat2x2.")
    testMatrix(2, 2)
end

function testMat2x3()
    print()
    print("Testing mat2x3.")
    testMatrix(2, 3)
end

function testMat2x4()
    print()
    print("Testing mat2x4.")
    testMatrix(2, 4)
end

function testMat3x2()
    print()
    print("Testing mat3x2.")
    testMatrix(3, 2)
end

function testMat3x3()
    print()
    print("Testing mat3x3.")
    testMatrix(3, 3)
end

function testMat3x4()
    print()
    print("Testing mat3x4.")
    testMatrix(3, 4)
end

function testMat4x2()
    print()
    print("Testing mat4x2.")
    testMatrix(4, 2)
end

function testMat4x3()
    print()
    print("Testing mat4x3.")
    testMatrix(4, 3)
end

function testMat4x4()
    print()
    print("Testing mat4x4.")
    testMatrix(4, 4)
end

testVec2()
testVec3()
testVec4()

testMat2x2()
testMat3x3()
testMat4x4()

--Non square matricies are currently not supported.
--testMat2x3()
--testMat2x4()
--testMat3x2()
--testMat3x4()
--testMat4x2()
--testMat4x3()


