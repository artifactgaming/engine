--[[
 Program: Grid Locked
 Author: James J Carl, jamescarl96@gmail.com
 Copyright (c) 2017 James J Carl
 Summery:
 Grid Locked is a block based sandbox style game. It's key focus is in the
 construction of cool machines, operations of business, and making things look
 cool. There are a lot more details to the game that are not included here.
]]--

--This file sets up a simple background.

local shader = GL.ShaderProgram.new({"main/GUI/background/background.frag", "main/GUI/background/background.vert"})

--This is going to take the longest to load, so this one gets started first.
local backgroundSource = GL.TextureSources.LocalFile.new("main/GUI/background/background.png")
local backgroundSampler = GL.Sampler2D.new(backgroundSource)

local vertices = GL.VertexBuffer2D.new(GL.VertexBuffer.types.static)

vertices:addVertex(vec2(0.0, 0.0))
vertices:addVertex(vec2(1.0, 0.0))
vertices:addVertex(vec2(0.0, 1.0))

vertices:addVertex(vec2(1.0, 1.0))
vertices:addVertex(vec2(1.0, 0.0))
vertices:addVertex(vec2(0.0, 1.0))

vertices:sendToGPU()

local texCoords = GL.VertexBuffer2D.new(GL.VertexBuffer.types.static)

texCoords:addVertex(vec2(0.0, 1.0))
texCoords:addVertex(vec2(1.0, 1.0))
texCoords:addVertex(vec2(0.0, 0.0))

texCoords:addVertex(vec2(1.0, 0.0))
texCoords:addVertex(vec2(1.0, 1.0))
texCoords:addVertex(vec2(0.0, 0.0))

texCoords:sendToGPU()

local vba = GL.VertexBufferArray.new(shader)
vba:setBuffer("vertexPosition", vertices)
vba:setBuffer("texCoords", texCoords)

local renderTarget = GL.RenderTarget.new(GUI.getScreen(),
function(target)
    GL.ScopedCall(target, shader, function(renderTarget, shader)
        shader:setSampler("sampler", backgroundSampler)
        shader:drawVertexBuffers(vba)
    end)
end)

local callback = GUI.getScreen():addResizeCallback(function(size)
    renderTarget:setSize(size)

    GL.ScopedCall(renderTarget, shader, function(renderTarget, shader)
        local projection

        if size.x > size.y then
            projection = mat4x4builders.ortho(0, 1, 0, size.y / size.x)
        else
            projection = mat4x4builders.ortho(0, size.x / size.y, 0, 1)
        end

        shader:setMatrix4x4("projection", projection)
    end)
end)

renderTarget:pushToBack()
