#version 330

// Inputs
in vec2 TexCoord;
uniform sampler2D sampler;

// Outputs
out vec4 color;

void main(void)
{
	color = texture(sampler, TexCoord);
}
