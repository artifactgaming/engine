#version 330

//Inputs
layout (location = 0) in vec2 vertexPosition;
layout (location = 1) in vec2 texCoords;


uniform mat4 projection;

//Outputs
out vec2 TexCoord;

void main(void)
{
	TexCoord = texCoords;
    gl_Position = projection * vec4(vertexPosition, 0.0, 1.0);
}
