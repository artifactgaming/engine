shader = GL.ShaderProgram.new({"main/world/shaders/phase1.frag", "main/world/shaders/phase1.vert"})

near = 0.1
far  = 256
tang = math.tan(10) --Using 10 here gives an actual view radius of about 50 degrees.
view = GL.View.new(shader)
view:setViewMatrix(mat4x4builders.lookAt(vec3(0, 0, 0), vec3(0, 0, 1), vec3(0, 1, 0)))

window:addResizeCallback(function(size)
    local aspect = size.x / size.y

    local nearHeight = near * tang
    local nearWidth = nearHeight * aspect

    view:setProjectionMatrix(mat4x4builders.frustum(-nearWidth, nearWidth, -nearHeight, nearHeight, near, far))
end)


dofile("main/world/setup_world_saves.lua")
local folder = Path.new("world_saves/0") --getNewWorldFolder()

function terrainSaver(chunk, this_world)
    --print("Save Chunk: "..chunk:getNativeX()..",\t"..chunk:getNativeY()..",\t"..chunk:getNativeZ())
    chunk:saveToFile()
    --print("Saved chunk to: "..file)
end

local terrainGenerators = {
    function(chunk, world)
        --print("Load Chunk: "..chunk:getNativeX()..",\t"..chunk:getNativeY()..",\t"..chunk:getNativeZ())
        return chunk:loadFromFile(file)
    end
    ,
    function(chunk, this_world)
        --print("Generate Chunk: "..chunk:getNativeX()..",\t"..chunk:getNativeY()..",\t"..chunk:getNativeZ())

        local block = this_world:createBlock("main/world/blocks/abstract_terrain-block.json")
        block:setDirectionH("south")

        for i=0,15 do
            for j=0,15 do
                for k=0,15 do
                    chunk:setBlockLocal_unsafe(k, j, i, block)
                end
            end
        end

        --print("Chunk generated.")

        return true
    end
}

local alreadyExisted = folder:exists()

test_world = world.World.new(
    folder,
    "Test World",
    {
        world.BlockController.get("main/world/blocks/abstract_terrain-block.json")
    },
    terrainGenerators,
    terrainSaver
)

local chunk = test_world:getChunkNativeCoords_safe(0, 0, -1)
visual_ref1 = chunk:getVisualRef()

test_world:getUserdata().core = {}
test_world:getUserdata().core.main_view = view

if not alreadyExisted then
    local player = test_world:spawnDynamicEntity("main/world/dynamic_entities/player-dynamic_entity.json", vec3(8, 8, 8), {})
else
    test_world:loadEntities()
end

print("It is time to save.")
test_world:save()
collectgarbage("collect")
