#version 330

//Inputs
layout (location = 0) in vec3 vertexPosition;
// layout (location = 1) in vec3 textureCoords;

uniform mat4 matrix;

//Outputs
out vec3 vTexCoord;

void main(void)
{
	gl_Position = matrix * vec4(vertexPosition, 1.0);
	// vTexCoord = textureCoords;
}
