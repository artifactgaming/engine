worldSaves = Path.new("world_saves")

if not worldSaves:exists() then
    worldSaves:mkdirs()
    print("World saves folder did not exist, so I created it.")
end

function getNewWorldFolder()
    local index = 0
    local path = worldSaves / string.format("%x", index)

    while (path:exists()) do
        index = index + 1
        path = worldSaves / string.format("%x", index)
    end

    print(path)

    return path
end

function listWorlds()
    local worldFolderList = worldSaves:listContents()

    local worlds = {}

    for i, v in pairs(worldFolderList) do
        headerPath = v / "header.bin"

        local data = world.readWorldHeader(headerPath)

        table.insert(worlds, data)
    end

    return worlds
end
