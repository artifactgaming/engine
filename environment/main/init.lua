--[[
 Program: Grid Locked
 Author: James J Carl, jamescarl96@gmail.com
 Copyright (c) 2017-2019 James J Carl
 Summery:
 Grid Locked is a block based sandbox style game. It's key focus is in the
 construction of cool machines, operations of business, and making things look
 cool. There are a lot more details to the game that are not included here.
]]--

function onStart()
    print("Game started.")

    setupControls()
    setupTopMenu()
end

function onShutdown()
    print("Shutdown")
end

function setupControls()
    --Setup general movement controls.
    cm.manager:newBooleanControl("Player Movement", "Forward"   , false, cm.buttons.computer["W"])
    cm.manager:newBooleanControl("Player Movement", "Left"      , false, cm.buttons.computer["A"])
    cm.manager:newBooleanControl("Player Movement", "Right"     , false, cm.buttons.computer["D"])
    cm.manager:newBooleanControl("Player Movement", "Back"      , false, cm.buttons.computer["S"])
    cm.manager:newBooleanControl("Player Movement", "Jump"      , false, cm.buttons.computer[" "])
    cm.manager:newBooleanControl("Player Movement", "Mode Shift", false, cm.buttons.computer["Left Shift"])

    cm.manager:newBooleanControl("Player Movement", "Tool Primary"  , false, cm.buttons.computer["Mouse Left"])
    cm.manager:newBooleanControl("Player Movement", "Tool Secondary", false, cm.buttons.computer["Mouse Right"])

    cm.manager:newAnalogControl("Player Movement", "Horizontal Turn", false, cm.analogs.computer["Mouse X"])
    cm.manager:newAnalogControl("Player Movement", "Lookup/Down"    , false, cm.analogs.computer["Mouse Y"])
    cm.manager:newAnalogControl("Player Movement", "Switch Tool"    , false, cm.analogs.computer["Vertical Scroll"])

    cm.manager:newBooleanControl("UI Management", "Break Focus", true, cm.buttons.computer["Tab"])
    cm.manager:addBooleanCallback("UI Management", "Break Focus", function(pushed)
        if pushed then
            GUI.getScreen():breakFocus()
        end
    end)
end

function setupTopMenu()
    --Init main menu.
    GUI.json.new("main/GUI/main-gui.json")
end

--If we forgot to protect anything from garbage collection on the C++ side, this is going to make it real obvious real fast.
collectgarbage("collect")
